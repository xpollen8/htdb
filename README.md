##What it is##

HTDB is a all-in-one "CMS" which delivers content for a handful of websites, and it does a great job
at doing what it was designed to do: allowing one to develop flexible template-based,
programmatic HTML documents, backed by databases, using a simple, concise interpretted
scripting language.

Being an interpretted language written in C, HTDB needs to be compiled, first.
Then, apache/nginx needs to be configured to pass web requests onto the HTDB interpretter.

To get started, please refer to instructions in the INSTALL file
in this directory.

An online tutorial may be found a http://htdb.org

##Bitter Historical Note##

HTDB started development about the same time PHP started development.
PHP released early, and often.
HTDB did not.

And that's the lesson for how not to take over the world, kids!

-del

#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

static void	doImageData(char *contentType, int_t bytes, char *data)
{
	/*
		spit the headers
	 */
	htdb_httpHeader("Content-type: %s", contentType);
	htdb_httpHeader("Content-Length: %d", sizeof(char) * bytes);

	if (htdb_checkval("nocache"))
		/*
			if specified, disable browser image caching
		 */
		htdb_httpExpires(999999);

	/*
		finish the headers
	 */
	htdb_httpHeaderEnd();

	/*
		output the data
	 */
	fwrite(data, sizeof(char), bytes, stdout);
}

static bool_t	doStaticImage(char *path)
{
	int		fd;
	bool_t	status = BOOL_FALSE;

	if ((fd = open(path, O_RDONLY)) > 0) {
		struct stat	buf;
		bool_t	badAccess = BOOL_FALSE;

		if (fstat(fd, &buf) == 0) {
			char	*buffer = (char *)malloc(sizeof(char) * buf.st_size);

			if (read(fd, (void *)buffer, buf.st_size) == buf.st_size) {
				/*
					slurp
				 */
				char	*ext = imageGetContentByData(buffer);

				if (ext && *ext && strstr(ext, "image/")) {
					/*
						this is valid image data
					 */
					doImageData(ext, (int_t)buf.st_size, buffer);
				} else
					/*
						if this is not an image file,
						then we disallow access
					 */
					badAccess = BOOL_TRUE;
			}

			free(buffer);
		}
		close(fd);
		status = (badAccess == BOOL_TRUE ? BOOL_FALSE : BOOL_TRUE);
	}
	return status;
}

void    do_main(void)
{
	bool_t	status = BOOL_FALSE;

	if (htdb_checkval("image_id")) {
		/*
		   looking for a specific id
	     */
		status = (dbGetRowByKey("img",
			"select data, ext from htdb.image where image_id=%ld limit 1",
			htdb_getint("image_id")) == 1) ? BOOL_TRUE : BOOL_FALSE;
	} else if (htdb_checkval("path")) {
		/*
			else, explicit path was given
		 */
		status = doStaticImage(htdb_getval("path"));
	} else {
		/*
			else, lookup by name
		 */
		status = (dbGetRowByKey("img",
			"select data, ext from htdb.image where name='%s' limit 1",
			htdb_getval("db")) == 1) ? BOOL_TRUE : BOOL_FALSE;
	}
	 
	if (BOOL_TRUE == status) {
		/*
			output the data as read from the database
		 */
		doImageData(imageGetContentByExtension(htdb_getval("img->ext")),
			htdb_getint("img->data.size"),
			htdb_getbin("img->data"));
	} else {
		/*
		   else, default to the NoPhoto resource value
		   (which is explicitly set in the config.htdb file)
	     */
		(void)doStaticImage(htdb_getval("confImageNoPhoto"));
	}
}

appinfo_t	appinfo = {
	"imgserver",
	"0.0.3",
	"",
	"confUseCookieSessioning=n",	/* this prevents session_id's being used up by images */
	do_main,
	FCGI_KEEPALIVE,
	""								/* (optional) command-line arguments */
};

int	main(int argc, char **argv)
{
	exit (htdb_main(argc, argv, &appinfo));
}

#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

void	handleGo(char **resource, char **error_resource)
{
	char	*lookup = strdup(htdb_getval("db"));

	htdb_include("macros");

	/*
		chop off any HTML extension
	 */
	{
		char	*dot = strstr(lookup, ".htm");
		if (dot && *dot)
			*dot = '\0';
	}

	/*
		limit lookup term to size of the database field
	 */
	if (strlen(lookup) > LEN_GOKEY)
		lookup[LEN_GOKEY] = '\0';

	if (BOOL_TRUE == isAdmin() && strlen(htdb_getval("dest"))) {
		char	*dest = htdb_getval("dest");
		int_t	go_id = dbInsert("insert into go set "
			"go_id=NULL, "
			"gokey='%s', "
			"goclass_id=0, "
			"dest='%s', "
			"count=0, "
			"maxcount=0, "
			"lastused=0, "
			"startdate=0, "
			"stopdate=0, "
			"isdeleted='F'", lookup, dest);
		if (go_id > 0) {
			htdb_doHeader(CONTENT_TYPE_TEXT_HTML);
			printf("/go/%s -> %s", lookup, dest);
			htdb_doFooter(CONTENT_TYPE_TEXT_HTML);
		} else {
			htdb_doHeader(CONTENT_TYPE_TEXT_HTML);
			printf("nope");
			htdb_doFooter(CONTENT_TYPE_TEXT_HTML);
		}
	} else if (dbGetRowByKey("dir",
		"select * from go where gokey='%s' "
		"and IF(startdate > 0, (now() >= startdate), 1) "
		"and IF(stopdate > 0, (now() <= stopdate), 1) "
		"and IF(maxcount > 0, (count < maxcount), 1) "
		"limit 1",
		lookup) == 1) {
		/*
			find the record..
			limit by count if `maxcount' is > 0
			limit by date if either `startdate' or `stopdate' are defined
		 */
		char	*dest;

		/*
			good - found the row..  update the usage count
		 */
		dbUpdate("update go set count=count+1, lastused=now() where go_id=%ld",
			htdb_getptrint("dir", "go_id"));

		if (htdb_checkval("dest")) {
			/*
				if we've got a `dest' in the environment,
				then that will over-ride our database destination.
			 */
			htdb_setptrval("dir", "dest", htdb_getval("dest"));
		}

		/*
			expand the thing
		 */
		dest = htdb_script("dir->dest");

		if (strstr(dest, "://"))
			/*
				dest was a URL (http://, https://, ftp://, etc)
			 */
			htdb_httpRedirect("%s", dest);
		else {
			/*
				dest was an intra-site redirect
			 */
			htdb_httpRedirect("%s%s%s%s",
				htdb_getval("cgi->http_protocol"),
				htdb_getval("cgi->server_name"),
				htdb_getval("cgi->http_port"),
				dest);
		}

		free(dest);

	} else {
		/*
			gokey not found.
			send to the homepage.
		 */
		htdb_httpRedirect("%s%s%s",
			htdb_getval("cgi->http_protocol"),
			htdb_getval("cgi->server_name"),
			htdb_getval("cgi->http_port"));
	}

	free(lookup);
}

void	do_main(void)
{
	char	*resource,
		*error_resource;

	handleGo(&resource, &error_resource);
}

appinfo_t	appinfo = {
	"go",
	"0.0.1",
	"the GO redirector",
	"",
	do_main,
	FCGI_KEEPALIVE,
	""
};

int main(int argc, char **argv)
{
	exit (htdb_main(argc, argv, &appinfo));
}

#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

/*
	the application information structure
 */
appinfo_t	appinfo = {
	/* the application name */
		"htdb",
	/* the application version */
		"1.2.3",
	/* command-line usage information */
		"this program utilizes the built-in handler " 
		"to process .htdb documents. \n\n" 
		"in short, URI strings of the form \n" 
		"\t /htdb/site/index.html \n\n" 
		"are parsed into the values of " 
		"`db'=site and `page'=index.html \n\n" 
		"the `db'.htdb file is then processed " 
		"and we spit out what document \n" 
		"we find defined for `page'.",
	/* initial htdb values */
		"",
	/* address of handler function */
		htdb_page_handler,
	/* serve this many pages per fastCGI loop */
		FCGI_KEEPALIVE,
	/* (optional) command-line arguments */
		""
};

int	main(int argc, char **argv)
{
	exit (htdb_main(argc, argv, &appinfo));
}

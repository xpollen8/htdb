#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

void	doThing(spaceData *b, char *str)
{
	char	*exp = htdb_expand(str);
	thing_append(b, exp, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	free(exp);
}

void	doTable(spaceData *b, char *database, char *table, int_t id)
{
	int_t		num;
	/*
	char	*dot = strchr(table, '.'),
			*tbl;

	if (dot && *dot) {
		tbl = dot + 1;
	} else
		tbl = table;
	*/

	doThing(b, "<table border=0><tr><td valign=top>");
	doThing(b, "<nobr>");
	doThing(b, "<b>Query:</b>");
	htdb_setint("_id", id);
	htdb_setvarg("_table", "%s%s%s", database, (database && *database) ? "." : "", table);
	htdb_setval("_tbl", table);
	doThing(b, "<i>select * from ${_table} where ${_tbl}_id=${_id}</i>");
	doThing(b, "</nobr>");
	doThing(b, "<br>");

	{
		char	lookup[512];
		strcpy(lookup, table);
		if (dbGetRowByKey(lookup,
			"select * from %s%s%s where %s_id=%ld", database, (database && *database) ? "." : "", table, table, id)) {
			num = htdb_getptrint(lookup, "numFields");
			htdb_setval("lookup", lookup);
			doThing(b, "${${lookup}->numFields} fields..<p>");
			doThing(b, "<table border=1>");
			doThing(b, "<tr>");
			doThing(b, "<td><b>#</b></td>");
			doThing(b, "<td><b>field</b></td>");
			doThing(b, "<td><b>value</b></td>");
			doThing(b, "</tr>");
			{
				int_t	x;
				for (x = 1 ; x <= num ; x++) {
					char	*thing = htdb_getptrval(lookup, htdb_getarrayname("field", x)),
							*fullthing = htdb_getptrdyname(lookup, thing);
					int_t	fullint = htdb_getint(fullthing);
					htdb_setval("fullthing", fullthing);
					doThing(b, "<tr>");
					doThing(b, "<td valign=top>");
					htdb_setint("x", x);
					doThing(b, "${x}");
					doThing(b, "</td>");
					doThing(b, "<td valign=top>");
					doThing(b, fullthing);
					doThing(b, "</td><td valign=top>");
					if (htdb_defined(fullthing)) {
						char	*bytes = htdb_getptrdyname(fullthing, "bytes");
						if (htdb_getnum(bytes) > 1024) {
							/*
								sheesh
							 */
							doThing(b, "** BLOB **");
						} else {
							doThing(b, "${${fullthing}}");
						}
					} else
						doThing(b, "N/A");

					{
						char	*ix;
						if (x > 1 && (ix = strstr(thing, "_id")) && fullint > 0) {
							*ix = '\0';
							doTable(b, database, thing, fullint);
						}
					}
					doThing(b, "</td></tr>");

					free(fullthing);
				}
				doThing(b, "</table>");
			}
		}
	}

	doThing(b, "</td></tr></table>");
}

void	handleDB(char **resource, char **error_resource)
{
	spaceData	b;
	*error_resource = "";

	if (htdb_defined("table") && htdb_getnum("id") > 0) {

		data_init(&b);

		{
			char	*table = strdup(htdb_getval("table")),
					*dot = strchr(table, '.'),
					*database = "",
					*tbl;

			if (dot && *dot) {
				database = table;
				*dot = '\0';
				tbl = dot + 1;
			} else
				tbl = table;

			doTable(&b, database, tbl, htdb_getnum("id"));

			free(table);
		}

		htdb_setval("dbResults", b.value);

		data_destroy(&b);
	}
	*resource = "dbIndex";
}

void	do_main(void)
{
	char	*resource,
		*error_resource;

	handleDB(&resource, &error_resource);

	htdb_include("macros");
	htdb_include("db");

	htdb_setval("pageResource", resource);

	htdb_doHeader(CONTENT_TYPE_TEXT_HTML);
	htdb_print(htdb_getval(resource));
	htdb_doFooter(CONTENT_TYPE_TEXT_HTML);
	htdb_log(resource, htdb_getval("logValue"));
}

appinfo_t	appinfo = {
	"db",
	"0.0.1",
	"the DB explorer",
	"",
	do_main,
	FCGI_KEEPALIVE,
	""
};

int main(int argc, char **argv)
{
	exit (htdb_main(argc, argv, &appinfo));
}

package HTDBCrypt;
# b64/rc4 encoder - decoder module for HTDB encryped strings
# usage:
# use Crypto::HTDBCrypt;
# $enc_string = HTDBCrypt->htdb_be1encrypt($plain_string);
# $enc_string = HTDBCrypt->htdb_pe1encrypt($plain_string);
# $enc_string = HTDBCrypt->htdb_se1encrypt($plain_string);
# $dec_string = HTDBCrypt->htdb_be1decrypt($enc_string);
# $dec_string = HTDBCrypt->htdb_pe1decrypt($enc_string);
# $dec_string = HTDBCrypt->htdb_se1decrypt($enc_string);

$| = 1;
use strict;
use CGI qw(:all); # Added by MS
use DBI; # Added by MS
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
use MIME::Base64;
require Exporter;

@ISA = qw(Exporter); 
@EXPORT = qw(htdb_encrypt htdb_decrypt htdb_pe1encrypt htdb_pe1decrypt htdb_se1encrypt htdb_se1decrypt htdb_be1encrypt htdb_be1decrypt );
$VERSION = '1.0';

# Added by MS - this block reads the session cookie.
my $cgi = new CGI;
my $session_cookie = $cgi->cookie("session");
if($session_cookie){
	$session_cookie =~ s/\|be1\|//o;
} else{
	die 'could not find a session cookie';
}

my $be1key = "YOUR KEY HERE - BE SURE TO ESCAPE QUOTES, AND INCLUDE TAB AND NL CHARS";
#
#	Session encryption will now match config.htdb except that we will use '$_session_int' rather than '${_session_int}'.
#
my  $se1key = "YOUR KEY HERE - BE SURE TO ESCAPE QUOTES, AND INCLUDE TAB AND NL CHARS but put in the $_session_int too";
my $pe1key = "YOUR KEY HERE - BE SURE TO ESCAPE QUOTES, AND INCLUDE TAB AND NL CHARS";

#
#	Added by MS - this block decrypts the cookie value using be1 and then reads the Session_ID from the db.
#
my $dec_cookie = _be1decrypt($session_cookie);
my $dsn = 'DBI:mysql:htdb:localhost';
my $db_user_name = _be1decrypt('CHANGE_ME'); # This value should be consistent with config.htdb.
my $db_password = _be1decrypt('CHANGE_ME_TOO');  # (this one too)
my $_session_int;
my $dbh = DBI->connect($dsn, $db_user_name, $db_password);
my $sth = $dbh->prepare(qq{
	select session_id from session where sessionkey = '$dec_cookie'
});
$sth->execute();
($_session_int) = $sth->fetchrow_array();
$sth->finish();


my $key_x;
my $key_y;
my @state;

sub _htdb_encrypt {
	my ($a, $key) = @_;
	my $b = rc4($a,$key);
	my $c = encode_base64($b);
	my $d = htdb_webesc($c);
	$d =~ s/=*$//;
	return $d;
}

sub _htdb_decrypt {
	my ($a, $key) = @_;
	my $b = htdb_webunesc($a);
	my $c = decode_base64($b);
	my $d = rc4($c,$key);
	return $d;
}

sub htdb_encrypt {
	my $a = $_[1];
	return _htdb_encrypt($a, $pe1key);
}

sub htdb_decrypt {
	my $a = $_[1];
	return _htdb_decrypt($a, $pe1key);
}

sub htdb_pe1encrypt {
	my $a = $_[1];
	return _htdb_encrypt($a, $pe1key);
}

sub htdb_pe1decrypt {
	my $a = $_[1];
	return _htdb_decrypt($a, $pe1key);
}

sub htdb_se1encrypt {
	my $a = $_[1];
	return _htdb_encrypt($a, $se1key);
}

sub htdb_se1decrypt {
	my $a = $_[1];
	return _htdb_decrypt($a, $se1key);
}

sub htdb_be1encrypt {
	my $a = $_[1];
	return _htdb_encrypt($a, $be1key);
}

sub htdb_be1decrypt {
	my $a = $_[1];
	return _htdb_decrypt($a, $be1key);
}

sub _be1decrypt {
	my $a = $_[0];
	return _htdb_decrypt($a, $be1key);
}

sub asctohex {
	my $asc = shift;
	my $ret;
	foreach (split(//,$asc)) {
	}
}

sub htdb_webesc {
	my $string = shift;
	$string =~ tr/=//;
	$string =~ tr/\//-/;
	$string =~ tr/+/_/;
	return $string;
}

sub htdb_webunesc {
	my $string = shift;
	$string =~ tr/-/\//;
	$string =~ tr/_/+/;
	return $string;
}

sub prepare_key() {
	my $key_data = $_[0];
	my $key_data_len = $_[1];
	my ($hold,$x);
	my @data = split(//, $key_data);
	$key_x = 0;
	$key_y = 0;
	@state = ();
	
	for($x = 0; $x < 256; $x++) {
		push (@state, $x);
	}
	
	my $index1 = 0;
	my $index2 = 0;
	
	for($x = 0; $x < 256; $x++) {
		$index2 = (ord($data[$index1]) + $state[$x] + $index2) % 256;
		$hold = $state[$x];
		$state[$x] = $state[$index2];
		$state[$index2] = $hold;
		$index1 = ($index1 + 1) % $key_data_len;
	}
}

sub rc4() {
	my $buf = $_[0];
	my $buf_len = length($buf);
	my @data = split(//, $buf);  
	
	my $key = $_[1];
	&prepare_key($key, length($key));
	my $x = $key_x;
	my $y = $key_y;
	my $xorIndex;
	my $i; 
	my $hold;
	my $ret = '';
	
	for($i=0; $i < $buf_len; $i++) {
		$x = ($x + 1) % 256;
		$y = ($state[$x] + $y) % 256;
		$hold = $state[$x];
		$state[$x] = $state[$y];
		$state[$y] = $hold;
		$xorIndex = ($state[$x] + $state[$y]) % 256;
		$ret .= pack('C', ord($data[$i]) ^ $state[$xorIndex]);
	}
	$key_x = $x;
	$key_y = $y;
	return($ret);
}

1;

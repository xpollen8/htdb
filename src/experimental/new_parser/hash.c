#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

void    htdb_destroyStringTuple(void *value)
{
	free(value);
		value = NULL;
		}

/*
int snprintf(char *out, size_t bytes, const char *fmt, ...)
{
    va_list args;
    if (fmt && *fmt) {
        va_start(args, fmt);
        vsprintf(out, fmt, args);
        out[bytes - 1] = '\0';
        va_end(args);
    } else
        strcpy(out, "");
    return bytes;
}
*/

/*
	ht_hash()
		The published hash algorithm used in the UNIX ELF format for object files.
 */
static unsigned long	ht_hash(char *key, int size)
{
	unsigned long	h = 0, g;
	while (key && *key) {
		h = ( h << 4 ) + tolower(*key++);
		if ((g = (h & 0xF0000000)))
			h ^= g >> 24;
		h &= ~g;
	}
	return h % size;
}

/*
	ht_create()				
		Allocate and initialize a hash table with at most HASHTABSIZE_MAX elements.	
 */
Hashtable	ht_create(void (*destructor)(void *), int size)
{
	Hashtable	newtable = (Hashtable)malloc(sizeof(struct hashtable_t));

	memset(newtable, 0L, (sizeof(struct hashtable_t)));

	if (size > HASHTABSIZE_MAX)
		size = HASHTABSIZE_MAX;

	newtable->size = size;
	newtable->numelements = newtable->maxchainlen = 0;
	newtable->destructor = destructor;

	return newtable;
}

static Bucket		bucket_duplicate(Bucket b)
{
	Bucket new; 
	new = (Bucket) malloc(sizeof(struct bucket_t));
	new->key = strdup(b->key);

	new->value = (void *) malloc(b->size + 1);
	memcpy(new->value, b->value, b->size + 1);

	new->chainlen = b->chainlen;
	new->size = b->size;
	new->limit = b->limit;
	new->next = b->next;
	return new;
}
		
/*
	ht_destroy()				
		Unallocate storage used by the bucket
 */
void	ht_destroy(Hashtable ht)
{
	int	idx;

	if (ht) {
		for (idx = 0; idx < ht->size; idx++) {
			if (ht->ht_table[idx]) {
				Bucket bkt = ht->ht_table[idx];
				while (bkt) {
					/*
						free the buckets
					 */
					Bucket freeme = bkt;
					bkt = bkt->next;
					if (freeme) {
						/*
							and the contents
						 */
						if (freeme->key)
							free(freeme->key);
						/*
							call the data destructor.
						 */
						if (ht->destructor && freeme->value)
							ht->destructor(freeme->value);
						free(freeme);
					}
				}
			}
		}
		/*
			then the table
		 */
		free(ht);
		ht = NULL;
	}
}

/*
	ht_duplicate()				
		duplicate a hash table.  Will only function properly with 
		freeable (read string-based) hash tables. 
		
*/
void	ht_duplicate(Hashtable from, Hashtable to)
{
	int	idx;

	if (!from || !to || !(from->destructor == htdb_destroyStringTuple))
		return;

	to->numelements = from->numelements;
	to->maxchainlen = from->maxchainlen;

	for (idx = 0; idx < from->size; idx++) {
		if (from->ht_table[idx]) {
			Bucket bkt = from->ht_table[idx];
			Bucket last_bkt = (Bucket) NULL;
			while (bkt) {
				Bucket new_bkt = bucket_duplicate(bkt);
				if ( last_bkt ) 
					last_bkt->next = new_bkt;
				else /* first entry on the chain. */
					to->ht_table[idx] = new_bkt;
			
				last_bkt = new_bkt;
				bkt = bkt->next;
			}
		}
	}				
}

/*
	ht_lookup()				
		routine used by lookup and insertion functions.	
		Returns	pointer to Bucket or NULL
 */
Bucket	ht_lookup(Hashtable ht, char *key)
{
	Bucket		tmpbkt = ht->ht_table[ht_hash(key, ht->size)];

	while (tmpbkt) {
		/*
			while there are bkts
		 */
		if (strcasecmp(tmpbkt->key, key) == 0) {
			/*
				Found a match!
			 */
			return tmpbkt;
		}
		/*
			else hump along
		 */
		tmpbkt = tmpbkt->next;
	}

	return NULL;
}

/*
	ht_define()
		return 0 if key was added to the table,			
		-1 if key was not added. (new() failed)		
 */
int	ht_define(Hashtable ht, char *key, void *value, int is_string)
{
	Bucket bkt;
	char	*str = (char *)value,
		*hold = &str[0];
	int	len;

	if (is_string == 1) {
		if (str && *str)
			len =  strlen(str);
		else
			len = 0;
	} else
		len = abs(is_string);

	/*
		clean strings: eat leading and trailing whitespace
	 */
	if (is_string == 1) {
		while (len > 0 && str && *str && isspace((int)*str)) {
			str++;
			len--;
		}

		if (str && *str && len > 0)
			while (len > 0) {
				if (isspace((int)str[len - 1]))
					str[--len] = '\0';
				else
					break;
			}
	}

	/*
		if `is_string' is set, then we know that we
		are dealing with strings and therefore it is
		safe to use strlen() on our incoming data.
		data stored as strings will be allowed to use
		grow-able buffers for fast(er) append operations.
	 */
	if ((bkt = ht_lookup(ht, key))) {
		/*
			update existing entry
		 */
		if (bkt->value && is_string == 1) {
			free(bkt->value);
			bkt->value = NULL;
		}
		if (is_string == 1) {
			bkt->value = (void *)strdup(str);
			free(hold);
		} else {
			bkt->value = (void *)value;
		}
		bkt->size = bkt->limit = len;
	} else {
		long int	chain = ht_hash(key, ht->size);

		bkt = (Bucket)malloc(sizeof(struct bucket_t));

		if (NULL == ht->ht_table[chain]) {
			/*
				on a NEW chain
			 */
			bkt->next = NULL;
			bkt->chainlen = 0;
		} else {
			/*
				front of existing chain; save old link
			 */
			bkt->next = ht->ht_table[chain];
			bkt->chainlen = bkt->next->chainlen;
		}

		if (is_string == 1) {
			bkt->value = (void *)strdup(str);
			free(hold);
		} else {
			bkt->value = (void *)value;
		}

		bkt->key = strdup(key);
		bkt->size = bkt->limit = len;

		if (++(bkt->chainlen) > ht->maxchainlen)
			ht->maxchainlen = bkt->chainlen;

		ht->ht_table[chain] = bkt;
		ht->numelements++;
	}
	if (is_string < 0) {
		char	binsize[BUF_LIMIT_SMALL + 1],
				binvalue[BUF_LIMIT_SMALL + 1];
		snprintf(binsize, BUF_LIMIT_SMALL, "%s.size", key);
		binsize[BUF_LIMIT_SMALL] = '\0';
		snprintf(binvalue, BUF_LIMIT_SMALL, "%d", len);
		binvalue[BUF_LIMIT_SMALL] = '\0';
		ht_define(ht, binsize, strdup(binvalue), 1);
	}
	return 0;
}

int	ht_size(Hashtable ht, char *key)
{
	Bucket	b = ht_lookup(ht, key);
	if (b)
		return b->size;
	return 0;
}

/*
	ht_value()				
		return the value associated with the key if key is
		in the table, NULL if it is not in the table				
 */
void	*ht_value(Hashtable ht, char *key)
{
	if (ht) {
		Bucket	bkt = ht_lookup(ht, key);
		if (bkt)
			return bkt->value;
	}
	return NULL;
}

void	ht_init(Hashtable ht)
{
	ht->bkt = NULL;
}

void	bucket_init(Bucket b)
{
	b->value = (void *)strdup("");
	b->size = b->limit = 0;
}

void	bucket_destroy(Bucket b)
{
	free(b->value);
	b->value = (char *)NULL;
	b->size = b->limit = 0;
}

Bucket	ht_traverse(Hashtable ht)
{
	if (ht->bkt == NULL)
		ht->idx = 0;
	else
		ht->bkt = ht->bkt->next;

	while (ht->bkt == NULL) {
		if (ht->idx >= ht->size)
			return NULL;
		ht->bkt = ht->ht_table[ht->idx++];
	}
	return ht->bkt;
}

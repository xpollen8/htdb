#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#ifndef	H_HASH
#define	H_HASH

#define	HASHTABSIZE_SMALL	71		/* the 20th prime */
#define	HASHTABSIZE_MEDIUM	409		/* the 80th prime */
#define	HASHTABSIZE_BIG		3571	/* the 500th prime */
#define	HASHTABSIZE_MAX		3571

typedef struct bucket_t {
	char	*key;
	void	*value;
	int		size,
			limit,
			chainlen;
	struct bucket_t *next;
}	*Bucket;

typedef struct hashtable_t {
	int		idx,
			valuesFreeable,
			size,
			numelements,
			maxchainlen;
	Bucket	bkt,
			ht_table[HASHTABSIZE_MAX];
	void	(*destructor)(void *);
}	*Hashtable;

extern Hashtable	ht_create(void (*destructor)(void *), int size);
extern void			ht_destroy(Hashtable);
extern Bucket		ht_lookup(Hashtable, char *);
extern int			ht_size(Hashtable, char *);
extern void			*ht_value(Hashtable, char *);
extern Bucket		ht_traverse(Hashtable);
extern void			ht_init(Hashtable);
extern void			bucket_init(Bucket);
extern void			bucket_destroy(Bucket);
extern int			ht_define(Hashtable, char *, void *, int);
extern void			ht_duplicate(Hashtable, Hashtable);

extern void    htdb_destroyStringTuple(void *value);

#endif

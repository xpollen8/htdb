#include	"htdb.h"

typedef	enum { BOOL_FALSE = 0, BOOL_TRUE } bool_t;

typedef enum	{
	TOK_EOS = 0,
	TOK_DEFINE,
	TOK_INCLUDE,
	TOK_COMMENT,
	TOK_LPAREN,
	TOK_RPAREN,
	TOK_LANGLE,
	TOK_RANGLE,
	TOK_LBRACK,
	TOK_RBRACK,
	TOK_LBRACE,
	TOK_RBRACE,
	TOK_PTR,
	TOK_COLON,
	TOK_DOLLAR,
	TOK_MINUS,
	TOK_MODULO,
	TOK_PLUS,
	TOK_SLASH,
	TOK_DQUOTE,
	TOK_DOT,
	TOK_TILDE,
	TOK_NOT,
	TOK_EQUAL,
	TOK_ASSIGN,
	TOK_ASSIGN_ENCLOSED,
	TOK_COMMA,
	TOK_OR,
	TOK_BITWISEOR,
	TOK_AND,
	TOK_BITWISEAND,
	TOK_SPLAT,
	TOK_WS,
	TOK_LITERAL,
	TOK_NUMBER,
	TOK_STRING
}	token_t;

char	*toks[] = {
	"EOS",
	"DEFINE",
	"INCLUDE",
	"COMMENT",
	"LPAREN",
	"RPAREN",
	"LANGLE",
	"RANGLE",
	"LBRACK",
	"RBRACK",
	"LBRACE",
	"RBRACE",
	"PTR",
	"COLON",
	"DOLLAR",
	"MINUS",
	"MODULO",
	"PLUS",
	"SLASH",
	"DQUOTE",
	"DOT",
	"TILDE",
	"NOT",
	"EQUAL",
	"ASSIGN",
	"ASSIGN_ENCLOSED",
	"COMMA",
	"OR",
	"BITWISEOR",
	"AND",
	"BITWISEAND",
	"SPLAT",
	"WS",
	"LITERAL",
	"NUMBER",
	"STRING"
};

static int	lineNo = 1;

typedef struct {
	token_t	tok;
	char	*start;
	off_t	bytes,
			lineNo,
			columnNo;
}	parse_t;

typedef struct {
	int	numParse,
		ptrParse;
	parse_t	*parse;
}	tree_t;

extern bool_t	doInclude(char *file, tree_t *tree);

#if	0
typedef	enum	{
	NODE_TEXT = 1,
	NODE_TAG,
	NODE_IF,
	NODE_ELSEIF,
	NODE_ELSE
}	type_t;

typedef struct	node {
	int		bytes;
	struct node	*next,
			*prev;
	type_t	type;
	union	{
			char	*text;
			struct node	*node;
	}		data;
}	node_t;

typedef struct	{
	char	*name,
			*filename;
	node_t	*node;
}	res_t;

node_t	*newNode(char *body)
{
	node_t	*node = (node_t *)malloc(sizeof(node_t));
	char	*tagStart = strchr(body, '<');

	if (tagStart) {
		node->type = NODE_TAG;
		node->data.text = body;
		node->bytes = tagStart - body;
		node->next = newNode(tagStart + 1);
	} else {
		node->bytes = strlen(body);
		node->next = node->prev = (node_t *)NULL;
		node->type = NODE_TEXT;
		node->data.text = body;
	}
	return node;
}

res_t	*newRes(char *filename, char *name, char *body)
{
	res_t	*res = (res_t *)malloc(sizeof(res_t));

	res->filename = strdup(filename);
	res->name = strdup(name);
	res->node = newNode(body);

	return res;
}

void	printNode(node_t *node)
{
	printf("node:\n");
	printf("bytes: %d\n", node->bytes);
	printf("type: %d\n", node->type);
	switch (node->type) {
		case NODE_TEXT:
			printf("data: %s\n", node->data.text);
		break;
		case NODE_TAG:
			printNode(node->data.node);
		break;
	}
}

void	printRes(res_t *res)
{
	printf("filename: %s\n", res->filename);
	printf("name: %s\n", res->name);
	printNode(res->node);
}

#endif

char	getChar(char **x)
{
	if ((*((*x) + 1)) == '\n')
		lineNo++;
	return (*(*x)++);
}

void	putChar(char **x)
{
	if ((*(*x)--) == '\n')
		lineNo--;
}

token_t	readToken(char **in)
{
	char	prev = ((*in) - 1) ? *((*in) - 1) : '\0',
			this = getChar(in);
	switch (this) {
		case '\0': return TOK_EOS;
		case '\\': return TOK_LITERAL;
		case ':': return TOK_COLON;
		case '.': return TOK_DOT;
		case '~': return TOK_TILDE;
		case '!': return TOK_NOT;
		case '=': {
			switch (getChar(in)) {
				case '=':
					return TOK_EQUAL;
				case '"':
					return TOK_ASSIGN_ENCLOSED;
			}
			putChar(in);
			return TOK_ASSIGN;
		}
		case ',': return TOK_COMMA;
		case '*': return TOK_SPLAT;
		case '(': return TOK_LPAREN;
		case ')': return TOK_RPAREN;
		case '<': return TOK_LANGLE;
		case '>': return TOK_RANGLE;
		case '[': return TOK_LBRACK;
		case ']': return TOK_RBRACK;
		case '$': {
			if (getChar(in) == '{')
				return TOK_LBRACE;
			putChar(in);
			return TOK_DOLLAR;
		}
		case '}': return TOK_RBRACE;
		case '-': {
			if (getChar(in) == '>')
				return TOK_PTR;
			putChar(in);
			return TOK_MINUS;
		}
		case '%': return TOK_MODULO;
		case '+': return TOK_PLUS;
		case '|': {
			if (getChar(in) == '|')
				return TOK_OR;
			return TOK_BITWISEOR;
		}
		case '&': {
			if (getChar(in) == '&')
				return TOK_AND;
			return TOK_BITWISEAND;
		}
		case '/': return TOK_SLASH;
		case '"': return TOK_DQUOTE;
		/*
		case '0': case '1': case '2': case '3': case '4':
		case '5': case '6': case '7': case '8': case '9':
			{
				while (isdigit(getChar(in)))
					;
				putChar(in);
				return TOK_NUMBER;
			}
			*/
		case ' ': case '\t': case '\n': case '\r': {
			{
				while (isspace(getChar(in)))
					;
				putChar(in);
				return TOK_WS;
			}
		}
		default:
			if (this == '#') {
				/*
					handle:
						\n#define
						\n#include
						\n#comment
				 */
				if (prev == '\0' || prev == '\n' || prev == '\r') {
					switch (getChar(in)) {
						case 'd': {
							if (getChar(in) == 'e' &&
								getChar(in) == 'f' &&
								getChar(in) == 'i' &&
								getChar(in) == 'n' &&
								getChar(in) == 'e')
								return TOK_DEFINE;
						}
						case 'i': {
							if (getChar(in) == 'n' &&
								getChar(in) == 'c' &&
								getChar(in) == 'l' &&
								getChar(in) == 'u' &&
								getChar(in) == 'd' &&
								getChar(in) == 'e')
								return TOK_INCLUDE;
						}
					}
					putChar(in);
					/*
						else is a comment...
						fallthrough and eat until end of line
					 */
					if ((*in = strchr(*in, '\n')))
						getChar(in);
					return TOK_COMMENT;
				} else {
					/*
						handle:
							# comment
						NOTE: since 'href=URI#target' is valid
						HTML, we can only test for end-of-line
						comments if there's whitespace before
						the '#' character.
					 */
					if (isspace(prev)) {
						/*
							else is a comment...
							fallthrough and eat until end of line
						 */
						*in = strpbrk(*in, "\n\r");
						return TOK_COMMENT;
					}
				}
			}
			/*
			return TOK_STRING;
				else, add to the string (non-recursively)
				until we hit the start of a potential new token
			 */
			do {
				switch (getChar(in)) {
					case '\0':
					case '\\':
					case '#':
					case ':':
					case '.':
					case '~':
					case '!':
					case '=':
					case ',':
					case '*':
					case '(':
					case ')':
					case '<':
					case '>':
					case '[':
					case ']':
					case '$':
					case '}':
					case '-':
					case '%':
					case '+':
					case '|':
					case '&':
					case '/':
					case '"':
					/*
					case '0': case '1': case '2': case '3': case '4':
					case '5': case '6': case '7': case '8': case '9':
					*/
					case ' ': case '\t': case '\n': case '\r':
					/*
						found a stop.
						put the char back and return string
					 */
					putChar(in);
					return TOK_STRING;
				}
			} while (1);
	}
}

parse_t	*fetch(tree_t *tree)
{
	if (tree->ptrParse < tree->numParse)
		return &(tree->parse[tree->ptrParse++]);
	else
		return NULL;
}

void	printTok(token_t tok, char *start, char *end, off_t lineNo)
{
	printf("%3llu:\t%s", lineNo, toks[tok]);
	{
		int	i;
		for (i = strlen(toks[tok]) ; i < 10 ; i++)
			putchar(' ');
	}
	putchar('[');
	while (start != end) {
		if (*(start) == '\n')
			printf("\\n");
		else if (*(start) == '\t')
			printf("\\t");
		else
			putchar(*(start));
		start++;
	}
	putchar(']');
	putchar('\n');
	fflush(stdout);
}

void	expected(token_t token)
{
	printf("EXPECTED '%s'\n", toks[token]);
	exit(1);
}

token_t	match(char **buffer, token_t token, bool_t required)
{
	char	*end = *buffer;
	token_t	tok = readToken(&end);
	if (BOOL_TRUE == required && tok != token)
		expected(token);
	*buffer = end;
	return tok;
}

token_t	fetchValue(char **buffer, int *bytes)
{
	char	*end = *buffer;
	token_t	tok;
	while (*buffer = end, tok = readToken(&end)) {
	}
	printTok(tok, *buffer, end, lineNo);
	return tok;
}

char	*scanTo(char **buffer, token_t find)
{
	token_t	tok;
	char	*prev = *buffer;

	do {
		char	*end = *buffer;
		tok = readToken(&end);
		printTok(tok, *buffer, end, lineNo);
		prev = *buffer;
		*buffer = end;
	} while (tok != TOK_EOS && tok != find);

	if (tok == TOK_EOS)
		expected(find);
	return prev;
}

char	*mkString(char *start, char *end)
{
	size_t	bytes = end - start;
	char	*out = (char *)malloc(sizeof(char) * (bytes + 1));
	strncpy(out, start, bytes);
	out[bytes] = '\0';
	return out;
}

void	initTree(tree_t *tree)
{
	tree->numParse = 0;
	tree->ptrParse = 0;
	tree->parse = (parse_t *)NULL;
}

void	parseInclude(char **buffer)
{
	printf("++INCLUDE\n");
	{
		token_t	tokFound = match(buffer, TOK_WS, BOOL_TRUE);	/* ws after '#include' */
		char	*start = *buffer;							/* mark this spot */

		/*
			quotes are optional.
			if we find one, then the next one will be required
		 */
		{
			char	*val;
			if (match(buffer, TOK_DQUOTE, BOOL_FALSE) == TOK_DQUOTE) {
				/*
					skip past the quote
				 */
				start++;
				/*
					fetch everything until the next quote
				 */
				val = scanTo(buffer, TOK_DQUOTE);
			} else {
				/*
					fetch everything until the next whitespace
				 */
				val = scanTo(buffer, TOK_WS);
			}

			{
				char	*filename = mkString(start, val);
				tree_t	tree;
				initTree(&tree);
				if (BOOL_FALSE == doInclude(filename, &tree))
					/*
						could not open the file
						(should this really be a fatal error?)
					 */
					expected(TOK_INCLUDE);
				free(filename);
			}
		}

	}
	printf("--INCLUDE\n");
}

void	parseIf(char **buffer)
{
	printf("IF\n");
	scanTo(buffer, TOK_RANGLE);
}

void	parseLoop(char **buffer)
{
	printf("LOOP\n");
	scanTo(buffer, TOK_RANGLE);
}

void	parseWhile(char **buffer)
{
	printf("WHILE\n");
	scanTo(buffer, TOK_RANGLE);
}

void	parseFor(char **buffer)
{
	printf("FOR\n");
	scanTo(buffer, TOK_RANGLE);
}


char	*reserveredTags[] = {
	"!--",
	"A",
	"ABBREV",
	"ACRONYM",
	"ADDRESS",
	"APPLET",
	"AREA",
	"AU",
	"AUTHOR",
	"B",
	"BANNER",
	"BASE",
	"BASEFONT",
	"BGSOUND",
	"BIG",
	"BLINK",
	"BLOCKQUOTE",
	"BQ",
	"BODY",
	"BR",
	"CAPTION",
	"CENTER",
	"CITE",
	"CODE",
	"COL",
	"COLGROUP",
	"CREDIT",
	"DEL",
	"DFN",
	"DIR",
	"DIV",
	"DL",
	"DT",
	"DD",
	"EM",
	"EMBED",
	"FIG",
	"FN",
	"FONT",
	"FORM",
	"FRAME",
	"FRAMESET",
	"H1",
	"H2",
	"H3",
	"H4",
	"H5",
	"H6",
	"HEAD",
	"HR",
	"HTML",
	"I",
	"IFRAME",
	"IMG",
	"INPUT",
	"INS",
	"ISINDEX",
	"KBD",
	"LANG",
	"LH",
	"LI",
	"LINK",
	"LISTING",
	"MAP",
	"MARQUEE",
	"MATH",
	"MENU",
	"META",
	"MULTICOL",
	"NOBR",
	"NOFRAMES",
	"NOTE",
	"OL",
	"OVERLAY",
	"OPTION",
	"P",
	"PARAM",
	"PERSON",
	"PLAINTEXT",
	"PRE",
	"Q",
	"RANGE",
	"SAMP",
	"SCRIPT",
	"SELECT",
	"SMALL",
	"SPACER",
	"SPOT",
	"STRIKE",
	"STRONG",
	"SUB",
	"SUP",
	"TAB",
	"TABLE",
	"TBODY",
	"TD",
	"TEXTAREA",
	"TEXTFLOW",
	"TFOOT",
	"TH",
	"THEAD",
	"TITLE",
	"TR",
	"TT",
	"U",
	"UL",
	"VAR",
	"WBR",
	"XMP",
	NULL
};

bool_t	isTagHTML(char *tag)
{
	static spaceName ccht = NULL;
	if (NULL == ccht) {
		/*
			build a hashtable of reserved HTML tags
		 */
		int	idx = 0;
		char	*tag;
		space_init(ccht, HASHTABSIZE_SMALL, htdb_destroyStringTuple);
		while ((tag = reserveredTags[idx++])) {
			space_store(ccht, tag,   strdup(tag), 1);
		}
	}

	return (space_lookup(ccht, tag) != NULL) ? BOOL_TRUE : BOOL_FALSE;
}

void	parseUnknownTag(char *tag, char **buffer)
{
	char	*hold = *buffer;
	/*
		is this a known HTML tag?
	 */
	if (BOOL_TRUE == isTagHTML(tag)) {
		/*
			eat until the next right angle not
			contained within quotes
		 */
		printf("HTML TAG(%s)\n", tag);
		scanTo(buffer, TOK_RANGLE);
		return;
	} else {
		/*
			parse a new tag definition
		 */
		scanTo(buffer, TOK_RANGLE);
		printf("NEW TAG(%s)(%s)\n", tag, mkString(hold, *buffer));
	}
}

void	parseTagCall(char **buffer)
{
	bool_t	doBreak = BOOL_FALSE;
	char	*start,
			*hold = *buffer;
	printf("++parseTagCall\n");
	do {
		token_t	tokFound;
		while (start = *buffer,
			(tokFound = readToken(buffer)) == TOK_STRING) {
			/*
				eat until the end of the token
			 */
		}
		switch (tokFound) {
			case TOK_SLASH:
				doBreak = BOOL_TRUE;
			break;
			case TOK_WS: {
				char	*tag = mkString(hold, start);
				if (strcasecmp(tag, "if") == 0)
					parseIf(buffer);
				else if (strcasecmp(tag, "loop") == 0)
					parseLoop(buffer);
				else if (strcasecmp(tag, "while") == 0)
					parseWhile(buffer);
				else if (strcasecmp(tag, "for") == 0)
					parseFor(buffer);
				else
					parseUnknownTag(tag, buffer);
				free(tag);
				doBreak = BOOL_TRUE;
			}
			break;
			case TOK_RANGLE: {
				char	*tag = mkString(hold, start);
				putChar(buffer);
				parseUnknownTag(tag, buffer);
				free(tag);
				doBreak = BOOL_TRUE;
			}
			break;
		}
	} while (BOOL_FALSE == doBreak);
	printf("--parseTagCall\n");
}

void	parseFunctionArglist(char **buffer)
{
	printf("++parseFunctionArglist\n");
	scanTo(buffer, TOK_RPAREN);
	printf("--parseFunctionArglist\n");
}

void	parseFunctionDeclaration(char *res, char **buffer)
{
	printf("++parseFunctionDeclaration(%s)\n", res);
	parseFunctionArglist(buffer);
	printf("--parseFunctionDeclaration(%s)\n", res);
}

void	parseTagArglist(char **buffer)
{
	printf("++parseTagArglist\n");
	scanTo(buffer, TOK_RPAREN);
	printf("--parseTagArglist\n");
}

void	parseTagDeclaration(char *res, char **buffer)
{
	bool_t	doBreak = BOOL_FALSE;
	char	*start = *buffer,
			*hold = *buffer;
	token_t tokFound;
	printf("++parseTagDeclaration(%s)\n", res);
	
	do {
		token_t	tokFound;
		bool_t	simpleTag = BOOL_FALSE;
		while ((tokFound = readToken(buffer)) != TOK_RANGLE) {
			/*
				eat until the end of the token
			 */
			if (tokFound == TOK_SLASH)
				simpleTag = BOOL_TRUE;
		}
		switch (tokFound) {
			case TOK_LPAREN: {
				parseTagArglist(buffer);
				doBreak = BOOL_TRUE;
			}
			break;
			case TOK_RANGLE: {
				doBreak = BOOL_TRUE;
			}
			break;
		}
	} while (BOOL_FALSE == doBreak);
	printf("--parseTagDeclaration(%s)\n", res);
}

void	parseDefine(char **buffer)
{
	char	*start,
			*res;
	token_t	tokFound;
	bool_t	tagDef = BOOL_FALSE,
			funcDef = BOOL_FALSE;

	printf("++parseDefine\n");
	/*
		eat whitespace before the resource name
	 */
	tokFound = match(buffer, TOK_WS, BOOL_TRUE);
	start = *buffer;

	/*
		determine the definition type:
			simple
			<tag ...>
			function(...)
	 */
	if (TOK_LANGLE == readToken(buffer)) {
		tagDef = BOOL_TRUE;
		start++;
	} else
		putChar(buffer);

	/*
		determining the resource name..
		stop at either:
			TOK_WS
			TOK_LPAREN
			TOK_RANGLE (if this is a tag declaration)
	 */
	while ((tokFound = readToken(buffer))) {
		if (tokFound == TOK_WS)
			break;
		if (tokFound == TOK_LPAREN) {
			funcDef = BOOL_TRUE;
			break;
		}
		if (BOOL_TRUE == tagDef &&
			tokFound == TOK_RANGLE)
			break;
	}

	res = mkString(start, *buffer - 1);

	printf("DEFINE(%s)\n", res);

	if (BOOL_TRUE == tagDef) {
		/*
			<tag>
		 */
		parseTagDeclaration(res, buffer);
	} else if (BOOL_TRUE == funcDef) {
		/*
			function()
		 */
		parseFunctionDeclaration(res, buffer);
	}

	/*
		done parsing the declaration syntax.
		now parse the body of the resource
	 */
	{
		bool_t	inTagBody = BOOL_FALSE,
				inTagDef = BOOL_FALSE;
		while (start = *buffer,
			(tokFound = match(buffer, TOK_STRING, BOOL_FALSE)) != TOK_DEFINE &&
			tokFound != TOK_INCLUDE &&
			tokFound != TOK_EOS) {
			printTok(tokFound, start, *buffer, lineNo);
			switch (tokFound) {
				case TOK_LANGLE: {
					if (getChar(buffer) != TOK_WS) {
						putChar(buffer);
						parseTagCall(buffer);
					} else
						/*
							continue
						 */
						;
				}
				break;
				default:
					/*
						continue
					 */
				break;
			}
		}
	}
	switch (tokFound) {
		case TOK_DEFINE:
			/*
				put back the '#define'
			 */
			putChar(buffer);
			putChar(buffer);
			putChar(buffer);
			putChar(buffer);
			putChar(buffer);
			putChar(buffer);
			putChar(buffer);
		case TOK_EOS:
			/*
				put back the EOS
			 */
			putChar(buffer);
		case TOK_INCLUDE:
			/*
				put back '#include'
			 */
			putChar(buffer);
			putChar(buffer);
			putChar(buffer);
			putChar(buffer);
			putChar(buffer);
			putChar(buffer);
			putChar(buffer);
			putChar(buffer);
	}
	free(res);
	printf("--parseDefine\n");
}

void	parseOther(char **buffer)
{
}

char	*addBlock(char **out, char *in, off_t bytes)
{
	size_t	newSize = strlen(*out) + bytes;

	/*
	printf("+++\n");
	{
	int	i;
	for (i = 0 ; i < bytes ; i++)
	putchar(in[i]);
	putchar('\n');
	}
	printf("---\n");
	*/
	(*out) = (char *)realloc((void *)(*out), sizeof(char) * (newSize + 1));
	strncat((*out), in, bytes);
}

char	*addToken(token_t tok, char *in, off_t bytes, off_t lineNo, tree_t *tree)
{
	if (tree->numParse == 0)
		tree->parse = (parse_t *)malloc(sizeof(parse_t) * (tree->numParse + 1));
	else
		tree->parse = (parse_t *)realloc(tree->parse, sizeof(parse_t) * (tree->numParse + 1));

	tree->parse[tree->numParse].tok = tok;
	tree->parse[tree->numParse].start = in;
	tree->parse[tree->numParse].bytes = bytes;
	tree->parse[tree->numParse].lineNo = lineNo;
	tree->numParse++;
}

char	*removeComments(char *in, tree_t *tree)
{
	char	*end = in,
			*out = strdup(""),
			*prev = in;
	token_t	tok;

	while (in = end, tok = readToken(&end)) {
		addToken(tok, in, end - in, lineNo, tree);
		if (tok == TOK_COMMENT) {
			/*
				add from the end of the previous
				comment to the start of this comment
				to the output buffer
			 */
			addBlock(&out, prev, in - prev);
			prev = end;
		}
	}
	addToken(tok, in, end - in, lineNo, tree);
	if (tok != TOK_COMMENT)
		/*
			add the rest to the output buffer
		 */
		addBlock(&out, prev, in - prev);

	{
		int	i;
		for (i = 0 ; i < tree->numParse ; i++) {
			parse_t	*p = &tree->parse[i];
			printTok(p->tok, p->start, p->start + p->bytes, p->lineNo);
		}
	}
	return out;
}

bool_t	doInclude(char *file, tree_t *tree)
{
	bool_t	status = BOOL_TRUE;
	int	holdLine = lineNo;
	lineNo = 1;	/* needs to be per-file */

	{
		int		fd = open(file, O_RDONLY);
		if (fd != -1) {
			char	*buffer,
					*hold;
			{
				struct	stat	buf;
				char	*raw;

				fstat(fd, &buf);
				raw = (char *)malloc(sizeof(char) * (buf.st_size + 1));
				read(fd, raw, buf.st_size);
				raw[buf.st_size] = '\0';

				hold = buffer = removeComments(raw, tree);

				printf("%s\n-----------\n", buffer);
				free(raw);

				close(fd);
			}

			{
				token_t	tok;
				char	*end = buffer;
				while (buffer = end, tok = readToken(&end)) {
					if (tok == TOK_INCLUDE)
						parseInclude(&end);
					else if (tok == TOK_DEFINE)
						parseDefine(&end);
					else
						parseOther(&end);
				}
			}
			free(hold);
		} else
			status = BOOL_FALSE;
	}
	lineNo = holdLine;
	return status;
}

main()
{
	/*
	res_t	*res = newRes("filename", "name", "hello world");

	printRes(res);
	doInclude("test.htdb");
	*/

	tree_t	tree;
	initTree(&tree);
	printf("past init\n");
	doInclude("test.htdb", &tree);
}


#include	<stdio.h>
#include	<stdlib.h>
#include	<strings.h>
#include	<string.h>
#include	<sys/types.h>
#include	<sys/stat.h>
#include	<fcntl.h>
#include    <stdarg.h>

#include	"hash.h"

typedef  Hashtable                       spaceName;
#define space_init(h, s, d)     if (!(h)) (h) = ht_create((d), (s))
#define space_store(h, k, v, s) ht_define((h), (k), (void *)(v), (s))
#define space_lookup(h, k)      ht_lookup((h), (k))

#define	BUF_LIMIT_SMALL	1024

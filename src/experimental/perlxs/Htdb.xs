#ifdef __cplusplus
extern "C" {
#endif
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#ifdef __cplusplus
}
#endif

#include "htdb.h"
#include "db.h"
#include "Av_CharPtrPtr.h"

MODULE = Htdb		PACKAGE = Htdb		


int
htdb_start(content_type)
	char *content_type

int      
start_of_cgi(argc, argv, app_name, app_version, app_usage)
	int argc
	char **argv
	char *app_name
	char *app_version
	char *app_usage

void
htdb_end(content_type)
	char *content_type

void
end_of_cgi()

int
htdb_setint(set, val)
	char *set
	int val

char	*
htdb_setval(to, from)
	char *to
	char *from

char	*
htdb_getval(of)
	char *of

void
htdb_doHeader(content_type)
	char *content_type

char	*
public_encrypt(object)
	char *object


char	*
public_decrypt(object)
	char *object


char	*
private_encrypt(object)
	char *object


char	*
private_decrypt(object)
	char *object

int
htdb_read(file)
	char *file

int
dbGetRowByKey(table, fmt)
	char *table
	char *fmt

char *
htdb_expand(in)
	char *in



#Add your own headers to this file as neccesary

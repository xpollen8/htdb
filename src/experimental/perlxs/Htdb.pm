package Htdb;

use strict;
use Carp;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK $AUTOLOAD);

require Exporter;
require DynaLoader;
require AutoLoader;
require	DBI;

@ISA = qw(Exporter DynaLoader);
# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.
@EXPORT = qw(
	VERSION_HTDBLIB
	htdb_start
	start_of_cgi
	htdb_end
	end_of_cgi
	htdb_setint
	htdb_setval
	htdb_getval
	htdb_doHeader
	public_encrypt
	public_decrypt
	private_encrypt
	private_decrypt
	htdb_read
	dbGetRowByKey
	htdb_expand
);
$VERSION = '0.01';

sub AUTOLOAD {
    # This AUTOLOAD is used to 'autoload' constants from the constant()
    # XS function.  If a constant is not found then control is passed
    # to the AUTOLOAD in AutoLoader.

    my $constname;
    ($constname = $AUTOLOAD) =~ s/.*:://;
    croak "& not defined" if $constname eq 'constant';
    my $val = constant($constname, @_ ? $_[0] : 0);
    if ($! != 0) {
	if ($! =~ /Invalid/) {
	    $AutoLoader::AUTOLOAD = $AUTOLOAD;
	    goto &AutoLoader::AUTOLOAD;
	}
	else {
		croak "Your vendor has not defined Htdb macro $constname";
	}
    }
    *$AUTOLOAD = sub () { $val };
    goto &$AUTOLOAD;
}

# This line lets htdb dso functions see libhtdb functions.  Bliss!
sub dl_load_flags{0x01}

bootstrap Htdb $VERSION;

sub dbi_connect()
{
	my $dbName = &htdb_getval("confDatabaseName");
	my $dbIP = &htdb_getval("confDatabaseIP");
	$dbIP = 'localhost' if ( $dbIP eq 'NULL' );

	my $dbconnect = "DBI:mysql:" . $dbName . ':' . $dbIP;
 
	my $dbh = DBI->connect($dbconnect, &htdb_getval("confDatabaseUser"),
				&htdb_getval("confDatabasePassword"));
	return ($dbh);
}

# Preloaded methods go here.

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__

=head1 NAME

Htdb - Perl extension for blah blah blah

=head1 SYNOPSIS

  use Htdb;
  And all that shit

=head1 DESCRIPTION

You know, the Hypertext Document Ball.
Or something.

=head1 Exported constants

  VERSION_HTDBLIB


=head1 AUTHOR

Vladimir

=head1 SEE ALSO

perl(1).

=cut

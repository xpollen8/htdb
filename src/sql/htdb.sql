create database htdb;
use htdb;

--
-- Table structure for table `accountype`
--

CREATE TABLE accountype (
  accountype_id int(11) NOT NULL auto_increment,
  name varchar(128) NOT NULL default '',
  description varchar(128) NOT NULL default '',
  dtcreated datetime NOT NULL default '0000-00-00 00:00:00',
  UNIQUE KEY idx0 (accountype_id),
  KEY idx1 (name)
) TYPE=MyISAM PACK_KEYS=1;

INSERT INTO accountype VALUES (1,'admin','priviledged administrators', now());
INSERT INTO accountype VALUES (2,'staff','employees', now());

--
-- Table structure for table `browser`
--

CREATE TABLE browser (
  browser_id int(10) unsigned NOT NULL auto_increment,
  user_agent varchar(80) NOT NULL default '',
  isrobot enum('T','F') default 'F',
  num int(10) unsigned NOT NULL default '1',
  ts timestamp(14) NOT NULL,
  isdeleted enum('T','F') NOT NULL default 'F',
  PRIMARY KEY  (browser_id),
  UNIQUE KEY idx1 (user_agent,isdeleted)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table `country`
--

CREATE TABLE country (
  country_id int(10) unsigned NOT NULL auto_increment,
  name varchar(30) NOT NULL default '',
  iso char(2) NOT NULL default '',
  PRIMARY KEY  (name),
  UNIQUE KEY idx0 (country_id)
) TYPE=MyISAM;

INSERT INTO country VALUES (1,'-- other --','');
INSERT INTO country VALUES (2,'Australia','au');
INSERT INTO country VALUES (3,'Brazil','br');
INSERT INTO country VALUES (4,'Canada','ca');
INSERT INTO country VALUES (5,'France','fr');
INSERT INTO country VALUES (6,'Germany','de');
INSERT INTO country VALUES (7,'Italy','it');
INSERT INTO country VALUES (8,'Japan','jp');
INSERT INTO country VALUES (9,'Mexico','mx');
INSERT INTO country VALUES (10,'USA','us');
INSERT INTO country VALUES (11,'United Kingdom','gb');
INSERT INTO country VALUES (12,'New Zealand','nz');
INSERT INTO country VALUES (13,'Austria','at');
INSERT INTO country VALUES (14,'Greece','gr');
INSERT INTO country VALUES (15,'Netherlands','nl');
INSERT INTO country VALUES (16,'Portugal','pt');
INSERT INTO country VALUES (17,'Israel','il');
INSERT INTO country VALUES (18,'Switzerland','ch');
INSERT INTO country VALUES (19,'Malaysia','my');
INSERT INTO country VALUES (20,'Poland','pl');
INSERT INTO country VALUES (21,'Belgium','be');
INSERT INTO country VALUES (22,'Ireland','ie');
INSERT INTO country VALUES (23,'Korea','kr');
INSERT INTO country VALUES (24,'Norway','no');
INSERT INTO country VALUES (25,'Spain','es');
INSERT INTO country VALUES (26,'Puerto Rico','');
INSERT INTO country VALUES (27,'Sweden','se');
INSERT INTO country VALUES (28,'Taiwan','tw');
INSERT INTO country VALUES (29,'Colombia','co');
INSERT INTO country VALUES (30,'Macedonia','mk');
INSERT INTO country VALUES (31,'Philippines','ph');
INSERT INTO country VALUES (32,'Russian Federation','ru');
INSERT INTO country VALUES (33,'Thailand','th');
INSERT INTO country VALUES (34,'Argentina','ar');
INSERT INTO country VALUES (35,'Egypt','eg');
INSERT INTO country VALUES (36,'Finland','fi');
INSERT INTO country VALUES (37,'Hungary','hu');
INSERT INTO country VALUES (38,'Singapore','sg');
INSERT INTO country VALUES (39,'South Africa','za');
INSERT INTO country VALUES (40,'India','in');
INSERT INTO country VALUES (41,'China','cn');
INSERT INTO country VALUES (42,'Chile','cl');
INSERT INTO country VALUES (43,'Czech Republic','cz');
INSERT INTO country VALUES (44,'Denmark','dk');
INSERT INTO country VALUES (45,'Estonia','ee');
INSERT INTO country VALUES (46,'Pakistan','pk');
INSERT INTO country VALUES (47,'Venezuela','ve');
INSERT INTO country VALUES (48,'Hong Kong','hk');
INSERT INTO country VALUES (49,'Indonesia','id');
INSERT INTO country VALUES (50,'Scotland','');
INSERT INTO country VALUES (51,'Bulgaria','bg');
INSERT INTO country VALUES (52,'Holland','nl');
INSERT INTO country VALUES (53,'Saudi Arabia','sa');
INSERT INTO country VALUES (54,'Iceland','is');
INSERT INTO country VALUES (55,'Latvia','lv');
INSERT INTO country VALUES (56,'Romania','ro');
INSERT INTO country VALUES (57,'Ukraine','ua');
INSERT INTO country VALUES (58,'Iran','');
INSERT INTO country VALUES (59,'Lithuania','lt');
INSERT INTO country VALUES (60,'Croatia','hr');
INSERT INTO country VALUES (61,'Slovakia','sk');
INSERT INTO country VALUES (62,'Luxembourg','lu');
INSERT INTO country VALUES (63,'Belarus','by');
INSERT INTO country VALUES (64,'Slovenia','si');
INSERT INTO country VALUES (65,'Namibia','na');
INSERT INTO country VALUES (66,'Antarctica','aq');
INSERT INTO country VALUES (67,'Armenia','am');
INSERT INTO country VALUES (68,'Kyrgyzstan','kg');
INSERT INTO country VALUES (69,'Turkey','tr');
INSERT INTO country VALUES (70,'Moldova','md');
INSERT INTO country VALUES (71,'Uruguay','uy');
INSERT INTO country VALUES (72,'Kazakhstan','kz');
INSERT INTO country VALUES (73,'Ecuador','ec');
INSERT INTO country VALUES (74,'Albania','al');
INSERT INTO country VALUES (75,'Yugoslavia','yu');
INSERT INTO country VALUES (76,'Uzbekistan','uz');
INSERT INTO country VALUES (77,'Bahrain','bh');
INSERT INTO country VALUES (78,'Vietnam','vt');
INSERT INTO country VALUES (79,'Jordan','jo');
INSERT INTO country VALUES (80,'Ghana','gh');
INSERT INTO country VALUES (81,'Cuba','cu');
INSERT INTO country VALUES (82,'United Arab Emirates','ae');
INSERT INTO country VALUES (83,'Sri Lanka','lk');
INSERT INTO country VALUES (84,'Peru','pe');
INSERT INTO country VALUES (85,'Malta','mt');
INSERT INTO country VALUES (86,'Panama','pa');
INSERT INTO country VALUES (87,'Lebanon','lb');
INSERT INTO country VALUES (88,'Nigeria','ng');
INSERT INTO country VALUES (89,'Costa Rica','cr');
INSERT INTO country VALUES (90,'Nepal','np');
INSERT INTO country VALUES (91,'Bangladesh','bd');
INSERT INTO country VALUES (92,'Trinidad and Tobago','tt');
INSERT INTO country VALUES (93,'Dominican Republic','do');
INSERT INTO country VALUES (94,'Cyprus','cy');
INSERT INTO country VALUES (95,'Jamaica','jm');
INSERT INTO country VALUES (96,'Kuwait','kw');
INSERT INTO country VALUES (97,'Guatemala','gt');
INSERT INTO country VALUES (98,'Barbados','bb');
INSERT INTO country VALUES (99,'Bolivia','bo');
INSERT INTO country VALUES (100,'Morocco','');
INSERT INTO country VALUES (101,'Georgia','');
INSERT INTO country VALUES (102,'El Salvador','');
INSERT INTO country VALUES (103,'Honduras','');
INSERT INTO country VALUES (104,'Bermuda','');

--
-- Table structure for table `domain`
--

CREATE TABLE domain (
  domain_id int(10) unsigned NOT NULL auto_increment,
  server varchar(30) NOT NULL default '',
  ts timestamp(14) NOT NULL,
  isdeleted enum('T','F') NOT NULL default 'F',
  PRIMARY KEY  (server,isdeleted),
  UNIQUE KEY idx0 (domain_id)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table `entrance`
--

CREATE TABLE entrance (
  entrance_id int(10) unsigned NOT NULL auto_increment,
  domain_id int(10) unsigned NOT NULL default '0',
  session_id int(10) unsigned NOT NULL default '0',
  browser_id int(10) unsigned NOT NULL default '0',
  affiliate_id int(10) unsigned NOT NULL default '0',
  referer_id int(10) unsigned NOT NULL default '0',
  resource varchar(30) NOT NULL default '',
  value varchar(20) NOT NULL default '',
  ts timestamp(14) NOT NULL,
  UNIQUE KEY idx0 (entrance_id),
  KEY idx1 (domain_id),
  KEY idx2 (session_id),
  KEY idx3 (browser_id),
  KEY idx4 (affiliate_id),
  KEY idx5 (referer_id),
  KEY idx6 (resource),
  KEY idx7 (ts)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table `feedback`
--

CREATE TABLE feedback (
  feedback_id int(10) unsigned NOT NULL auto_increment,
  session_id int(10) unsigned NOT NULL default '0',
  domain_id int(10) unsigned NOT NULL default '0',
  uri varchar(120) NOT NULL default '',
  ordinal int(10) unsigned NOT NULL default '0',
  subject varchar(120) NOT NULL default '',
  dtcreated datetime NOT NULL default '0000-00-00 00:00:00',
  host varchar(120) NOT NULL default '',
  who varchar(120) NOT NULL default '',
  whence varchar(120) NOT NULL default '',
  comments text NOT NULL,
  isdeleted enum('T','F') NOT NULL default 'F',
  PRIMARY KEY  (feedback_id)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table `goclass`
--

DROP TABLE IF EXISTS goclass;

#
# Table structure for table 'goclass'
#
CREATE TABLE goclass (
  goclass_id int(10) unsigned auto_increment NOT NULL,
  description char(50) DEFAULT '' NOT NULL,
  isdeleted enum('T','F') DEFAULT 'F' NOT NULL,
  UNIQUE idx0 (goclass_id)
);

--
-- Table structure for table `go`
--

CREATE TABLE go (
  go_id int(10) unsigned NOT NULL auto_increment,
  gokey varchar(20) NOT NULL default '',
  goclass_id int(10) unsigned default '1',
  dest text NOT NULL,
  count int(10) unsigned NOT NULL default '1',
  maxcount int(10) unsigned NOT NULL default '1',
  lastused timestamp(14) NOT NULL,
  startdate timestamp(8) NOT NULL default '00000000',
  stopdate timestamp(8) NOT NULL default '00000000',
  isdeleted enum('T','F') NOT NULL default 'F',
  PRIMARY KEY  (go_id),
  UNIQUE KEY idx1 (gokey),
  KEY idx3 (goclass_id)
) TYPE=MyISAM;

--
-- Table structure for table `hit`
--

CREATE TABLE hit (
  hit_id int(10) unsigned NOT NULL auto_increment,
  domain_id int(10) unsigned NOT NULL default '0',
  resource varchar(30) NOT NULL default '',
  num int(10) unsigned NOT NULL default '1',
  isdeleted enum('T','F') NOT NULL default 'F',
  ts timestamp(14) NOT NULL,
  PRIMARY KEY  (domain_id,resource,isdeleted),
  UNIQUE KEY idx0 (hit_id)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table `image`
--

CREATE TABLE image (
  image_id int(10) unsigned NOT NULL auto_increment,
  user_id int(10) unsigned NOT NULL default '0',
  name varchar(30) NOT NULL default '',
  width int(3) unsigned NOT NULL default '0',
  height int(3) unsigned NOT NULL default '0',
  data blob,
  bytes int(5) unsigned NOT NULL default '0',
  ext enum('jpg','gif','png') default NULL,
  isdeleted enum('T','F') NOT NULL default 'F',
  tscreated timestamp(14) NOT NULL,
  PRIMARY KEY  (image_id),
  UNIQUE KEY idx2 (name),
  KEY idx1 (user_id)
) TYPE=MyISAM;

--
-- Table structure for table `namespace`
--

CREATE TABLE namespace (
  namespace_id int(11) NOT NULL auto_increment,
  language char(50) NOT NULL default 'en',
  branding char(50) NOT NULL default '',
  PRIMARY KEY  (namespace_id),
  UNIQUE KEY idx2 (language,branding)
) TYPE=MyISAM;

--
-- Table structure for table `pageview`
--

CREATE TABLE pageview (
  pageview_id int(10) unsigned NOT NULL auto_increment,
  session_id int(10) unsigned NOT NULL default '0',
  domain_id int(10) unsigned NOT NULL default '0',
  user_id int(10) unsigned NOT NULL default '0',
  browser_id int(10) unsigned NOT NULL default '0',
  resource varchar(30) NOT NULL default '',
  value varchar(20) NOT NULL default '',
  tscreated timestamp(14) NOT NULL,
  affiliate_id int(10) unsigned NOT NULL default '0',
  UNIQUE KEY idx0 (pageview_id),
  KEY idx1 (session_id),
  KEY idx2 (user_id),
  KEY idx3 (resource),
  KEY idx4 (affiliate_id)
) TYPE=MyISAM AVG_ROW_LENGTH=50 PACK_KEYS=1;

--
-- Table structure for table `referer`
--

CREATE TABLE referer (
  referer_id int(10) unsigned NOT NULL auto_increment,
  domain_id int(10) unsigned NOT NULL default '0',
  url varchar(120) NOT NULL default '',
  num int(10) unsigned NOT NULL default '1',
  ts timestamp(14) NOT NULL,
  isdeleted enum('T','F') NOT NULL default 'F',
  PRIMARY KEY  (domain_id,url,isdeleted),
  UNIQUE KEY idx0 (referer_id)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table `session`
--

CREATE TABLE session (
  session_id int(10) unsigned NOT NULL auto_increment,
  sessionkey char(20) NOT NULL default '',
  referer_id int(10) unsigned NOT NULL default '0',
  domain_id int(10) unsigned NOT NULL default '0',
  user_id int(10) unsigned NOT NULL default '0',
  affiliate_id int(10) unsigned NOT NULL default '0',
  browser_id int(10) unsigned NOT NULL default '0',
  dtcreated datetime NOT NULL default '0000-00-00 00:00:00',
  ip char(16) default NULL,
  PRIMARY KEY  (session_id),
  UNIQUE KEY idx5 (sessionkey),
  KEY idx1 (domain_id),
  KEY idx3 (user_id),
  KEY idx4 (affiliate_id)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table `sessiondata`
--

CREATE TABLE sessiondata (
  sessiondata_id int(10) unsigned NOT NULL auto_increment,
  session_id int(10) unsigned NOT NULL default '0',
  name varchar(50) NOT NULL default '',
  value text NOT NULL,
  dtcreated datetime NOT NULL,
  UNIQUE KEY idx0 (sessiondata_id),
  KEY idx1 (session_id),
  KEY idx2 (name),
  KEY idx3 (dtcreated)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table `state`
--

CREATE TABLE state (
  state_id int(10) unsigned NOT NULL auto_increment,
  name char(30) NOT NULL default '',
  abbrev char(2) NOT NULL default '',
  PRIMARY KEY  (name,abbrev),
  UNIQUE KEY idx0 (state_id)
) TYPE=MyISAM MAX_ROWS=100 PACK_KEYS=1;

INSERT INTO state VALUES (1,'-- other --','');
INSERT INTO state VALUES (2,'Alabama','AL');
INSERT INTO state VALUES (3,'Alaska','AK');
INSERT INTO state VALUES (4,'Arizona','AZ');
INSERT INTO state VALUES (5,'Arkansas','AR');
INSERT INTO state VALUES (6,'California','CA');
INSERT INTO state VALUES (7,'Colorado','CO');
INSERT INTO state VALUES (8,'Connecticut','CT');
INSERT INTO state VALUES (9,'DC','DC');
INSERT INTO state VALUES (10,'Delaware','DE');
INSERT INTO state VALUES (11,'Florida','FL');
INSERT INTO state VALUES (12,'Georgia','GA');
INSERT INTO state VALUES (13,'Hawaii','HI');
INSERT INTO state VALUES (14,'Idaho','ID');
INSERT INTO state VALUES (15,'Illinois','IL');
INSERT INTO state VALUES (16,'Indiana','IN');
INSERT INTO state VALUES (17,'Iowa','IA');
INSERT INTO state VALUES (18,'Kansas','KS');
INSERT INTO state VALUES (19,'Kentucky','KY');
INSERT INTO state VALUES (20,'Louisiana','LA');
INSERT INTO state VALUES (21,'Maine','ME');
INSERT INTO state VALUES (22,'Maryland','MD');
INSERT INTO state VALUES (23,'Massachusetts','MA');
INSERT INTO state VALUES (24,'Michigan','MI');
INSERT INTO state VALUES (25,'Minnesota','MN');
INSERT INTO state VALUES (26,'Mississippi','MS');
INSERT INTO state VALUES (27,'Missouri','MO');
INSERT INTO state VALUES (28,'Montana','MT');
INSERT INTO state VALUES (29,'Nebraska','NE');
INSERT INTO state VALUES (30,'Nevada','NV');
INSERT INTO state VALUES (31,'New Hampshire','NH');
INSERT INTO state VALUES (32,'New Jersey','NJ');
INSERT INTO state VALUES (33,'New Mexico','NM');
INSERT INTO state VALUES (34,'New York','NY');
INSERT INTO state VALUES (35,'North Carolina','NC');
INSERT INTO state VALUES (36,'North Dakota','ND');
INSERT INTO state VALUES (37,'Ohio','OH');
INSERT INTO state VALUES (38,'Oklahoma','OK');
INSERT INTO state VALUES (39,'Oregon','OR');
INSERT INTO state VALUES (40,'Pennsylvania','PA');
INSERT INTO state VALUES (41,'Rhode Island','RI');
INSERT INTO state VALUES (42,'South Carolina','SC');
INSERT INTO state VALUES (43,'South Dakota','SD');
INSERT INTO state VALUES (44,'Tennessee','TN');
INSERT INTO state VALUES (45,'Texas','TX');
INSERT INTO state VALUES (46,'Utah','UT');
INSERT INTO state VALUES (47,'Vermont','VT');
INSERT INTO state VALUES (48,'Virginia','VA');
INSERT INTO state VALUES (49,'Washington','WA');
INSERT INTO state VALUES (50,'West Virginia','WV');
INSERT INTO state VALUES (51,'Wisconsin','WI');
INSERT INTO state VALUES (52,'Wyoming','WY');
INSERT INTO state VALUES (53,'Hysteria','');
INSERT INTO state VALUES (54,'Ontario','ON');
INSERT INTO state VALUES (55,'Manitoba','MB');
INSERT INTO state VALUES (56,'Quebec','QC');
INSERT INTO state VALUES (57,'Victoria','');
INSERT INTO state VALUES (58,'British Columbia','BC');
INSERT INTO state VALUES (59,'Alberta','AB');
INSERT INTO state VALUES (60,'Queensland','');
INSERT INTO state VALUES (61,'New South Wales','');
INSERT INTO state VALUES (62,'Limburg','');
INSERT INTO state VALUES (63,'Guam','GU');
INSERT INTO state VALUES (64,'Saskatchewan','SK');
INSERT INTO state VALUES (65,'Nova Scotia','NS');
INSERT INTO state VALUES (66,'South Australia','');
INSERT INTO state VALUES (67,'Western Australia','');
INSERT INTO state VALUES (68,'New Brunswick','NB');
INSERT INTO state VALUES (69,'Cheshire','');
INSERT INTO state VALUES (70,'Hampshire','');
INSERT INTO state VALUES (71,'Lancashire','');
INSERT INTO state VALUES (72,'Newfoundland','NF');
INSERT INTO state VALUES (73,'Yorkshire','');
INSERT INTO state VALUES (74,'American Samoa','AS');
INSERT INTO state VALUES (75,'Marianas Islands','MP');
INSERT INTO state VALUES (76,'Marshall Islands','MH');
INSERT INTO state VALUES (77,'Micronesia','FM');
INSERT INTO state VALUES (78,'Palau','PW');
INSERT INTO state VALUES (79,'Puerto Rico','PR');
INSERT INTO state VALUES (80,'Virgin Islands','VI');
INSERT INTO state VALUES (81,'Northwest Territory','NT');
INSERT INTO state VALUES (82,'Prince Edward Island','PE');
INSERT INTO state VALUES (83,'Yukon','YK');
INSERT INTO state VALUES (84,'Tasmania','');

--
-- Table structure for table `ttg`
--

CREATE TABLE ttg (
  ttg_id int(10) unsigned NOT NULL auto_increment,
  domain_id int(10) unsigned NOT NULL default '0',
  hostname varchar(50) NOT NULL default '',
  resource varchar(255) NOT NULL default '',
  begts double(20,6) NOT NULL default '0.000000',
  endts double(20,6) NOT NULL default '0.000000',
  PRIMARY KEY  (ttg_id),
  KEY idx1 (resource),
  KEY idx2 (domain_id),
  KEY idx3 (begts),
  KEY idx4 (endts),
  KEY idx5 (hostname)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table `user`
--

CREATE TABLE user (
  user_id int(10) unsigned NOT NULL auto_increment,
  isconfirmed enum('T','F') NOT NULL default 'F',
  email varchar(50) NOT NULL default '',
  bounces int(3) NOT NULL default '0',
  screenname varchar(50) NOT NULL default '',
  password varchar(15) NOT NULL default '',
  hint varchar(30) default '',
  autologin enum('T','F') NOT NULL default 'F',
  firstname varchar(20) NOT NULL default '',
  lastname varchar(30) NOT NULL default '',
  address varchar(40) default NULL,
  city varchar(30) default '',
  state_id int(10) unsigned NOT NULL default '1',
  statewritein varchar(33) default '',
  postalcode varchar(10) default '',
  country_id int(10) unsigned NOT NULL default '1',
  countrywritein varchar(33) default '',
  gender enum('M','F') default NULL,
  birthyear year(4) NOT NULL default '0000',
  referal varchar(30) default '',
  language varchar(10) NOT NULL default 'en',
  isdeleted enum('T','F') NOT NULL default 'F',
  ts timestamp(14) NOT NULL,
  dtcreated datetime NOT NULL default '0000-00-00 00:00:00',
  lastactive timestamp(14) NOT NULL default '00000000000000',
  PRIMARY KEY  (user_id),
  UNIQUE KEY idx3 (screenname,isdeleted),
  UNIQUE KEY idx2 (email,password,isdeleted),
  UNIQUE KEY idx1 (email)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table `user_accountype`
--

CREATE TABLE user_accountype (
  user_accountype_id int(11) NOT NULL auto_increment,
  user_id int(11) NOT NULL default '0',
  accountype_id int(11) NOT NULL default '0',
  dtcreated datetime NOT NULL default '0000-00-00 00:00:00',
  startdate date NOT NULL default '0000-00-00',
  stopdate date NOT NULL default '0000-00-00',
  isdeleted enum('T','F') NOT NULL default 'F',
  UNIQUE KEY idx3 (user_id,accountype_id),
  UNIQUE KEY idx0 (user_accountype_id),
  KEY idx1 (user_id),
  KEY idx2 (accountype_id)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table `user_session`
--

CREATE TABLE user_session (
  user_session_id int(10) unsigned NOT NULL auto_increment,
  user_id int(10) unsigned NOT NULL default '0',
  session_id int(10) unsigned NOT NULL default '0',
  ip char(20) NOT NULL default '',
  dtcreated datetime NOT NULL,
  UNIQUE KEY idx0 (user_session_id),
  KEY idx1 (user_id,session_id),
  KEY idx2 (ip)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table `userdata`
--

CREATE TABLE userdata (
  userdata_id int(10) unsigned NOT NULL auto_increment,
  user_id int(10) unsigned NOT NULL default '0',
  name varchar(50) NOT NULL default '',
  value text NOT NULL,
  dtcreated datetime NOT NULL,
  isdeleted enum ('F','T') NOT NULL DEFAULT 'F',
  UNIQUE KEY idx0 (userdata_id),
  KEY idx1 (user_id),
  KEY idx2 (name),
  KEY idx3 (dtcreated)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Table structure for table `verbage`
--

CREATE TABLE verbage (
  verbage_id int(10) unsigned NOT NULL auto_increment,
  resource varchar(255) NOT NULL default '',
  name varchar(255) NOT NULL default '',
  summary varchar(255) NOT NULL default '',
  body text NOT NULL,
  dtcreated datetime NOT NULL default '0000-00-00 00:00:00',
  ishidden enum('T','F') NOT NULL default 'F',
  isdeleted enum('T','F') NOT NULL default 'F',
  UNIQUE KEY idx0 (verbage_id),
  KEY idx1 (resource),
  KEY idx2 (name)
) TYPE=MyISAM PACK_KEYS=1;


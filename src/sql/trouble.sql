create database trouble;
use trouble;

--
-- Table structure for table `links`
--

CREATE TABLE links (
  links_id int(10) unsigned NOT NULL auto_increment,
  name varchar(255) NOT NULL default '',
  UNIQUE KEY idx0 (links_id)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Dumping data for table `links`
--

INSERT INTO links VALUES (1,'T1');
INSERT INTO links VALUES (2,'SMDS');
INSERT INTO links VALUES (3,'dual T1');
INSERT INTO links VALUES (4,'T3');
INSERT INTO links VALUES (5,'AX.25 Link');
INSERT INTO links VALUES (6,'clothesline');
INSERT INTO links VALUES (7,'toaster');
INSERT INTO links VALUES (8,'ATM');
INSERT INTO links VALUES (9,'14.4 dialup');
INSERT INTO links VALUES (10,'frame relay');
INSERT INTO links VALUES (11,'AUXnet');
INSERT INTO links VALUES (12,'NFC');
INSERT INTO links VALUES (13,'OC128');
INSERT INTO links VALUES (14,'Sneakernet connection');
INSERT INTO links VALUES (15,'phonenet');
INSERT INTO links VALUES (16,'dental floss line');
INSERT INTO links VALUES (17,'EOIU link');
INSERT INTO links VALUES (18,'SSMN link');
INSERT INTO links VALUES (19,'wamnet');
INSERT INTO links VALUES (20,'OPI');
INSERT INTO links VALUES (21,'TTYT');
INSERT INTO links VALUES (22,'ISDN');
INSERT INTO links VALUES (23,'ubfuscated dangling linky-winky');
INSERT INTO links VALUES (24,'poo');
INSERT INTO links VALUES (25,'There&#39;s TROUBLE in River City');
INSERT INTO links VALUES (26,'If you&#39;re into FreeFlight, FUBAR is Big Trouble!');
INSERT INTO links VALUES (27,'hedgehog');
INSERT INTO links VALUES (28,'zeppelin');
INSERT INTO links VALUES (29,'algorithm');
INSERT INTO links VALUES (30,'aaa');
INSERT INTO links VALUES (31,'animals fall in my crack-a-thon');

--
-- Table structure for table `locations`
--

CREATE TABLE locations (
  locations_id int(10) unsigned NOT NULL auto_increment,
  name varchar(255) NOT NULL default '',
  UNIQUE KEY idx0 (locations_id)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Dumping data for table `locations`
--

INSERT INTO locations VALUES (1,'your office');
INSERT INTO locations VALUES (2,'your house');
INSERT INTO locations VALUES (3,'the POP');
INSERT INTO locations VALUES (4,'the demarc');
INSERT INTO locations VALUES (5,'the moon pop');
INSERT INTO locations VALUES (6,'headquarters');
INSERT INTO locations VALUES (7,'Barbados');
INSERT INTO locations VALUES (8,'New Zealand');
INSERT INTO locations VALUES (9,'Network B');
INSERT INTO locations VALUES (10,'Eaton, Maine');
INSERT INTO locations VALUES (11,'the central office');
INSERT INTO locations VALUES (12,'the central switching station');
INSERT INTO locations VALUES (13,'Westfalia');
INSERT INTO locations VALUES (14,'Naptown');
INSERT INTO locations VALUES (15,'Woodtucky, CA');
INSERT INTO locations VALUES (16,'Akron');
INSERT INTO locations VALUES (17,'this link is down because the goverment feels we&#39;re to stupid to read it.');
INSERT INTO locations VALUES (18,'monkeys fly out of my ass');

--
-- Table structure for table `rfos`
--

CREATE TABLE rfos (
  rfos_id int(10) unsigned NOT NULL auto_increment,
  name varchar(255) NOT NULL default '',
  UNIQUE KEY idx0 (rfos_id)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Dumping data for table `rfos`
--

INSERT INTO rfos VALUES (1,'Neal Bahu');
INSERT INTO rfos VALUES (2,'Windows 95');
INSERT INTO rfos VALUES (3,'a fibercut');
INSERT INTO rfos VALUES (4,'a mistake');
INSERT INTO rfos VALUES (5,'a power outage');
INSERT INTO rfos VALUES (6,'an errant well-driller');
INSERT INTO rfos VALUES (7,'backhoes');
INSERT INTO rfos VALUES (8,'birds and fog');
INSERT INTO rfos VALUES (9,'cereal weevils');
INSERT INTO rfos VALUES (10,'electromagnetic pulse');
INSERT INTO rfos VALUES (11,'chemjets');
INSERT INTO rfos VALUES (12,'black helecopters');
INSERT INTO rfos VALUES (13,'solar flares');
INSERT INTO rfos VALUES (14,'luser interference');
INSERT INTO rfos VALUES (15,'Microsoft');
INSERT INTO rfos VALUES (16,'hungry gophers');
INSERT INTO rfos VALUES (17,'hungry wombats');
INSERT INTO rfos VALUES (18,'a dog urine situation');
INSERT INTO rfos VALUES (19,'excess traffic');
INSERT INTO rfos VALUES (20,'a routing problem');
INSERT INTO rfos VALUES (21,'network hiccups');
INSERT INTO rfos VALUES (22,'Electron Interference');
INSERT INTO rfos VALUES (23,'electromagnetic pulse');
INSERT INTO rfos VALUES (24,'oversynchronous');
INSERT INTO rfos VALUES (25,'the world being generally fucked up');
INSERT INTO rfos VALUES (26,'ecuadorean earthquake');

--
-- Table structure for table `states`
--

CREATE TABLE states (
  states_id int(10) unsigned NOT NULL auto_increment,
  name varchar(255) NOT NULL default '',
  UNIQUE KEY idx0 (states_id)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Dumping data for table `states`
--

INSERT INTO states VALUES (1,'down');
INSERT INTO states VALUES (2,'AFU');
INSERT INTO states VALUES (3,'fried');
INSERT INTO states VALUES (4,'wigging');
INSERT INTO states VALUES (5,'patchy');
INSERT INTO states VALUES (6,'errored');
INSERT INTO states VALUES (7,'hosed');
INSERT INTO states VALUES (8,'taking hits');
INSERT INTO states VALUES (9,'dropping packets');
INSERT INTO states VALUES (10,'resetting');
INSERT INTO states VALUES (11,'FUBAR');
INSERT INTO states VALUES (12,'incognito');
INSERT INTO states VALUES (13,'in flux');
INSERT INTO states VALUES (14,'hiccupping');
INSERT INTO states VALUES (15,'not up');
INSERT INTO states VALUES (16,'doing a hard resync');
INSERT INTO states VALUES (17,'doing a cold reboot');
INSERT INTO states VALUES (18,'discombobulated');
INSERT INTO states VALUES (19,'Die W. Die');
INSERT INTO states VALUES (20,'the world being generally fucked up');
INSERT INTO states VALUES (21,'waffling');
INSERT INTO states VALUES (22,'ecuadorean earthquake');
INSERT INTO states VALUES (23,'squanky');

--
-- Table structure for table `telcos`
--

CREATE TABLE telcos (
  telcos_id int(10) unsigned NOT NULL auto_increment,
  name varchar(255) NOT NULL default '',
  UNIQUE KEY idx0 (telcos_id)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Dumping data for table `telcos`
--

INSERT INTO telcos VALUES (1,'NYNEX');
INSERT INTO telcos VALUES (2,'Bell Atlantic');
INSERT INTO telcos VALUES (3,'GTE');
INSERT INTO telcos VALUES (4,'Bell South');
INSERT INTO telcos VALUES (5,'SNET');
INSERT INTO telcos VALUES (6,'US West');
INSERT INTO telcos VALUES (7,'Ameritech');
INSERT INTO telcos VALUES (8,'Pac Bell');
INSERT INTO telcos VALUES (9,'Southwest Bell');
INSERT INTO telcos VALUES (10,'Bobby');
INSERT INTO telcos VALUES (11,'Dubya');
INSERT INTO telcos VALUES (12,'Nextel');
INSERT INTO telcos VALUES (13,'Some engineer at PacBell');
INSERT INTO telcos VALUES (14,'Verizon');
INSERT INTO telcos VALUES (15,'Covad');
INSERT INTO telcos VALUES (16,'Concentric');
INSERT INTO telcos VALUES (17,'XO Communications');
INSERT INTO telcos VALUES (18,'Grandma');
INSERT INTO telcos VALUES (19,'GrandmaComm.net');
INSERT INTO telcos VALUES (20,'Global Crossing');
INSERT INTO telcos VALUES (21,'Southwest Bell');
INSERT INTO telcos VALUES (22,'Qwest');
INSERT INTO telcos VALUES (23,'simco resolution decay modality decay');
INSERT INTO telcos VALUES (24,'battering the ramparts');
INSERT INTO telcos VALUES (25,'MCI');

--
-- Table structure for table `telcostates`
--

CREATE TABLE telcostates (
  telcostates_id int(10) unsigned NOT NULL auto_increment,
  name varchar(255) NOT NULL default '',
  UNIQUE KEY idx0 (telcostates_id)
) TYPE=MyISAM PACK_KEYS=1;

--
-- Dumping data for table `telcostates`
--

INSERT INTO telcostates VALUES (1,'asleep');
INSERT INTO telcostates VALUES (2,'working on it');
INSERT INTO telcostates VALUES (3,'RNA');
INSERT INTO telcostates VALUES (4,'dispatching');
INSERT INTO telcostates VALUES (5,'testing');
INSERT INTO telcostates VALUES (6,'in a meeting');
INSERT INTO telcostates VALUES (7,'on strike');
INSERT INTO telcostates VALUES (8,'taking a break');
INSERT INTO telcostates VALUES (9,'havin\' a smoke');
INSERT INTO telcostates VALUES (10,'blaming it on our equipment');
INSERT INTO telcostates VALUES (11,'researching the nature of the problem.');
INSERT INTO telcostates VALUES (12,'being tolerated at this point .. there will be no progress until this state is repaired');
INSERT INTO telcostates VALUES (13,'recombobulating the interface');
INSERT INTO telcostates VALUES (14,'disconfiguring the transformer');
INSERT INTO telcostates VALUES (15,'resetting the terminal server');
INSERT INTO telcostates VALUES (16,'sucking ass');
INSERT INTO telcostates VALUES (17,'ordering pizza');
INSERT INTO telcostates VALUES (18,'doing what they can to expedite the situation');
INSERT INTO telcostates VALUES (19,'uninterested in upgrading the necessary componants');
INSERT INTO telcostates VALUES (20,'calling a consultant');
INSERT INTO telcostates VALUES (21,'waiting on parts');
INSERT INTO telcostates VALUES (22,'rebooting');
INSERT INTO telcostates VALUES (23,'doing a low-level fsck');
INSERT INTO telcostates VALUES (24,'doing nothing');
INSERT INTO telcostates VALUES (25,'back-ordered on 82-pin SIMMS');
INSERT INTO telcostates VALUES (26,'digging a new trench and laying cable');
INSERT INTO telcostates VALUES (27,'an awful racket');
INSERT INTO telcostates VALUES (28,'defunct.');


#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

extern void	do_wget(void);

/*
	the application information structure
 */
appinfo_t	appinfo = {
	/* the application name */
		"htdb_wget",
	/* the application version */
		"0.0.1",
	/* command-line usage information */
		"this is a simple 'wget' URL sucker program\n"
		"offered as a proof of concept.",
	/* initial htdb values */
		"",
	/* address of handler function */
		do_wget,
	/* serve this many pages per fastCGI loop */
		FCGI_KEEPALIVE,
	/* (optional) command-line arguments */
		"<URL>"
};

void	do_wget(void)
{
	if (htdb_getint("argc") < 2) {
		htdb_usage(&appinfo);
		exit(1);
	}
	htdb_wget("o", htdb_getval("argv[1]"), 3);
	printf("%s\n", htdb_getval("o"));
}

int	main(int argc, char **argv)
{
	exit (htdb_main(argc, argv, &appinfo));
}

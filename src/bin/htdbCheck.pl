#!/usr/bin/perl

my $file;

#Current line number.
my $x = 1;

#Current if-level.
my $l = 1;

#Current while-level.
my $w = 1;

#Current loop-level.
my $lp = 1;

#current curly-brace level
my $cb = 1;

#current square-brace level
my $sb = 1;

#current paren level
my $p = 1;

#levels of if structures
my %iflevels;

#levels of while structures
my %wlevels;

#levels of loop structures
my %lplevels;

#levels of curly-braces
my %cblevels;

#levels of square braces
my %sblevels;

#leves of parenthese
my %plevels;

sub finish_live()
{
	while ($l > 1) {
		$l--;
		print "$file: line $iflevels{$l}: Unmatched #live if statement\n";
	}

	while ($w > 1) {
		$w--;
		print "$file: line $iwlevels{$w}: Unmatches #live while statement\n";
	}

	while ($lp > 1) {
		$lp--;
		print "$file: line $ilplevels{$lp}: Unmatches #live loop statement\n";
	}
}

sub finish_braces()
{
	print "$file: line $cblevels{--$cb}: mismatched {\n" while ( $cb > 1 );
	print "$file: line $sblevels{--$sb}: mismatched [\n" while ( $sb > 1 );
	print "$file: line $plevels{--$p}: mismatched (\n" while ( $p > 1 );
	$cb = $sb = $p = 1;
}
 

sub match_braces()
{
	my $line = $_[0];
	my $linenumber = $_[1];
	my $isliveline = $_[2];
	my $prevch;

#	if ( $isliveline ) {
#		#all bets off at this point.
#		&finish_braces();	
#	}

	my @c = split(//, $line);
	while(@c) {
		$ch = shift(@c);
		if ( $ch eq '{' ) {
			$cblevels{$cb} = $linenumber;
			$cb++;
		} elsif ( $ch eq '}' ) {
			$cb--;
			if ( $cb < 1 ) {
				print "$file: line $linenumber: Mismatched }\n"; 
				$cb = 1;
			}
		} elsif ( $ch eq '[' ) {
			$sblevels{$sb} = $linenumber;
			$sb++;
		} elsif ( $ch eq ']' ) {
			$sb--; 
			if ( $sb < 1 ) {
				print "$file: line $linenumber: Mismatched ]\n"; 
				$sb = 1;
			}
# Rules on parentheses only apply within curly braces
		} elsif ( $ch eq '(' && $cb > 1 ) {
			$plevels{$p} = $linenumber;
			$p++;
		} elsif ( $ch eq ')' && $cb > 1 ) {
			$p--; 
			if ( $p < 1 ) {
				print "$file: line $linenumber: Mismatched )\n"; 
				$p = 1;
			}
		}
		$prevch = $ch;
	}	
}

while(@ARGV) {
	$file = shift(@ARGV);
	if ( -d $file ) {
		print STDERR "$file: is a directory\n";
		next;
	}
	open(INPUT, $file) || die("couldn't open file");
	$x = 1;
	$first_line = 0;
	while (<INPUT>) {
		if ( $_ ne "\n" ) {
			$first_line = 1;
		}

		if ( $_ =~ /^#live/ ) {
			if ( $_ =~ /end\s*if/  ) {
				$l--;
				if ( $l < 1 ) { 
					print "$file: line $x: Unmatched endif\n";
					$l++;
				}
			} elsif ( $_ =~ /if/ && $_ !~ /else/ ) {
				$iflevels{$l} = $x;
				$l++;
			} elsif ( $_ =~ /loop/ && $_ !~ /endloop/ ) {
				$lplevels{$lp} = $x;
				$lp++;
			} elsif ( $_ =~ /endloop/ ) {
				$lp--;
				if ( $lp < 1 ) { 	
					print "$file: line $x: unmatched endloop at line $x\n";
					$lp++;
				} 
			} elsif ( $_ =~ /while/ && $_ !~ /endwhile/ ) {
				$wlevels{$w} = $x;
				$w++;
			} elsif ( $_ =~ /endwhile/ ) {
				$w--;
				if ( $w < 1 ) { 	
					print "$file: line $x: unmatched endwhile at line $x\n";
					$w++;
				} 
			}	
			&match_braces($_, $x, 1);	
#		} elsif ( $_ eq "\n" ) {
#			if ( !$first_line ) {
#				print "$file: line $x: Warning: new lines at the top of the file.\n";
#			}
#
#			if ($l > 1 || $w > 1 || $lp > 1) {
#				print "$file: line $x: Warning: blank line within conditional\n";
#			}
		} elsif ( $_ =~ /^#define/ ) {
			&finish_live();
		} else {
			&match_braces($_, $x, 0);	
		}
		$x++;
	}


	finish_braces();
	finish_live();
}

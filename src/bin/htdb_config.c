#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdbconfig.h"
#include	<stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
	if (argc == 1) {
		printf("Usage: %s [OPTIONS]\n", argv[0]);
		printf("Options:\n");
		printf("   --cflags   [%s]\n", COMPILE_CFLAGS);
		printf("   --include  [%s]\n", COMPILE_INCLUDE);
		printf("   --libs     [%s]\n", COMPILE_LIBS);
		printf("   --etc      [%s]\n", COMPILE_ETC);
		printf("   --bin      [%s]\n", COMPILE_BIN);
		printf("   --version  [%s]\n", LIBHTDB_VERSION);
	} else {
		while (--argc > 0) {
			if (strcasestr(argv[argc], "--cflags"))
				printf("%s\n", COMPILE_CFLAGS);
			else if (strcasestr(argv[argc], "--include"))
				printf("%s\n", COMPILE_INCLUDE);
			else if (strcasestr(argv[argc], "--libs"))
				printf("%s\n", COMPILE_LIBS);
			else if (strcasestr(argv[argc], "--etc"))
				printf("%s\n", COMPILE_ETC);
			else if (strcasestr(argv[argc], "--bin"))
				printf("%s\n", COMPILE_BIN);
			else if (strcasestr(argv[argc], "--version"))
				printf("%s\n", LIBHTDB_VERSION);
		}
	}
}

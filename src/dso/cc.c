#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

char	*dso_ccNumber(char *in)
{
	char	ret[BUF_LIMIT_SMALL + 1];
	int		indx = 0;
	while (in && *in && indx < BUF_LIMIT_SMALL) {
		if (isdigit((int)*in))
			ret[indx++] = *in;
		in++;
	}
	ret[indx] = '\0';
	return DSO_RETURNS_SUCCESS(ret);
}

#define	SYNTAX_ccValid	"usage: \\$\\{ccValid(credit card #)\\}"

char	*dso_ccValid(char *in)
{
	if (ccValid(in))
		return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
	else
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_ccValid);
}

/*
	dso_ccType()
		credit card number validation
		uses LUHN Formula (Mod 10) for Validation of Primary Account Number
		see http://www.beachnet.com/~hstiles/cardtype.html
*/
char	*dso_ccType(char *in)
{
	char	*ptr = dso_ccNumber(in),
			*type = "unknown";
	int		length = strlen(ptr);

	if (length == 16 && *ptr == '5' &&
		(*(ptr + 1) == '1' ||
		*(ptr + 1) == '2' ||
		*(ptr + 1) == '3' ||
		*(ptr + 1) == '4' ||
		*(ptr + 1) == '5'))
		type = "mc";
	else if ((length == 13 || length == 16) &&
		*ptr == '4')
		type = "visa";
	else if (length == 15 && *ptr == '3' &&
		(*(ptr + 1) == '4' ||
		*(ptr + 1) == '7'))
		type = "amex";
	else if (length == 14 && *ptr == '3' &&
		((*(ptr + 1) == '0' &&
		(*(ptr + 2) == '0' ||
		*(ptr + 2) == '1' ||
		*(ptr + 2) == '2' ||
		*(ptr + 2) == '3' ||
		*(ptr + 2) == '4' ||
		*(ptr + 2) == '5')) ||
		(*(ptr + 1) == '6' ||
		*(ptr + 1) == '8')))
		type = "diners";
	else if (length == 16 &&
		*ptr == '6' &&
		*(ptr + 1) == '0' &&
		*(ptr + 2) == '1' &&
		*(ptr + 3) == '1')
		type = "discover";
	else if (length == 15 &&
		*ptr == '2' &&
		(
		(*(ptr + 1) == '0' && *(ptr + 2) == '1' && *(ptr + 3) == '4') ||
		(*(ptr + 1) == '1' && *(ptr + 2) == '4' && *(ptr + 3) == '9')
		))
		type = "enroute";
	else if (length == 16 && *ptr == '3')
		type = "jcb";
	else if (length == 15 &&
		(
		(*(ptr + 1) == '2' && *(ptr + 2) == '1' && *(ptr + 3) == '3' &&
			*(ptr + 4) == '1') ||
		(*(ptr + 1) == '1' && *(ptr + 2) == '8' && *(ptr + 3) == '0' &&
			*(ptr + 4) == '0')
		))
		type = "jcb";
	else
		type = "unknown";

	free(ptr);
	return DSO_RETURNS_SUCCESS(type);
}

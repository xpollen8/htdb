#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

/*
	dso_log_debug()
		show default-level debugging
 */
char	*dso_log_debug(char *in)
{
	char	*comma = strchr(in, ','),
			*out = (char *)NULL;

	if (comma && *comma) {
		*comma = '\0';
		debug_it(DEBUG_LEVEL_DEBUG, in, comma + 1, &out);
	} else
		debug_it(DEBUG_LEVEL_DEBUG, in, NULL, &out);

	return DSO_RETURNS_SUCCESS_ASIS(out);
}

/*
	dso_log_debug()
		show info-level debugging
 */
char	*dso_log_info(char *in)
{
	char	*comma = strchr(in, ','),
			*out = (char *)NULL;

	if (comma && *comma) {
		*comma = '\0';
		debug_it(DEBUG_LEVEL_INFO, in, comma + 1, &out);
	} else
		debug_it(DEBUG_LEVEL_INFO, in, NULL, &out);

	return DSO_RETURNS_SUCCESS_ASIS(out);
}

/*
	dso_log_warning()
		show warning-level debugging
 */
char	*dso_log_warning(char *in)
{
	char	*comma = strchr(in, ','),
			*out = (char *)NULL;

	if (comma && *comma) {
		*comma = '\0';
		debug_it(DEBUG_LEVEL_WARNING, in, comma + 1, &out);
	} else
		debug_it(DEBUG_LEVEL_WARNING, in, NULL, &out);

	return DSO_RETURNS_SUCCESS_ASIS(out);
}

/*
	dso_log_error()
		show error-level debugging
 */
char	*dso_log_error(char *in)
{
	char	*comma = strchr(in, ','),
			*out = (char *)NULL;

	if (comma && *comma) {
		*comma = '\0';
		debug_it(DEBUG_LEVEL_ERROR, in, comma + 1, &out);
	} else
		debug_it(DEBUG_LEVEL_ERROR, in, NULL, &out);

	return DSO_RETURNS_SUCCESS_ASIS(out);
}

/*
	dso_log_fatal()
		show fatal-level debugging
 */
char	*dso_log_fatal(char *in)
{
	char	*comma = strchr(in, ','),
			*out = (char *)NULL;

	if (comma && *comma) {
		*comma = '\0';
		debug_it(DEBUG_LEVEL_FATAL, in, comma + 1, &out);
	} else
		debug_it(DEBUG_LEVEL_FATAL, in, NULL, &out);

	return DSO_RETURNS_SUCCESS_ASIS(out);
}



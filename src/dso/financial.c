#ident	"$Header$"

/*
	Copyright � 2005-2008 Matthew Sewell
	all rights reserved.

	Permission to use, copy, modify, and non-commercially distribute
	this software and its documentation for any purpose is hereby granted
	without fee, provided that the above copyright notice appear in all
	copies and that both that copyright notice and this permission notice
	appear in supporting documentation.  No representations are made about
	the suitability of this software for any purpose.  It is provided
	"as is" without express or implied warranty.
 */
#include	"htdb.h"

#define SYNTAX_rror "usage: rror(nominal, inflation, taxrate)"
char	*dso_rror(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 3);

	if (3 == num_args) {
		float	nominal = atof(args[0]) * (strchr(args[0], '%') ? 0.01 : 1.0),
				inflation = atof(args[1]) * (strchr(args[1], '%') ? 0.01 : 1.0),
				taxrate = atof(args[2]) * (strchr(args[2], '%') ? 0.01 : 1.0),
				real = (1 + nominal * (1 - taxrate)) / (1 + inflation) - 1;
		char	result[BUF_LIMIT_SMALL];

		sprintf(result, "%0.2f%%", real * 100);

		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS(result);
	}
	htdb_deleteArgs(args,num_args);
	return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_rror);
}

#define SYNTAX_nror "usage: nror(real, inflation, taxrate)"
char	*dso_nror(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 3);

	if (3 == num_args) {
		float	real = atof(args[0]) * (strchr(args[0], '%') ? 0.01 : 1.0),
				inflation = atof(args[1]) * (strchr(args[1], '%') ? 0.01 : 1.0),
				taxrate = atof(args[2]) * (strchr(args[2], '%') ? 0.01 : 1.0),
				nominal = ((real + 1) * (1 + inflation) - 1) / (1 - taxrate);
		char	result[BUF_LIMIT_SMALL];

		sprintf(result, "%0.2f%%", nominal * 100);

		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS(result);
	}
	htdb_deleteArgs(args,num_args);
	return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_nror);
}

#define SYNTAX_fvCalc "usage: fvCalc(present_value, rate_of_return, duration_years, additional_yearly_contributions)"
/*
 *	This function takes the current value of a savings or investment account and applies a yearly rate of return
 *	and and duration (in year) to it and returns the future value.  If you will not be making annual contributions,
 *	use '0' for the 4th argument.
 */
char	*dso_fvCalc(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 4);

	if (4 == num_args) {
		float	fv,
			duration = atof(args[2]),
			present_value = atof(args[0]),
			rate_of_return = atof(args[1]) * (strchr(args[1], '%') ? 0.01 : 1.0),
			contributions = atof(args[3]),
			z = rate_of_return + 1; //This is really just to make things a little more clear in the next block
		
		if(rate_of_return > 0){
			fv = present_value*pow(z, duration) + contributions*((pow(z, duration+1)-z)/(z-1));
		}else{ 
			fv = present_value + contributions*duration;
		}
		
		{
			char	result[BUF_LIMIT_SMALL];

			sprintf(result, "%0.2f", fv);

			htdb_deleteArgs(args, num_args);
			return DSO_RETURNS_SUCCESS(result);
		}
	}
	htdb_deleteArgs(args,num_args);
	return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_fvCalc);
}

#define SYNTAX_pvCalc "usage: pvCalc(future_value, rate_of_return, duration)"

char	*dso_pvCalc(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 3);

	if (3 == num_args) {
		int		duration = atoi(args[2]);
		float	future_value = atof(args[0]),
				rate_of_return = atof(args[1]) * (strchr(args[1], '%') ? 0.01 : 1.0),
				pv = future_value / pow (1 + rate_of_return, duration);
		char	result[BUF_LIMIT_SMALL];

		sprintf(result, "%0.2f", pv);

		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS(result);
	}
	htdb_deleteArgs(args,num_args);
	return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_pvCalc);
}


#define SYNTAX_getMortgagePayment "usage: getMotgagePayment(loan_amount, interest_rate, loan_years)"
/*	This function takes the loan principal, interest rate ( ie. 7.25%), and loan duration 
  	(in years) and returns the monthly payment.
*/
char	*dso_getMortgagePayment(char *in)
{
	char	**args;
	int	num_args = htdb_splitArgs(in, ",", &args, 3);
	if(3 == num_args) {
		int     loan_years = atoi(args[2]);
		float   loan_amount = atof(args[0]),
			loan_months = loan_years*12,
			interest_rate = atof(args[1])*(strchr(args[1], '%') ? 0.01 : 1.0),
			payment = (loan_amount*pow(1+(interest_rate/12), loan_months)*(interest_rate/12))/(pow((1+(interest_rate/12)), loan_months)-1);
			char    result[BUF_LIMIT_SMALL];

		sprintf(result, "%0.2f", payment);

		htdb_deleteArgs(args, num_args);
                return DSO_RETURNS_SUCCESS(result);
	}
        htdb_deleteArgs(args,num_args);
        return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_getMortgagePayment);
}




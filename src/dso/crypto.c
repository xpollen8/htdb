#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

char	*dso_encrypt(char *source)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_encryptor("pe1", source));
}

char	*dso_be1(char *source)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_encryptor("be1", source));
}

char	*dso_se1(char *source)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_encryptor("se1", source));
}

char	*dso_pe1(char *source)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_encryptor("pe1", source));
}

/*
	dso_crypto()

		this is really an internal function - used to dynamically
		create encryptor/decryptor functions based upon encryptor
		definitions found in config.htdb.
 */
char	*dso_crypto(char *source)
{
	char	**args,
			*out = (char *)NULL;
	int		num_args = htdb_splitArgs(source, ",", &args, 2);

	if (num_args == 2) {
		out = htdb_encryptor(args[0], args[1]);
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_ASIS(out);
	}

	htdb_deleteArgs(args, num_args);
	return DSO_RETURNS_SUCCESS_CHALLENGED("");
}

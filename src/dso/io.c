#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

#define	SYNTAX_fopen	"usage: \\$\\{dso_fopen(handle, path, mode)\\}"
char    *dso_fopen(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 3);

	if (num_args == 3) {
		char	*prefix = args[0],
				*path = args[1],
				*mode = args[2];
		spaceData	*b = file_lookup(prefix);

		if (b == (spaceData *)NULL) {
			filep_t	*fpt = (filep_t *)malloc(sizeof(filep_t));
			/*
				new file request
			 */
			fpt->path = strdup(path);
			fpt->mode = strdup(mode);
			fpt->fp = (FILE *)fopen(fpt->path, fpt->mode);
			file_store(prefix, (void *)fpt);
		} else {
			/*
				file's already open
			 */
		}

		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS("");
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_fopen);
	}
}

#define	SYNTAX_fclose	"usage: \\$\\{dso_fclose(handle)\\}"
char    *dso_fclose(char *in)
{
	char	*prefix = in;

	if (prefix && *prefix) {
		spaceData	*b = file_lookup(prefix);

		if (b == (spaceData *)NULL) {
			return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_fclose);
		} else {
			/*
				close the file, clean up.
			 */
			htdb_destroyFilePointer(b->value);
			b->value = NULL;
			free(b->key);
			b->key = strdup("");
			return DSO_RETURNS_SUCCESS("");
		}

	} else {
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_fclose);
	}
}

#define	SYNTAX_fwrite	"usage: \\$\\{dso_fwrite(handle, string)\\}"
char    *dso_fwrite(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 2);

	if (num_args == 2) {
		char	*prefix = args[0],
				*string = args[1];
		spaceData	*b = file_lookup(prefix);

		if (b == (spaceData *)NULL) {
			htdb_deleteArgs(args, num_args);
			return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_fwrite);
		} else {
			filep_t	*fpt = (filep_t *)b->value;
			fprintf((FILE *)fpt->fp, "%s", string);
		}

		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS("");
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_fwrite);
	}
}

#define	SYNTAX_wget	"usage: wget(prefix, URL)"

char	*dso_wget(char *in)
{
	char 	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 2);

	if (num_args == 2) {
		char	*prefix = args[0],
				*url = args[1];

		if (BOOL_TRUE == htdb_wget(prefix, url, WGET_DEFAULT_TIMEOUT)) {
			htdb_deleteArgs(args, num_args);
			return DSO_RETURNS_SUCCESS("");
		} else {
			htdb_deleteArgs(args, num_args);
			return DSO_RETURNS_SUCCESS_CHALLENGED("");
		}
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_wget);
	}
}

char	*dso_json2htdb(char *in)
{
	char 	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 2);
	bool_t	status = BOOL_FALSE;

	if (num_args == 2) {
		/*
			convert JSON 'data' into htdb objects
		 */
		status = htdb_json2htdb(args[0], args[1]);
	}
	htdb_deleteArgs(args, num_args);
	if (BOOL_TRUE == status)
		return DSO_RETURNS_SUCCESS("");
	else
		return DSO_RETURNS_SUCCESS_CHALLENGED("");
}

char	*dso_htdb2json(char *in)
{
	char 	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 1);
	char	*ret = (char *)NULL;

	if (num_args == 1) {
		/*
			walk all objects with ${prefix} base
			and populate a JSON object
		 */
		ret = htdb_htdb2json(args[0]);
	}
	htdb_deleteArgs(args, num_args);
	if (ret)
		return DSO_RETURNS_SUCCESS_ASIS(ret);
	else
		return DSO_RETURNS_SUCCESS_CHALLENGED("");
}

char	*dso_rss(char *in)
{
	char 	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 0);
	int_t	timeout = WGET_DEFAULT_TIMEOUT;
	bool_t	status = BOOL_FALSE;

	if (num_args > 0) {
		if (num_args == 2) {
			timeout = atoi(args[1]);
		}
		if (timeout <= 0)
			timeout = WGET_DEFAULT_TIMEOUT;
		{
			if (BOOL_TRUE == htdb_wget(args[0], args[0], timeout)) {
				status = htdb_parseXML(args[0], htdb_getval(args[0]));
			}
		}
	}
	htdb_deleteArgs(args, num_args);
	if (BOOL_TRUE == status)
		return DSO_RETURNS_SUCCESS("");
	else
		return DSO_RETURNS_SUCCESS_CHALLENGED("");
}

char	*dso_cleanupRSS(char *in)
{
	htdb_clearXML(in);
	return DSO_RETURNS_SUCCESS("");
}

char	*dso_cleanupXML(char *in)
{
	htdb_clearXML(in);
	return DSO_RETURNS_SUCCESS("");
}

#define	SYNTAX_fileExists	"fileExists: no such file"

/*
	dso_fileExists()
		make a filesystem call to see if file exists
 */
char	*dso_fileExists(char *in)
{
	struct	stat    buf;
	if (stat(in, &buf) == 0)
		return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
	return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_fileExists);
}

#define SYNTAX_system	"usage: \\$\\{system(command)\\}"

/*
	dso_system()
		makes a system call as whatever user the process is running as.
		begeesus.  i can't believe i'm including this..
		we try to do *some* damage control by not allowing root processes.
 */
char	*dso_system(char *source)
{
	if (BOOL_TRUE == amRoot())
		return DSO_RETURNS_SUCCESS_CHALLENGED(ARE_YOU_INSANE);
	else {
		return DSO_RETURNS_SUCCESS_ASIS(htdb_system(source));
	}
}

#define	SENDMAIL_SYNTAX		"usage: \\$\\{sendmail(message)\\}"
#define	SENDMAIL_MISSING	"sendmail undefined in config.htdb"

/*
	dso_sendmail()
		pipelines the input string through whatever resource
		is defined as `confSendmail'
 */
char	*dso_sendmail(char *source)
{
	if (BOOL_TRUE == amRoot())
		return DSO_RETURNS_SUCCESS_CHALLENGED(ARE_YOU_INSANE);
	else {
		return (sendmail(htdb_getval("confSendmail"), source) == 0) ?
			DSO_RETURNS_SUCCESS_CHALLENGED(SENDMAIL_MISSING) :
			DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
	}
}


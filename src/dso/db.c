#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

/*
	dso_dbSafe()
		literals single quote characters
		(should probably also literal double quote characters)
 */
char	*dso_dbSafe(char *in)
{
	char	*out = (char *)malloc(sizeof(char) * ((2 * strlen(in)) + 1));
	int	indx = 0;

	while (in && *in) {
		if (*in == '\'') {
			out[indx++] = '\\';
			out[indx++] = *in;
		} else if (isalnum((int)*in) || *in == ' ' || *in == '.' || *in == '@' || *in == '/')
			out[indx++] = *in;
		in++;
	}

	out[indx] = '\0';
	return DSO_RETURNS_SUCCESS_ASIS(out);
}

#define	SYNTAX_isql		"isql: ${_error_query}"

/*
	dso_isql()
		makes a SQL call and makes callbacks for each row of the result
		set to the resource named li`prefix' and build-ups a result set
		named lo`prefix'
 */
char	*dso_isql(char *source)
{
	char	*prefix,
		*colon = strchr(source, ':');

	if (colon && (colon - source) < 20) {
		/*
			user-selected prefix for results
			(only if the colon is in the first 20 characters)
		 */
		prefix = source;
		*colon = '\0';
		source = colon + 1;
	} else
		/*
			default
		 */
		prefix = "sql";

	if (dbProcessByRow(source, prefix) < 0)
		/*
			error
		 */
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_isql);
	else
		return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
}

#define	SYNTAX_sql		"usage: sql(table:SQL) - bad SQL: \"${_error_query}\""

/*
	dso_sql()
		makes a SQL call and stores the result set in `prefix'-named namespace
 */
char	*dso_sql(char *source)
{
	char	*prefix,
		*colon = strchr(source, ':');

	if (colon && (colon - source) < 20) {
		/*
			user-selected prefix for results
			(only if the colon is in the first 20 characters)
		 */
		prefix = source;
		*colon = '\0';
		source = colon + 1;
	} else
		/*
			default
		 */
		prefix = "sql";

	if (dbGetRowByKey(prefix, source) < 0)
		/*
			error
		 */
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_sql);
	else
		return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
}

/*
	dso_deleteSessionData()
		deletes all sessiondata table rows for the current session_id
 */
char	*dso_deleteSessionData(char *in)
{
	int_t	session_id = htdb_getint("_session_int");
	deleteSessionData(session_id, in);
	return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
}

#define	SYNTAX_storeSessionData	"usage: \\$\\{storeSessionData(name, value)\\}"

char	*dso_storeSessionData(char *in)
{
	int_t	session_id = htdb_getint("_session_int");
	char	*colon = strchr(in, ',');
	if (colon && *colon) {
		*colon = '\0';
		storeSessionData(session_id, in, colon + 1);
		return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
	} else
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_storeSessionData);
}

char	*dso_fetchSessionData(char *in)
{
	int_t	session_id = htdb_getint("_session_int");
	fetchSessionData(session_id, in);
	if (in && *in)
		return DSO_RETURNS_SUCCESS(htdb_getval("sessiondata->value"));
	else
		return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
}

/*
	dso_deleteUserData()
		deletes all userdata table rows for the current user_id
 */
char	*dso_deleteUserData(char *in)
{
	int_t	user_id = htdb_getint("user->user_id");
	if (user_id > 0) {
		deleteUserData(user_id, in);
		return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
	} else
		return DSO_RETURNS_SUCCESS("");
}

#define	SYNTAX_storeUserData	"usage: \\$\\{storeUserData(name, value)\\}"

char	*dso_storeUserData(char *in)
{
	int_t	user_id = htdb_getint("user->user_id");
	if (user_id > 0) {
		char	*colon = strchr(in, ',');
		if (colon && *colon) {
			*colon = '\0';
			storeUserData(user_id, in, colon + 1);
			return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
		} else
			return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_storeUserData);
	} else
		return DSO_RETURNS_SUCCESS("");
}

char	*dso_fetchUserData(char *in)
{
	int_t	user_id = htdb_getint("user->user_id");
	if (user_id > 0) {
		fetchUserData(user_id, in);
		if (in && *in)
			return DSO_RETURNS_SUCCESS(htdb_getval("userdata->value"));
		else
			return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
	} else
		return DSO_RETURNS_SUCCESS("");
}


#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

#define	SYNTAX_include	"usage: \\$\\{include(HTDB resource file in ${_htdb_root}/[language]/[branding])\\}"

/*
	dso_include()
		include another HTDB file in-place
 */
char	*dso_include(char *in)
{
	if (htdb_include(in))
		return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
	else
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_include);
}

#define	SYNTAX_define	"usage: \\$\\{define([name], [value])\\}"

/*
	dso_define()
		inline equivalent of #define
 */
char	*dso_define(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 2);

	if (num_args == 1 || num_args == 2) {
		char	*sparen,
			*eparen;
		if ((sparen = strchr(args[0], '(')) &&
			(eparen = strchr(args[0], ')'))) {
			/*
				things get trippy..
				if the `name' argument contains parenthesis,
				then we assume we are being asked to declare
				a script function dynamically.
				self-creating code, anyone?
			 */
			*sparen = '\0';
			*eparen = '\0';
			htdb_setfunc(args[0], sparen + 1, (num_args == 1 ? "" : args[1]), BOOL_FALSE);
		} else {
			/*
				otherwise, this is a vanilla definition.
			 */
			htdb_setval(args[0], (num_args == 1 ? "" : args[1]));
		}
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS("");
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS(SYNTAX_define);
	}
}

#define	SYNTAX_undefine	"usage: \\$\\{undefine([name], [value])\\}"

/*
	dso_undefine()
 */
char	*dso_undefine(char *in)
{
	if (in && *in) {
		char	*lparen = strchr(in, '(');
		if (lparen && *lparen && strchr(in, ')')) {
			/*
				things get trippy..
				if the `name' argument contains parenthesis,
				then we assume we are being asked to undefine
				a script function.
			 */
			*lparen = '\0';
			sfunc_undefine(in);
		} else {
			/*
				otherwise, this is a vanilla definition.
			 */
			htdb_undefine(in);
		}
		return DSO_RETURNS_SUCCESS("");
	} else {
		return DSO_RETURNS_SUCCESS(SYNTAX_undefine);
	}
}

#define	SYNTAX_resolve	"usage: \\$\\{resolve([variable], [default])\\}"

char	*dso_resolve(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 2);

	if (num_args == 1 || num_args == 2) {
		char	*val = args[0],
				*def = (num_args == 1 ? "" : args[1]),
				*ret;

		if (htdb_defined(val)) {
			ret = htdb_script(val);
		} else {
			ret = strdup(def);
		}
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_ASIS(ret);
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS(SYNTAX_define);
	}
}

/*
	dso_describe()

		used to self document HTDB objects
		(to the extent possible)

		behold - the only embedded HTML you will find in htdb source code.
 */
char	*dso_describe(char *in)
{
	spaceData	*b;
	spaceData	bx;
	int	found = 0;

	data_init(&bx);

	if (cfunc_lookup(in)) {
		thing_append(&bx, "<table border=\"1\" width=\"100%\"><tr>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, "<td width=\"5%\">name</td><td>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, in, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, "</td></tr>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);

		thing_append(&bx,
			"<tr><td width=\"5%\">type</td><td>Compiled Function", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		found++;
		thing_append(&bx, "</td></tr></table>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	}
	if ((b = sfunc_lookup(in))) {
		char	tmp[512];
		int	i;
		char	*ptr;
		sfunc_t	*sf = (sfunc_t *)b->value;

		thing_append(&bx, "<table border=\"1\" width=\"100%\"><tr>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, "<td width=\"5%\">name</td><td>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, in, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, "</td></tr>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);

		thing_append(&bx,
			"<tr><td width=\"5%\">type</td><td>Resource File Script Function",
			FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx,
			(sf->internally_defined == BOOL_TRUE ? " (internally-defined)" : ""),
			FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx,
			"</td></tr>",
			FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, "<tr><td width=\"5%%\">usage</td><td>$\\{", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, in, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_appendChar(&bx, '(', BUF_LIMIT_BIG);
		for (i = 0 ; i < sf->num_args ; i++) {
			thing_append(&bx, sf->args[i], FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			thing_append(&bx, "=...", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			if (i < sf->num_args - 1)
				thing_append(&bx, ", ", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		}
		thing_append(&bx, ")}</td></tr>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		sprintf(tmp, "<tr><td width=\"5%%\">bytes</td><td>%ld</td></tr>"
			"<tr><td width=\"5%%\" valign=\"top\">body</td><td><code>",
			(long)strlen(sf->body));
		thing_append(&bx, tmp, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, ptr = htdb_htdb2html(sf->body),
			FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		free(ptr);
		found++;
		thing_append(&bx, "</td></tr></table>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	}
	if ((b = static_lookup(in))) {
		char	tmp[512],
				*ptr;
		thing_append(&bx, "<table border=\"1\" width=\"100%\"><tr>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, "<td width=\"5%\">name</td><td>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, in, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, "</td></tr>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);

		thing_append(&bx,
			"<tr><td width=\"5%\">type</td><td>Static Resource File Definition</td></tr>",
			FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		sprintf(tmp,
			"<tr><td width=\"5%%\">bytes</td><td>%ld</td></tr><tr>"
			"<td width=\"5%%\" valign=\"top\">value</td><td><code>",
			(long)b->size);
		thing_append(&bx, tmp, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, ptr = htdb_htdb2html(b->value), FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		free(ptr);
		found++;
		thing_append(&bx, "</td></tr></table>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	} else if ((b = htdb_lookup(in))) {
		char	tmp[512],
				*ptr;
		thing_append(&bx, "<table border=\"1\" width=\"100%\"><tr>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, "<td width=\"5%\">name</td><td>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, in, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, "</td></tr>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);

		thing_append(&bx,
			"<tr><td width=\"5%\">type</td><td>Resource File Definition</td></tr>",
			FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		sprintf(tmp,
			"<tr><td width=\"5%%\">bytes</td><td>%ld</td></tr><tr>"
			"<td width=\"5%%\" valign=\"top\">value</td><td><code>",
			(long)b->size);
		thing_append(&bx, tmp, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, ptr = htdb_htdb2html(b->value), FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		free(ptr);
		found++;
		thing_append(&bx, "</td></tr></table>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	}
	if (found == 0) {
		thing_append(&bx, "<table border=\"1\" width=\"100%\"><tr>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, "<td width=\"5%\">name</td><td>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, in, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, "</td></tr>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);

		thing_append(&bx, "<tr><td width=\"5%\">type</td><td>Undefined", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		thing_append(&bx, "</td></tr></table>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	}

	return DSO_RETURNS_SUCCESS_ASIS((char *)bx.value);
}

#define	SYNTAX_indexOf	"usage: \\$\\{indexOf(table, field, value)\\}"

char	*dso_indexOf(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 3);

	if (num_args == 3) {
		char	*table = args[0],
				*field = args[1],
				*value = args[2];
		int	status = myIndexOf(table, field, value);

		htdb_deleteArgs(args, num_args);

		if (status) {
			char	ret[BUF_LIMIT_SMALL + 1];
			snprintf(ret, BUF_LIMIT_SMALL, "%d", status);
			return DSO_RETURNS_SUCCESS(ret);
		} else
			return DSO_RETURNS_SUCCESS_CHALLENGED("");
	} else {
		htdb_deleteArgs(args, num_args);
	}
	return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_indexOf);
}

#define	SYNTAX_idOf	"usage: \\$\\{idOf(table, field, value)\\}"

char	*dso_idOf(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 3);

	if (num_args == 3) {
		char	*table = args[0],
				*field = args[1],
				*value = args[2];
		int	this_index,
			found = 1;

		for (this_index = 1 ; found == 1 ; this_index++) {
			char	*lookup = htdb_getobjname(table, this_index, field);

			if (htdb_checkval(lookup)) {
				if (htdb_equal(lookup, value)) {
					char	ret[BUF_LIMIT_SMALL + 1];

					snprintf(ret, BUF_LIMIT_SMALL, "%s[%d]->%s_id",
						table, this_index, table);
					ret[BUF_LIMIT_SMALL] = '\0';

					htdb_deleteArgs(args, num_args);
					return DSO_RETURNS_SUCCESS(htdb_getval(ret));
				}
			} else
				found = 0;
		}
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_idOf);
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_idOf);
	}
}

/*
	dso_inList()
		returns the substring if it is one of a specified subset
		found within the input string.
 */
char	*dso_inList(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 0),
			found = 0;

	if (num_args > 1) {
		char	*target = args[0];
		int		i;
		for (i = 1 ; !found && i < num_args ; i++)
			if (strcasestr(target, args[i]))
				found = i;
	}

	if (found) {
		char	*foundString = strdup(args[found]);
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_ASIS(foundString);
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED("");
	}
}

#define	SYNTAX_sort	"usage: sort(obj_name, sort_field)"

char	*dso_sort(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 2);

	if (num_args == 2) {
		htdb_objsort(args[0], args[1]);
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_sort);
	}
}

#define	SYNTAX_MAKEARRAY	"usage: \\$\\{makeArray(target, [separator,] string)\\}"

/*
	dso_makeArray()
		breaks a string into an htdb array object.
		array characteristics are determined by the separator value
 */
char	*dso_makeArray(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 3);

	if (num_args == 3 || num_args == 2) {
		char	*target = args[0],
				*separator = (num_args == 3) ? args[1] : "",
				*string = (num_args == 3) ? args[2] : args[1];
		bool_t	subbed = BOOL_FALSE;	// signals need to free

		int	done = 0;

		if (num_args == 3) {
			if (strlen(separator) == 0) {
				/*
					empty separator value.
					break on characters.
				 */
				int	index = 0;

				while (string && *string) {
					char	val[2];
					val[0] = *string;
					val[1] = '\0';
					htdb_setarrayval(target, ++index, val);
					string++;
				}
				htdb_setptrint(target, "numResults", index);
				done = 1;
			} else {
				separator = htdb_replaceMagic(separator);
				subbed = BOOL_TRUE;
			}
		} else if (num_args == 2) {
			/*
				only 2 arguments.
				break on word boundaries.
			 */
			separator = " ";
		}

		if (!done) {
			char	**n_args;
			int		n_num_args = htdb_splitArgs(string, separator, &n_args, 0),
					i;

//			log_info("makeArray", "(%s)(%s):%d", string, separator, n_args);
			for (i = 0 ; i < n_num_args ; i++)
				htdb_setarrayval(target, i + 1, n_args[i]);

			htdb_setptrint(target, "numResults", n_num_args);

			htdb_deleteArgs(n_args, n_num_args);
		}

		htdb_deleteArgs(args, num_args);
		if (BOOL_TRUE == subbed) {
			if (separator) { free(separator); }
		}
		return DSO_RETURNS_SUCCESS("");
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_MAKEARRAY);
	}
}

char	*dso_forEach(char *in)
{
	int		i;
	char	**args,
			forEachPrefix[BUF_LIMIT_BIG + 1];
	int		num_args = htdb_splitArgs(in, ",", &args, 0);

	snprintf(forEachPrefix, BUF_LIMIT_BIG, "%s", args[0]);
	forEachPrefix[BUF_LIMIT_BIG] = '\0';
	for (i = 1; i < num_args; i++) {
		htdb_setobjval(forEachPrefix, i, "val", args[i]);
	}
	htdb_setptrint(forEachPrefix, "numResults", i);
	htdb_deleteArgs(args, num_args);
	return DSO_RETURNS_SUCCESS("");
}

#define	SYNTAX_TERNARY	"usage: \\$\\{ternary((expression), true, false)\\}"

char	*dso_ternary(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 3);
	char	*expression = "",
			*caseTrue = "",
			*caseFalse = "",
			*out = "";

	if (num_args == 2) {
		expression = strdup(args[0]);
		caseTrue = args[1];
	} else if (num_args == 3) {
		expression = strdup(args[0]);
		caseTrue = args[1];
		caseFalse = args[2];
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_TERNARY);
	}

	if (BOOL_TRUE == live_eval_expr(expression))
		out = htdb_expand(caseTrue);
	else
		out = htdb_expand(caseFalse);

	free(expression);

	htdb_deleteArgs(args, num_args);

	return DSO_RETURNS_SUCCESS(out);
}

char	*dso_isUnconfirmed(char *in)
{
	if (BOOL_TRUE == isUnconfirmed())
		return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
	else
		return DSO_RETURNS_SUCCESS_CHALLENGED("");
}

char	*dso_isConfirmed(char *in)
{
	if (BOOL_TRUE == isConfirmed())
		return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
	else
		return DSO_RETURNS_SUCCESS_CHALLENGED("");
}

char	*dso_isUser(char *in)
{
	if (BOOL_TRUE == isUser())
		return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
	else
		return DSO_RETURNS_SUCCESS_CHALLENGED("");
}

char	*dso_isAdmin(char *in)
{
	if (BOOL_TRUE == isAdmin())
		return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
	else
		return DSO_RETURNS_SUCCESS_CHALLENGED("");
}

/*
	dso_literalizeCommas()
		um, literalizes commsa
 */
char	*dso_literalizeCommas(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(literalizeCommas(in));
}

/*
	dso_literalizeQuotes()
		um, literalizes quotes
 */
char	*dso_literalizeQuotes(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(literalizeQuotes(in));
}

/*
	dso_deLiteralizeCommas()
		removes literal marks from commas on input
 */
char	*dso_deLiteralizeCommas(char *in)
{
	spaceData	b;
	data_init(&b);
	while (in && *in) {
		if (*in == '\\' && (in + 1) && *(in + 1) && *(in + 1) == ',')
			;
		else
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
		in++;
	}

	return DSO_RETURNS_SUCCESS_ASIS((char *)b.value);
}

/*
	dso_filterProgramArguments()
		filter HTDB-sensitive values
 */
char	*dso_filterProgramArguments(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(filterProgramArguments(in));
}

/*
	dso_unfilter()
		converts HTML escape sequences back to quotes, backslashes, etc 
 */
char	*dso_unfilter(char *jin)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_unfilter(jin)); 
}

/*
	dso_unfilter_unsafe()
		converts HTML escape sequences back to quotes, backslashes, etc 
 */
char	*dso_unfilter_unsafe(char *jin)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_unfilter_unsafe(jin)); 
}

/*
	dso_unfilter_noDoubleQuotes()
		like unfilter except turns double quotes into single quotes.
 */
char	*dso_unfilter_noDoubleQuotes(char *jin)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_unfilter_nodoublequotes(jin)); 
}


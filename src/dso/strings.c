#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

char	*dso_eatWhitespace(char *in)
{
	char	ret[BUF_LIMIT_SMALL + 1];
	int		indx = 0;
	while (in && *in && indx < BUF_LIMIT_SMALL) {
		if (!isspace((int)*in))
			ret[indx++] = *in;
		in++;
	}
	ret[indx] = '\0';
	return DSO_RETURNS_SUCCESS(ret);
}

/*
	dso_cleanLines()
		minimizes whitespace in input string
 */
char	*dso_cleanLines(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_cleanLines(in));
}

char	*dso_stripWhitespace(char *in)
{
	char *work = strdup(in);
	htdb_stripWhitespace(work);
	return DSO_RETURNS_SUCCESS_ASIS(work);
}

char	*dso_lower(char *source)
{
	char	*ret = strdup(source),
			*hold = &ret[0];

	while (ret && *ret) {
		*(ret) = tolower(*(ret));
		ret++;
	}

	return DSO_RETURNS_SUCCESS_ASIS(hold);
}

char	*dso_upper(char *source)
{
	char	*ret = strdup(source),
			*hold = &ret[0];

	while (ret && *ret) {
		*(ret) = toupper(*(ret));
		ret++;
	}

	return DSO_RETURNS_SUCCESS_ASIS(hold);
}

#define	SYNTAX_substr		"usage: \\$\\{substr(start, count, string)\\}"

/*
	dso_substr()
		return a range of characters from within a string
 */
char	*dso_substr(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 3);

	if (num_args == 3) {
		int		start = atoi(args[0]),
				range = atoi(args[1]),
				len = strlen(args[2]);
		char	*str = args[2],
				*ret;

		if (start < 0)
			start = 0;

		if (range == 0)
			/*
				substr(4, 0, hello world)
			 */
			range = len;

		if (start > len)
			/*
				asked for past end of string,
				return empty string
			 */
			ret = strdup("");
		else {

			if (range >= 0) {
				/*
					range goes left -> right
				 */
				if (start + range > len)
					/*
						range takes us past the end.
						truncate the range to match string
					 */
					range = len - start;

				ret = strdup(&str[start]);
				ret[range] = '\0';
			} else {
				/*
					range goes right <- left
				 */
				if (start + range + 1 < 0)
					/*
						range takes us past the beginning.
						truncate the range to match string
					 */
					range = 0 - start - 1;

				ret = strdup(&str[start + range + 1]);
				ret[1 - range - 1] = '\0';
			}

		}

		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_ASIS(ret);
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_substr);
	}
}

static char	*_replace(char *source, char *in_search, char *replace, int case_sensitive)
{
	char * (*sstr) (const char *, const char *);
	spaceData b;
	char *pos, *old;
	char *search = htdb_replaceMagic(in_search);
	int replaceLen = strlen(replace),
		searchLen = strlen(search);

	sstr = case_sensitive ? strstr : strcasestr;

	if (0 == strcmp(search, replace)) {
		/*
			replacement is same as source!
			do nothing
		 */
		if (search) { free(search); }
		return strdup(source);
	}

	data_init(&b);

	pos = old = source;
//	log_info("search", "(%s)(%s)(%s)", source, search, replace);
	while ( (pos = sstr(pos, search)) ) {
		thing_append(&b, old, pos - old, BUF_LIMIT_SMALL);
		thing_append(&b, replace, replaceLen, BUF_LIMIT_SMALL);
		pos += searchLen;
		old = pos;
	}
	thing_append(&b, old, (source + strlen(source)) - old, BUF_LIMIT_SMALL);

	if (search) { free(search); }
	return b.value;
}

#define	SYNTAX_replace		"usage: \\$\\{replace(source, searchstring, replacestring)}"

/*
	dso_replace()
		replace all occurences of searchstring within source with replacestring
 */
char	*dso_replace(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 3);

	if (num_args == 3) {
		char *ret = _replace(args[0], args[1], args[2], 1);
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_ASIS(ret);
	} else if (num_args == 2) {
		char *ret = _replace(args[0], args[1], "", 1);
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_ASIS(ret);
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_replace);
	}
}

#define	SYNTAX_replacei		"usage: \\$\\{replacei(source, searchstring, replacestring)}"

/*
	dso_replacei()
		replace all occurences of searchstring within source with replacestring
 */
char	*dso_replacei(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 3);

	if (num_args == 3) {
		char *ret = _replace(args[0], args[1], args[2], 0);
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_ASIS(ret);
	} else if (num_args == 2) {
		char *ret = _replace(args[0], args[1], "", 0);
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_ASIS(ret);
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_replacei);
	}
}

char	*dso_pretty(char *ugly)
{
	return DSO_RETURNS_SUCCESS_ASIS(pretty(ugly));
}

/*
	dso_index()
		returns a number showing the first occurence of string2 in 
		string 1.  Named according to perl syntax, or to follow
		substr().
	UNTESTED.  Ben, 05-15-2001
*/

#define	SYNTAX_index		"usage: \\$\\{index(string1, string2)\\}"
char	*dso_index(char *in)
{
	char 	**args,
			buf[BUF_LIMIT_SMALL + 1];
	int		num_args = htdb_splitArgs(in, ",", &args, 2);

	if (num_args == 2) {
		char *strfound;
		strfound = strstr(args[0], args[1]);
		if (strfound) {
			snprintf(buf, BUF_LIMIT_SMALL, "%d", (int) (strfound - args[0]));
			buf[BUF_LIMIT_SMALL] = '\0';
			htdb_deleteArgs(args, num_args);
			return DSO_RETURNS_SUCCESS(buf);
		} else {
			htdb_deleteArgs(args, num_args);
			return DSO_RETURNS_SUCCESS("0");
		}
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_index);
	}
}	

#define	SYNTAX_trunc	"usage: \\$\\{trunc(maxlen, string)\\}"

char	*dso_trunc(char *in)
{
	int		len;
	char	*comma = strchr(in, ',');
	
	if ( !comma ) 
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_trunc);

	*comma = '\0';

	len = atoi(in);
	comma++;

	while (isspace(*comma))
		comma++;
	
	{
		/*  take out anything that could screw up our trunc 
		 	Ok to be unsafe here because we immediately re-filter. */
		char	*compact = htdb_unfilter_unsafe(comma),
				*out = (char *)malloc(sizeof(char) * (len + 4)),
				*full;
		
		strncpy(out, compact, len); out[len] = '\0';

		if (strlen(compact) > len)
			strcat(out, "..");
		
		full = filterProgramArguments(out);
		free(out);
		free(compact);
		return DSO_RETURNS_SUCCESS_ASIS(full);
	}
}

#define	SYNTAX_safetrunc 	"usage: \\$\\{safetrunc(maxlen | string)\\}"

char	*dso_safetrunc(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, "|", &args, 2);

	if (num_args == 2) {
		char	*ret = html_safe_trunc(args[1], atoi(args[0]));
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_ASIS(ret);
	} else  {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_safetrunc);
	}
}

#define	SYNTAX_prettyWrap	"usage: \\$\\{prettyWrap([margin, ]string)\\}"

char	*dso_prettyWrap(char *in)
{
	int	indx = 0,
		margin = 0;
	char	*comma = strchr(in, ','),
			*out = (char *)malloc(sizeof(char) * ((strlen(in) * 2) + 1)),
			*hold = &out[0];

	if (comma && *comma) {
		*comma = '\0';
		if (atoi(in) > 0) {
			margin = atoi(in);
			in = comma + 1;
		} else
			*comma = ',';

	}
	while (in && *in) {
		if (indx > 0 && *in == '\n') {
			*(out++) = ' ';
		} else if (*in == '-' && (in - 1 && *(in - 1) == '\n')) {
			indx = 0;
			*(out++) = '\n';
		} else {
			if (++indx > margin && isspace((int)*in)) {
				*(out++) = '\n';
				indx = 0;
			} else {
				*(out++) = *in;
			}
		}
		in++;
	}
	*out = '\0';
	return DSO_RETURNS_SUCCESS_ASIS(hold);
}

/*
	dso_strlen()
		return (a string representation)
		of the number of characters in `token'.
 */
char	*dso_strlen(char *token)
{
	char	ret[BUF_LIMIT_SMALL + 1],
			*expanded = htdb_expand(token);

	snprintf(ret, BUF_LIMIT_SMALL, "%d",
		(int)strlen(expanded));
	ret[BUF_LIMIT_SMALL] = '\0';
	free(expanded);
	return DSO_RETURNS_SUCCESS(ret);
}

/*
	dso_space2underscore()
		turns spaces into underscores
 */
char	*dso_underscore2space(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(underscore2space(in));
}

/*
	dso_space2underscore()
		turns spaces into underscores
 */
char	*dso_space2underscore(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(space2underscore(in));
}

/*
	dso_nonalnum2underscore()
		turns things not alnums into underscores
 */
char	*dso_nonalnum2underscore(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(makeFileSystemSafe(strdup(in)));
}

/*
	dso_shh()
		return an empty string
 */
char	*dso_shh(char *in)
{
	return DSO_RETURNS_SUCCESS("");
}

/*
	dso_reverse()
		reverses a string
 */
char	*dso_reverse(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(reverseString(in));
}

#define	SYNTAX_pad	"usage: pad([+-]count, [pad_string, ] string)"

char	*dso_pad(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 3),
			pad = 0,
			padNeeded = 0,
			padStringLen = 0;
	char	*padString,
			*orig;
	spaceData	b;

	data_init(&b);

	if (2 == num_args) {
		padString = strdup(" ");
		orig = args[1];
	} else if (3 == num_args) {
		padString = strdup(args[1]);
		orig = args[2];
	} else {
		htdb_deleteArgs(args, num_args); 
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_pad);
	}

	pad = abs(atoi(args[0]));

	if (strlen(orig) > pad)
		orig[pad] = '\0';

	padNeeded = pad - strlen(orig);
	padStringLen = strlen(padString);

	if (padNeeded <= 0) {
		thing_append(&b, orig, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	} else {
		if (args[0][0] == '-') {
			while (b.size < padNeeded) {
				if (b.size + padStringLen > padNeeded) {
					padString[padNeeded - b.size] = '\0';
					padStringLen = (padNeeded - b.size);
				}
				if (padStringLen > 0)
					thing_append(&b, padString, padStringLen, BUF_LIMIT_BIG);
			}
			thing_append(&b, orig, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		} else {
			thing_append(&b, orig, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			while (b.size < pad) {
				if (b.size + padStringLen > pad) {
					padString[pad - b.size] = '\0';
					padStringLen = (pad - b.size);
				}
				if (padStringLen > 0)
					thing_append(&b, padString, padStringLen, BUF_LIMIT_BIG);
			}
		}
	}
	free(padString);
	htdb_deleteArgs(args, num_args); 
	return DSO_RETURNS_SUCCESS_ASIS(b.value);
}

#define	PLURALIZE_USAGE_STRING	"usage: pluralize(x, singular, plural)\\}"

/*
	dso_pluralize()
		if the first argument is singular, then output the
		second argument modified, else output the 3rd argument modified
 */
char	*dso_pluralize(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 3);

	if (num_args == 3) {
		char	out[BUF_LIMIT_SMALL + 1];
		int		num = atoi(args[0]);

		if (num == 1)
			snprintf(out, BUF_LIMIT_SMALL, "%d %s", num, args[1]);
		else if (num == 0)
			snprintf(out, BUF_LIMIT_SMALL, "no %s", args[2]);
		else
			snprintf(out, BUF_LIMIT_SMALL, "%d %s", num, args[2]);
		out[BUF_LIMIT_SMALL] = '\0';

		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS(out);
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(PLURALIZE_USAGE_STRING);
	}
}

#define	SYNTAX_truncAt	"usage: \\$\\{truncAt(delimiter, string)\\}"

/*
	dso_truncAt()
		end the string past the delimiting string
 */
char	*dso_truncAt(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 2);

	if (num_args == 2) {
		char	*truncAt = args[0],
			*text = strdup(args[1]),
			*c = strcasestr(text, truncAt);
		if (c && *c)
			*c = '\0';
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_ASIS(text);
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS(SYNTAX_truncAt);
	}
}

#define	SYNTAX_MAPSTR	"usage: \\$\\{mapStr(pattern1, pattern2, string)\\}"

char	*dso_mapStr(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 3);

	if (num_args == 3) {
		char	*pattern1 = args[0],
				*pattern2 = args[1],
				*string = args[2],
				*str = htdb_mapStr(pattern1, pattern2, string);

		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_ASIS(str);
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_MAPSTR);
	}
}

char	*dso_summarizeText(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 5);

	if (num_args == 5) {
		int_t	lenBeforeText= atoi(args[0]),
				lenAfterText = atoi(args[1]);
		char	*term = args[2],
				*hilited = args[3],
				*text = args[4],
				*ptr = strcasestr(text, term);

		if (lenBeforeText < 0 && lenBeforeText > 100)
			lenBeforeText = 10;

		if (lenAfterText < 0 && lenAfterText > 100)
			lenAfterText = 20;

		if (ptr && *ptr) {
			spaceData   b;
			int		pos = 0;
			char	*ret;

			data_init(&b);

			/*
				calculate how much "prior" text to add
			 */
			if ((ptr - lenBeforeText) <= text) {
				pos = 0;
			} else {
				thing_append(&b, "...", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
				pos = ptr - text - lenBeforeText;
			}
			/*
				add "prior" text
			 */
			while (&text[pos] < ptr) {
				thing_appendChar(&b, text[pos], BUF_LIMIT_BIG);
				pos++;
			}
			/*
				add the thing
			 */
			thing_append(&b, hilited, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);

			pos += strlen(term);

			{
				int		num = 0;

				for (pos = pos ; num++ < lenAfterText && text[pos] ; pos++) {
					thing_appendChar(&b, text[pos], BUF_LIMIT_BIG);
				}
				if (text[pos])
					thing_append(&b, "...", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			}

			ret = strdup((char *)b.value);
			data_destroy(&b);

			htdb_deleteArgs(args, num_args);
			return DSO_RETURNS_SUCCESS_ASIS(ret);
		}
	}
	htdb_deleteArgs(args, num_args);
	return DSO_RETURNS_SUCCESS("");
}

char	*dso_replaceText(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 3);

	if (num_args == 3) {
		char	*pattern1 = args[0],
				*pattern2 = args[1],
				*input = args[2];
		bool_t	inTag = BOOL_FALSE;

		spaceData   b,
					textBuffer;

		data_init(&b);
		data_init(&textBuffer);

		while (input && *input) {
			if (*input == '>' && BOOL_TRUE == inTag) {
				/*
					end of the tag
				 */
				inTag = BOOL_FALSE;

				/*
					finish off the tag buffer value
				 */
				thing_appendChar(&b, *input, BUF_LIMIT_BIG);

			} else if (*input == '<') {
				/*
					do substitution on the text buffer
				 */
				if ((char *)textBuffer.value && *(char *)textBuffer.value) {
					char	*mapped = htdb_mapStr(pattern1, pattern2, (char *)textBuffer.value);
					thing_append(&b, mapped, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
					free(mapped);
					data_destroy(&textBuffer);
					data_init(&textBuffer);
				}
				/*
					into tag
				 */
				inTag = BOOL_TRUE;
				thing_appendChar(&b, *input, BUF_LIMIT_BIG);
			} else if (inTag == BOOL_FALSE) {
				/*
					add text not in tag to buffer
				 */
				thing_appendChar(&textBuffer, *input, BUF_LIMIT_BIG);
			} else {
				thing_appendChar(&b, *input, BUF_LIMIT_BIG);
			}
			input++;
		}
		if ((char *)textBuffer.value &&
			*(char *)textBuffer.value) {
			char	*mapped = htdb_mapStr(pattern1, pattern2, (char *)textBuffer.value);
			thing_append(&b, mapped, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			free(mapped);
		}
		htdb_deleteArgs(args, num_args);
		data_destroy(&textBuffer);
		{
			char	*ret = strdup((char *)b.value);
			data_destroy(&b);
			return DSO_RETURNS_SUCCESS_ASIS(ret);
		}
	}
	htdb_deleteArgs(args, num_args);
	return DSO_RETURNS_SUCCESS_CHALLENGED("");
}

static	char* xml_safe_entities[] = {
	"amp;",
	"gt;",
	"lt;", 
	"quot;",
	"apos;",
	NULL
};

char	*dso_xmlSafe(char *in)
{
	char *ptr = in;
	spaceData b;

	data_init(&b);

	while ( ptr && *ptr ) {
		if ( *ptr == '&' ) {
			int i;
			
			ptr++;	
			if ( !*ptr ) {
				thing_append(&b, "&amp;", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
				break;
			}

			if ( *ptr == '#' ) {
				/* check for &#39; style characters */
				int entity_len = 0;	
				char *ent = ptr + 1;

				/* walk to the end of the digits, figuring 10 digits is more than enough */
				while (ent && *ent && isdigit(*ent) && entity_len < 10) {
					entity_len++;
					ent++;
				}
				
				if ( *ent != ';' || !entity_len ) {
					/* illegal entity, just barf and move along (leaving the ptr at the pound sign) */
					thing_append(&b, "&amp;", 5, BUF_LIMIT_BIG);
				} else {
					/* ah, pointer arithmatic.  that's "append the ampersand, plus the pound, plus the semicolon, 
						plus however many digits the entity was" */
					thing_append(&b, ptr - 1, entity_len + 3, BUF_LIMIT_BIG);
					ptr += entity_len + 2;
				}
			} else {
				int i, safe = 0;
				for(i=0; xml_safe_entities[i]; i++) { 
					if (strncasecmp(xml_safe_entities[i], ptr, strlen(xml_safe_entities[i])) == 0) {
						thing_append(&b, ptr - 1, strlen(xml_safe_entities[i]) + 1, BUF_LIMIT_BIG);
						ptr += strlen(xml_safe_entities[i]);	
						safe = 1;
						break;
					}
				}
			
				if ( !safe ) {
					/* at last.  a fucking bare ampersand. */
					thing_append(&b, "&amp;", 5, BUF_LIMIT_BIG);
				}
			}	
		} else {
			if ( *ptr == '\'' ) 
				thing_append(&b, "&apos;", 6, BUF_LIMIT_BIG);
			else if ( *ptr == '>' )	
				thing_append(&b, "&gt;", 4, BUF_LIMIT_BIG);
			else if ( *ptr == '<' )	
				thing_append(&b, "&lt;", 4, BUF_LIMIT_BIG);
			else if ( *ptr == '"' )	
				thing_append(&b, "&quot;", 6, BUF_LIMIT_BIG);
			else 	
				thing_appendChar(&b, *ptr, BUF_LIMIT_BIG);
			ptr++;
		}
	}

	return DSO_RETURNS_SUCCESS_ASIS(b.value);
}


char *dso_newline2br(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(newline2br(in));
}

#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

char	*dso_braille(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_braille(in));
}

/*
	dso_rtoi()
		turns roman numerals into integers
 */
char	*dso_rtoi(char *in)
{
	char	*out = (char *)malloc(sizeof(char) * ((strlen(in) * 5) + 1));
	sprintf(out, "%d", htdb_rtoi(in));
	return DSO_RETURNS_SUCCESS_ASIS(out);
}

/*
	dso_itor()
		turns integers into roman numerals
 */
char	*dso_itor(char *in)
{
	char	*out = (char *)malloc(sizeof(char) * ((strlen(in) * 5) + 1));

	htdb_itor(atoi(in), &out);
	return DSO_RETURNS_SUCCESS_ASIS(out);
}

/*
	dso_rot13()
		super-secret rot13 encryption.  adobe may sue!
 */
char	*dso_rot13(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_rot13(in));
}

/*
	dso_igpayatinlay()
		pig-latin encoding
 */
char	*dso_igpayatinlay(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_igpayatinlay(in));
}

/*
	morse()
		translate and output `str' into morse code.
		god save our souls.
 */
char	*dso_morse(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_morse(in));
}

/*
	demorse()
		translate morse code into cleartext.
		god save our souls.
 */
char	*dso_demorse(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_demorse(in));
}

/*
	dso_imagify()
		sick.  for every printable character in the input,
		spit out an HTML img tag.
 */
char	*dso_imagify(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_imagify(in));
}

#define	SYNTAX_IMAGESCALEANDSTORE	"usage: \\$\\{imageScaleAndStore(table, dir, file, name, width, height)\\}"

char	*dso_imageScaleAndStore(char *in)
{
	bool_t	status = BOOL_FALSE;
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 6);

	if (num_args == 6) {
		char	*table = args[0],
				*dir = args[1],
				*file = args[2],
				*name = args[3];
		int_t	user_id = htdb_getint("user->user_id");
		int		width = atoi(args[4]),
				height = atoi(args[5]);
		status = imageScaleAndStore(table, user_id, name, dir, file, width, height);
	}

	htdb_deleteArgs(args, num_args);

	if (BOOL_TRUE == status)
		return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
	else
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_IMAGESCALEANDSTORE);

}

#define	SYNTAX_FETCHVERBAGE		"usage: \\$\\{fetchVerbage(prefix, lookup)\\}"

char	*dso_fetchVerbage(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 2);

	if (num_args == 2) {
		char	*prefix = args[0],
				*lookup = args[1];
		int_t	num = htdb_fetchVerbage(prefix, lookup);

		htdb_deleteArgs(args, num_args);

		if (num > 0)
			return DSO_RETURNS_SUCCESS(GENERIC_SUCCESS_STRING);
		else
			return DSO_RETURNS_SUCCESS_CHALLENGED(GENERIC_FAILURE_STRING);
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_FETCHVERBAGE);
	}
}

#define	SYNTAX_ttgDetail	"usage: ttgDetail(resource, domain_id, precision, bars, limit)"

char	*dso_ttgDetail(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 5);

	if (num_args >= 4) {
		char	*resource = args[0];
		int		domain_id = atoi(args[1]),
				precision = atoi(args[2]),
				bars = atoi(args[3]),
				limit = (num_args == 5) ? atoi(args[4]) : 0;
		char	*ret = ttgDetail(resource, domain_id, precision, bars, limit);
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_ASIS(ret);
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_ttgDetail);
	}
}

char	*dso_ttg(char *in)
{
	return htdb_ttg(in);
}

/*
	dso_checkFeedback()
		return number of comments left for the specified domain_id
		and resource (page) value
 */
char	*dso_checkFeedback(char *in)
{
	char	*colon = strchr(in, ':'),
		out[512];
	int_t	domain_id = 0;

	if (colon && *colon) {
		*colon = '\0';
		if (atoi(in) > 0) {
			domain_id = atol(in);
			in = colon + 1;
		} else
			*colon = ':';

		dbGetRowByKey("fdbk",
			"select count(feedback_id) as cnt from feedback where uri = '%s' and "
			"domain_id = %ld", in, domain_id);

		sprintf(out, "%s comment%s",
			htdb_getint("fdbk->cnt") > 0 ? htdb_getval("fdbk->cnt") : "no",
			htdb_getint("fdbk->cnt") == 1 ? "" : "s");
	} else
		strcpy(out, "no comments");

	return DSO_RETURNS_SUCCESS(out);
}

char	*parseHometown(char *in)
{
	char	*city = "",
			*state = "",
			*country = "",
			*state_select = "",
			*state_writein = "",
			*country_select = "",
			*country_writein = "",
			out[BUF_LIMIT_SMALL + 1],
			**args;
	int		num_args = htdb_splitArgs(in, ":", &args, 5);

	if (num_args > 0)
		city = args[0];
	if (num_args > 1)
		state_select = args[1];
	if (num_args > 2)
		state_writein = args[2];
	if (num_args > 3)
		country_select = args[3];
	if (num_args > 4)
		country_writein = args[4];

	state = (state_select && *state_select && *state_select != '-' &&
		*state_select != '*') ? state_select :
		(state_writein && *state_writein && *state_writein != '-' &&
		*state_writein != '*') ? state_writein : "";
	country = (country_select && *country_select && *country_select != '-' &&
		*country_select != '*') ? country_select :
		(country_writein && *country_writein && *country_writein != '-' &&
		*country_writein != '*') ? country_writein : "";

	if (strcasecmp(country, "USA") == 0)
		country = "";

	if (state && *state)
		if (strcasecmp(city, state) == 0)
			state = "";
	if (state && *state)
		if (country && *country)
			if (strcasecmp(state, country) == 0)
				state = "";

	snprintf(out, BUF_LIMIT_SMALL, "%s%s%s%s%s",
		city,
		(city && *city && state && *state) ? ", " : "", state,
		(((city && *city) || (state && *state)) && country && *country) ? ", " : "", country);

	htdb_deleteArgs(args, num_args);

	return pretty(out);
}

/*
	(city:state_select:state_writein:country_select:country_writein)
 */
char	*dso_smartHometown(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(parseHometown(in));
}

/*
	(trunc, lookup_prefix)
 */
char	*dso_easyHometown(char *in)
{
	char	buf[BUF_LIMIT_BIG + 1] = { "" },
			**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 2),
			trunc = atoi(num_args > 0 ? args[0] : "");

	strcpy(buf, "");

	if (num_args == 2)
		snprintf(buf, BUF_LIMIT_BIG, "%s:%s:%s:%s:%s",
			htdb_getptrval(args[1], "city"),
			htdb_getobjval("state",
				myIndexOf("state", "state_id", htdb_getptrval(args[1], "state_id")), "name"),
			htdb_getptrval(args[1], "statewritein"),
			htdb_getobjval("country",
				myIndexOf("country", "country_id", htdb_getptrval(args[1], "country_id")), "name"),
			htdb_getptrval(args[1], "country")); 

	{
		char	*prettified = parseHometown(buf);

		if (!*prettified) {
			/*
				no result?  use "unspecified".  lame. 
			 */
			free(prettified);
			prettified = strdup("Unspecified");
		}

		if (trunc && trunc < strlen(prettified)) {
			prettified[trunc] = '\0';
			prettified = (char *)realloc(prettified, sizeof(char) * (trunc + 5));
			strcat(prettified, "...");
		}

		htdb_deleteArgs(args, num_args);

		return DSO_RETURNS_SUCCESS_ASIS(prettified);
	}
}

#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

/*
	date/time formatting
 */
char	*dso_parseDate(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseDate(in));
}

char	*dso_parseRFC2822(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseRFC2822(in));
}

char	*dso_parseISO8601(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseISO8601(in));
}

char	*dso_parseMysqlTimestamp(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseMysqlTimestamp(in));
}

char	*dso_parseMysqlDateTime(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseMysqlDateTime(in));
}

char	*dso_parseDateTime(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseDateTime(in));
}

char	*dso_parseTime(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseTime(in));
}

char	*dso_parseMonthNum(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseMonthNum(in));
}

char	*dso_parseMon(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseMon(in));
}

char	*dso_parseMonth(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseMonth(in));
}

char	*dso_parseDay(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseDay(in));
}

char	*dso_parseYear(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseYear(in));
}

char	*dso_parseHour(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseHour(in));
}

char	*dso_parseMinute(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseMinute(in));
}

char	*dso_parseSecond(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseSecond(in));
}

char	*dso_parseAMPM(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseAMPM(in));
}

char	*dso_parseWeekday(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseWeekday(in));
}

char	*dso_parseUnix(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_parseUnix(in));
}

char	*dso_date(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_date(in));
}

char	*dso_month2num(char *in)
{
	char	*ret = htdb_month2num(in);

	if (ret && *ret)
		return DSO_RETURNS_SUCCESS(ret);

	return DSO_RETURNS_SUCCESS_CHALLENGED(in);
}

char	*dso_num2mon(char *in)
{
	char	*ret = htdb_num2mon(in);
	
	if (ret && *ret)
		return DSO_RETURNS_SUCCESS(ret);

	return DSO_RETURNS_SUCCESS_CHALLENGED(in);
}

char	*dso_num2month(char *in)
{
	char	*ret = htdb_num2month(in);
	
	if (ret && *ret)
		return DSO_RETURNS_SUCCESS(ret);

	return DSO_RETURNS_SUCCESS_CHALLENGED(in);
}

/*
	dso_fetchNextMonth()
		given a number between 1 and 12, return the integer value
		of the next month
			1 -> 2
			12 -> 1
 */
char	*dso_fetchNextMonth(char *in)
{
	char	*ret = htdb_fetchNextMonth(in);

	if (ret && *ret)
		return DSO_RETURNS_SUCCESS(ret);

	return DSO_RETURNS_SUCCESS_CHALLENGED(in);
}

/*
	dso_fetchPrevMonth()
		given a number between 1 and 12, return the integer value
		of the previous month
			1 -> 12
			2 -> 1
 */
char	*dso_fetchPrevMonth(char *in)
{
	char	*ret = htdb_fetchPrevMonth(in);

	if (ret && *ret)
		return DSO_RETURNS_SUCCESS(ret);

	return DSO_RETURNS_SUCCESS_CHALLENGED(in);
}

char	*dso_pretty_date(char *ugly)
{
	char	pretty[BUF_LIMIT_SMALL + 1];
	time_t	t = atol(ugly);
	sstrncpy(pretty, ctime(&t));
	pretty[24] = '\0';
	return DSO_RETURNS_SUCCESS(pretty);
}

char	*dso_pretty_time(char *ugly)
{
	double	min;
	int		hour;
	char	pretty[BUF_LIMIT_SMALL + 1],
			*c;
	if ((c = strchr(ugly, '.'))) {
		hour = atoi(ugly);
		min = atof(c);
	} else {
		hour = atoi(ugly);
		min = 0.0;
	}
	c = (hour < 12 || hour > 23) ? "AM" : "PM";
	if (hour == 0) hour = 12;
	if (hour > 12) hour -=12;
	if (min == 0)
		snprintf(pretty, BUF_LIMIT_SMALL, "%d %s",
			hour, c);
	else
		snprintf(pretty, BUF_LIMIT_SMALL, "%d:%2d %s",
			hour, (int)(min * 60), c);
	pretty[BUF_LIMIT_SMALL] = '\0';
	return DSO_RETURNS_SUCCESS(pretty);
}

/*
	dso_fetchTimestamp()
		return unix timestamp w/microseconds
 */
char	*dso_fetchTimestamp(char *in)
{
	struct timeval  now;
	char	buf[BUF_LIMIT_SMALL + 1];

	gettimeofday(&now, (void *)NULL);

	snprintf(buf, BUF_LIMIT_SMALL, "%ld.%06ld",
		now.tv_sec, (long)now.tv_usec);
	buf[BUF_LIMIT_SMALL] = '\0';

	return DSO_RETURNS_SUCCESS(buf);
}

/*
	dso_prettySeconds()
			1 year 212 days 19:53:42
			254 days 23:55:00
			10:23
			23 minutes
			10 seconds
		`in' assumed to be in SECONDS
 */
char	*dso_prettySeconds(char *in)
{
	return DSO_RETURNS_SUCCESS(prettySeconds(atol(in)));
}

/*
	dso_prettySecondsTerse()
		outputs of the form:
			1 year, 5 days
			56 days, 10 hours
			10 hours, 23 minutes
			23 minutes, 10 seconds
			10 seconds
		`in' assumed to be in SECONDS
 */
char	*dso_prettySecondsTerse(char *in)
{
	return DSO_RETURNS_SUCCESS(prettySecondsTerse(atol(in)));
}

/*
	dso_prettySecondsTerser()
		outputs of the form:
			1 year
			56 days
			10 hours
			23 minutes
			10 seconds
		`in' assumed to be in SECONDS
 */
char	*dso_prettySecondsTerser(char *in)
{
	char	out[BUF_LIMIT_SMALL + 1];
	long	tm = atol(in);
	int		secs = tm % 60,
			mins = (tm / 60) % 60,
			hours = (tm / 3600) % 24,
			days = ((tm / 3600) / 24) % 365,
			years = (tm / 31536000);

	if (years > 0) {
		snprintf(out, BUF_LIMIT_SMALL, "%d year%s",
			years, (years > 1) ? "s" : "");
		return DSO_RETURNS_SUCCESS(out);
	}

	if (days > 0) {
		snprintf(out, BUF_LIMIT_SMALL, "%d day%s",
			days, (days > 1) ? "s" : "");
		return DSO_RETURNS_SUCCESS(out);
	}

	if (hours > 0) {
		snprintf(out, BUF_LIMIT_SMALL, "%d hour%s",
			hours,
			(hours > 1 ? "s" : ""));
		return DSO_RETURNS_SUCCESS(out);
	}

	if (mins > 0) {
		snprintf(out, BUF_LIMIT_SMALL, "%d minute%s",
			mins,
			(mins > 1 ? "s" : ""));
		return DSO_RETURNS_SUCCESS(out);
	} else if (secs > 0) {
		snprintf(out, BUF_LIMIT_SMALL, "%d second%s",
			secs,
			(secs > 1 ? "s" : ""));
		return DSO_RETURNS_SUCCESS(out);
	} else
		strcpy(out, "now");

	return DSO_RETURNS_SUCCESS(out);
}

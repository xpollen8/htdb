#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

extern int evaluate(char *line, double *val);

/*
	dso_eval()
		where `token' is a string representation of
		an arithmetic expression, return `token'
		evaluated into a numeric value.
 */
char	*dso_eval(char *in)
{
	double	evaluated;
	char	out[BUF_LIMIT_SMALL + 1];

	if (evaluate(in, &evaluated) == 0) {
		if (evaluated == (int)evaluated)
			snprintf(out, BUF_LIMIT_SMALL, "%d", (int)evaluated);
		else
			snprintf(out, BUF_LIMIT_SMALL, "%g", evaluated);

		out[BUF_LIMIT_SMALL] = '\0';
		return DSO_RETURNS_SUCCESS(out);
	} else
		snprintf(out, BUF_LIMIT_SMALL, "Syntax error within: (%s)", in);

	out[BUF_LIMIT_SMALL] = '\0';
	return DSO_RETURNS_SUCCESS_CHALLENGED(out);
}

char	*dso_min(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 0);

	if (num_args >= 2) {
		char	ret[BUF_LIMIT_SMALL + 1];
		int		i,
				min_index = 0;
		bool_t	isString = BOOL_FALSE;

		for (i = 0 ; BOOL_FALSE == isString && i < num_args ; i++) {
			/*
				is this argument non-numeric?
				if so, we'll user string context for all comparisons
			 */
			if (!(isdigit((int)args[i][0]) || args[i][0] == '-')) {
				isString = BOOL_TRUE;
			}
		}
			
		if (BOOL_FALSE == isString) {
			/*
				numeric context
			 */
			for (i = 1 ; i < num_args ; i++) {
				if (atof(args[i]) < atof(args[min_index])) {
					min_index = i;
				}
			}
		} else {
			/*
				string context
			 */
			for (i = 1 ; i < num_args ; i++) {
				if (strcmp(args[i], args[min_index]) < 0) {
					min_index = i;
				}
			}
		}
		sstrncpy(ret, args[min_index]);
		ret[BUF_LIMIT_SMALL] = '\0';
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS(ret);
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS(in);
	}
}

char	*dso_max(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 0);

	if (num_args >= 2) {
		char	ret[BUF_LIMIT_SMALL + 1];
		int		i,
				min_index = 0;
		bool_t	isString = BOOL_FALSE;

		for (i = 0 ; BOOL_FALSE == isString && i < num_args ; i++) {
			/*
				is this argument non-numeric?
				if so, we'll user string context for all comparisons
			 */
			if (!(isdigit((int)args[i][0]) || args[i][0] == '-')) {
				isString = BOOL_TRUE;
			}
		}
			
		if (BOOL_FALSE == isString) {
			/*
				numeric context
			 */
			for (i = 1 ; i < num_args ; i++) {
				if (atof(args[i]) > atof(args[min_index])) {
					min_index = i;
				}
			}
		} else {
			/*
				string context
			 */
			for (i = 1 ; i < num_args ; i++) {
				if (strcmp(args[i], args[min_index]) > 0) {
					min_index = i;
				}
			}
		}
		sstrncpy(ret, args[min_index]);
		ret[BUF_LIMIT_SMALL] = '\0';
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS(ret);
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS(in);
	}
}

#define	SYNTAX_AVG	"usage: \\$\\{avg(value [, value [,value...]])\\}"

char	*dso_avg(char *in)
{
	char	**args;
	int		i,
			num_args = htdb_splitArgs(in, ",", &args, 0);
	double	sum = 0.0,
			avg = 0.0;
	char	ret[BUF_LIMIT_SMALL + 1];
	bool_t	allInt = BOOL_TRUE;

	for (i = 0 ; i < num_args ; i++) {
		sum += atof(args[i]);
		if (strchr(args[i], '.'))
			allInt = BOOL_FALSE;
	}
	htdb_deleteArgs(args, num_args);

	if (num_args > 0)
		avg = sum / num_args;

	snprintf(ret, BUF_LIMIT_SMALL, (BOOL_FALSE == allInt ? "%.2f" : "%.0f"), avg);

	return DSO_RETURNS_SUCCESS(ret);
}

char	*dso_abs(char *in)
{
	char	out[BUF_LIMIT_SMALL + 1];
	snprintf(out, BUF_LIMIT_SMALL, "%ld", labs(atol(in)));
	out[BUF_LIMIT_SMALL] = '\0';
	return DSO_RETURNS_SUCCESS(out);
}

/*
	dso_signed()
		output `+' or `-' on single integer value
 */
char	*dso_signed(char *in)
{
	char	out[BUF_LIMIT_SMALL + 1];

	if (in && *in) {
		if (*in == '-')
			sstrncpy(out, in);
		else
			snprintf(out, BUF_LIMIT_SMALL, "+%s", in);
	} else
		sstrncpy(out, "");
	out[BUF_LIMIT_SMALL] = '\0';
	return DSO_RETURNS_SUCCESS(out);
}

/*
	dso_random()
		where `token' is a string representation of
		a number, return a random number modulo
		the integer value of `token'.
 */
char	*dso_random(char *token)
{
	char	ret[BUF_LIMIT_SMALL + 1],
			*expanded = htdb_expand(token);

	snprintf(ret, BUF_LIMIT_SMALL, "%ld", htdb_random(atol(expanded)));
	ret[BUF_LIMIT_SMALL] = '\0';
	free(expanded);

	return DSO_RETURNS_SUCCESS(ret);
}

/*
	dso_padZero()
		if a number is < 10, then represent it as "03"
 */
char	*dso_padZero(char *in)
{
	char	out[BUF_LIMIT_SMALL + 1];

	snprintf(out, BUF_LIMIT_SMALL, "%02d", atoi(in));
	out[BUF_LIMIT_SMALL] = '\0';

	return DSO_RETURNS_SUCCESS(out);
}

/*
	dso_hex2base10()
		given a hex number as input, output a base-10 number
 */
char	*dso_hex2base10(char *in)
{
	char	out[BUF_LIMIT_SMALL + 1];
	snprintf(out, BUF_LIMIT_SMALL, "%ld", strtol(in, (char **)NULL, 16));
	out[BUF_LIMIT_SMALL] = '\0';
	return DSO_RETURNS_SUCCESS(out);
}

/*
	dso_hex2base10()
		returns hex of the input
 */
char	*dso_num2hex(char *in)
{
	char	out[BUF_LIMIT_SMALL + 1];
	snprintf(out, BUF_LIMIT_SMALL, "%x", atoi(in));
	out[BUF_LIMIT_SMALL] = '\0';
	return DSO_RETURNS_SUCCESS(out);
}

/*
	dso_prettyNumber()
		1        -> 1
		100      -> 100
		1000     -> 1,000
		1234567  -> 1,234,567
		etc
 */
char	*dso_prettyNumber(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_prettyNumber(in));
}

char	*dso_ordinalSuffix(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_ordinalSuffix(atoi(in)));
}

/*
	dso_int()
		returns the integer value of the string
 */
char	*dso_int(char *in)
{
	char	buh[BUF_LIMIT_SMALL + 1];
	snprintf(buh, BUF_LIMIT_SMALL, "%ld", atol(in));
	buh[BUF_LIMIT_SMALL] = '\0';
	return DSO_RETURNS_SUCCESS(buh);
}

char	*dso_md5base64(char *in)
{
	char b64_md5[320];
	{
		unsigned char from_md5[32];
		memset(from_md5, 0, 32);
		memset(b64_md5, 0, 32);
		md5_digest(in, from_md5, 0, strlen(in));
		base64_encode((const char *)from_md5, 16, b64_md5);
	}
	return DSO_RETURNS_SUCCESS(b64_md5);
}

/*
	dso_ntoa()
		dottted-quad representation of integer input
 */
char	*dso_ntoa(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_ntoa(in));
}

/*
	dso_aton()
		decoding of dottted-quad representation of network address
 */
char	*dso_aton(char *in)
{
	return DSO_RETURNS_SUCCESS(htdb_aton(in));
}

#define	SYNTAX_PARSEFLOAT	"usage: parseFloat(float, precision)"

char	*dso_parseFloat(char *in)
{
	char	buf[BUF_LIMIT_SMALL + 1],
			**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 2);

	if (num_args != 2) {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_PARSEFLOAT);
	}

	snprintf(buf, BUF_LIMIT_SMALL, "%.*f",  atoi(args[1]), atof(args[0]));

	htdb_deleteArgs(args, num_args);
	return DSO_RETURNS_SUCCESS(buf);
}


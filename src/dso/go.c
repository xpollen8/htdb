#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

#define	SYNTAX_goLimited		"usage: \\$\\{goLimited(count, url)\\}"

/*
	dso_goLimited()
		inserts a row into the go table
		with limiting characteristics
 */
char	*dso_goLimited(char *in)
{
	char	*comma = strchr(in, ',');
	if (comma && *comma) {
		char	*key = genGoKey(),
				*server = htdb_script("serverHTTP"),
				*ptr,
				*dest;
		int	maxcount;

		*comma = '\0';

		if ( !key )
			return DSO_RETURNS_SUCCESS_CHALLENGED("");

		maxcount = atoi(in);

		dest = strstr(comma + 1, "http");
		ptr = strstr(dest, server);

		if (ptr && *ptr)
			dest += strlen(server);

		dbInsert("insert into go set "
			"go_id=NULL, "
			"gokey='%s', "
			"dest='%s', "
			"count=0, "
			"maxcount=%d, "
			"lastused=now(), "
			"startdate='', "
			"stopdate='', "
			"isdeleted='F'",
			key, dest, maxcount);

		free(server);

		return DSO_RETURNS_SUCCESS_ASIS(key);
	} else
		return DSO_RETURNS_SUCCESS_CHALLENGED(SYNTAX_goLimited);
}

/*
	dso_goUnlimited()
		inserts a row into the go table
		with unlimited characteristics
 */
char	*dso_goUnlimited(char *in)
{
	char	*key = genGoKey(),
			*server = htdb_script("serverHTTP"),
			*ptr = strstr(in, server);

	if ( !key )
		return DSO_RETURNS_SUCCESS_CHALLENGED("");

	if (ptr && *ptr)
		in += strlen(server);

	dbInsert("insert into go set "
		"go_id=NULL, "
		"gokey='%s', "
		"dest='%s', "
		"count=0, "
		"maxcount=0, "
		"lastused=now(), "
		"startdate='', "
		"stopdate='', "
		"isdeleted='F'",
		key, in);

	free(server);

	return DSO_RETURNS_SUCCESS_ASIS(key);
}

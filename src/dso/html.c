#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"htdb.h"

/*
	dso_stripHTML()
		turns angle brackets into HTML-printable equivalents
 */
char	*dso_stripHTML(char *in)
{
	int	in_tag = 0;

	spaceData	b;

	if ( !strchr(in, '<') )
		return DSO_RETURNS_SUCCESS(in);

	data_init(&b);

	while (in && *in) {
		if (*in == '<') {
			if ((in - 1) && *(in - 1) == '\\') {
				thing_appendChar(&b, *in, BUF_LIMIT_BIG);
				in_tag = 1;
			} else {
				/*
					all others get converted into
					a displayable equivalent
				 */
				thing_append(&b, "&lt;", 4, BUF_LIMIT_BIG);
			}
		} else if (*in == '>') {
			if (in_tag) {
				thing_appendChar(&b, *in, BUF_LIMIT_BIG);
				in_tag = 0;
			} else {
				if ((in - 1) && *(in - 1) == '\\')
					thing_appendChar(&b, *in, BUF_LIMIT_BIG);
				else
					/*
						all others get converted into
						a displayable equivalent
					 */
					thing_append(&b, "&gt;", 4, BUF_LIMIT_BIG);
			}
		} else
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
		in++;
	}

	return DSO_RETURNS_SUCCESS_ASIS(b.value);
}

/*
	dso_filterHTML()
		only allows certain HTML tags through, converts
		others into HTML-printable versions.
 */
char	*dso_filterHTML(char *in)
{
	int	in_tag = 0,
		cnt = 0;

	spaceData	b;

	data_init(&b);

	while (in && *in) {
		if (isspace((int)*in))
			cnt = 0;

		if (++cnt < 35) {
			if (*in == '<') {
				if ((in - 1) && *(in - 1) == '\\')
					thing_appendChar(&b, *in, BUF_LIMIT_BIG);
				else {
					char	*gottag = strdup("");
					/*
						we will only let the following subset
						of HTML pass by..
					 */
					if (isTag("p", in, &gottag) ||
						isTag("font", in, &gottag) ||
						isTag("/font", in, &gottag) ||
						isTag("strong", in, &gottag) ||
						isTag("/strong", in, &gottag) ||
						isTag("em", in, &gottag) ||
						isTag("/em", in, &gottag) ||
						isTag("h1", in, &gottag) ||
						isTag("/h1", in, &gottag) ||
						isTag("h2", in, &gottag) ||
						isTag("/h2", in, &gottag) ||
						isTag("h3", in, &gottag) ||
						isTag("/h3", in, &gottag) ||
						isTag("u", in, &gottag) ||
						isTag("/u", in, &gottag) ||
						isTag("/p", in, &gottag) ||
						isTag("br", in, &gottag) ||
						isTag("/b", in, &gottag) ||
						isTag("b", in, &gottag) ||
						isTag("ol", in, &gottag) ||
						isTag("/ol", in, &gottag) ||
						isTag("ul", in, &gottag) ||
						isTag("/ul", in, &gottag) ||
						isTag("li", in, &gottag) ||
						isTag("/li", in, &gottag) ||
						isTag("/i", in, &gottag) ||
						isTag("i", in, &gottag) ||
						isTag("hr", in, &gottag)) {
						int	len = strlen(gottag);
						in_tag = 1;
						thing_appendChar(&b, '<', BUF_LIMIT_BIG);
						thing_append(&b, gottag, len, BUF_LIMIT_BIG);
						in += len;
					} else {
						/*
							all others get converted into
							a displayable equivalent
						 */
						thing_append(&b, "&lt;", 4, BUF_LIMIT_BIG);
					}
					free(gottag);
				}
			} else if (*in == '>') {
				if (in_tag) {
					thing_appendChar(&b, *in, BUF_LIMIT_BIG);
					in_tag = 0;
				} else {
					if ((in - 1) && *(in - 1) == '\\')
						thing_appendChar(&b, *in, BUF_LIMIT_BIG);
					else {
						thing_append(&b, "&gt;", 4, BUF_LIMIT_BIG);
					}
				}
			} else
				thing_appendChar(&b, *in, BUF_LIMIT_BIG);
		}
		in++;
	}

	return DSO_RETURNS_SUCCESS_ASIS(b.value);
}

/*
	dso_smartHREF()
		given a string with at least one `.', add "http://" if necessary
		and output the result
 */
char	*dso_smartHREF(char *in)
{
	if (strchr(in, '.') && !strchr(in, '@')) {
		/*
			can't be a URL without at least a fricking DOT, now can it?
		 */
		char	*url = (char *)malloc(sizeof(char) * (strlen(in) + 30));
		if (strncasecmp(in, "http://", 7) == 0)
			strcpy(url, in);
		else
			sprintf(url, "http://%s", in);
		
		return DSO_RETURNS_SUCCESS_ASIS(url);
	} else
		return DSO_RETURNS_SUCCESS(in);
}

/*
	dso_smartURL()
		turn the incoming string into an HREF-able item.
		trys to work with URLS and mailtos.
 */
char	*dso_smartURL(char *in)
{
	if (strchr(in, '.') && !strchr(in, '@')) {
		/*
			can't be a URL without at least a fricking DOT, now can it?
		 */
		char	*url = (char *)malloc(sizeof(char) * (strlen(in) + 30));
		if (strncasecmp(in, "http://", 7) == 0)
			strcpy(url, in);
		else
			sprintf(url, "http://%s", in);
		{
			char	*out = (char *)malloc(sizeof(char) * ((strlen(url) * 2) + 30)),
					*displayurl = strdup(url);
			if (strlen(displayurl) > 50) {
				displayurl[47] = '\0';
				strcat(displayurl, "...");
			}
			sprintf(out, "<a href=\"%s\" target=\"new\">%s</a>", url, displayurl);
			free(url);
			free(displayurl);
			return DSO_RETURNS_SUCCESS_ASIS(out);
		}
	} else if (BOOL_TRUE == isValidEmailAddress(in)) {
		/*
			looks like an email address.
			make a mailto link
		 */
		char	*out = (char *)malloc(sizeof(char) * ((strlen(in) * 2) + 30)),
				*displayurl = strdup(in);
		if (strlen(displayurl) > 50) {
			displayurl[47] = '\0';
			strcat(displayurl, "...");
		}
		sprintf(out, "<a href=\"mailto:%s\">%s</a>", in, displayurl);
		free(displayurl);
		return DSO_RETURNS_SUCCESS_ASIS(out);
	} else
		return DSO_RETURNS_SUCCESS(in);
}

char	*dso_formSafe(char *in)
{
	char	*out = strdup(in),
			*ptr = &out[0];
	while (ptr && *ptr) {
		if (*ptr == '\n' || *ptr == '\r')
			*ptr = ' ';
		else if (*ptr == '"')
			*ptr = '\'';
		ptr++;
	}
	return DSO_RETURNS_SUCCESS_ASIS(out);
}

/*
	dso_truncEmail()
		filters `XX@YYY.COM' -> `XXX@...'
 */
char	*dso_truncEmail(char *in)
{
	char	*dp = strdup(in),
			*at = strchr(dp, '@');

	if (at && *at) {
		*(at + 1) = '\0';
		strcat(dp, "...");
	}

	return DSO_RETURNS_SUCCESS_ASIS(dp);
}

/*
	dso_javascriptDocWrite()
		for each newline-terminated line in the input, output:
			document.write("XXXX");
		where `XXXX' is a line of the input
 */
char	*dso_javascriptDocWrite(char *in)
{
	spaceData	b;

	data_init(&b);

	thing_append(&b, "document.write(\"", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	while (in && *in) {
		if (*in == '\"')
			thing_appendChar(&b, '\'', BUF_LIMIT_BIG);
		else if (*in == '\n' || *in == '\r')
			thing_append(&b, " \\\\n\");\ndocument.write(\"", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		else if (*in != '\t')
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
		in++;
	}
	thing_append(&b, "\");\n", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);

	return DSO_RETURNS_SUCCESS_ASIS(b.value);
}

#define	SYNTAX_linkInternal	"usage: \\$\\{linkInternal(href [, text])\\}"

/*
	dso_linkInternal()
		turn the incoming value(s) into a simple HREF
 */
char	*dso_linkInternal(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 2);

	if (num_args == 2) {
		char	*href = args[0],
			*text = args[1],
			*buf = (char *)malloc(sizeof(char) *
				(strlen(href) + strlen(text) + 100));
		if (strstr(href, "@"))
			sprintf(buf, "<a href=\"mailto:%s\">%s</a>", href, text);
		else
			sprintf(buf, "<a href=\"%s\">%s</a>", href, text);
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_ASIS(buf);
	} if (num_args == 1) {
		char	*href = args[0],
			*text = args[0],
			*buf = (char *)malloc(sizeof(char) *
				(strlen(href) + strlen(text) + 100));
		if (strstr(href, "@"))
			sprintf(buf, "<a href=\"mailto:%s\">%s</a>", href, text);
		else
			sprintf(buf, "<a href=\"%s\">%s</a>", href, text);
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_ASIS(buf);
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS(SYNTAX_linkInternal);
	}
}

#define	SYNTAX_linkExternal	"usage: \\$\\{linkExternal(href [, text])\\}"

/*
	dso_linkExternal()
		turn the incoming values into a targeted HREF
 */
char	*dso_linkExternal(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 2);

	if (num_args == 2) {
		char	*href = args[0],
			*text = args[1],
			*buf = (char *)malloc(sizeof(char) *
				(strlen(href) + strlen(text) + 100));
		if (strstr(href, "@"))
			sprintf(buf, "<a href=\"mailto:%s\">%s</a>", href, text);
		else if (strstr(href, "://"))
			sprintf(buf, "<a href=\"%s\" target=\"new\">%s</a>", href, text);
		else
			sprintf(buf, "<a href=\"http://%s\" target=\"new\">%s</a>", href, text);
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_ASIS(buf);
	} if (num_args == 1) {
		char	*href = args[0],
			*text = args[0],
			*buf = (char *)malloc(sizeof(char) *
				(strlen(href) + strlen(text) + 100));
		if (strstr(href, "@"))
			sprintf(buf, "<a href=\"mailto:%s\">%s</a>", href, text);
		else if (strstr(href, "://"))
			sprintf(buf, "<a href=\"%s\" target=\"new\">%s</a>", href, text);
		else
			sprintf(buf, "<a href=\"http://%s\" target=\"new\">%s</a>", href, text);
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS_ASIS(buf);
	} else {
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS(SYNTAX_linkExternal);
	}
}

#define SYNTAX_linkAjax	"usage:\\$\\{linkAjax(url, text, target [, loading_action, complete_action]\\}"

/*
 	dso_linkAjax()
		populate div with an ajax request. 
*/
char	*dso_linkAjax(char *in)
{
	char **args;
	int	num_args=htdb_splitArgs(in, ",", &args, 5);

	char 	*url = args[0],
		*text = args[1],
		*target = args[2],
		*loading_action = "",
		*complete_action = "";

	if(num_args==5){
		loading_action = args[3];
		complete_action = args[4];
	} else if(num_args==4){
	       	loading_action = args[3];
	} else if(num_args != 3){
		htdb_deleteArgs(args, num_args);
		return DSO_RETURNS_SUCCESS(SYNTAX_linkAjax);
	}
	char	*buf = (char *)malloc(sizeof(char)*
			(strlen(url)+strlen(text)+strlen(target)+
			strlen(loading_action)+strlen(complete_action)+256));

	sprintf(buf, "<a href=\"%s\" onClick=\"new Ajax.Updater(\'%s\', \'%s\', {asynchronous:true, evalScripts:true, onComplete:function(request){%s}, onLoading:function(request){%s}}); return false;\">%s</a>", url, target, url, complete_action, loading_action, text);

	htdb_deleteArgs(args, num_args);
	return DSO_RETURNS_SUCCESS_ASIS(buf);
}

#define SYNTAX_formTagAjax "usage:\\$\\{formTagAjax(url, method, target [, loading_action, complete_action]\\}"

/*
 *         dso_formTag()
 *                         insert a <form> tag with ajax stuff in it..
 *                         
 */
char    *dso_formTagAjax(char *in)
{
        char **args;
        int     num_args=htdb_splitArgs(in, ",", &args, 5);

        char 	*url = args[0],
		*method = args[1],
		*target = args[2],
		*loading_action = "",
		*complete_action = "";

        if(num_args == 5){
                loading_action = args[3];
                complete_action = args[4];
        } else if(num_args == 4){
                loading_action = args[3];
        } else if(num_args != 3){
                htdb_deleteArgs(args, num_args);
                return DSO_RETURNS_SUCCESS(SYNTAX_formTagAjax);
        }
        char    *buf = (char *)malloc(sizeof(char)*
	               (strlen(url)+strlen(method)+strlen(target)+
		       strlen(loading_action)+strlen(complete_action)+256));
		       
	sprintf(buf, "<form  action=\"%s\" method=\"%s\" onSubmit=\"new Ajax.Updater(\'%s\', \'%s\', {asynchronous:true, parameters:Form.serialize(this), onComplete:function(request){%s}, onLoading:function(request){%s}}); return false;\">", url, method, target, url, complete_action, loading_action);

	htdb_deleteArgs(args, num_args);
	return DSO_RETURNS_SUCCESS_ASIS(buf);
}



/*
	dso_codeify()
		add HTML line breaks and whitespace to the incoming string
 */
char	*dso_codeify(char *in)
{
	spaceData	b;
	data_init(&b);

	while (in && *in) {
		if (*in == '\n')
			thing_append(&b, "<br/>\n", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		else if (*in == '&')
			thing_append(&b, "&amp;", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		else if (*in == '\t')
			thing_append(&b, "&nbsp;", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		else
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
		in++;
	}
	return DSO_RETURNS_SUCCESS_ASIS(b.value);
}

/*
	dso_makeHTML()
		add HTML line breaks and whitespace to the incoming string
 */
char	*dso_makeHTML(char *in)
{
	spaceData	b;
	data_init(&b);

	while (in && *in) {
		if (*in == '\n')
			thing_append(&b, "<br/>\n", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		else if (*in == '\t')
			thing_append(&b, "&nbsp;", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		else
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
		in++;
	}
	return DSO_RETURNS_SUCCESS_ASIS(b.value);
}

/*
	dso_safety()
		poorly-named function.
		turns HTDB-sensitive values into HTML-printable equivalents
 */
char	*dso_safety(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(htdb_htdb2html(in));
}

/*
	dso_encode()
		URL encoding
 */
char	*dso_encode(char *source)
{
	return DSO_RETURNS_SUCCESS_ASIS(encode(source));
}

char	*dso_unescape(char *in)
{
	char	*out = strdup(in);
	uncgi_url_unescape((unsigned char *)out);
	return DSO_RETURNS_SUCCESS_ASIS(out);
}

char	*dso_escape(char *in)
{
	return DSO_RETURNS_SUCCESS_ASIS(url_escape(in));
}

char	*dso_stripTags(char *in)
{
	int	intag = 0,
		inexp = 0;
	spaceData	b;
	char	*filtered = htdb_unfilter_unsafe(in),
			*expanded = htdb_expand(filtered),
			*hold = &expanded[0];

	free(filtered);

	data_init(&b);

	while (expanded && *expanded) {
		if (intag && *expanded == '>') {
			intag = 0;
		} else if (!intag && *expanded == '<') {
			intag = 1;
		} else if (inexp > 0 && *expanded == '}') {
			inexp--;
		} else if (*expanded == '$' && (expanded + 1) && *(expanded + 1) && *(expanded + 1) == '{') {
			inexp++;
		} else if (0 == intag && 0 == inexp) {
			thing_appendChar(&b, *expanded, BUF_LIMIT_BIG);
		}
		expanded++;
	}
	free(hold);

	return DSO_RETURNS_SUCCESS_ASIS(b.value);
}

/*	
	routines for entering and exiting secure server mode; 
	assumes the same server name,
	ie http://www.htdb.org and https://www.htdb.org
*/
char	*dso_enterSecure(char *in)
{
	char buf[BUF_LIMIT_BIG + 1]; 
	
	if (strncasecmp(in, "http", 4) != 0)
		return DSO_RETURNS_SUCCESS_CHALLENGED("");
	
	if (tolower(in[4]) == 's')	/* already in secure mode */
		return DSO_RETURNS_SUCCESS(in);

	snprintf(buf, BUF_LIMIT_BIG, "https%s", in + 4);
	return DSO_RETURNS_SUCCESS(buf);
}

char	*dso_exitSecure(char *in)
{
	char buf[BUF_LIMIT_BIG + 1]; 
	
	if (strncasecmp(in, "http", 4) != 0)
		return DSO_RETURNS_SUCCESS_CHALLENGED("");
	
	if (tolower(in[4]) == ':')	/* already in insecure mode */
		return DSO_RETURNS_SUCCESS(in);

	snprintf(buf, BUF_LIMIT_BIG, "http%s", in + 5);
	return DSO_RETURNS_SUCCESS(buf);
}



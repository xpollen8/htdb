#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#define	OUR_BUFSIZ			(1024 * 50)
#define	ONE_BIGASSED_FILE	(15 * 1024 * 1024)

#include	"libhtdb.h"

/* form-based file upload in HTML: RFC 1867 */

/* platform strings */
#define	EUNUCHS	"X11"
#define	MACINIZ	"Mac"
#define	WINDOZE	"Win"

#define	TMPDIR	"/tmp"

#undef DEBUG

static char loadir[PATH_MAX + 1] = "";
static char errstr[PATH_MAX + 1] = "";
static char platform[PATH_MAX + 1] = "";
static char *crlf = "\r\n";
static char *marker = "--";
static char *defeol = "\n";
static unsigned total = 0;
static int loaderr = 0;

static LinkedList nvlist = NULL, finfolist = NULL;

static int doForm(char *begin);

/* opacity assumes that name string is first component of structure... */
static int	comp(void *v1, void *v2)
{
	return(strcmp((char *)v1, (char *)v2));
}

static void	mkTempDir(char *dirname)
{
	sprintf(dirname, "%s/upLoad%05d%ld/", TMPDIR, (int)getpid(), (long)time(NULL));
}

static char	*getValue(char *dest, char *src, char *lead, char *tail)
{
	char *ptr;

	if (dest == NULL || src == NULL || lead == NULL) {
#ifdef DEBUG
		log_info("upload", "getValue(NULL argument(s))");
#endif
		return(NULL);
	}

	if ((ptr = strstr(src, lead)) == NULL) {
#ifdef DEBUG
		log_info("upload", "getValue('%s' not in '%s')", lead, src);
#endif
		return(NULL);
	}

	strcpy(dest, ptr + strlen(lead));

	if (tail != NULL && (ptr = strpbrk(dest, tail)) != NULL) {
		*ptr = '\0';
	}

#ifdef DEBUG
	log_info("upload", "getValue returning(%s)", dest);
#endif
	if (dest && *dest)
		return(dest);
	else
		return NULL;
}

static int	getLine(FILE *fp, char buf[], int bufsiz, char *eol)
{
	static char *overflow = NULL;
	static int overcnt = 0;
	register char *ptr = buf, *delim = eol;
	register int c;

	/* because this routine is supposed to detect multipart boundaries */
	/* it can save up partial boundary markers from previous invocations */
	/* that need to be restarted here */
	if (overflow != NULL) {
		char *old = overflow;

		while (overcnt-- > 0)
			*ptr++ = *old++;

		free(overflow);
		overflow = NULL;
	}

	/* read the data one char at a time, looking for boundary markers */
	/* we save room in the buffer for the boundary marker partials, but don't put */
	/* them into the buffer until we can definately tell they are not markers */
	while (ptr + (delim - eol) < &buf[bufsiz]) {
		c = getc(fp);
		if (feof(fp)) {
			break;
		} else if (c == *delim) {		/* are we starting */
			if (*(++delim) == '\0') {	/* or ending a boundary seq */
				total += strlen(eol);
				break;
			}
		} else {
			if (delim != eol) {				/* we thought we had a boundary, but were wrong */
				char *last = eol;				/* copy the partial data into output buf */
				while (last != delim)
					*ptr++ = *last++;
				delim = eol;
	
				/* is this new char, yet the begining of a NEW boundary sequence? */
				if (c == *delim) {
					delim++;
					continue;
				}
			}
			/* whew! just data */
			*ptr++ = c;
		}
	} 

	/* We are here because we hit EOF, or the buffer filled up */ 
	/* Either way there's no room to read more in the current buffer, but */
	/* enough room for the partial boundary we may be reading, just not the rest of it */
	if (delim > eol && *delim != '\0') { 	/* part check in progress */
		char *tmp;

		if ((tmp = malloc(strlen(delim))) == NULL) {
			log_error("upload", "overflow malloc failure (%s)", strerror(errno));
			return -1;
		}

		overcnt = fread(tmp, 1, strlen(delim), fp); 
		
		if (overcnt <= 0) {	/* odd, but we are done */
			/* move the partial boundary marker into the output buf, remember we've been */
			/* saving room for it */
			strncpy(ptr, eol, delim - eol);
			ptr += delim - eol;
			free(tmp);
		} else if (strcmp(delim, tmp) == 0) {	/* real end of buffer */
			/* nothing to do but add to total bytes read in content length. */
			total += strlen(eol);
			free(tmp);
		} else {	/* partial data not boundary marker */
			/* fill up existing buffer the rest of the way, */
			/* save the rest for the next invocation */
			strncpy(ptr, eol, delim - eol);
			ptr += delim - eol;	
		 
			overflow = tmp;
		}
	}

	if (ptr < &buf[bufsiz]) {	/* null terminate if not end of buf */
		*ptr = '\0';
	}

#ifdef DEBUG
	log_info("upload", "getLine(%d bytes)", ptr - buf);
	if ((ptr - buf) != bufsiz) {
		char dbuf[1024];
		char *dbufp = dbuf;
		int i;

		log_info("upload", "boundary marker (%s)", eol);

		for (i=-8; i < 1; i++)
			dbufp += sprintf(dbufp, "|%02x", *(ptr + i));
		sprintf(dbufp, "|");
		log_info("upload", "last 8 bytes (%s)", dbuf);
		log_info("upload", "overflowcnt (%d)", overcnt);
	}
#endif

	if (feof(fp) && ptr == buf)
		return(EOF);
	else
		return(ptr - buf);
}

static int	doFile(char *dirname, char *filename, char *fieldname, char *boundary)
{
	static const char deftype[] = "text";
	char buf[OUR_BUFSIZ], path[PATH_MAX + 1], cliff[PATH_MAX + 1];
	FILE *fp;
	int count, textlines = 0;
	register fileinfo *finfo;

	if ((finfo = calloc(1, sizeof(fileinfo))) == NULL) {
		snprintf(errstr, PATH_MAX, "%s", strerror(errno));
		return(-1);
	}

	/* Windoze client sends full pathname, including drive letter! */
	if (strncmp(platform, WINDOZE, strlen(WINDOZE)) == 0) {
		char *ptr = strrchr(filename, '\\');

		if (ptr)
			strcpy(finfo->name, (char *)makeFileSystemSafe(++ptr));
		else
			strcpy(finfo->name, (char *)makeFileSystemSafe(filename));
	} else {
		strcpy(finfo->name, (char *)makeFileSystemSafe(filename));
	}

	strcpy(finfo->type, deftype); /* default type may not be specified */
	strcpy(finfo->field, fieldname);
	finfo->size = finfo->status = 0;
	if (finfolist) {
		(void) ll_add(finfolist, finfo, comp);
	}

	/*
	 * at this point, input format is:
	 *	Content headers (optional)
	 *	CRLF
	 *	file data (which may contain CRLFs!)
	 *	CRLF
	 *	--boundary string (may have trailing '--' if last file)
	 */

	sprintf(cliff, "%s%s", crlf, boundary);

	/* skip over leading Content headers and CRLF (count == 0) */
	while ((count = getLine(stdin, buf, sizeof(buf), crlf)) > 0) {
		static	char typetok[] = "Content-Type: ";

		total += count;

#ifdef DEBUG
		log_info("upload", "%s", buf);
#endif
		if (strncasecmp(buf, typetok, sizeof(typetok) - 1) == 0) {
			static	char mpartok[] = "multipart/mixed";

			(void) getValue(finfo->type, buf, ": ", NULL);
#ifdef DEBUG
			log_info("upload", "%s: %s", buf, finfo->type);
#endif

			/*
			 * NOT TESTED!
			 * haven't found clients which use this mechanism
			 */
			if (strncmp(finfo->type, mpartok, sizeof(mpartok) - 1) == 0) {
				loaderr = doForm(buf);
			}
		}
	}

	if (strlen(dirname) == 0) {
		mkTempDir(dirname);
	} else if (*dirname != '/') {
		char temp[PATH_MAX + 1];
		strcpy(temp, dirname);

		sprintf(dirname, "%s%s", getenv("DOCUMENT_ROOT"), temp);
	}

	if (mkdir(dirname, S_IRWXU | S_IRWXG | S_IRWXO) == -1) {
#ifdef DEBUG
		log_error("upload", "mkdir(%s): %s", dirname, strerror(errno));
#endif
		if (errno != EEXIST) {
			sprintf(errstr, "couldn't create directory %s: %s",
				dirname, strerror(errno));
			finfo->status = errno;
			return(-1);
		}
	}

	sprintf(path, "%s/%s", dirname, finfo->name);

	if ((fp = fopen(path, "w+")) == NULL) {
#ifdef DEBUG
		log_error("upload", "%s: %s", path, strerror(errno));
#endif
		sprintf(errstr, "%s: %s", path, strerror(errno));
		finfo->status = errno;
		return(-1);
	}

#ifdef DEBUG
	log_info("upload", "doFile(%s)", path);
#endif
	{
		int_t	too_big = ONE_BIGASSED_FILE;

		if (htdb_getint("confUploadSizeLimit") > 0)
			/*
				cinfigurable upload size limit
			 */
			too_big = htdb_getint("confUploadSizeLimit");

		/*
			handle the file data, converting EOL on text data only
		 */
		while ((count = getLine(stdin, buf, sizeof(buf), cliff)) > 0) {
			finfo->size += count;

			if (fwrite(buf, count, 1, fp) != 1) {
				sprintf(errstr, "%s: %s", path, strerror(errno));
				finfo->status = errno;
				break;
			}

			/*
				EOF or are we too big?
			 */
			if ((count != sizeof(buf)) ||
				(finfo->size >= too_big))
				break;
		}
	}

	if (count == -1) {
		sprintf(errstr, "unexpected EOF: %s %s", path,
			strerror(errno));
		finfo->status = errno;
		return(-1);
	}

	fchmod(fileno(fp), S_IRWXU | S_IRWXG | S_IRWXO);

	(void) fclose(fp);

	total += finfo->size;	/* total already includes EOL strings */
	finfo->size += textlines * strlen(defeol);

#ifdef DEBUG
	log_info("upload", "%s has %d bytes", filename, finfo->size);
#endif

	return(0);
}

static namval	*addNamVal(char *name, char *value, int vallen)
{
	if (nvlist && name != NULL && *name && value != NULL && *value) {
		namval *n = calloc(1, sizeof(namval));
#ifdef DEBUG
		log_info("upload", "addNamVal(%s, %s, %d)", name, value, vallen);
#endif

		n->value = calloc(1, (vallen > 0) ? (vallen + 1) : 1);

		if (n) {
			strncpy(n->name, name, sizeof(n->name) - 1);
			n->name[sizeof(n->name) - 1] = '\0';
			if (value != NULL && vallen > 0) {
				strncpy(n->value, value, vallen);
				n->value[vallen] = '\0';
			} else {
				n->value[0] = '\0';
			}
			(void) ll_add(nvlist, n, comp);
		}

		return(n);
	} else {
		return(NULL);
	}
}

static int	doForm(char *begin)
{
	static	char dispostok[] = "Content-Disposition";
	static	char nametok[] = "name=\"", fnametok[] = "filename=\"";
	char	name[NAME_MAX] = "", filename[NAME_MAX] = "",
		buf[OUR_BUFSIZ], boundary[NAME_MAX], *val = NULL;
	int	count, error = 0, boundlen = 0, vallen = 0, vlenalloced = 0;

	if (getValue(buf, begin, "boundary=", NULL) == NULL) {
		sprintf(errstr, "couldn't find boundary in '%s'", begin);
		return(-1);
	}

	strcpy(boundary, marker);
	sstrcat(boundary, buf, NAME_MAX);
	boundlen = strlen(boundary);

#ifdef DEBUG
	log_info("upload", "Part boundary is '%s' [%d]", boundary, boundlen);
#endif

	while ((count = getLine(stdin, buf, sizeof(buf), crlf)) != EOF) {

		total += count;

		if (count == 0 || strcmp(buf, marker) == 0) {
			continue;
		}

		if (strncasecmp(buf, dispostok, sizeof(dispostok) - 1) == 0) {
#ifdef DEBUG
			log_info("upload", "%s", buf);
#endif
			(void) getValue(name, buf, nametok, "\"");

			if (getValue(filename, buf, fnametok, "\"") != NULL) {
				(void) addNamVal(name, filename,
					strlen(filename));

				if (strlen(filename) > 0) {
					error = doFile(loadir, filename, name,
						 boundary);
				}
			} else {
				if (val != NULL) {
#ifdef	DEBUG
					log_info("FREE", "(%s)", val);
#endif
					(void) free(val);
					val = (char *)NULL;
				}
				if ((val = calloc(1, OUR_BUFSIZ)) == NULL) {
					error = -1;
					break;
				}
				vallen = 0;
				vlenalloced = OUR_BUFSIZ;
			}
		} else if (strncmp(buf, boundary, boundlen) == 0) {
			if (val && *val) {
#ifdef	DEBUG
				log_info("htdb_setval", "(%s)(%s)", name, val ? val : "NULL");
#endif
				/*
					normal value..
				 */
				uncgi_process_name_value(name, val);
				if (htdb_checkval("upLoadDir"))
					/*
						bit messy, but `upLoadDir' *could* have come
						in during the last batch of (potentially)
						encrypted values.. (or, it *could* be the current
						value in cleartext..)	at any rate, if we find
						this in the environment, then we need to override
						where we store files.
					 */
					strcpy(loadir, htdb_getval("upLoadDir"));
			}
		} else {
			if ((vlenalloced - (vallen + count)) <= 0)
				val = realloc(val, vlenalloced += OUR_BUFSIZ);
				if (vallen > 0) {
					/* gratuitously add a space on later lines */
					sstrcat(val, " ", vlenalloced);
					vallen++;
			}
			strncat(val, buf, count);
#ifdef	DEBUG
			log_info("SETTING", "(%s)", val ? val : "NULL");
#endif
			vallen += count;
		}
	} 

	if (val != NULL) {
#ifdef	DEBUG
		log_info("FREE", "(%s)", val);
#endif
		(void) free(val);
		val = (char *)NULL;
	}
#ifdef DEBUG
	log_info("doForm","returns (%d)", error);
#endif

	return(error);
}

int	getUpLoadInfo(char *dir, LinkedList *nv, LinkedList *files, char *errstring)
{
	if (dir) {
		strcpy(dir, loadir);
	}

	if (nv) {
		*nv = nvlist;
	}

	if (files) {
		*files = finfolist;
	}

	if (errstring) {
		strcpy(errstring, errstr);
	}

	return(loaderr);
}

void	setUpLoadInfo(char *dir, LinkedList nv, LinkedList files)
{
	if (dir != NULL) {
		strncpy(loadir, dir, sizeof(loadir));
		loadir[sizeof(loadir) - 1] = '\0';
	} else {
		strcpy(loadir, "");
	}

	nvlist = nv;

	finfolist = files;
}

void	destroyUpLoadInfo(void)
{
	LLItem i;

	if (nvlist) {
		ll_iterate(nvlist);

		while ((i = ll_traverse(nvlist))) {
			namval *n = (namval *) i->value;
			ll_del(nvlist, i->value, comp);
			free(n->value);
			free(n);
		}

		ll_destroy(nvlist);
		nvlist = NULL;
	}

	if (finfolist) {
		ll_iterate(finfolist);

		while ((i = ll_traverse(finfolist))) {
			fileinfo *f = (fileinfo *) i->value;
			ll_del(finfolist, i->value, comp);
			free(f);
		}

		ll_destroy(finfolist);
		finfolist = NULL;
	}

	if (strlen(loadir) > 0) {
		removeDirectory(loadir);
	}
}

void	doUpLoad(void)
{
	char	*method = getenv("REQUEST_METHOD");
#ifdef DEBUG
	unsigned int length = atol(getenv("CONTENT_LENGTH"));
#endif

	loaderr = total = 0;
	strcpy(errstr, "success");

	if (strcasecmp(method, "POST") != 0) {
		sprintf(errstr, "expected: REQUEST_METHOD=POST, got %s",
			method); 
		loaderr = -1;
		return;
	}

	if (nvlist == NULL) {
		nvlist = ll_create();
	}

	if (finfolist == NULL) {
		finfolist = ll_create();
	}

	/* HTTP_USER_AGENT format is "Browser/Version (Operating System)" */
	(void) getValue(platform, getenv("HTTP_USER_AGENT"), "(", "; )");

	loaderr = doForm(getenv("CONTENT_TYPE"));

#ifdef DEBUG
	if (length != total) {
		log_info("doUpLoad", "expected: form length: %d, got %d",
			length, total); 
	}
#endif
}

#ifdef	TEST
#define	DEBUG

main()
{
#ifdef DEBUG
	LLItem i;

	setlinebuf(stdout);
	log_info("upload", "Content-Type: text/html\r\n\r\n");
#endif

	setUpLoadInfo("/tmp/upLoad", NULL, NULL);

	doUpLoad();

#ifdef DEBUG
	if (nvlist) {
		ll_iterate(nvlist);

		log_info("upload", "name-value pair list follows");
		while (i = ll_traverse(nvlist)) {
			namval *n = (namval *) i->value;
			log_info("upload", "%s=%s", n->name, n->value);
		}
	}

	if (finfolist) {
		ll_iterate(finfolist);

		log_info("upload", "file info list follows");
		while (i = ll_traverse(finfolist)) {
			fileinfo *f = (fileinfo *) i->value;
			log_info("upload", "%s(%s, %d bytes)",
				 f->name, f->type, f->size);
		}
	}

	log_info("upload", "upload to '%s' -> %s", loadir, errstr);
#endif

	destroyUpLoadInfo();
}
#endif /* TEST */

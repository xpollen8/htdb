#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/

/*
	originally swiped from Steven Grimm's "uncgi" code (circa 1994)
	and highly modified since.  here's the due anyway:
	*
	* Copyright 1994, Steven Grimm <koreth@midwinter.com>.
	*
	* Permission is granted to redistribute freely and use for any purpose,
	* commercial or private, so long as this copyright notice is retained
	* and the source code is included free of charge with any binary
	* distributions.
	*
 */
#include	"libhtdb.h"

static void uncgi_trace(char *name)
{
	/*
		store the name of the variable
	 */
	htdb_setarrayval("uncgi", htdb_getint("uncgi->numResults") + 1, name);
	/*
		increment the count of the number of GET/POST values
	 */
	htdb_setptrint("uncgi", "numResults", htdb_getptrint("uncgi", "numResults") + 1);
}

/*
	christ..
 */
static void	uncgi_pipe_unescape(unsigned char *str)
{
	unsigned char	*dest = str;

	if (str && *str) {
		while (str && *str) {
			if (str[0] == '%' && ishex(str[1]) && ishex(str[2]) &&
				str[1] == '7' && tolower((int)str[2]) == 'c') {
				/*
					convert `%7c' back into `|' characters.
				 */
				dest[0] = (unsigned char)uncgi_htoi(str + 1);
				str += 2;
			} else
				dest[0] = str[0];
			str++;
			dest++;
		}

		dest[0] = '\0';
	}
}

/*
	uncgi_error()
		print the start of an error message.
 */
static void	uncgi_error(char *mesg)
{
	htdb_httpHeader("Status: 400");

	printf("Content-type: text/html\n\n");

	puts("<html><head><title>Error!</title></head><body>");
	puts("<h1>An error has occurred while processing your request.</h1>");
	puts("<p>\n");
	puts(mesg);
	puts("</body></html>");
	exit(1);
}

static void _uncgi_process_name_value(char *name, char *value)
{
	/*
		clear off leading whitespace
	 */
	while (isspace((int)*value))
		value++;

	/*
		clear off trailing whitespace
	 */
	{
		int	len = strlen(value);

		while (len > 0 && isspace((int)value[len - 1]))
			value[--len] = '\0';

		if (len <= 0) {
			/*
				no value found - ignore the request
			 */
			return;
		}
	}

	{
		/*
			graphical submit buttons send up <value>.y
			and <value>.x - since we are only interested
			in <value>, toss the .x and .y bits.
		 */
		char	*graphic_button = strstr(name, ".x");
		if (graphic_button && *graphic_button)
			*graphic_button = '\0';
		graphic_button = strstr(name, ".y");
		if (graphic_button && *graphic_button)
			*graphic_button = '\0';
	}
	if (*name == '*') {
		if (*value == '|') {
			char	*jnk = htdb_decryptor(value);
			if (jnk && *jnk && strchr(jnk, '='))
				/*
					we will only accept this
					unencrypted string if it
					contains an `=' character.
				 */
				uncgi_scanquery(jnk);
			free(jnk);
		} else if (value && strchr(value, '=')) {
			char	*jnk = strdup(value);
			/*
				we will only accept this
				unencrypted string if it
				contains an `=' character.

				in other words, "?X" will NOT decode.
				it honestly has to be "?X=blah" to work.

				this bit of fascism ensures enforcement of
				an unambiguous GET API, that's why.
			 */
			uncgi_scanquery(jnk);
			free(jnk);
		}
	} else {
		char	*ptr = filterProgramArguments(value);
		/*
			gotta name/value pair
		 */
		if (htdb_checkval(name)) {
			/*
				erg.  this is kinda messy.
				this little bit of code is needed
				to support the "select multiple" case,
				where we have many values coming at us under the
				same name.  in this case, we will actually store
				the values at the names of the form `name[X]->value'
				where X is increased for each value we find.
				when this is the case, we keep the index counter handy
				in the `name->numMultiple' variable.
			 */
			char	*multiName = (char *)malloc(sizeof(char) * (strlen(name) + 20));
			int	maxIndex;

			/*
				lookup the current index count
			 */
			sprintf(multiName, "%s->numMultiple", name);
			maxIndex = htdb_getint(multiName);

			if (maxIndex == 0)
				/*
					first time in..
					populate "name[1]->value" from the value of "name"
				 */
				htdb_setobjval(name, ++maxIndex, "value", htdb_getval(name));

			/*
				increment the index counter
			 */
			htdb_setint(multiName, ++maxIndex);
			/*
				set the object value
			 */
			htdb_setobjval(name, maxIndex, "value", ptr);
			/*
				and done with the temp storage
			 */
			free(multiName);
		}
		/*
			if this is the first occurrence of the
			value, add it to the trace
		 */
		{
			static bool_t	do_set = BOOL_TRUE;

			if (!htdb_defined(name)) {
					do_set = BOOL_TRUE;
					uncgi_trace(name);
			} else if (0 == strncasecmp(name, "conf", 4)) {
				/*
					baddy trying to override conf* value
				 */
				do_set = BOOL_FALSE;
			}
			
			if (BOOL_TRUE == do_set) {
				/*
					clobber existing values (if any) at this name.
					(last guy in wins - force 'em to check `name->numMultiple'
					to see if it is a "select multiple" context.)
				 */
				htdb_setval(name, ptr);
				free(ptr);
			}
		}
	}


}

void	uncgi_process_name_value(char *name, char *value)
{
	/*
		get rid of URL encodings..
	 */
	uncgi_url_unescape((unsigned char *)name);
	uncgi_url_unescape((unsigned char *)value);

	_uncgi_process_name_value(name, value);
}

void	uncgi_process_name_value_noescape(char *name, char *value)
{
	_uncgi_process_name_value(name, value);
}

/*
	uncgi_stuffenv()
		Stuff a URL-unescaped variable, with the prefix on its
		name, into the environment.  Uses the "=" from the CGI
		arguments.  Putting an "=" in a field name is probably
		a bad idea.  If the variable is already defined, append
		a ',' to it along with the new value.
 */
void	uncgi_stuffenv(char *var)
{
	char	*name = strdup(var),
		*equal = strchr(name, '='),
		*value;

	if (equal == NULL) {
		/*
			this handles GET args the case of `?${encrypt(n1=v1&n2=v2)}'

			the decrypted string is
			assumed to contain additional name/value
			pairs to decode further..
		 */
		if (*name == '|') {
			char	*jnk = htdb_decryptor(name);
			if (jnk && *jnk && strchr(jnk, '='))
				/*
					we will only accept this
					unencrypted string if it
					contains an `=' character.

					in other words, "?X" will NOT decode.
					it honestly has to be "?X=blah" to work.

					this bit of fascism ensures enforcement of
					an unambiguous GET API, that's why.
				 */
				uncgi_scanquery(jnk);
			free(jnk);
		}
	} else {
		/*
			this string contains an `=' sign
		 */
		*equal = '\0';
		value = equal + 1;

		uncgi_process_name_value(name, value);
	}
	free(name);
}

static char	*findNextSeperator(char *in)
{
	char	*ptr = strchr(in, '&');
	while (ptr && *ptr) {
		char	*nxt = (ptr + 1);
		if (!(nxt && *nxt && *nxt == '#')) {
			/*
				name&value
				return now
			 */
			return ptr;
		}
		/*
			name&#value
			keep looking
		 */
		ptr = strchr(ptr + 1, '&');
	}
	/*
		no seperator found
	 */
	return NULL;
}

/*
	uncgi_scanquery()
		Scan a query string, stuffing variables into the
		environment.  This should ideally just use strtok(),
		but that's not available everywhere.
 */
void    uncgi_scanquery(char *q)
{
	if (q && *q) {

		/*
			and now go through and pick out
			any found arguments
		 */
		while (q && *q) {
			char	*ptr = findNextSeperator(q);

			if (ptr) {
				*ptr = '\0';
				if (q && *q)
					uncgi_stuffenv(q);
				q = ptr + 1;
			} else {
				if (q && *q)
					uncgi_stuffenv(q);
				break;
			}
		}
	}
}

/*
	uncgi_postread()
		Read a POST query from standard input into a dynamic
		buffer.  Terminate it with a null character.
 */
static char	*uncgi_postread(void)
{
	char	*buf = getenv("CONTENT_TYPE");

	if (buf && strcasestr(buf, "multipart/form-data")) {
		/*
			some awful cruft to support file upload.
			this whole section (and upload.c) needs
			to be re-written.  it works, but it ain't pretty.
		 */
		char    mesg[512],
				dir[512];
		LinkedList	vars = NULL, files = NULL;
		setUpLoadInfo(NULL, NULL, NULL);
		doUpLoad();
		getUpLoadInfo(dir, &vars, &files, mesg);
		htdb_setval("upload.Dir", dir);
		htdb_setval("cgi->upload->directory", dir);
		{
			LLItem  thang;
			ll_iterate(vars);
			while ((thang = ll_traverse(vars))) {
				namval  *n = (namval *)thang->value;
				if (n->value && *n->value) {
					htdb_setval(n->name, n->value);
					uncgi_trace(n->name);
				}
			}
		}
		{
			LLItem  thang;
			int     count = 0;

			ll_iterate(files);
			while ((thang = ll_traverse(files))) {
				fileinfo        *n = (fileinfo *)thang->value;
				char    jnk[BUF_LIMIT_SMALL + 1];
				count++;

				htdb_setobjvarg("cgi->upload", count, "path", "%s%s", dir, n->name);
				htdb_setobjval("cgi->upload", count, "directory", dir);
				htdb_setobjval("cgi->upload", count, "file", n->name);
				htdb_setobjval("cgi->upload", count, "field", n->field);
				htdb_setobjval("cgi->upload", count, "type", n->type);
				htdb_setobjint("cgi->upload", count, "size", n->size);
				htdb_setobjint("cgi->upload", count, "status", n->status);

				if (1 == count) {
					htdb_setptrvarg("cgi->upload", "path", "%s%s", dir, n->name);
					htdb_setptrval("cgi->upload", "directory", dir);
					htdb_setptrval("cgi->upload", "file", n->name);
					htdb_setptrval("cgi->upload", "field", n->field);
					htdb_setptrval("cgi->upload", "type", n->type);
					htdb_setptrint("cgi->upload", "size", n->size);
					htdb_setptrint("cgi->upload", "status", n->status);
				}

				snprintf(jnk, BUF_LIMIT_SMALL, "upload.%d.Path", count); jnk[BUF_LIMIT_SMALL] = '\0';
				htdb_setvarg(jnk, "%s%s", dir, n->name);
				snprintf(jnk, BUF_LIMIT_SMALL, "upload.%d.Dir", count); jnk[BUF_LIMIT_SMALL] = '\0';
				htdb_setval(jnk, dir);
				snprintf(jnk, BUF_LIMIT_SMALL, "upload.%d.Name", count); jnk[BUF_LIMIT_SMALL] = '\0';
				htdb_setval(jnk, n->name);
				snprintf(jnk, BUF_LIMIT_SMALL, "upload.%d.Field", count); jnk[BUF_LIMIT_SMALL] = '\0';
				htdb_setval(jnk, n->field);
				snprintf(jnk, BUF_LIMIT_SMALL, "upload.%d.Type", count); jnk[BUF_LIMIT_SMALL] = '\0';
				htdb_setval(jnk, n->type);
				snprintf(jnk, BUF_LIMIT_SMALL, "upload.%d.Size", count); jnk[BUF_LIMIT_SMALL] = '\0';
				htdb_setint(jnk, n->size);
				snprintf(jnk, BUF_LIMIT_SMALL, "upload.%d.Status", count); jnk[BUF_LIMIT_SMALL] = '\0';
				htdb_setint(jnk, n->status);
			}
			htdb_setint("upload.NumFiles", count);
			htdb_setint("cgi->upload->numResults", count);
		}
	} else if (buf == NULL ||
		strcasestr(buf, "application/x-www-form-urlencoded") == NULL)
		uncgi_error("No content type was passed to uncgi.");

	buf = getenv("CONTENT_LENGTH");

	if (buf == NULL)
		uncgi_error("I do not know how long the request was");
	
	{
		int	got,
			sofar = 0,
			size = atoi(buf);

		if ((buf = malloc(size + 1)) == NULL)
			uncgi_error("out of memory: postread");

		do {
			got = fread(buf + sofar, 1, size - sofar, stdin);
			sofar += got;
		} while (got && sofar < size);

		buf[sofar] = '\0';
	}

	return (buf);
}

/*
	uncgi()
		HTTP argument processing.
		shoves all incoming values into the HTDB namespace
		so we can play with them later.
 */
void	uncgi(void)
{
	char	*method = getenv("REQUEST_METHOD"),
			*query = getenv("QUERY_STRING"),
			*post_query;

	/*
		this'll keep track of how many values
		were passed-in via GET/POST method
	 */
	htdb_setptrint("uncgi", "numResults", 0);

	/*
		Get the query string, and stick its component
		parts into the in-memory cgiSpace hash table.
		POST values have precedence over GET values.
		yes, virginia, you can mix and match POST and GET,
		because sometimes it is really handy to do so.
	 */
	if (query && *query) {
		/*
			this guy handles GET method
		 */
		char	*tmpbuf = strdup(query);

		uncgi_pipe_unescape((unsigned char *)tmpbuf);

		uncgi_scanquery(tmpbuf);
		free(tmpbuf);
	}

	if (method && *method && strcasecmp(method, "POST") == 0) {
		/*
			and this guy handles POST method
		 */
		post_query = uncgi_postread();
		if (post_query && *post_query)
			uncgi_scanquery(post_query);
		free(post_query);
	}
}

void	htdb_httpHeader(char *fmt, ...)
{
	va_list	args;
	char	val[BUF_LIMIT_MEDIUM + 1];

	va_start(args, fmt);
	vsnprintf(val, BUF_LIMIT_MEDIUM, fmt, args);
	val[BUF_LIMIT_MEDIUM] = '\0';
	va_end(args);

	printf("%s\n", val);
	fflush(stdout);
}

void	htdb_httpHeaderEnd(void)
{
	putchar('\n');
}

void	htdb_httpRedirect(char *fmt, ...)
{
	va_list	args;
	char	val[BUF_LIMIT_MEDIUM + 1];

	va_start(args, fmt);
	vsnprintf(val, BUF_LIMIT_MEDIUM, fmt, args);
	val[BUF_LIMIT_MEDIUM] = '\0';
	va_end(args);

	printf("Status: 302\n");
	printf("Location: %s\n", val);
	htdb_httpHeaderEnd();
	fflush(stdout);
}

void	htdb_httpExpires(time_t the_past)
{
	struct timeval	tv;
	struct timezone	tz;
	char	date[BUF_LIMIT_SMALL + 1];
	time_t	clock;

	gettimeofday(&tv, &tz);
	clock = (long)tv.tv_sec;
	/*
		make our expiration sometime in the past..
	 */
	clock -= the_past;
	sstrncpy(date, ctime(&clock));
	date[strlen(date) - 1] = '\0';
	htdb_httpHeader("Expires: %s", date);
}

void	htdb_doHeader(char *content_type)
{
	if (content_type && *content_type) {
		htdb_httpHeader(content_type);
		htdb_httpHeaderEnd();

		/*
			if htm[l], then spit out what was defined as our document top
		 */
		if (strcasestr(content_type, "htm")) {
			htdb_print(htdb_getval("confDocTop"));
			putchar('\n');
		}

		/*
			flush out what we got so far.
			this might speed up perceived loadtime.
		 */
		fflush(stdout);
	}
}

void	htdb_doFooter(char *content_type)
{
	if (content_type && *content_type) {
		/*
			spit out what was defined as our document bottom
		 */
		if (strcasestr(content_type, "htm"))
			htdb_print(htdb_getval("confDocBottom"));
	}
}

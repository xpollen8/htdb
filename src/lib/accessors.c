#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

/*
	static_setlong()
		shove a long integer into `key'
 */
long_t	static_setlong(char *key, long_t val)
{
	char	str[BUF_LIMIT_TINY + 1];
	snprintf(str, BUF_LIMIT_TINY, "%lld", val);
	str[BUF_LIMIT_TINY] = '\0';
	return atoll(static_setval(key, str));
}

/*
	static_setint()
		shove an integer into `key'
 */
int_t	static_setint(char *key, int_t val)
{
	char	str[BUF_LIMIT_TINY + 1];
	snprintf(str, BUF_LIMIT_TINY, "%ld", val);
	str[BUF_LIMIT_TINY] = '\0';
	return atol(static_setval(key, str));
}

/*
	static_setval()
		shove a string into `key'
 */
char	*static_setval(char *key, char *val)
{
	if (val && *val) {
		static_store(key, strdup(val));
		return val;
	} else {
		static_store(key, strdup(""));
		return "";
	}
}

/*
	static_getval()
		if `name' exists in our look-up table, then
		returns its value, else return an empty string.
 */
char	*static_getval(char *name)
{
	char	*value = static_value(name);
	return (value == NULL || *value == '\0') ?
		"" :		/* not found - return empty string */
		value;		/* cool - found */
}

/*
	htdb_setlong()
		shove a long integer into `key'
 */
long_t	htdb_setlong(char *key, long_t val)
{
	char	str[BUF_LIMIT_TINY + 1];
	snprintf(str, BUF_LIMIT_TINY, "%lld", val);
	str[BUF_LIMIT_TINY] = '\0';
	return atoll(htdb_setval(key, str));
}

/*
	htdb_setint()
		shove an integer into `key'
 */
int_t	htdb_setint(char *key, int_t val)
{
	char	str[BUF_LIMIT_TINY + 1];
	snprintf(str, BUF_LIMIT_TINY, "%ld", val);
	str[BUF_LIMIT_TINY] = '\0';
	return atol(htdb_setval(key, str));
}

/*
	htdb_setval()
		shove a string into `key'
 */
char	*htdb_setval(char *key, char *val)
{
	if (val && *val) {
		htdb_store(key, strdup(val));
		return val;
	} else {
		htdb_store(key, strdup(""));
		return "";
	}
}

/*
	htdb_setbin()
		shove binary data into `key'
 */
void	*htdb_setbin(char *key, void *val, off_t size)
{
	if (val && size) {
		char	*data = (char *)malloc(size + 1);
		memcpy((void *)data, val, size);
		data[size] = '\0';
		htdb_binary(key, data, (size + 1));
		return val;
	} else {
		htdb_store(key, strdup(""));
		return (void *)"";
	}
}

float	htdb_setfloat(char *key, float val, int precision)
{
	static char format[BUF_LIMIT_SMALL + 1];
	static char buf[BUF_LIMIT_BIG + 1];

	snprintf(format, BUF_LIMIT_SMALL, "%%.%df", precision);
	snprintf(buf, BUF_LIMIT_BIG, format, val);
	htdb_store(key, strdup(buf));
	return val;
}
/*
	htdb_getvargval()
		variable-argument version of htdb_getval()
*/
char	*htdb_getvargval(char *fmt, ...)
{
	va_list args;
	static char	lookup[BUF_LIMIT_MAX + 1];

	va_start(args, fmt);
	vsnprintf(lookup, BUF_LIMIT_MAX, fmt, args);
	lookup[BUF_LIMIT_MAX] = '\0';
	va_end(args);
	return htdb_getval(lookup);
}

/*
	htdb_getvarglong()
		variable-argument version of htdb_getlong()
*/
long_t	htdb_getvarglong(char *fmt, ...)
{
	va_list args;
	static char	lookup[BUF_LIMIT_MAX + 1];

	va_start(args, fmt);
	vsnprintf(lookup, BUF_LIMIT_MAX, fmt, args);
	lookup[BUF_LIMIT_MAX] = '\0';
	va_end(args);
	return htdb_getlong(lookup);
}

/*
	htdb_getvargint()
		variable-argument version of htdb_getint()
*/
int_t	htdb_getvargint(char *fmt, ...)
{
	va_list args;
	static char	lookup[BUF_LIMIT_MAX + 1];

	va_start(args, fmt);
	vsnprintf(lookup, BUF_LIMIT_MAX, fmt, args);
	lookup[BUF_LIMIT_MAX] = '\0';
	va_end(args);
	return htdb_getint(lookup);
}


/*
	htdb_setvarg()
		variable-argument version of htdb_setval()
 */
char	*htdb_setvarg(char *name, char *fmt, ...)
{
	va_list	args;
	static char	val[BUF_LIMIT_MAX + 1];

	va_start(args, fmt);
	vsnprintf(val, BUF_LIMIT_MAX, fmt, args);
	val[BUF_LIMIT_MAX] = '\0';
	va_end(args);
	return htdb_setval(name, val[0] ? val : "");
}

/*
	static_setvarg()
		variable-argument version of static_setval()
 */
char	*static_setvarg(char *name, char *fmt, ...)
{
	va_list	args;
	static char	val[BUF_LIMIT_MAX + 1];

	va_start(args, fmt);
	vsnprintf(val, BUF_LIMIT_MAX, fmt, args);
	val[BUF_LIMIT_MAX] = '\0';
	va_end(args);
	return static_setval(name, val[0] ? val : "");
}

/*
	htdb_getval()
		if `name' exists in our look-up table, then
		returns its value, else return an empty string.
 */
char	*htdb_getval(char *name)
{
	char	*value = htdb_value(name);
	return (value == NULL || *value == '\0') ?
		"" :		/* not found - return empty string */
		value;		/* cool - found */
}

/*
	htdb_getbin()
		return the (presumably) binary data stored at `name'
 */
void	*htdb_getbin(char *name)
{
	return htdb_value(name);
}

/*
	htdb_getarrayname()
		given `name' and `index'
		return `name[index]'
 */
char	*htdb_getarrayname(char *name, int index)
{
	static char	lookup[BUF_LIMIT_SMALL + 1];
	snprintf(lookup, BUF_LIMIT_SMALL, "%s[%d]", name, index);
	lookup[BUF_LIMIT_SMALL] = '\0';
	return lookup;
}

/*
	htdb_setarrayval()
		given `name' and `index' and varargs
		set `name[index]' to the varargs string
 */
char	*htdb_setarrayval(char *name, int index, char *fmt, ...)
{
	va_list	args;
	static char	val[BUF_LIMIT_MAX + 1];

	va_start(args, fmt);
	vsnprintf(val, BUF_LIMIT_MAX, fmt, args);
	val[BUF_LIMIT_MAX] = '\0';
	va_end(args);
	return htdb_setval(htdb_getarrayname(name, index), val[0] ? val : "");
}

/*
	static_setarrayval()
		given `name' and `index' and varargs
		set `name[index]' to the varargs string
 */
char	*static_setarrayval(char *name, int index, char *fmt, ...)
{
	va_list	args;
	static char	val[BUF_LIMIT_MAX + 1];

	va_start(args, fmt);
	vsnprintf(val, BUF_LIMIT_MAX, fmt, args);
	val[BUF_LIMIT_MAX] = '\0';
	va_end(args);
	return static_setval(htdb_getarrayname(name, index), val[0] ? val : "");
}

/*
	htdb_getarrayval()
		given `name' and `index'
		return the string stored at `name[index]'
 */
char	*htdb_getarrayval(char *name, int index)
{
	return htdb_getval(htdb_getarrayname(name, index));
}

/*
	htdb_setarraylong()
		given `name' and `index' and `value'
		set `name[index]' to the long integer value of `value'
 */
long_t	htdb_setarraylong(char *name, int index, long_t value)
{
	return htdb_setlong(htdb_getarrayname(name, index), value);
}

/*
	htdb_setarrayint()
		given `name' and `index' and `value'
		set `name[index]' to the integer value of `value'
 */
int_t	htdb_setarrayint(char *name, int index, int_t value)
{
	return htdb_setint(htdb_getarrayname(name, index), value);
}

/*
	htdb_getarraylong()
		given `name' and `index'
		return the long integer stored at `name[index]'
 */
long_t	htdb_getarraylong(char *name, int index)
{
	return htdb_getlong(htdb_getarrayname(name, index));
}

/*
	htdb_getarrayint()
		given `name' and `index'
		return the integer stored at `name[index]'
 */
int_t	htdb_getarrayint(char *name, int index)
{
	return htdb_getint(htdb_getarrayname(name, index));
}

/*
	htdb_getobjdyname()
		given `name' and `index' and `field'
		return DYNAMIC string `name[index]->field'
 */
char	*htdb_getobjdyname(char *name, int index, char *field)
{
	if (name && *name && index > 0 && field && *field) {
		static char	lookup[BUF_LIMIT_SMALL + 1];
		snprintf(lookup, BUF_LIMIT_SMALL, "%s[%d]->%s", name, index, field);
		lookup[BUF_LIMIT_SMALL] = '\0';
		return strdup(lookup);
	} else
		return strdup("");
}

/*
	htdb_getobjname()
		given `name' and `index' and `field'
		return STATIC string `name[index]->field'
 */
char	*htdb_getobjname(char *name, int index, char *field)
{
	if (name && *name && index > 0 && field && *field) {
		static char	lookup[BUF_LIMIT_SMALL + 1];
		snprintf(lookup, BUF_LIMIT_SMALL, "%s[%d]->%s", name, index, field);
		lookup[BUF_LIMIT_SMALL] = '\0';
		return lookup;
	} else
		return "";
}

char	*htdb_getptrname(char *name, char *field)
{
	if (name && *name && field && *field) {
		static char	lookup[BUF_LIMIT_SMALL + 1];
		snprintf(lookup, BUF_LIMIT_SMALL, "%s->%s", name, field);
		lookup[BUF_LIMIT_SMALL] = '\0';
		return lookup;
	} else
		return "";
}

char	*htdb_getptrdyname(char *name, char *field)
{
	if (name && *name && field && *field) {
		static char	lookup[BUF_LIMIT_SMALL + 1];
		snprintf(lookup, BUF_LIMIT_SMALL, "%s->%s", name, field);
		lookup[BUF_LIMIT_SMALL] = '\0';
		return strdup(lookup);
	} else
		return strdup("");
}

/*
	htdb_getptrval()
		given `name' and `field'
		return string stored at `name->field'
 */
char	*htdb_getptrval(char *name, char *field)
{
	char	*lookup = htdb_getptrdyname(name, field),
			*val = htdb_getval(lookup);
	free(lookup);
	return val;
}

/*
	htdb_getptrlong()
		given `name' and `field'
		return long integer stored at `name->field'
 */
long_t	htdb_getptrlong(char *name, char *field)
{
	char	*lookup = htdb_getptrdyname(name, field);
	long_t	val = htdb_getlong(lookup);
	free(lookup);
	return val;
}

/*
	htdb_getptrint()
		given `name' and `field'
		return integer stored at `name->field'
 */
int_t	htdb_getptrint(char *name, char *field)
{
	char	*lookup = htdb_getptrdyname(name, field);
	int_t		val = htdb_getint(lookup);
	free(lookup);
	return val;
}

float	htdb_getptrfloat(char *name, char *field)
{
	char	*lookup = htdb_getptrdyname(name, field);
	float		val = htdb_getfloat(lookup);
	free(lookup);
	return val;
}

/*
	htdb_setptrval()
		given `name' and `field' and `setto'
		set `name->field' to string value of `setto'
 */
char	*htdb_setptrval(char *name, char *field, char *setto)
{
	char	*lookup = htdb_getptrdyname(name, field),
			*val = htdb_setval(lookup, setto);
	free(lookup);
	return val;
}

/*
	htdb_setptrvarg()
		variable-argument version of htdb_setval()
 */
char	*htdb_setptrvarg(char *name, char *field, char *fmt, ...)
{
	va_list	args;
	static char	val[BUF_LIMIT_MAX + 1];

	va_start(args, fmt);
	vsnprintf(val, BUF_LIMIT_MAX, fmt, args);
	val[BUF_LIMIT_MAX] = '\0';
	va_end(args);
	return htdb_setptrval(name, field, val[0] ? val : "");
}

/*
	htdb_setptrlong()
		given `name' and `field' and `setto'
		set `name->field' to long integrer value of `setto'
 */
long_t	htdb_setptrlong(char *name, char *field, long_t setto)
{
	char	*lookup = htdb_getptrdyname(name, field);
	long_t	val = htdb_setlong(lookup, setto);
	free(lookup);
	return val;
}

/*
	htdb_setptrint()
		given `name' and `field' and `setto'
		set `name->field' to integrer value of `setto'
 */
int_t	htdb_setptrint(char *name, char *field, int_t setto)
{
	char	*lookup = htdb_getptrdyname(name, field);
	int_t		val = htdb_setint(lookup, setto);
	free(lookup);
	return val;
}

/*	
	htdb_setptrfloat()
		given 'name', 'field', 'val' and 'precision',
		set `name->field' to floating point value of precision decimal places
*/ 
float	htdb_setptrfloat(char *name, char *field, float val, int precision)
{
	char	*lookup = htdb_getptrdyname(name, field);
	float		ret = htdb_setfloat(lookup, val, precision);
	free(lookup);
	return ret;
}
		

/*
	htdb_getobjval()
		given `name' and `index' and `field'
		return string stored at `name[index]->field'
 */
char	*htdb_getobjval(char *name, int index, char *field)
{
	char	*lookup = htdb_getobjdyname(name, index, field),
			*val = htdb_getval(lookup);
	free(lookup);
	return val;
}

/*
	htdb_getobjlong()
		given `name' and `index' and `field'
		return long integer stored at `name[index]->field'
 */
long_t	htdb_getobjlong(char *name, int index, char *field)
{
	char	*lookup = htdb_getobjdyname(name, index, field);
	long_t	val = htdb_getlong(lookup);
	free(lookup);
	return val;
}

/*
	htdb_getobjint()
		given `name' and `index' and `field'
		return integer stored at `name[index]->field'
 */
int_t	htdb_getobjint(char *name, int index, char *field)
{
	char	*lookup = htdb_getobjdyname(name, index, field);
	int_t		val = htdb_getint(lookup);
	free(lookup);
	return val;
}

/*
	htdb_getobjfloat()
		given `name' and `index' and `field'
		return floating point stored at `name[index]->field'
 */
float	htdb_getobjfloat(char *name, int index, char *field)
{
	char	*lookup = htdb_getobjdyname(name, index, field);
	float		val = htdb_getfloat(lookup);
	free(lookup);
	return val;
}

/*
	htdb_setobjvarg()
		given `name' and `index' and `field' and varagrs
		set `name[index]->field' to varargs value
 */
char	*htdb_setobjvarg(char *name, int index, char *field, char *fmt, ...)
{
	va_list	args;
	static char	setto[BUF_LIMIT_MAX + 1];

	va_start(args, fmt);
	vsnprintf(setto, BUF_LIMIT_MAX, fmt, args);
	setto[BUF_LIMIT_MAX] = '\0';
	va_end(args);
	{
		char	*lookup = htdb_getobjdyname(name, index, field),
				*val = htdb_setval(lookup, setto);
		free(lookup);
		return val;
	}
}

/*
	htdb_setobjval()
		given `name' and `index' and `field' and `setto'
		set `name[index]->field' to string value of `setto'
 */
char	*htdb_setobjval(char *name, int index, char *field, char *setto)
{
	char	*lookup = htdb_getobjdyname(name, index, field),
			*val = htdb_setval(lookup, setto);
	free(lookup);
	return val;
}

/*
	static_setobjval()
		given `name' and `index' and `field' and `setto'
		set `name[index]->field' to string value of `setto'
 */
char	*static_setobjval(char *name, int index, char *field, char *setto)
{
	char	*lookup = htdb_getobjdyname(name, index, field),
			*val = static_setval(lookup, setto);
	free(lookup);
	return val;
}

/*
	htdb_setobjlong()
		given `name' and `index' and `field' and `setto'
		set `name[index]->field' to long integer value of `setto'
 */
long_t	htdb_setobjlong(char *name, int index, char *field, long_t setto)
{
	char	*lookup = htdb_getobjdyname(name, index, field);
	long_t	val = htdb_setlong(lookup, setto);
	free(lookup);
	return val;
}

/*
	htdb_setobjint()
		given `name' and `index' and `field' and `setto'
		set `name[index]->field' to integer value of `setto'
 */
int_t	htdb_setobjint(char *name, int index, char *field, int_t setto)
{
	char	*lookup = htdb_getobjdyname(name, index, field);
	int_t	val = htdb_setint(lookup, setto);
	free(lookup);
	return val;
}

/*	
	htdb_setobjfloat()
		given 'name', 'index', 'field', 'val' and 'precision',
		set `name[index]->field' to floating point value of precision decimal places
*/ 
float	htdb_setobjfloat(char *name, int index, char *field, float val, int precision)
{
	char	*lookup = htdb_getobjdyname(name, index, field);
	float		ret = htdb_setfloat(lookup, val, precision);
	free(lookup);
	return ret;
}

/*
	htdb_cond()
		set the destination resource `to' from
		the evaluated value of `from'
char	*htdb_cond(char *to, char *from)
{
	char	*expanded = htdb_expand(htdb_getval(from)),
			*hold = strdup(htdb_setval(to, expanded));
	free(expanded);
	return hold;
}
 */

/*
	htdb_loop()
		append the destination resource `to' with
		the evaluated value of `from'
 */
void	htdb_loop(char *to, char *from)
{
	htdb_appendvarg(to, "\n%s", htdb_getval(from));
}

/*
	htdb_append()
		similar to htdb_loop() without an embedded newline;
		append the destination resource `to' with
		the evaluated value of `from'
 */
void	htdb_append(char *to, char *from)
{
	char	*expanded = htdb_expand(from);
	spaceData	*b = htdb_lookup(to);

	if (b && b->value && *((char *)b->value))
		/*
			append existing
		 */
		thing_append(b, expanded, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	else
		/*
			first time in
		 */
		htdb_setval(to, expanded);

	free(expanded);
}

/*
	htdb_appendvarg()
		variable-argument version of htdb_append()
 */
void	htdb_appendvarg(char *to, char *fmt, ...)
{
	va_list	args;
	char	from[BUF_LIMIT_MAX + 1];

	va_start(args, fmt);
	vsnprintf(from, BUF_LIMIT_MAX, fmt, args);
	from[BUF_LIMIT_MAX] = '\0';
	va_end(args);

	htdb_append(to, from);
}

#ident "$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

/*
	global storage for open DSO handles
 */
int		numDSO = 0;
void	**dsoHandles;

/*
	stripString

		creates a new string from the incoming string
		so that it has at most a single space between "words".
 */
static char	*stripString(char *in)
{
	char	*out = (char *)malloc(sizeof(char) * (strlen(in) + 1)),
			last = (char)255;
	int		indx = 0;

	while (in && *in && isspace((int)*in))
		in++;

	while (in && *in) {
		if (isspace((int)*in)) {
			if (!isspace((int)last))
				out[indx++] = ' ';
		} else
			out[indx++] = *in;
		last = *in;
		in++;
	}
	out[indx] = '\0';

	while (indx > 0 && isspace((int)out[indx - 1]))
		out[--indx] = '\0';

	return out;
}

/*
	getNextInList

		returns the next start-of-word from a multi-words string.
		changes space characters into end-of-string markers.
 */
static char	*getNextInList(char **in)
{
	char	*str = *in,
			*ptr = strchr(str, ' ');

	if (ptr) {
		*ptr = '\0';
		*in = ++ptr;
	}

	return str;
}

/*
	dso_loadFiles

		given an incoming string contains fullpaths to
		DSO files, load all the DSO functions from those files.
 */
bool_t	dso_loadFiles(char *dsoList)
{
	bool_t	status = BOOL_TRUE;

	if (numDSO == 0) {
		char	*fileList = stripString(dsoList),
			*hold = &fileList[0],
			*this,
			*last = NULL;

		while ((this = getNextInList(&fileList))) {
			void	*dsoThis;

			if (this == last)
				/*
					done with the list
				 */
				break;

			/*
				RTLD_LAZY, do not resolve symbols until needed
			 */
			dsoThis = dlopen(this, RTLD_LAZY);

			if (dsoThis == NULL) {
				/*
					couldn't open this DSO file for whatever reason.
					dump something to stdout
				 */
				char	*mesg = dlerror();

				if (!(mesg && *mesg))
					mesg = "unspecified";

				if (BOOL_TRUE == status)
					/*
						first time 'round.
						spit out the header
					 */
					htdb_doHeader("Content-type: text/plain");

				status = BOOL_FALSE;

				if (strstr(mesg, "CHANGE_ME")) {
					/*
						gack.
						this is what's happening here..
						the default distribution `config.htdb' file
						contains several values that the user needs to tweak
						before HTDB is properly set up.
						here, we've detected that they've not yet
						changed the default distribution file,
						so we'lll print out a message to that effect,
						and exit.
						ugly, but it works.
					 */
					printf("%s\n", htdb_getval("confConfigMessage"));
					/*
						frees the "stripString()'d" list of files
					 */
					free(hold);
					return BOOL_FALSE;
				}
				printf("dso_loadFiles: %s\n", mesg);
				fflush(stdout);
			} else {
				/*
					add this DSO handle to our (global)
					array of DSO handles.  having an array
					will make function lookup and the closing
					of the DSOs a snap at tear-down time
				 */
				numDSO++;

				if (1 == numDSO)
					/*
						first element - create the array
					 */
					dsoHandles = (void **)
						malloc(sizeof(void *));
				else
					/*
						append to the existing array
					 */
					dsoHandles = (void **)
						realloc(dsoHandles,
						(sizeof(void *) * numDSO));
				dsoHandles[numDSO - 1] = dsoThis;
			}

			last = this;
		}
		/*
			frees the "stripString()'d" list of files
		 */
		free(hold);
	}
	return status;
}

/*
	dso_unloadFiles()
		close all open DSO handles, free storage, and
		zero out everything related.
 */
void	dso_unloadFiles(void)
{
	if (dsoHandles) {
		int	i;
		for (i = 0 ; i < numDSO ; i++)
			/*
				close all open DSO files
			 */
			dlclose(dsoHandles[i]);
		/*
			and free the array we used to hold the handles
		 */
		free(dsoHandles);
	}
	dsoHandles = (void **)NULL;
	numDSO = 0;
}

/*
	dso_fetchFunction

		given a function name, look through the list of
		known DSO functions and set the address associated with
		the function if found.  otherwise set to NULL.
 */
void	dso_fetchFunction(char *functionName, char *(**functionPtr)(char *))
{
	char	*fName = (char *)malloc(sizeof(char) * (strlen(functionName) + 10));

	/*
		our naming convention is always to have our
		DSO with the `dso_' prefix.
	 */
	sprintf(fName, "dso_%s", functionName);

	*functionPtr = NULL;

	{
		int		i;
		for (i = 0 ; i < numDSO && NULL == *functionPtr ; i++) {
			void	**dsoThis = dsoHandles[i];
			if (dsoThis != NULL)
				*functionPtr = (char *(*)(char *))dlsym(dsoThis, fName);
		}
	}

	free(fName);
}

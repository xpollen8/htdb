#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

typedef struct	{
	int	offset;
	char	*type,
		*cookie,
		*content,
		*extension;
}	cookieData_t;

cookieData_t	cookieData[] = {
	{ 0,	"string",	"II*",			"image/TIFF",		"tiff"	},
	{ 6,	"string",	"JFIF",			"image/JPEG",		"jpg"	},
	{ 6,	"string",	"Exif",			"image/JPEG",		"jpg"	},	/* nikon 950 digital jpeg */
	{ 0,	"string",	"GIF8",			"image/GIF",		"gif"	},
	{ 2,	"byte",		"0x01da0001",	"image/RGB",		"rgb"	},
	{ 8,	"string",	"AIFF",			"audio/AIFF",		"aiff"	},
	{ 0,	"string",	".snd",			"audio/AU",			"au"	},
	{ 0,	"long",		"0x2e736e64",	"audio/AU",			"au"	},
	{ 0,	"long",		"0x646e732e",	"audio/AU",			"au"	},
	{ 0,	"string",	".ra",			"audio/REAL",		"ra"	},
	{ 0,	"string",	".rmf",			"audio/REAL",		"ram"	},
	{ 8,	"string",	"AIFC",			"video/AIFC",		"aifc"	},
	{ 4,	"string",	"mdat",			"video/Quicktime",	"qt"	},
	{ 4,	"string",	"moov",			"video/Quicktime",	"mov"	},
	{ 0,	"string",	"RIFF",			"video/AVI",		"avi"	},
	{ 0,	"byte",		"0x000001b3",	"video/MPEG",		"mpg"	},
	{ 0,	"long",		"0xcafebabe",	"binary/java",		"class"	}
};
#define	NUM_COOKIES	(sizeof(cookieData) / sizeof(cookieData[0]))

/*
	in-memory magic cookie list type
 */
typedef struct	{
	union	{
		char	s[50];
		long	l;
		short	h;
		char	b;
	}	val;
}	cookie_t;

/*
	imageGetDimensionsGIFFromBuffer()

 		given a buffer containing GIF data, walk through the header
		to determine the width and height of the image.
 */
static void     imageGetDimensionsGIFFromBuffer(unsigned char *buffer,
		int *width, int *height, int bytes)
{
	int	value,
		indx = 6;

	value = (unsigned short)(buffer[indx + 1] << 8);
	value |= (unsigned short)buffer[indx];
	*width = value;
	indx += sizeof(char) * 2;
	value = (unsigned short)(buffer[indx + 1] << 8);
	value |= (unsigned short)(buffer[indx]);
	*height = value;
}

/*
	imageGetDimensionsGIF()

 		given the path to a (presumably) GIF image file,
		determine the width an height of the image.
 */
static bool_t	imageGetDimensionsGIF(char *path, int *width,
		int *height, int bytes)
{
	int		fd = open(path, O_RDONLY);
	bool_t	status = BOOL_FALSE;
	unsigned char	*buffer = (unsigned char *)malloc(sizeof(unsigned char) * bytes);

	if (read(fd, buffer, sizeof(char) * bytes) == bytes) {
		imageGetDimensionsGIFFromBuffer(buffer, width, height, bytes);
		status = BOOL_TRUE;
	}
	free(buffer);
	close(fd);

	return status;
}

#define readbyte(a,b) do if(((a)=getc((b))) == EOF) return 0; while (0)
#define readword(a,b) do { int cc_=0,dd_=0; \
						  if((cc_=getc((b))) == EOF \
						  || (dd_=getc((b))) == EOF) return 0; \
						  (a) = (cc_<<8) + (dd_); \
					  } while(0)

/*
	imageGetDimensionsJPEG()

		given the path to a (presumably) JPEG image file,
		determine the dimensions of JPEG file
		- code by Tom 7 - public domain
		portions derived from IJG code
 */
static bool_t	imageGetDimensionsJPEG(char *path, int *image_width, int *image_height)
{
	int 	marker=0,
			dummy=0;
	FILE	*infile = (FILE *)NULL;

	if ((infile = fopen(path, "rb")) == NULL)
		return BOOL_FALSE;

	if (getc(infile) != 0xFF || getc(infile) != 0xD8) {
		fclose(infile);
		return BOOL_FALSE;
	}

	for (;;) {
		int	discarded_bytes=0;

		readbyte(marker,infile);
		while (marker != 0xFF) {
			discarded_bytes++;
			readbyte(marker,infile);
		}
		do readbyte(marker,infile); while (marker == 0xFF);

		if (discarded_bytes != 0) {
			fclose(infile);
			return BOOL_FALSE;
		}

		switch (marker) {
			case 0xC0:
			case 0xC1:
			case 0xC2:
			case 0xC3:
			case 0xC5:
			case 0xC6:
			case 0xC7:
			case 0xC9:
			case 0xCA:
			case 0xCB:
			case 0xCD:
			case 0xCE:
			case 0xCF: {
				readword(dummy,infile);   /* usual parameter length count */
				readbyte(dummy,infile);
				readword((*image_height),infile);
				readword((*image_width),infile);
				readbyte(dummy,infile);

				fclose(infile);
				return BOOL_TRUE;
				break;
			}
			case 0xDA:
			case 0xD9:
				fclose(infile);
				return BOOL_FALSE;
			default: {
				int length;

				readword(length,infile);

				if (length < 2) {
					fclose(infile);
					return BOOL_FALSE;
				}
				length -= 2;
				while (length > 0) {
					readbyte(dummy, infile);
					length--;
				}
			}
			break;
		}
	}
	fclose(infile);
}

/*
	imageGetDimensions()

		wrapper function to determine the width and height
		of an on-disk GIF or JPEG image.
 */
bool_t     imageGetDimensions(char *path, int *width, int *height)
{
	char	*buffer;
	bool_t	status = BOOL_FALSE;
	int		fd,
			bytes;
	struct stat	buf;

	*width = *height = 0;

	if (stat(path, &buf) != 0)
		return status;

	if ((fd = open(path, O_RDONLY)) == -1)
		return status;

	bytes = buf.st_size;
	buffer = (char *)malloc(sizeof(char) * bytes);

	if (read(fd, (void *)buffer, bytes) == bytes) {
		char	*ext = imageGetExtensionByData(buffer);

		if (strcasecmp(ext, "gif") == 0) {
			status = imageGetDimensionsGIF(path, width, height, bytes);
		} else if (strcasecmp(ext, "jpg") == 0) {
			status = imageGetDimensionsJPEG(path, width, height);
		} else {
			/*
				what ever to do?
			 */
			status = BOOL_FALSE;
		}
	}
	free(buffer);
	close(fd);
	return status;
}

/*
	imageGetContentByData()

		guess the appropriate HTTP Content-type given
		a buffer containing binary data.
 */
char	*imageGetContentByData(char *buf)
{
	int	i;

	for (i = 0 ; i < NUM_COOKIES ; i++) {
		cookieData_t	*src = &cookieData[i];
		cookie_t	this;
		int	len = 0;

		if (strcmp(src->type, "string") == 0) {
			strcpy(this.val.s, src->cookie);
			len = strlen(src->cookie);
		} else if (strcmp(src->type, "long") == 0) {
			sscanf(src->cookie, "%li", &this.val.l);
			len = sizeof(long);
		} else if (strcmp(src->type, "short") == 0) {
			sscanf(src->cookie, "%hd", &this.val.h);
			len = sizeof(short);
		} else if (strcmp(src->type, "byte") == 0) {
			int	tmp;
			sscanf(src->cookie, "%d", &tmp);
			this.val.b = (char)tmp;
			len = sizeof(char);
		}
		if (len && memcmp(&buf[src->offset], (char *)&this.val, len) == 0)
			/*
				we found our man.
			 */
			return src->content;
	}
	return "";
}

/*
	imageGetContentByExtension()

		guess the appropriate HTTP Content-type given
		only the extension of a file name.
 */
char	*imageGetContentByExtension(char *ext)
{
	int	i;

	for (i = 0 ; i < NUM_COOKIES ; i++) {
		cookieData_t	*src = &cookieData[i];

		if (strcmp(src->extension, ext) == 0)
			/*
				we found our man.
			 */
			return src->content;
	}
	return "";
}

/*
	imageGetExtensionByData()

		guess the file extension given only
		a buffer containing binary data.
 */
char	*imageGetExtensionByData(char *buf)
{
	int	i;

	for (i = 0 ; i < NUM_COOKIES ; i++) {
		cookieData_t	*src = &cookieData[i];
		cookie_t	this;
		int	len = 0;

		if (strcmp(src->type, "string") == 0) {
			strcpy(this.val.s, src->cookie);
			len = strlen(src->cookie);
		} else if (strcmp(src->type, "long") == 0) {
			sscanf(src->cookie, "%li", &this.val.l);
			len = sizeof(long);
		} else if (strcmp(src->type, "short") == 0) {
			sscanf(src->cookie, "%hd", &this.val.h);
			len = sizeof(short);
		} else if (strcmp(src->type, "byte") == 0) {
			int	tmp;
			sscanf(src->cookie, "%d", &tmp);
			this.val.b = (char)tmp;
			len = sizeof(char);
		}
		if (len && memcmp(&buf[src->offset], (char *)&this.val, len) == 0)
			/*
				we found our man.
			 */
			return src->extension;
	}
	return "";
}

bool_t	imageStore(char *table, int_t user_id, char *name, char *buf, char *ext, off_t media_bytes, int w, int h)
{
	off_t   dbSafe_bytes = 0;
	char	*ptr = prepareBinaryForDatabase(buf, media_bytes, &dbSafe_bytes),
			beg[BUF_LIMIT_SMALL + 1],
			*end;
	int		static_bytes = 0;

	/*
		get rid of images with the same image name prefix
		and same width as that incoming.
	 */
	dbDelete("delete from %s where name like '%s\\_%dx%%' and user_id=%ld",
		table, name, w, user_id);

	/*
		mysql BLOBs basically suck
	 */
	snprintf(beg, BUF_LIMIT_SMALL,
		"insert into %s set "
		"user_id = %ld, "
		"name = '%s_%dx%d', "
		"bytes = %lld, "
		"ext = '%s', "
		"width = %d, "
		"height = %d, "
		"tscreated = now(), "
		"isdeleted = 'F', "
		"data='",
		table,
		user_id,
		name,
		w, h,
		media_bytes,
		ext,
		w, h);

	/*
		how much storage do we need for this frankenstein
		static + dynamic buffer?  sure?  no?  then give it some slop.
	 */
	end = (char *)malloc(sizeof(char) * ((static_bytes = strlen(beg)) + dbSafe_bytes + 100));
	/*
		copy in the static part of the SQL statement
	 */
	strcpy(end, beg);
	/*
		append the mysql-ready media data to the end of the buffer.
	 */
	memcpy(&end[static_bytes], ptr, dbSafe_bytes);
	/*
		add the final single-quote so the query is correct
	 */
	strcpy(&end[static_bytes + dbSafe_bytes], "'");
	/*
		and then call the binary version of our insert routine
	 */
	htdb_dbQueryBinary(end, (static_bytes + dbSafe_bytes + 1));

	/*
		ciao!
	 */
	free(end);
	free(ptr);
	return BOOL_TRUE;
}

bool_t imageScaleAndStore(char *table, int_t user_id, char *name, char *dir, char *file, int width, int height)
{
	char	cmd[BUF_LIMIT_SMALL + 1],
			arguments[BUF_LIMIT_SMALL + 1];
	int		h = 0,
			w = 0;

	snprintf(cmd, BUF_LIMIT_SMALL, "%s/%s", dir, file);

	/*
		we determine the image height and width as the file was uploaded
	 */
	if (imageGetDimensions(cmd, &w, &h) == 0) {
		/*
			bad file
		 */
		return BOOL_FALSE;
	}

	if (height == 0)
		/*
			if the height is not specified, then keep the aspect ratio en-tact.
		 */
		snprintf(arguments, BUF_LIMIT_SMALL, " -width %d", (width ? width : w));
	else if (width == 0)
		/*
			if the width is not specified, then keep the aspect ratio en-tact.
		 */
		snprintf(arguments, BUF_LIMIT_SMALL, " -height %d", (height ? height : h));
	else
		/*
			else, use the specified dimensions.
		 */
		snprintf(arguments, BUF_LIMIT_SMALL, " -width %d -height %d", width, height);

	/*
		this is the actual scaling
	 */
	snprintf(cmd, BUF_LIMIT_SMALL,
		"%s %s/%s | %s %s | %s > %s/%d.jpg",
		htdb_getval("confAppDJPEG"),
		dir, file,
		htdb_getval("confAppPNMSCALE"),
		arguments,
		htdb_getval("confAppCJPEG"),
		dir,
		width);

	/*
		and here we take a nap
	 */
	if (system(cmd) == -1) {
		/*
			system failure
		 */
		return BOOL_FALSE;
	}

	/*
		prepare the scaled image for database inclusion
	 */
	{
		struct stat buf;

		snprintf(cmd, BUF_LIMIT_SMALL, "%s/%d.jpg", dir, width);

		/*
			we determine the image height and width as
			it is stored as well as the image data.
		 */
		if (imageGetDimensions(cmd, &w, &h) == 0) {
			/*
				failure encoding
			 */
			return BOOL_FALSE;
		}

		if (stat(cmd, &buf) != 0 || buf.st_size == 0) {
			/*
				failure encoding
			 */
			return BOOL_FALSE;
		}
		{
			/*
				need someplace to store the image data in-memory
			 */
			char	*buffer = (char *)malloc(sizeof(char) * (buf.st_size + 1));
			int fd = open(cmd, O_RDONLY);
			bool_t	status = BOOL_FALSE;
			if (fd > 0) {
				/*
					read it off disk
				 */
				if (read(fd, (void *)buffer, buf.st_size) == buf.st_size)
					/*
						and shove it to the database
					 */
					status = imageStore(table, user_id, name, buffer, "jpg", buf.st_size, w, h);
				close(fd);
			}
			free(buffer);
			return status;
		}
	}
}

#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

long	debug_visual = MASK_NONE;
long	debug_log = MASK_FATAL;
int		debug_visual_innit = 0;
int		debug_conf_set = 0;
int		debug_addr_match = 0;

static char	*debug_prettyHTMLify(char *ugly)
{
	spaceData	b;

	data_init(&b);

	while (ugly && *ugly) {
		if (*ugly == '<')
			thing_append(&b, "&lt;", 4, BUF_LIMIT_BIG);
		else if (*ugly == '>')
			thing_append(&b, "&gt;", 4, BUF_LIMIT_BIG);
		else if (*ugly == '\t')
			thing_append(&b, "TAB", 3, BUF_LIMIT_BIG);
		else if (*ugly == '\n')
			thing_append(&b, "\\n<br>", 6, BUF_LIMIT_BIG);
		else
			thing_appendChar(&b, *ugly, BUF_LIMIT_BIG);
		ugly++;
	}

	return b.value;
}

static char	*debug_nameMask(int mask)
{
	if (mask == DEBUG_LEVEL_DEBUG) return "DEBUG";
	if (mask == DEBUG_LEVEL_INFO) return "INFO";
	if (mask == DEBUG_LEVEL_WARNING) return "WARN";
	if (mask == DEBUG_LEVEL_ERROR) return "ERR";
	if (mask == DEBUG_LEVEL_FATAL) return "FATAL";
	return "?";
}

static int debug_unMask(int in, int mask)
{
	if (in & mask & DEBUG_LEVEL_DEBUG) return DEBUG_LEVEL_DEBUG;
	if (in & mask & DEBUG_LEVEL_INFO) return DEBUG_LEVEL_INFO;
	if (in & mask & DEBUG_LEVEL_WARNING) return DEBUG_LEVEL_WARNING;
	if (in & mask & DEBUG_LEVEL_ERROR) return DEBUG_LEVEL_ERROR;
	if (in & mask & DEBUG_LEVEL_FATAL) return DEBUG_LEVEL_FATAL;
	return 0;
}

static int	debug_getDebugLevel(char *name, int which)
{
	if (strcasestr(name, "debug")) return MASK_DEBUG;
	if (strcasestr(name, "all")) return MASK_INFO;
	if (strcasestr(name, "info")) return MASK_INFO;
	if (strcasestr(name, "warn")) return MASK_WARNING;
	if (strcasestr(name, "err")) return MASK_ERROR;
	if (strcasestr(name, "fat")) return MASK_FATAL;
	if (strcasestr(name, "no")) return MASK_NONE;

	if (which == 0) return MASK_ERROR;
	else return MASK_NONE;
}

void	debug_it(int mask, char *label, char *msg, char **out_buf)
{
	if (out_buf)
		/*
			so we have something to free in case
			we don't actually need to show dso output
		 */
		*out_buf = strdup("");

	if (debug_conf_set == 0) {
		/*
			only need to determine this stuff once
		 */
		char	*visual = htdb_getval("confDebugVisualLevel"),
				*log = htdb_getval("confDebugLogLevel");

		debug_visual = debug_getDebugLevel(visual, 0);
		debug_log = debug_getDebugLevel(log, 1);

		debug_conf_set = 1;

		if ( htdb_equal("confDebugAddr", "all") )
			debug_addr_match = 1;
		else 
			debug_addr_match =
				(htdb_equal("cgi->remote_addr", htdb_getval("confDebugAddr")) ||
				(getenv("SERVER_NAME") == NULL)) ? 1 : 0;
	}
	if (debug_addr_match && (debug_visual & mask)) {
		char	*ptr;

		if (out_buf) {
			char    value[BUF_LIMIT_MAX + 1];

			snprintf(value, BUF_LIMIT_MAX, "<b>%s|%s:</b> <p>%s<p>\n",
				debug_nameMask(debug_unMask(debug_visual, mask)), label,
				ptr = debug_prettyHTMLify(msg));
			value[BUF_LIMIT_MAX] = '\0';
			free(*out_buf);
			free(ptr);
			*out_buf = strdup(value);
		} else {
			if (debug_visual_innit == 0)
				printf("Content-type: text/html\n\n");
			debug_visual_innit = 1;
			printf("<b>%s|%s:</b> <p>%s<p>\n",
				debug_nameMask(debug_unMask(debug_visual, mask)), label,
				ptr = debug_prettyHTMLify(msg));
			free(ptr);
			fflush(stdout);
		}
	}
	if (debug_log & mask) {
		/*
			need to add syslog type stuff here...
		(why doesn't this work as-is?)
		*/

		openlog("WEB", LOG_PID, LOG_LOCAL0);
		syslog(LOG_INFO, "%s: %s: %s", htdb_getval("cgi->server_name"), label, msg);
		closelog();
	}
}

void	log_debug(char *label, char *fmt, ...)
{
	va_list args;
	if (fmt && *fmt) {
		char    value[BUF_LIMIT_MAX + 1];
		va_start(args, fmt);
		vsnprintf(value, BUF_LIMIT_MAX, fmt, args);
		value[BUF_LIMIT_MAX] = '\0';
		debug_it(DEBUG_LEVEL_DEBUG, label, value, NULL);
		va_end(args);
	} else
		debug_it(DEBUG_LEVEL_DEBUG, label, "NULL", NULL);
}

void	log_info(char *label, char *fmt, ...)
{
	va_list args;
	if (fmt && *fmt) {
		char    value[BUF_LIMIT_MAX + 1];
		va_start(args, fmt);
		vsnprintf(value, BUF_LIMIT_MAX, fmt, args);
		value[BUF_LIMIT_MAX] = '\0';
		debug_it(DEBUG_LEVEL_INFO, label, value, NULL);
		va_end(args);
	} else
		debug_it(DEBUG_LEVEL_INFO, label, "NULL", NULL);
}

void	log_warning(char *label, char *fmt, ...)
{
	va_list args;
	if (fmt && *fmt) {
		char    value[BUF_LIMIT_MAX + 1];
		va_start(args, fmt);
		vsnprintf(value, BUF_LIMIT_MAX, fmt, args);
		value[BUF_LIMIT_MAX] = '\0';
		debug_it(DEBUG_LEVEL_WARNING, label, value, NULL);
		va_end(args);
	} else
		debug_it(DEBUG_LEVEL_WARNING, label, "NULL", NULL);
}

void	log_error(char *label, char *fmt, ...)
{
	va_list args;
	if (fmt && *fmt) {
		char    value[BUF_LIMIT_MAX + 1];
		va_start(args, fmt);
		vsnprintf(value, BUF_LIMIT_MAX, fmt, args);
		value[BUF_LIMIT_MAX] = '\0';
		debug_it(DEBUG_LEVEL_ERROR, label, value, NULL);
		va_end(args);
	} else
		debug_it(DEBUG_LEVEL_ERROR, label, "NULL", NULL);
}

void	log_file(char *file, char *fmt, ...)
{
	va_list args;
	FILE	*fp = fopen(file, "a+");
	if ( !fp )
		return;
	if (fmt && *fmt) {
		char    value[BUF_LIMIT_MAX + 1];
		va_start(args, fmt);
		vsnprintf(value, BUF_LIMIT_MAX, fmt, args);
		value[BUF_LIMIT_MAX] = '\0';
		fprintf(fp, "%s\n", value);
		va_end(args);
	} else {
		fprintf(fp, "%s\n", fmt);
	}
	fclose(fp);
}

void	log_fatal(char *label, char *fmt, ...)
{
	va_list args;
	if (fmt && *fmt) {
		char    value[BUF_LIMIT_MAX + 1];
		va_start(args, fmt);
		vsnprintf(value, BUF_LIMIT_MAX, fmt, args);
		value[BUF_LIMIT_MAX] = '\0';
		debug_it(DEBUG_LEVEL_FATAL, label, value, NULL);
		va_end(args);
	} else
		debug_it(DEBUG_LEVEL_FATAL, label, "NULL", NULL);
}

void	log_intro(void)
{
	debug_visual = MASK_NONE;
	debug_log = MASK_FATAL;
	debug_visual_innit = 0;
	debug_conf_set = 0;
}

void	log_outro(void)
{
	debug_visual = MASK_NONE;
	debug_log = MASK_NONE;
	debug_visual_innit = 0;
	debug_conf_set = 0;
	debug_addr_match = 0;
}

static double	_profiler(int start)
{
	static struct timeval profile_start;
	struct timeval profile_end;
	if ( start ) {
		gettimeofday(&profile_start, (void *) NULL);
		return 0;
	} else {
		long usecs;
		double ret;
		gettimeofday(&profile_end, (void *) NULL);
		usecs = profile_end.tv_usec - profile_start.tv_usec;
		if ( usecs < 0 ) {
			profile_end.tv_sec -= 1;
			usecs = usecs + 1000000;
		}	
		ret = (profile_end.tv_sec - profile_start.tv_sec) + (.000001 * usecs);
		return ret;
	}
}
			

void	profile_begin(void)
{
	_profiler(1);
}

double	profile_end(void)
{
	return (_profiler(0));
}

void	log_profile_end(char *mesg)
{
	char buf[BUF_LIMIT_SMALL + 1];
	snprintf(buf, BUF_LIMIT_SMALL, "%s: %f", mesg, profile_end());
	buf[BUF_LIMIT_SMALL] = '\0';
	log_fatal("profiler", buf);
}


void	debug_resDump(char *file)
{
	spaceData	*b;
	FILE	*pp = fopen(file, "w");

	if (pp == (FILE *)NULL)
		return;

	fprintf(pp, "######################################################################\n");
	fprintf(pp, "#\n");
	fprintf(pp, "#\tvariables\n");
	fprintf(pp, "#\n");
	fprintf(pp, "######################################################################\n\n");
	htdb_initwalk();
	while ((b = htdb_walkspace()))
		fprintf(pp, "#define\t%s\n(%s)\n\n", b->key, (char *)b->value), fflush(pp);

	fprintf(pp, "######################################################################\n");
	fprintf(pp, "#\n");
	fprintf(pp, "#\tfunctions\n");
	fprintf(pp, "#\n");
	fprintf(pp, "######################################################################\n\n");
	sfunc_initwalk();
	while ((b = sfunc_walkspace())) {
		sfunc_t		*sf = (sfunc_t *)b->value;
		int			i;

		fprintf(pp, "#define\t%s(", b->key);
		for (i = 0 ; i < sf->num_args ; i++) {
			fprintf(pp, "%s%s", sf->args[i],
				(i < sf->num_args - 1) ? ", " : "");
		}
		fprintf(pp, ")\n\t%s\n\n", sf->body), fflush(pp);
	}

#ifdef	USE_XML_STYLEE
	fprintf(pp, "######################################################################\n");
	fprintf(pp, "#\n");
	fprintf(pp, "#\ttags\n");
	fprintf(pp, "#\n");
	fprintf(pp, "######################################################################\n\n");
	tag_initwalk();
	while ((b = tag_walkspace())) {
		char	*args = htdb_hash_value(b->value, "raw_args"),
				*num_args = htdb_hash_value(b->value, "num_args"),
				*body = htdb_hash_value(b->value, "body");
		int		i,
				num = atoi(num_args);

		fprintf(pp, "#define\t<%s", b->key);
		for (i = 0 ; i < num ; i++) {
			char	jnk[BUF_LIMIT_SMALL + 1];
			snprintf(jnk, BUF_LIMIT_SMALL, "arg_%d", i);
			jnk[BUF_LIMIT_SMALL] = '\0';
			fprintf(pp, " %s", htdb_hash_value(b->value, jnk));
		}
		fprintf(pp, ">\n\t%s\n\n", body), fflush(pp);
	}
#endif

	fclose(pp);
}

void	dump(char *str)
{
	FILE	*fp = fopen("/tmp/htdb.dump", "a");
	fprintf(fp, "(%s)\n\n", str); fflush(fp);
	fclose(fp);
}

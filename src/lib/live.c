#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

/***********************************************************************

	begin private functions

 ***********************************************************************/

static	void	htdb_parseBuffer(char *str, spaceData *b);

typedef	enum { OPER_EQEQ = 1, OPER_EQ, OPER_NE, OPER_LT, OPER_GT, OPER_LEQ, OPER_GEQ } oper_t;

int	if_values[MAX_IF_RECURSE][MAX_IF_DEPTH],
	if_depth[MAX_IF_RECURSE],
	if_recurse_level = 0,
	while_recurse_level = 0,
	loop_recurse_level = 0;

static char	*htdb_scriptFunction(spaceData *h, char *function_args)
{
	sfunc_t		*sf = (sfunc_t *)h->value;
	char	    **args; 
	int			num_args = htdb_splitArgs(function_args, ",", &args, sf->num_args),
				i,
				funcnameLength = strlen(h->key);
	bool_t		useAssignmentStylee = function_args 
					&& strchr(function_args, '=') ? BOOL_TRUE : BOOL_FALSE;

	/*
		first walk through the argument prototype list
		and clear out the values.
		this will allow non-passed arguments to
		not stay un-expanded.  ta-da.
	 */
	
	for (i = 0 ; i < sf->num_args ; i++) {
		char	*lookup = (char *)malloc(sizeof(char) * (funcnameLength + 8 + strlen(sf->args[i])));
		sprintf(lookup, "func_%s->%s", h->key, sf->args[i]);
		if (*(sf->defaults[i]))
			/*
				we've got a default value to set up
			 */
			htdb_setval(lookup, sf->defaults[i]);
		else
			/*
				else, we'll clear out this variable
			 */
			htdb_setval(lookup, "");
		free(lookup);
	}

	/*
		then, walk through the passed arguments
		and shove the values into the function-private namespace
		so that a call to htdb_expand() looks in the private namespace.
	 */
	for (i = 0 ; i < num_args ; i++) {
		char	*dp = args[i],
				*equal = (dp && *dp ? strchr(dp, '=') : NULL);
		if (equal && *equal) {
			*equal = '\0';	/* this truncates the arg value... this is OK */
			{
				char	*lookup = (char *)malloc(sizeof(char) * (funcnameLength + 8 + strlen(dp)));
				sprintf(lookup, "func_%s->%s", h->key, dp);
				htdb_setval(lookup, equal + 1);
				free(lookup);
			}
		} else if ( num_args == sf->num_args && !useAssignmentStylee ) {
			/* if you've got all the arguments and didn't use any equals signs
			   , we'll just assume you got 'em in the right order. */
			{
				char	*lookup = (char *)malloc(sizeof(char) * (funcnameLength + 8 + strlen(sf->args[i])));

				sprintf(lookup, "func_%s->%s", h->key, sf->args[i]);
				htdb_setval(lookup, dp);
				free(lookup);
			}
		}	
	}

	/*
		finally, process the body.
		this is cool.
	 */
	{
		char	*ret = htdb_expand(sf->body);
		/*
			now that the body has been processed,
			walk through the namespace and clear out
			any temporary values that have been set
		 */
		for (i = 0 ; i < num_args ; i++) {
			char	*dp = args[i];
			if (dp && *dp) {
					char	*lookup = (char *)malloc(sizeof(char) * (funcnameLength + 8 + strlen(dp)));
				sprintf(lookup, "func_%s->%s", h->key, dp);
				htdb_setval(lookup, "");
				free(lookup);
			}
		}

		htdb_deleteArgs(args, num_args);

		return ret;
	}
}


/*
	htdb_parse_token()
		given a string of form `token(value)',
		return `value', expanded if necessary.
 */
static char	*htdb_parse_token(char *str, int *tok_indx, int len)
{
	char	*token = (char *)malloc(sizeof(char) * (len + 1)),
		*hold = &token[0];

	token[*tok_indx = 0] = '\0';

	{
		/*
			we gotta be careful to grab the *matching* paren
		 */
		int	paren_count = 1,
			i = 0;
		while (i < len && paren_count >= 1) {
			if (*str == '(')
				paren_count++;
			else if (*str == ')')
				paren_count--;
			token[(*tok_indx)++] = *(str++);
			len--;
		}
	}
	if (*tok_indx > 0) {
		token[--(*tok_indx)] = '\0';
		{
			/*
				jesus, what convoluted logic
				just to keep from leaking memory..
			 */
			char	*tmp = strdup(token),	/* take a copy */
					*ptr = &tmp[0];			/* save the start of it */
			free(hold);						/* free the original */
			token = strdup(htdb_stripWhitespace(tmp));
			free(ptr);						/* free the temporary */
		}
		if (strchr(token, HTDB_TOKEN_BEGIN_ONE)) {
			char	*expanded = htdb_expand(token);
			free(token);
			token = expanded;
		}
	}
	*tok_indx = strlen(token);
	return token;
}

static char	*(*fetchNamedDSO(char *nmp))(char *)
{
	char	*(*funcPtr)(char *) = NULL;
	/*
		got a DSO.
		yes - the precedence allows script-writers
		to over-ride/mask compiled functions.
		some might consider this a security threat..
	 */
	spaceData	*cb = cfunc_lookup(htdb_stripWhitespace(nmp));

	if (cb)
		/*
			this function is already in our hash table.
		 */
		funcPtr = cb->value;
	else {
		/*
			is there a DSO function by this name?
		 */
		dso_fetchFunction(nmp, &funcPtr);
		if (funcPtr && *funcPtr)
			/*
				yes, add it to the function hash table.
			 */
			cfunc_store(nmp, funcPtr);
	}
	return funcPtr;
}

/*
	htdb_callable()
		we are being asked to call (evaluate) a DSO function.
		return success (BOOL_TRUE) or failure (BOOL_FALSE)
 */
bool_t	htdb_callable(spaceData *b, char *key, int *arg_length, bool_t *error)
{
	/*
		have we found a built-in stand-alone function?
		if so, return the value of what the function does to the key.
	 */
	char	*nm = strchr(key, '(');
	bool_t	found = BOOL_FALSE;

	*error = BOOL_FALSE;
	*arg_length = 0;

	if (nm && *nm) {
		/*
			do we have a DSO function?
		 */
		char	*nmp;
		{
			int		func_length = nm - key + 1;

			nmp = (char *)malloc(sizeof(char) * (func_length + 1));

			/*
				build the lookup string
			 */
			strncpy(nmp, key, func_length);
			nmp[--func_length] = '\0';
		}

		{
			spaceData	*bb = sfunc_lookup(htdb_stripWhitespace(nmp));

			if (bb) {
				/*
					got a user script function.
					yes - the precedence allows script-writers
					to over-ride/mask compiled functions.
					i suppose this could be seen as a feature or a design oversight..
				 */
				char	*args = nm + 1,
						*arguments = htdb_parse_token(args, arg_length, strlen(args));

				if (!strstr(arguments, HTDB_TOKEN_BEGIN)) {
					/*
						if the argument contains
						any un-expanded macro expansions, we
						will NOT call the function at this
						time - otherwise, the function
						would act upon the (probably)
						un-expanded macro definition,
						which is most probably not what
						is desired.
					 */
					char	*val = htdb_scriptFunction(bb, arguments);
					thing_append(b, val, FUNC_CALCULATES_SIZE, BUF_LIMIT_TINY);
					*error = (val && *val && val == strstr(val, GENERIC_FAILURE_STRING)) ? BOOL_TRUE : BOOL_FALSE;
					*error |= !(val && *val) ? BOOL_TRUE : BOOL_FALSE;
					free(val);
					found = BOOL_TRUE;
				}
				free(arguments);
			} else {
				char	*(*funcPtr)(char *) = fetchNamedDSO(nmp);

				/*
					call the function.
				 */
				if (funcPtr && *funcPtr) {
					char	*args = nm + 1,
							*arguments = htdb_parse_token(args, arg_length, strlen(args));

					if (!strstr(arguments, HTDB_TOKEN_BEGIN)) {
						/*
							if the argument contains
							any un-expanded macro expansions, we
							will NOT call the function at this
							time - otherwise, the function
							would act upon the (probably)
							un-expanded macro definition,
							which is most probably not what
							is desired.
						 */
						char	*val = funcPtr(arguments);
						thing_append(b, val, FUNC_CALCULATES_SIZE, BUF_LIMIT_TINY);
						/*
							ug..
						 */
						*error = (val && *val && val == strstr(val, GENERIC_FAILURE_STRING)) ? BOOL_TRUE : BOOL_FALSE;
						free(val);
						found = BOOL_TRUE;
					}
					free(arguments);
				}
			}

			/*
				done with the lookup string
			 */
			free(nmp);
		}
	}

	return found;
}

/*
	live_eval_str()

		we now have a parenthesis-clean string of the form
			TOK OPER VAL
		evaluate this string.
 */
static int	live_eval_str(char *str, int len)
{
	int	status = -1,
		bare_token = 0;
	char	*token = (char *)NULL;
	oper_t	operator;
	bool_t	error;

	{
		int		length;
		spaceData b;
		data_init(&b);

		/*
			call the defined function.
			ie: if (func(args))
			then: the callable function is=`func'
		 */
		if (BOOL_FALSE == htdb_callable(&b, str, &length, &error) ||
			error == BOOL_TRUE) {
#if	0
			/*
				we found something of the form:

					#live if(x) or
						  if(x OPR y) or
						  if(x->y OPR y)

				we will essentially treat this as a shortcut for:

					#live if (getval(x)) etc
			 */
			bare_token = 1;
			/*
				walk until the end of the token
				we've got to be able to parse
					(x)
					x
					x->x
				as tokens
			 */
			str = htdb_stripWhitespace(str);
			{
				int	len = strlen(str),
					i,
					idx = 0,
					done = 0;
				token = strdup(str);

				for (i = 0 ; done == 0 && i < len ; i++) {
					char	c = str[i];
					/*
						hit an operator?
					 */
					if (c == '>') {
						if (idx > 0 && token[idx - 1] != '-')
							done = 1;
					} else if (isspace((int)c) || c == '<' || c == '=') {
						done = 1;
					}
					if (!done)
						token[idx++] = c;
				}
				token[idx] = '\0';
				str += i;
				/*
					now we need to evaluate the token
				 */
				{
					char	*hold = token,
							*tmp = htdb_getval(token);
					free(hold);
					token = strdup(tmp);
				}
			}
#else
			free(b.value);
			return 0;
#endif
		}

		if (bare_token == 0) {
			/*
				we will set the `token' to be the result of
				our called function.
				ie: if (func(args))
				then: token=`args'
			 */
			token = b.value;

			/*
				does the token even exist?
			 */
			if (token && !(*token)) {
				free(token);
				if (BOOL_FALSE == error)
					return 1;
				else
					return 0;
			}

			/*
				move the string pointer past the `(args)'
				..so str is pointing to the character *after* the matching `)'
			 */
			{
				char	*c = strchr(str, '(');
				str += length + (c - str) + 2;
			}
		}
	}

	/*
		eat before the operator
	 */
	while (*str && isspace((int)*str))
		str++;

	/*
		get the operator
	 */
	if ((str && *str == '!') && ((str + 1) && *(str + 1) == '=')) {
		operator = OPER_NE;
		str += 2;
	} else if (str && *str == '=') {
		if ((str + 1) && *(str + 1) == '=') {
			operator = OPER_EQEQ;
			str += 2;
		} else {
			operator = OPER_EQ;
			str += 1;
		}
	} else if (str && *str == '<') {
		if ((str + 1) && *(str + 1) == '=') {
			operator = OPER_LEQ;
			str += 2;
		} else {
			operator = OPER_LT;
			str += 1;
		}
	} else if (str && *str == '>') {
		if ((str + 1) && *(str + 1) == '=') {
			operator = OPER_GEQ;
			str += 2;
		} else {
			operator = OPER_GT;
			str += 1;
		}
	} else {
		/*
			no operator.
			does this token exist and point to anything?
			ie: `if (getval(token))'
		 */
		if (token && *token) {
			free(token);
			return 1;
		} else {
			free(token);
			return 0;
		}
	}

	/*
		eat before the comparison string
	 */
	while (*str && isspace((int)*str))
		str++;

	/*
	if (strchr(str, '(')) {
		bool_t	error;
		int		length;
		spaceData b;
		data_init(&b);
		if (BOOL_FALSE == htdb_callable(&b, str, &length, &error)) {
		}
		str = b.value;
		data_destroy(&b);
	}
	*/
	{
		char	*expanded = htdb_expand(token),
				*comp_str = htdb_expand(str);
		if (isStringNumeric(expanded) && isStringNumeric(comp_str)) {
			/*
				numeric context
			 */
			double	eval_val = (double)atof(expanded),
					comp_val = (double)atof(comp_str);

			switch (operator) {
				case OPER_EQ:
				case OPER_EQEQ:
					/*
						for numbers, `=' and `==' are equivalent
					 */
					status = (eval_val == comp_val) ? 1 : 0;
				break;

				case OPER_LEQ:
					status = (eval_val <= comp_val) ? 1 : 0;
				break;

				case OPER_GEQ:
					status = (eval_val >= comp_val) ? 1 : 0;
				break;

				case OPER_LT:
					status = (eval_val < comp_val) ? 1 : 0;
				break;

				case OPER_GT:
					status = (eval_val > comp_val) ? 1 : 0;
				break;

				default:
					/*
						unknown operator..
						fall back to not equal
					 */
					status = (eval_val != comp_val) ? 1 : 0;
			}
		} else {
			/*
				string context
			 */
			switch (operator) {
				case OPER_EQ:
					/*
						exact match
					 */
					status = (0 == strcasecmp(expanded, comp_str)) ? 1 : 0;
				break;

				case OPER_EQEQ:
					/*
						"contains" match
					 */
					status = (NULL == strcasestr(expanded, comp_str)) ? 0 : 1;
				break;

				case OPER_LEQ:
					status = (strcasecmp(expanded, comp_str) <= 0) ? 1 : 0;
				break;

				case OPER_GEQ:
					status = (strcasecmp(expanded, comp_str) >= 0) ? 1 : 0;
				break;

				case OPER_LT:
					status = (strcasecmp(expanded, comp_str) < 0) ? 1 : 0;
				break;

				case OPER_GT:
					status = (strcasecmp(expanded, comp_str) > 0) ? 1 : 0;
				break;

				default:
					/*
						unknown operator..
						fall back to not equal
					 */
					status = (NULL == strcasestr(expanded, comp_str)) ? 1 : 0;
			}
		}
		free(comp_str);
		free(expanded);
	}
	free(token);
	return status;
}

/*
	live_bump_paren()

		bump our pointer along until the next matching parenthesis
 */
static void	live_bump_paren(char **str)
{
	int	paren_count = 1;

	while (*str && **str && paren_count >= 1) {
		if (**str == '(')
			paren_count++;
		else if (**str == ')')
			paren_count--;
		(*str)++;
	}
}

/*
	htdb_parse_paren()

		evaluate the string until the next matching parenthesis
 */
static int	htdb_parse_paren(char **str)
{
	int	paren_count = 1,
		i = 0,
		last_expr,
		len = (*str && *(*str) ? strlen(*str) : 0),
		eval_indx = 0;
	char	*eval_str = (char *)malloc(sizeof(char) * (len + 1));

	while (i < len && paren_count >= 1) {
		if (*(*str) == '(')
			paren_count++;
		else if (*(*str) == ')')
			paren_count--;
		eval_str[eval_indx++] = *(*str);
		(*str)++;
		len--;
	}
	if (eval_indx < 1)
		/*
			prevent underflow in case of syntax problems
		 */
		eval_indx = 1;
	eval_str[eval_indx - 1] = '\0';
	last_expr = live_eval_str(eval_str, eval_indx);
	free(eval_str);
	return last_expr;
}

/*
	live_clear_iflevels()

		we can have `if' levels up to MAX_IF_DEPTH deep.
		initialise these levels.
 */
static void	live_clear_iflevels(int recurse_level)
{
	int	i;

	/*
		clear the if levels
	 */
	for (i = 0 ; i < MAX_IF_DEPTH ; i++)
		if_values[recurse_level][i] = 1;
}

/*
	live_scan_expr()

		process this line of live code
 */
static void	live_scan_expr(char *str, int len)
{
	if (len >= 2 && strncmp(str, "if", 2) == 0) {
		int	*depth = &(if_depth[if_recurse_level]);
		/*
			increase the if level
		 */
		if (++(*depth) >= MAX_IF_DEPTH - 1)
			/*
				maxxed-out the if levels!
			 */
			*depth = MAX_IF_DEPTH - 1;
		if (*depth > 0 &&
			if_values[if_recurse_level][*depth - 1] == 1)
			/*
				is this expression true?
			 */
			if_values[if_recurse_level][*depth] =
				live_eval_expr(&str[2]);
		else
			if_values[if_recurse_level][*depth] = -1;
	} else if (len >= 6 && strncmp(str, "elseif", 6) == 0) {
		int	*depth = &(if_depth[if_recurse_level]),
			*value = &(if_values[if_recurse_level][*depth]);
		if (*depth > 0 &&
			if_values[if_recurse_level][*depth - 1] == 1) {
				if (*value == 0)
					/*
						if the previous expression was
						false then evaluate this
						expression
					 */
					*value = live_eval_expr(&str[6]);
				else
					/*
						mark this level
						as already parsed
					 */
					*value = -1;
		} else
			*value = -1;
	} else if (len >= 7 && strncmp(str, "else if", 7) == 0) {
		int	*depth = &(if_depth[if_recurse_level]),
			*value = &(if_values[if_recurse_level][*depth]);
		/*
			alternate version of `elseif'...
		 */
		if (*depth > 0 &&
			if_values[if_recurse_level][*depth - 1] == 1) {
				if (*value == 0)
					/*
						if the previous expression was
						false then evaluate this
						expression
					 */
					*value = live_eval_expr(&str[7]);
				else
					/*
						mark this level
						as already parsed
					 */
					*value = -1;
		} else
			*value = -1;
	} else if (len >= 4 && strncmp(str, "else", 4) == 0) {
		int	*depth = &(if_depth[if_recurse_level]),
			*value = &(if_values[if_recurse_level][*depth]);
		/*
			if the previous expression was false
			then force to use this else
		 */
		if (*value == 0)
			*value = 1;
		else
			/*
				mark this level as already parsed
			 */
			*value = -1;
	} else if (len >= 5 && strncmp(str, "endif", 5) == 0) {
		int	*depth = &(if_depth[if_recurse_level]);
		/*
			decrease the if level
		 */
		if (--(*depth) < 0)
			/*
				minned-out the if levels!
			 */
			(*depth) = 0;
		if ((*depth) == 0)
			/*
				re-set all our if level markers
			 */
			live_clear_iflevels(if_recurse_level);
	}
}

/*
	live_matches()
		give a string of form "#live\s+foo[(\s\0]", tell us if it's of a special type

	returns a pointer to AFTER the match
	(actually, no one really pays attention to the return pointer).
*/
char *live_matches(char *live, const char *match)
{
	live += strlen("#live");
	
	while(live && *live && isspace(*live))
		live++;

	if ( live && *live && strncasecmp(live, match, strlen(match)) != 0 )
		return NULL;

	/* make sure it's not #live	MATCH_foo if we're looking for MATCH */
	live += strlen(match);

	if ( live && (isspace(*live) || *live == '(' || !*live ))
		return live;
	else 
		return NULL;
}

/*
	match_loopend()

		given a buffer that starts with the next line after the first 
		#live [while|loop](), return a pointer at the (matching)
		#live [endwhile|endloop]
 */
static char	*match_loopend(char *in, const char *begtok, const char *endtok)
{
	/*
		actually, this needs to find the *matching* endwhile,
		not just the first one we find..  once this is done,
		nested while's should work..
		as it is, exactly 1 nested while loop works.
	 */
	int level=0;

	char *live, *endmatch;
	
	while ( (live = strstr(in, "\n#live")) ) {
		live++;
		endmatch = live_matches(live, endtok);

		if ( endmatch ) { 
			if ( level ) {
				level--;
				in = live + strlen("#live");
			} else
				return live;
		} else {
			if ( live_matches(live, begtok) )
				level++;

			in = live + strlen("#live");
		}
	}
	return NULL; 
}


#define	SYNTAX_LOOP	"<h1>SYNTAX:<br>\\#\\live loop (variable, start, stop [, step])<br>...<br>\\#\\live endloop</h1>"

/*
	live_loop()

		supports the
			#live	loop(X, Y, Z)
		construct
 */
static int	live_loop(char *str, Bucket b)
{
	int	body_bytes;
	char	*body;
	int	this_index;
	char	**args,
			*sos,
			*eos,
			*var = "",
			*eol = strchr(str, '\n');
	int		beginat = 0,
			stopat = 0,
			step = 1,
			num_args;

	if (eol) 
		/*
			clear the EOL so the parse works right
		 */
		*eol = '\0';

	num_args = htdb_splitArgs(str, ",", &args, 4);

	if (num_args >= 3) {
		char	*ptr = strchr(args[0], '(');
		var = ptr + 1;
		ptr = htdb_expand(var);
		free(args[0]);
		var = args[0] = ptr;
		ptr = htdb_expand(args[1]);
		beginat = atoi(ptr);
		free(ptr);
		ptr = htdb_expand(args[2]);
		stopat = atoi(ptr);
		free(ptr);
		if (num_args == 4) {
			/*
				we have a step value
			 */
			ptr = htdb_expand(args[3]);
			step = atoi(ptr);
			if (0 == step)
				step = 1;
			free(ptr);
		}
	} else {
		thing_append(b, SYNTAX_LOOP, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		return (strchr(str, '\n') - str) + 1;
	}

	if (eol)
		/*
			reset the EOL
		 */
		*eol = '\n';

	sos =  eol;
	eos =  match_loopend(sos, "loop", "endloop");

	/* skip the newline. */
	sos++;

	if (sos == NULL || eos == NULL) {
		thing_append(b, SYNTAX_LOOP, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		return (strchr(str, '\n') - str) + 1;
	}
	/*
		read until the "endloop" into a buffer
	 */
	body_bytes = (eos - sos);
	body = (char *)malloc(sizeof(char) * (body_bytes + 1));
	strncpy(body, sos, body_bytes);
	body[body_bytes] = '\0';
	if (beginat <= stopat) {
		/*
			incremental loop
		 */
		for (this_index = beginat ; this_index <= stopat ; this_index += step) {
			htdb_setint(var, this_index);
			htdb_parseBuffer(body, b);
		}
	} else if (step < 0) {
		/*
			decremental loop.
			note that decrement looping will only occur if the "step"
			parameter is used and if it is negative.  this will prevent
			undefined endpoint variables from causing an unexpected
			decrement loop where an incremental loop was desired (and expected!).
		 */
		for (this_index = beginat ; this_index >= stopat ; this_index += step) {
			htdb_setint(var, this_index);
			htdb_parseBuffer(body, b);
		}
	}
	free(body);
	
	htdb_deleteArgs(args, num_args);

	/* skip the #live endloop line */	
	while ( eos && *eos && *eos != '\n')
		eos++;
#if	0
	/* skip the newline, if it exists */
	if ( *eos ) 
		eos++;
#endif		
	return eos - sos;
}



#define	SYNTAX_WHILE	"<h1>SYNTAX:<br>\\#\\live while (x = y[*]z)<br>...<br>\\#\\live endwhile</h1>"

/*
	live_while()

		supports ths
			#live	while (x = X[*]Y)
		construct.
 */
static int	live_while(char *str, spaceData *b)
{
	char	*var = (char *)NULL,
			*body = (char *)NULL,
			*preSplat = (char *)NULL,
			*postSplat = (char *)NULL,
			*sos = strchr(str, '\n'),
			*endparen = strchr(str, ')'),
			*eos = (sos && *sos ? match_loopend(sos, "while", "endwhile") : NULL);
	int_t	return_bytes;

	if (sos == NULL || eos == NULL || endparen == NULL || endparen > sos) {
		/*
			either missing end paren or endwhile.
			spew and let's get out of here.
		 */
		thing_append(b, SYNTAX_WHILE, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		return (strchr(str, '\n') - str) + 1;
	}

	/*
		walk past the newline
	 */
	sos++;

	/*
		read until the "endwhile" into a buffer.
		this is the body of code that we will iterate over.
	 */
	{
		int_t	body_bytes = (eos - sos);

		body = (char *)malloc(sizeof(char) * (body_bytes + 1));
		strncpy(body, sos, body_bytes);
		body[body_bytes] = '\0';

	
		/* skip the #live endwhile line */	
		while ( eos && *eos && *eos != '\n')
			eos++;

		/* skip the newline, if it exists */
		if ( *eos ) 
			eos++;

		return_bytes = eos - sos;
	}

	/*
		parse out:
			#live while (<var> = <preSplat>*<postSplat>)
	 */
	{
		char	*dupline = malloc(sizeof(char) * ((endparen - str) + 1));

		strncpy(dupline, str, (endparen - str));
		dupline[endparen - str] = '\0';

		{
			char	**eq_args;
			int		eq_num_args = htdb_splitArgs(strchr(dupline, '(') + 1, "=", &eq_args, 2);
			bool_t	goodparse = BOOL_FALSE;

			if (eq_num_args == 2) {
				char	**splat_args;
				int		splat_num_args = htdb_splitArgs(eq_args[1], "*", &splat_args, 2);

				if (splat_num_args == 1 || splat_num_args == 2) {
					var = strdup(eq_args[0]);
					preSplat = htdb_expand(splat_args[0]);

					if (splat_num_args == 2)
						/*
							args==2:
								splat was embedded within the string:
								'thing_*_rest'
						 */
						postSplat = htdb_expand(splat_args[1]);
					else
						/*
							args==1:
								splat was at the end of the string:
								'thing_*'
						 */
						postSplat = strdup("");

					goodparse = BOOL_TRUE;
				}
				htdb_deleteArgs(splat_args, splat_num_args);
			}
			htdb_deleteArgs(eq_args, eq_num_args);
			if (BOOL_FALSE == goodparse) {
				/*
					either missing equals or splat.
					cleanup, spew and let's get out of here.
				 */
				thing_append(b, SYNTAX_WHILE, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
				if (dupline)
					free(dupline);
				if (body)
					free(body);
				if (preSplat)
					free(preSplat);
				if (postSplat)
					free(postSplat);
				if (var)
					free(var);

				return return_bytes;
			}
		}
		if (dupline)
			free(dupline);
	}

	/*
		if the string was of the form:
			thing[*]->blah
		then we will see if a value exists for
			thing->numResults
		and we will use that for an upper value
		for the while loop.  this makes sense
		for preventing "off the end" conditions
		when a query with fewer results has been
		performed over a prior query in the same
		namespace that had more reults.

		discuss.
		
		// Ben, totally off-topic late at night
		Seems to me that the "*" operator implies
		"give me anything that matches this"
		so foo[bar], if it exists, would return a value

		I understand that this would require a full
		tree-walk at this point... totally ineffecient
		but something to think about for the days of 
		ternary trees
		
		hmm... that would allow us to fake associative arrays
		in htdb.  Unfortunately, I really can't think of a decent 
		use for them in the context... one could step through the rows
		in a result set
		
		#live	while(y = resultSet[*]->id)
		#live	while(x = foo[${y}]->*)
			${foo[${y}]->${x}}
		#live	endwhile
		#live	endwhile
	
		useless?  Maybe.
	
		(incidentally, I agree with the ->numResults logic)	
	 */
	{
		int	numResults = 0;
		{
			char	*lb = strchr(preSplat, '[');
			if (lb && *lb) {
				char	*buh = strdup(preSplat);
				buh[strlen(buh) - 1] = '\0';
				numResults = htdb_getptrint(buh, "numResults");
				free(buh);
			}
		}

		{
			int	this_index,
				found = 1;
			/*
				now, iterate over the body until we can no longer
				find the target variable set in the environment for
				the current index value.
			 */
			for (this_index = 1 ; found == 1 ; this_index++) {
				char	lookup[BUF_LIMIT_SMALL + 1];

				snprintf(lookup, BUF_LIMIT_SMALL, "%s%d%s",
					preSplat, this_index, postSplat);
				lookup[BUF_LIMIT_SMALL] = '\0';

				if (htdb_checkval(lookup)) {
					/*
						the loop value exists..
						set the index variable and
						expand the body into the output buffer.
					 */
					htdb_setint(var, this_index);
					htdb_parseBuffer(body, b);
				} else
					/*
						we're done.
					 */
					found = 0;

				if (numResults > 0 && this_index >= numResults)
					/*
						force out of while if we've iterated past an
						object's "numResults" value - but only if this
						is a true htdb object with a "numResults" value
					 */
					found = 0;
			}
		}
	}
	if (body)
		free(body);
	if (preSplat)
		free(preSplat);
	if (postSplat)
		free(postSplat);
	if (var)
		free(var);

	return return_bytes;
}

/*
	htdb_expandInternal()
		given a string buffer, return a DYNAMIC string buffer
		with any resolvable ${} strings expanded/evaluated.
		if we cannot resolve a particular expansion (due to unexpanded
		dependencies), then we leave it in-place for a subsequent call.

		it is up to the caller to free the value returned..
 */
static void	htdb_expandInternal(spaceData *b, char *in, bool_t doLives)
{
	char	*ptr,
			*str_dup;
	int		force_done = 0;

	if (in && *in && !strchr(in, '#') && !strstr(in, HTDB_TOKEN_BEGIN)) {
		/*
			if the incoming string contains nothing that can be expanded,
			then we just return the string duplicated immediately..
		 */
		b->value = strdup(in);
		b->size = b->limit = strlen(b->value);
		return;
	}

	data_init(b);

	if (in == NULL || *in == '\0')
		return;

	if (BOOL_TRUE == doLives && strstr(in, "#live")) {
		spaceData	bb;
		data_init(&bb);
		htdb_parseBuffer(in, &bb);
		str_dup = (char *)bb.value;
	} else
		str_dup = strdup(in);

	ptr = &str_dup[0];

	while (ptr && *ptr) {
		char	*beg = strstr(ptr, HTDB_TOKEN_BEGIN),
				*end;

		if (beg) {
			int	pair = 1,
				realLength = 0,
				length, 
				is_func = 0;
			if (beg != ptr)
				/*
					add the stuff before the match
				 */
				thing_append(b, ptr, (beg - ptr), BUF_LIMIT_TINY);
			/*
				walk past `HTDB_TOKEN_BEGIN'
			 */
			ptr = beg + 2;

			/*
				find the matching 'HTDB_TOKEN_END'
			 */
			{
				char	*hump = ptr;
				while (hump && *hump && pair > 0) {
					if ( pair == 1 && *hump == HTDB_TOKEN_OPENCALLABLE )
						is_func = 1;	

					if (*hump == HTDB_TOKEN_BEGIN_ONE &&
						(*(hump + 1) == HTDB_TOKEN_BEGIN_TWO))
						pair++;
					else if ((!is_func || pair > 1) && *hump == HTDB_TOKEN_END)
						pair--;
					else if (is_func && pair == 1 && *hump == HTDB_TOKEN_CLOSECALLABLE) {
						hump++; realLength++;
						while ( *hump && isspace(*hump) ) { hump++; realLength++;}
						if ( *hump == HTDB_TOKEN_END )
							pair--; 
						else {
							/* not the end function token, back off */
							hump--; realLength--;
						}
	
					}
	
					hump++;
					realLength++;
				}
				end = hump;
				realLength--;
			}
			length = realLength;
			if (pair == 0 && (length > 0)) {
				/*
					prepare to lookup this variable
				 */
				bool_t	error;
				char	*lookup = (char *)NULL,
						*key = (char *)malloc
						(sizeof(char) * (length + 1));
				int		arg_length;

				strncpy(key, ptr, length);
				key[length] = '\0';

				if (strchr(key, HTDB_TOKEN_BEGIN_ONE)) {
					/*
						needs to be expanded further
					 */
					spaceData	bb;
					htdb_expandInternal(&bb, key, doLives);
					free(key);
					key = (char *)bb.value;
					length = bb.size;
				}

				if (BOOL_TRUE == htdb_callable(b, key, &arg_length, &error)) {
					/*
						a DSO/builtin function was called.
					 */
				} else if ((lookup = htdb_value(key)) == NULL) {
					/*
						variable not found; add to the buffer
						so that it is caught later
					 */
					thing_append(b, HTDB_TOKEN_BEGIN, HTDB_TOKEN_BEGIN_LENGTH, BUF_LIMIT_TINY);
					thing_append(b, key, length, BUF_LIMIT_TINY);
					thing_appendChar(b, HTDB_TOKEN_END, BUF_LIMIT_TINY);
				} else {
					/*
						variable found;
					 */
					if (lookup && *lookup) {
						if (strchr(lookup, '#') || strstr(lookup, HTDB_TOKEN_BEGIN)) {
							/*
								this looked-up segment
								contains a possible
								expansion; do a
								recursive substitution
							 */
							spaceData	bb;
							htdb_expandInternal(&bb, lookup, doLives);
							thing_append(b, (char *)bb.value, bb.size, BUF_LIMIT_TINY);
							data_destroy(&bb);
						} else {
							/*
								this looked-up segment
								contains no further
								expansions.
							 */
							thing_append(b, lookup, htdb_size(key), BUF_LIMIT_TINY);
						}
					}
				}
				free(key);
				/*
					walk past `HTDB_TOKEN_END'
				 */
				ptr = end;
			} else {
				/*
					no end-of-expansion character;
					we ignore this guy, but back up the pointer in case the 
					definition becomes complete later.
				 */
				ptr -= 2;
				force_done = 1;
			}
		} else
			/*
				we contain no more start-of-expansions
			 */
			force_done = 1;

		if (ptr && *ptr && force_done == 1) {
			/*
				no more substitutions on this string;
				concatenate the remainder and exit
			 */
			thing_append(b, ptr, FUNC_CALCULATES_SIZE, BUF_LIMIT_TINY);
			break;
		}
	}

	free(str_dup);

#ifdef	USE_DELAYED_LOOKUP
	/*
		strip whitespace from start and end of this expanded thing
	 */
	{
		char	*p = (char *)b->value;
		if (isspace((int)p[0]) || isspace((int)p[b->size - 1])) {
			char	*hold = b->value,
					*ret = strdup(htdb_stripWhitespace(p));
			free(hold);
			b->value = ret;
		}
	}
#endif
}


/*
	htdb_parseBuffer()
		given a string buffer which may or may not contain HTDB script,
		recursively evaluate what can be evaluated, all the while
		shoving evaluated strings into the passed-in bucket structure.

		note that this function evaluates line-by-line.
 */
static void	htdb_parseBuffer(char *str, spaceData *b)
{
	int	eob = 0; /* end-of-buffer */
	spaceData bb, bb_tmp, running;
	
	data_init(&bb);
	data_init(&bb_tmp);
	data_init(&running);

	if (strstr(str, "#live")) {
		char	*eol = strchr(str, '\n');
		int	len = (eol && *eol ? (eol - str) : strlen(str));

		if_recurse_level++;
		/*
			initialise the if-level array to all 1's.
			this forces regular HTDB/HTML text to be written always.
			if we encounter #live "if"'s, then the current if_level
			array is set to 1 or 0, which signals whether or not
			to print out anything within that #live "if" body
		 */
		live_clear_iflevels(if_recurse_level);

		do {
			/*
				loop until out of document body text
			 */
			if (len > 5 && strncmp(str, "#live", 5) == 0) {
				/*
					this line contains #live code
					which we need to interpret.

					if there's anything built-up in the buffer, evaluate it 
					before we go on.
				 */

				if ( running.value ) {
					htdb_expandInternal(&bb_tmp, running.value, BOOL_TRUE);
					data_destroy(&running);
					
					thing_append(&bb, bb_tmp.value, bb_tmp.size, BUF_LIMIT_BIG);
					data_destroy(&bb_tmp);
				}
				
				if (if_values[if_recurse_level][if_depth[if_recurse_level]] == 1 &&
					live_matches(str, "loop")) {
					len += live_loop(str, &bb);
				} else if (if_values[if_recurse_level][if_depth[if_recurse_level]] == 1 &&
					live_matches(str, "while")) {
					len += live_while(str, &bb);
				} else {
					/*
						looks like a #live if, elseif, else line.  
						put it into a buffer and evaluate it.
					 */
					int		expr_len;
					char	*buf = htdb_parse_token(&str[5], &expr_len, len - 4);

					live_scan_expr(buf, expr_len);

					free(buf);
				}
			} else if (if_values[if_recurse_level][if_depth[if_recurse_level]] == 1) {
				/*
					we get here if either outside any #live "if" blocks,
					or if a #live "if" block evaluated to true.

					regular HTDB/HTML content, write it out
				 */

				thing_append(&running, str, len, BUF_LIMIT_BIG);

				if ( !eob ) 
					/*
						need - otherwise dynamic javascript breaks
						(look into this..)
					 */
					thing_appendChar(&running, '\n', BUF_LIMIT_BIG);
			}
			/*
				SOMEWHERE in the next few lines of code lies THE BUG.

				THE BUG is the years-old bastard that:
					1) causes null-followed "while" and "loop"
					   blocks to crash.
					2) prevents us from delaying script evaluation
					   at print time (our ultimate goal) 
			 */
			if ( eob ) 
				break;
			/*
				walk past the just-handled line of the string
				Ben: possibility of end of string here.
			 */
			
			str += (len + 1);


			/* 
				and step past blank lines?
			*/
			while (str && (*str == '\n' || *str == '\r'))
				str++;

			/*
				and find the end of the next line
			 */
			eol = strchr(str, '\n');

			/*
				ben: parse the last line, if it exists
			*/
			if ( eol && *eol ) 
				len = eol - str;
			else {
				/* last line... go one more time and quit. */	
				eob = 1;
				len = (str && *str) ? strlen(str) : 0;
			}
		} while (str && *str && len > 0);
		if_recurse_level--;
	} else {
		thing_append(&bb, str, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	}

	if ( running.value ) {
		htdb_expandInternal(&bb_tmp, running.value, BOOL_TRUE);
		data_destroy(&running);

		thing_append(&bb, bb_tmp.value, bb_tmp.size, BUF_LIMIT_BIG);
		data_destroy(&bb_tmp);
	}

	/* 
		take one more pass at the buffer.
	*/
	if (strchr(bb.value, '#') || strchr(bb.value, HTDB_TOKEN_BEGIN_ONE)) {
		htdb_expandInternal(&bb_tmp, bb.value, BOOL_TRUE);
		free(bb.value);
		bb.value = bb_tmp.value;
		bb.limit = bb.size = bb_tmp.size;
	}

	thing_append(b, bb.value, bb.size, BUF_LIMIT_BIG);
	data_destroy(&bb);
}

/*
	pretty_html()

		this is where we finally send the built-up output buffer
		through some (optional) final filters and blat to stdout..

		filters:  we allow the `\' character to "literal" the
		immediately-following character.
		also, if there is a value for `confAntiSpam' that is not equal
		to `@', then we will convert all occurrences of `@' in
		the output buffer to whatever is the value of `confAntiSpam'.

		note that there is not insignificant overhead when these filters
		are triggered(!)
 */
static void	pretty_html(spaceData *b)
{
	char	*out = (char *)b->value;
	int	thwart_email_spiders = ((!htdb_checkval("confAntiSpam") ||
			htdb_equal("confAntiSpam", "@")) ? 0 : 1),
		have_email = (thwart_email_spiders == 1 ? (strchr(out, '@') == NULL ? 0 : 1) : 0),
		do_literalize = (htdb_equal("confLiteralize", "False") ? 0 : 1),
		have_literal = (do_literalize == 1 ? (strchr(out, '\\') == NULL ? 0 : 1) : 0);

	if (have_literal == 0 && have_email == 0) {
		/*
			nothing to translate - output right now
		 */
		fwrite(out, sizeof(char), b->size, stdout);
	} else {
		/*
			else walk the string and build up an output buffer
		 */
		spaceData	bout;
		char		*antiSpam = htdb_getval("confAntiSpam");
		int			antiSpamLen = 0,
					last_was_literal = 0;

		data_init(&bout);

		while (out && *out) {
			if (last_was_literal) {
				/*
					literal this character
				 */
				thing_appendChar(&bout, *out, BUF_LIMIT_BIG);
				last_was_literal = 0;
			} else if (*out == '@' && thwart_email_spiders) {
				if (antiSpamLen == 0)
					/*
						just calculate this once
					 */
					antiSpamLen = strlen(antiSpam);
				/*
					translate the `@' character
				 */
				thing_append(&bout, antiSpam, antiSpamLen, BUF_LIMIT_BIG);
			} else if (*out == '\\' && do_literalize)
				/*
					literal the next character
				 */
				last_was_literal = 1;
			else
				/*
					pass this char along as-is
				 */
				thing_appendChar(&bout, *out, BUF_LIMIT_BIG);

			out++;
		}

		/*
			off with ye
		 */
		fwrite((char *)bout.value, sizeof(char), bout.size, stdout);

		data_destroy(&bout);
	}
	fflush(stdout);
}

/***********************************************************************

	begin public functions

 ***********************************************************************/

/*
	live_eval_expr()

		given a string containing one or more expressions,
		return the logical glob of those expressions.

		we are down with recursion.
 */
int	live_eval_expr(char *str)
{
	int	last_expr = 1;

	if (!(str))
		/*
			bad syntax
		 */
		return -1;

	while (isspace((int)*str))
		/*
			remove leading whitespace
		 */
		str++;

	if (*str == '!') {
		if ((last_expr = live_eval_expr(&str[1])) == -1)
			/*
				pass back the bad syntax error code
			 */
			return -1;
		last_expr = !last_expr;
	} else if (*str == '(') {
		/*
			nested.. recurse
		 */
		int	new_expr = live_eval_expr(&str[1]);
		if (new_expr == -1)
			/*
				pass back the bad syntax error code
			 */
			return -1;
		last_expr &= new_expr;
	} else {

		/*
			process the first expression we run into
		 */
		last_expr = htdb_parse_paren(&str);
		if (last_expr == -1)
			return -1;

		do {
			int	new_expr,
				have_not = 0;	/* TODO: does this belong outside the do loop?? */
			/*
				eat space
			 */
			while (str && *str && isspace((int)*str))
				str++;

			/*
				walk through the remainder until
				we are out of string to process.
			 */
			if (str && *str) {
				if (strncmp(str, "||", 2) == 0) {
					/*
						we have an expression to OR
					 */
					str += 2;
					new_expr = live_eval_expr(str);
					if (new_expr == -1)
						return -1;
					if (have_not == 1) {
						/*
							invert da result
						 */
						new_expr = !new_expr;
						/* DEAD ASSIGNMENT have_not = 0; */
					}
					/*
						OR this result with previous
					 */
					last_expr |= new_expr;
					/*
						move to the end of this expr..
					 */
					live_bump_paren(&str);
				} else if (strncmp(str, "&&", 2) == 0) {
					/*
						we have an expression to AND
					 */
					str += 2;
					new_expr = live_eval_expr(str);
					if (new_expr == -1)
						return -1;
					if (have_not == 1) {
						/*
							invert da result
						 */
						new_expr = !new_expr;
						/* DEAD ASSIGNMENT have_not = 0; */
					}
					/*
						AND this result with previous
					 */
					last_expr &= new_expr;
					/*
						move to the end of this expr..
					 */
					live_bump_paren(&str);
				} else if (*str == '!') {
					/*
						negate the next expression
					 */
					/* DEAD ASSIGNMENT have_not = 1; */
					str++;
				} else
					/*
						eat anything else
					 */
					str++;
			}
		} while (str && *str);
	}

	return last_expr;
}

char	*live_getval(char *token)
{
	char	*ret = GENERIC_FAILURE_STRING;

	if (htdb_checkval(token))
		ret = htdb_getval(token);

	return strdup(ret);
}

/*
	thing_append()
		the heart of darkness.
		we grow a current definition by a specified buffer size.
		we use the spaceData type for handy storage.
 */
void	thing_append(spaceData *b, char *expanded, int length, int growby)
{
	if (length >= FUNC_CALCULATES_SIZE) {
		int	oldsize = b->size,
			newsize = (length == FUNC_CALCULATES_SIZE ?
				(expanded && *expanded ? (int)strlen(expanded) : 0) :
				length),
			big = (newsize > growby ? newsize : growby);

		if (newsize == 0)
			/*
				no need..
			 */
			return;

		if ((oldsize + newsize) >= b->limit) {
			/*
				grow the buffer
			 */
			b->limit += big;
			b->value = (char *)
				realloc((char *)b->value,
					((b->limit + 1) * sizeof(char)));
		}

		/*
			update the running length count
			by the length of the incoming string.
		 */
		b->size += newsize;

		/*
			and append the new string to the old
		strncat((char *)b->value, expanded, newsize);
		 */
		memcpy(&((char *)b->value)[oldsize], expanded, newsize);
		memset(&((char *)b->value)[oldsize + newsize], 0, 1);
	}
}

/*
	thing_appendChar
		single-character version of thing_append()
 */
void	thing_appendChar(spaceData *b, char expanded, int growby)
{
	int	oldsize = b->size,
		newsize = 1,
		big = (newsize > growby ? newsize : growby);

	if ((oldsize + newsize) >= b->limit) {
		/*
			grow the buffer
		 */
		b->limit += big;
		b->value = (char *)
			realloc((char *)b->value,
				((b->limit + 1) * sizeof(char)));
	}

	/*
		update the running length count by one.
	 */
	b->size += newsize;

	/*
		append the character to the buffer.
	 */
	((char *)b->value)[b->size - 1] = expanded;

	/*
		and terminate the buffer.
	 */
	((char *)b->value)[b->size] = '\0';
}

/*
	htdb_expand()
		given a string buffer, return a DYNAMIC string buffer
		with any resolvable ${} strings expanded/evaluated.
		if we cannot resolve a particluar expansion (due to unexpanded
		dependencies), then we leave it in-place for a subsequent call.

		it is up to the caller to free the value returned..
 */
char	*htdb_expand(char *str)
{
	spaceData	bb;
	htdb_expandInternal(&bb, str, BOOL_TRUE);
	return (char *)bb.value;
}

char	*htdb_barewordexpand(char *str)
{
	spaceData	bb;
	htdb_expandInternal(&bb, str, BOOL_FALSE);
	return (char *)bb.value;
}

/*
	htdb_parse()
		take the given string and perform any necessary
		live processing before printing it out
 */
void	htdb_parse(char *str)
{
	spaceData b;

	data_init(&b);

	/*
		parse this str for #live content
	 */
	htdb_parseBuffer(str, &b);

	/*
		send it to its final resting place (stdout)
	 */
	pretty_html(&b);

	/*
		and forget about it
	 */
	free(b.value);
}

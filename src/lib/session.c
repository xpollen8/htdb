#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

/********************************************************************************

	begin private functions

 ********************************************************************************/

/*
	fetchBrowserID()

		given a user_agent string, return either an existing
		browser_id from the database, or insert a new record
		and return that browser_id.
 */
static int_t	fetchBrowserID(char *user_agent)
{
	int_t	browser_id = 0;

	if (user_agent && *user_agent) {
		char	*tmp = strdup(user_agent);

		if (strlen(tmp) > 79)
			tmp[79] = '\0';

		if (dbGetRowByKey("browser",
			"select browser_id, user_agent, isrobot "
			"from browser where user_agent = '%s' "
			"and isdeleted = 'F' limit 1",
			tmp)) {
			browser_id = htdb_getint("browser->browser_id");
			dbUpdate("update LOW_PRIORITY browser set "
				"num = num + 1 "
				"where browser_id = %ld",
				browser_id);
		} else {
			browser_id = dbInsert("insert into browser set "
				"browser_id = NULL, "
				"user_agent = '%s'",
				tmp);
		}
		free(tmp);
	}
	return browser_id;
}

/*
	fetchDomainID()

		return the domain_id for a given server name string,
		or insert a new record and return that domain_id.

		folds "X.COM" and "WWW.X.COM" into the same domain_id.
 */
static int_t	fetchDomainID(char *server)
{
	int_t	domain_id = 0;

	if (server && *server) {
		char	*tmp = strdup(server);

		/*
			here's the thing.  its pretty damned annoying that
			`www.thing.com' and `thing.com' normally map to two
			different domain ids.  this confuses the page hit and
			feedback systems wherein there are "parallel" hit counts
			and feedback for what would appear should be the same page.

			so what we do is..

			if we don't find TWO (2) `.' characters in the incoming
			server name, then we will prepend the lookup string
			with "www."
		 */
		{
			char	*p1 = strchr(tmp, '.'),
				*p2 = (p1 && *p1 ? strchr(p1 + 1, '.') : NULL);
			if (!(p2 && *p2)) {
				char	buf[BUF_LIMIT_SMALL + 1];
				snprintf(buf, BUF_LIMIT_SMALL, "www.%s", tmp);
				buf[BUF_LIMIT_SMALL] = '\0';
				free(tmp);
				tmp = strdup(buf);
			}
		}

		if (strlen(tmp) > 29)
			tmp[29] = '\0';

		if (dbGetRowByKey("domain",
			"select * "
			"from domain where server = '%s' and isdeleted = 'F' limit 1",
			tmp)) {
			domain_id = htdb_getint("domain->domain_id");
		} else {
			domain_id = dbInsert("insert into domain set "
				"domain_id = NULL, "
				"server = '%s'",
				tmp);
		}
		free(tmp);
	}
	return domain_id;
}

/*
	fetchRefererID()

		given a server referer string, return the existing referer_id,
		or insert a new record and return that referer_id.
 */
static int_t	fetchRefererID(char *http_referer, int_t domain_id)
{
	int_t	referer_id = 0;

	if (http_referer && *http_referer) {
		char	*tmp = strdup(http_referer);

		if (strlen(tmp) > 119)
			tmp[119] = '\0';

		if (dbGetRowByKey("referer",
			"select referer_id, domain_id, url "
			"from referer where domain_id = %ld and url = '%s' "
			"and isdeleted = 'F' limit 1",
			domain_id, tmp)) {
			referer_id = htdb_getint("referer->referer_id");
			dbUpdate("update LOW_PRIORITY referer set "
				"num = num + 1 "
				"where referer_id = %ld",
				referer_id);
		} else {
			referer_id = dbInsert("insert into referer set "
				"referer_id = NULL, "
				"domain_id = %ld, "
				"url = '%s'",
				domain_id,
				tmp);
		}
		free(tmp);
	}
	return referer_id;
}

/*
	processReferer()

		basic logic: if the HTTP_REFERER does not contain our
		server's name, then the refer ID string had *better* be
		what our list says it should be.  if it is not,
		then we force it to be re-generated later.
 */
static void	processReferer(user_t *userinfo)
{
	char	*referer = htdb_getval("cgi->http_referer"),
			*server = htdb_getval("cgi->server_name");

	if (*referer && *server && strstr(referer, server))
		/*
			the server's namestring is contained within
			the referer string; we only moved intra-site.
			no need to determine the referer_id
		 */
		return;
	else if (*referer)
		/*
			else, we just arrived from offsite.
			determine the referer_id
		 */
		userinfo->referer_id = fetchRefererID(referer, userinfo->domain_id);
}

/*
	handleLogoff()

		this is how we log 'em off.
		simply clear the user_id field of the session table.
 */
static void	handleLogoff(user_t *userinfo)
{
	userinfo->user_id = 0;
	dbUpdate("update session set "
		"user_id = %ld "
		"where session_id = %ld",
		userinfo->user_id,
		userinfo->session_id);
}

/*
	processUserLogin()

		given a userinfo struct, perform either a logoff,
		a login or a transparent (session-authenticated) login.
		called per page request.
 */
static void	processUserLogin(user_t *userinfo)
{
	htdb_setval("user->authenticated", "no");

	if (userinfo->session_id != 0) {
		bool_t	loggedIn = BOOL_FALSE;

		if (htdb_checkval("logoff")) {
			handleLogoff(userinfo);
		} else if (htdb_checkval("email") && htdb_checkval("password")) {
			/*
				initial login
				attempt to find by email address.
			 */
			int	found = dbGetRowByKey("user",
				"select * from user where "
				"email = '%s' and password = '%s' "
				"and isdeleted = 'F' limit 1",
				htdb_getval("email"),
				htdb_getval("password"));
				
			if (found != 1)
				/*
					fall back to screenname check
				 */
				found = dbGetRowByKey("user",
					"select * from user where "
					"screenname = '%s' and password = '%s' "
					"and isdeleted = 'F' limit 1",
					htdb_getval("email"),
					htdb_getval("password"));
				
			if (found == 1) {
				loggedIn = BOOL_TRUE;

				/*
					log them in; update session.user_id
				 */
				dbUpdate("update session set "
					"user_id = %ld, "
					"ip='%s' "
					"where session_id = %ld",
					htdb_getint("user->user_id"),
					userinfo->ip,
					userinfo->session_id);

				/*
					and drop an entry into the user_session table
				 */
				dbInsert("insert into user_session set "
					"user_session_id=NULL, "
					"user_id=%ld, "
					"session_id=%ld, "
					"ip='%s', "
					"dtcreated=now() ",
					htdb_getint("user->user_id"),
					userinfo->session_id,
					userinfo->ip);
			} else {
				/*
					bad attempt.
					force a logoff.
				 */
				handleLogoff(userinfo);
			}
		} else if (userinfo->user_id > 0 &&
			dbGetRowByKey("user",
				"select *, s.dtcreated as sdtcreated, u.dtcreated as dtcreated "
				"from user u, session s where s.session_id = %ld and "
				"s.user_id = u.user_id and u.isdeleted = 'F' limit 1",
				userinfo->session_id) == 1) {
				loggedIn = BOOL_TRUE;
		}
		if (BOOL_TRUE == loggedIn) {
			htdb_setval("user->authenticated", "yes");
			userinfo->user_id = htdb_getint("user->user_id");
			fetchNamedUserData("user", htdb_getint("user->user_id"), "");
			fetchUserAccountTypes("user->accountype", htdb_getint("user->user_id"));
		}
		/*
			HERE.
			place logic to allow idle-log-out based upon
			a value pulled from userdata
		 */
	}
}

static char	*makeSessionKey(void)
{
	static	char	sessionkey[SESSIONKEY_LENGTH + 1];
	int	i = 0;

	for (i = 0 ; i < SESSIONKEY_LENGTH ; i++)
		sessionkey[i] = genAlnum();
	sessionkey[i] = '\0';

	return sessionkey;
}

void	htdb_sendSessionCookie(char *sessionkey)
{
	/*
		set the session cookie.
	 */
	char	theDate[BUF_LIMIT_SMALL + 1],
			*encryptedSessionKey = htdb_encryptor("be1", sessionkey);

	/*
		cookie expiration defaults to ~30 months from now.
		if `confCookieExpiration' exists, then we use that instead.
	 */
	{
		time_t	theSeconds = time(NULL);
		int_t	defaultSeconds = 30 * 30 * 24 * 3600,
				confSeconds = htdb_getint("confCookieExpiration");
		
		if (htdb_checkval("confCookieExpiration")) {
			/*
				cookie expiration is specified in the config file
			 */
			if (0 == confSeconds) {
				/*
					if we had "confCookieExpiration 0",
					then this means to NOT assign a cookie value.
					this will cause cookies to cease working once
					their browser closes.
				 */
				strcpy(theDate, "");
			} else {
				theSeconds += confSeconds;

				strftime(theDate, BUF_LIMIT_SMALL,
					"%a, %d-%b-%Y %H:%M:%S GMT", gmtime(&theSeconds));
			}
		} else {
			/*
				cookie expiration is NOT specified in the config file;
				we'll fall-back to ~30 months from now
			 */
			theSeconds += defaultSeconds;

			strftime(theDate, BUF_LIMIT_SMALL,
				"%a, %d-%b-%Y %H:%M:%S GMT", gmtime(&theSeconds));
		}
	}

	{
		char	*nm = htdb_getval("cgi->server_name");
		int		numDots = 0;

		/*
			if we find "htdb.org", then
			we need to set the cookie for all domains
			inside "htdb.org" - by prepending a
			dot to the domain name.

			if we find a fully qualified domain name,
			then the cookie will only be set for the
			FQDN in question: "www.htdb.org"
		 */
		while (nm && *nm) {
			if (*(nm++) == '.')
				numDots++;
		}

		htdb_httpHeader("Set-Cookie: "
			"session=%s; "
			"path=/; "
			"domain=%s%s"
			"%s%s",
			encryptedSessionKey,
			(numDots == 1 ? "." : ""),
			htdb_getval("cgi->server_name"),
			theDate[0] ? "; expires=" : "",
			theDate[0] ? theDate : "");
	}

	free(encryptedSessionKey);
}

/*
	fetchSessionInformation()

		if we have a cookie, validate it, else assign one.
		perform user authentication if appropriate.
		determine domain/browser/referer ids.
 */
static void	fetchSessionInformation(user_t *userinfo)
{
	/*
		if we have a cookie, make sure that the decoded cookie
		decodes properly.  sanity checking against the
		database will occur later on..
	 */
	{
		char	*cookie = strdup(htdb_getval("cgi->http_cookie")),
				*startOfSession = strstr(cookie, "session=");

		if (startOfSession) {
			char	*semi = strchr(startOfSession, ';'),
					*decryptedSessionID;

			if (semi)
				/*
					lop off after the semicolon
				 */
				*semi = '\0';

			/*
				decrypt the cookie value
				(8 characters jumps past `session=')
			 */
			decryptedSessionID = htdb_encryptor("be1", &startOfSession[8]);
			{
				/*
					cookies are randomly generated 20-character
					keys stored in the session table.
				 */
				if (decryptedSessionID && *decryptedSessionID &&
					strlen(decryptedSessionID) == SESSIONKEY_LENGTH) {
					/*
						let's be super-paranoid and remove any
						chance of SQL injection by making sure
						the decrypted session_id is database-safe.
					 */
					char	*sanitized = prepareStringForDatabase(decryptedSessionID);

					sstrncpy(userinfo->sessionkey, sanitized);
					free(sanitized);
				}
			}
			free(decryptedSessionID);
		}
		free(cookie);
	}

	if (userinfo->sessionkey[0] != '\0' &&
		htdb_checkval("logout")) {
		/*
			this is how we log 'em off - the hard way.
			this will remove the session from the database.
		 */
		dbDelete("delete from session where "
			"sessionkey = '%s'",
			userinfo->sessionkey);
		userinfo->user_id = 0;
		userinfo->session_id = 0;
		sstrncpy(userinfo->sessionkey, "");
	}

	/*
		does the incoming session id exist in the database?
	 */
	if (userinfo->sessionkey[0] != '\0' &&
		dbGetRowByKey("session",
			"select session_id, referer_id, domain_id, user_id, "
			"affiliate_id, browser_id, ip "
			"from session where sessionkey = '%s' limit 1",
			userinfo->sessionkey) == 1) {
			/*
				the existing session id looks ok.
			 */
			userinfo->referer_id = htdb_getint("session->referer_id");
			userinfo->domain_id = htdb_getint("session->domain_id");
			userinfo->user_id = htdb_getint("session->user_id");
			userinfo->browser_id = htdb_getint("session->browser_id");
			userinfo->session_id = htdb_getint("session->session_id");
	} else {
		/*
			get the id for this browser
		 */
		userinfo->browser_id = fetchBrowserID(htdb_getval("cgi->http_user_agent"));

		/*
			what's in a server name?
		 */
		userinfo->domain_id = fetchDomainID(htdb_getval("cgi->server_name"));

		/*
			take care of the referer
		 */
		processReferer(userinfo);

		/*
			create the new session ID
		 */
		if (!(*htdb_getval("confUseCookieSessioning") == 'n')) {
			sstrncpy(userinfo->sessionkey, makeSessionKey());
			if ((userinfo->session_id = dbInsert("insert into session set "
					"session_id = NULL, "
					"sessionkey = '%s', "
					"referer_id = %ld, "
					"domain_id = %ld, "
					"user_id = %ld, "
					"affiliate_id = 0, "
					"browser_id = %ld, "
					"dtcreated = now(), "
					"ip = '%s'",
					userinfo->sessionkey,
					userinfo->referer_id,
					userinfo->domain_id,
					userinfo->user_id,
					userinfo->browser_id,
					userinfo->ip)) > 0) {
					htdb_sendSessionCookie(userinfo->sessionkey);
					/*
						god, i hate setting messy "globals",
						but there's just not a cleaner way to
						let the logging-end of the process know
						that a cookie was just handed-out.
					 */
					htdb_setval("cookie->wasSet", "yup");
			} else {
				/*
					insert failed
				 */
				userinfo->session_id = 0;
				sstrncpy(userinfo->sessionkey, "");
			}
		}
	}
}

/********************************************************************************

	begin public functions

 ********************************************************************************/

/*
	htdb_SetFromPathInfo()

		given an encrypted string, decode any encrypted values
		and populate the resource pointed to by `lookup' if
		appropriate.  used internally by get_db_page() to decrypt
		values embedded within the PATH_INTO string.
 */
void	htdb_SetFromPathInfo(char *lookup, char *val)
{
	char	*dec = htdb_decryptor(val);

	if (dec && *dec && (strchr(dec, '=')))
		/*
			this handles the case of:
				<cgi>/${encrypt(nam1=val1&nam2=val2)}
		 */
		uncgi_stuffenv(val);
	else
		/*
			this handles the case of:
				<cgi>/${encrypt(name)}
				(in which case `lookup' is set to the value of `name')
		 */
		htdb_setval(lookup, dec);

	free(dec);
}

/*
	htdb_fetchHitCount()

		given a domain_id and a resource name, increment the
		counter for this record in the hit database, or insert
		a new record if this is the first page hit for the resource.
		(called only if config.htdb:confLogPageHits == 'y')
 */
int_t	htdb_fetchHitCount(int_t domain_id, char *resource, char *table)
{
	int_t	num_hits = 0;

	if (resource && *resource) {
		char	*tmp = strdup(resource);

		if (strlen(tmp) > 29)
			tmp[29] = '\0';

		if (dbGetRowByKey("hit",
			"select hit_id, num "
			"from %s where domain_id = %ld and resource = '%s' "
			"and isdeleted = 'F' limit 1",
			table, domain_id, tmp)) {
			num_hits = htdb_getint("hit->num") + 1;

			dbUpdate("update LOW_PRIORITY %s set "
				"num = num + 1 "
				"where hit_id = %ld",
				table,
				htdb_getint("hit->hit_id"));
		} else {
			num_hits = 1;
			dbInsert("insert into %s set "
				"hit_id = NULL, "
				"domain_id = %ld, "
				"resource = '%s'",
				table,
				domain_id,
				tmp);
		}
		free(tmp);
	}
	return num_hits;
}

/*
	htdb_handleSessioning()

		end result: the visiting computer will end up with
		a valid session_id (if they accept the cookie),
		and any credentials tied to it.
 */
bool_t	htdb_handleSessioning(void)
{
	user_t	userinfo;

	/*
		init some things which need to be.
	 */
	userinfo.referer_id = 0;
	userinfo.user_id = 0;
	userinfo.session_id = 0;
	userinfo.ip = htdb_getval("cgi->remote_addr");
	sstrncpy(userinfo.sessionkey, "");

	/*
		if we have a DOCUMENT_ROOT in the environment,
		then we assume we are running as a CGI.
		(a bit iffy, but it works alright)
	 */
	if (!(*htdb_getval("confUseCookieSessioning") == 'n') &&
		getenv("DOCUMENT_ROOT") != NULL) {

		/*
			for whatever reason, we need the leading `/' stripped
			from the front of the path_info value.
		 */
		if (htdb_checkval("cgi->path_info"))	
			htdb_setval("cgi->path_info", htdb_getval("cgi->path_info") + 1);

		/*
			populate our session struct with data either
			from the database, or from the environment.
		 */
		fetchSessionInformation(&userinfo);

		/*
			handle the user login and ID checking.
		 */
		processUserLogin(&userinfo);
	}

	/*
		finally, shove some stuff into the environment.
		(and yeah, someday the naming convention will be
		standardized..  ahhh backward compatibility)
	 */
	htdb_setint("_session_int", userinfo.session_id);
	htdb_setint("user->user_id", userinfo.user_id);
	htdb_setint("user->session_id", userinfo.session_id);
	htdb_setval("user->sessionkey", userinfo.sessionkey);
	htdb_setint("_browser_id", userinfo.browser_id);
	htdb_setint("_domain_id", userinfo.domain_id);

	return BOOL_FALSE;
}

/********************************************************************************

	misc user/session-related public functions

 ********************************************************************************/

void	deleteSessionData(int_t session_id, char *name)
{
	if (name && *name)
		dbDelete("delete from sessiondata where session_id=%ld and name='%s'",
			session_id,
			name);
	else
		dbDelete("delete from sessiondata where session_id=%ld",
			session_id);
}

void	storeSessionData(int_t session_id, char *name, char *value)
{
	if (session_id > 0) {
		deleteSessionData(session_id, name);
		dbInsert("insert into sessiondata set "
			"sessiondata_id=NULL, "
			"session_id=%ld, "
			"name='%s', "
			"dtcreated=now(), "
			"value='%s'",
			session_id,
			name,
			value);
	}
}

int	fetchSessionData(int_t session_id, char *name)
{
	int	i,
		num = 0;

	if (name && *name)
		num = dbGetRowByKey("sessiondata",
			"select * from sessiondata where session_id=%ld and name='%s'",
			session_id,
			name);
	else
		num = dbGetRowByKey("sessiondata",
			"select * from sessiondata where session_id=%ld",
			session_id);

	for (i = 0 ; i < num ; i++) {
		char	out[BUF_LIMIT_SMALL + 1];

		snprintf(out, BUF_LIMIT_SMALL, "sessiondata->%s", htdb_getobjval("sessiondata", i + 1, "name"));
		out[BUF_LIMIT_SMALL] = '\0';
		htdb_setval(out, htdb_getobjval("sessiondata", i + 1, "value"));
	}

	return num;
}

void	deleteUserData(int_t user_id, char *name)
{
	if (user_id > 0) {
		if (name && *name)
			dbDelete("delete from userdata where user_id=%ld and name='%s'",
				user_id,
				name);
		else
			dbDelete("delete from userdata where user_id=%ld",
				user_id);
	}
}

void	storeUserData(int_t user_id, char *name, char *value)
{
	if (user_id > 0) {
		deleteUserData(user_id, name);
		dbInsert("insert into userdata set "
			"userdata_id=NULL, "
			"user_id=%ld, "
			"name='%s', "
			"dtcreated=now(), "
			"value='%s'",
			user_id,
			name,
			value);
	}
}

void	fetchNamedUserData(char *prefix, int_t user_id, char *name)
{
	if (user_id > 0) {
		int	i,
			num;
		if (name && *name)
			num = dbGetRowByKey("ttwy4u",
				"select name, value from userdata where user_id=%ld and name='%s' and isdeleted='F'",
				user_id,
				name);
		else
			num = dbGetRowByKey("ttwy4u",
				"select name, value from userdata where user_id=%ld and isdeleted='F'",
				user_id);

		for (i = 0 ; i < num ; i++) {
			char	out[BUF_LIMIT_SMALL + 1];

			snprintf(out, BUF_LIMIT_SMALL, "%s->%s", prefix, htdb_getobjval("ttwy4u", i + 1, "name"));
			out[BUF_LIMIT_SMALL] = '\0';
			htdb_setval(out, htdb_getobjval("ttwy4u", i + 1, "value"));
		}
	}
}

void	fetchUserData(int_t user_id, char *name)
{
	fetchNamedUserData("userdata", user_id, name);
}

void	fetchUserAccountTypes(char *prefix, int_t user_id)
{
	if (user_id > 0) {
		int	i,
			num = dbGetRowByKey("_ttwy4u",
				"select a.name, ua.dtcreated as date from user_accountype ua, accountype a "
				"where ua.user_id=%ld and ua.accountype_id=a.accountype_id "
				"and ua.isdeleted='F' "
				"and IF(ua.startdate > 0, (now() >= ua.startdate), 1) "
				"and IF(ua.stopdate > 0, (now() <= ua.stopdate), 1) ",
				user_id);

		for (i = 0 ; i < num ; i++) {
			htdb_setptrval(prefix, htdb_getobjval("_ttwy4u", i + 1, "name"),
				htdb_getobjval("_ttwy4u", i + 1, "date"));
		}
	}
}

#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

#if	(defined(HAVE_LIBCURL) && defined(USE_LIBCURL))

#include <curl/curl.h>
#include <curl/easy.h>

typedef struct {
	char	*response_buffer;
	size_t	response_size;
} curldata_t;

size_t htdb_writeCurlData(void *buffer, size_t size, size_t nmemb, void *userp)
{
	char *ptr;
	curldata_t	*ic = (curldata_t *)userp;

	ic->response_buffer = realloc(ic->response_buffer, ic->response_size + (size * nmemb) + 1);

	if ( !ic->response_buffer )
		return -1;

	ptr = ic->response_buffer + ic->response_size;
	memcpy(ptr, buffer, (size * nmemb));
	ic->response_size += (size * nmemb);

	return ( size * nmemb );
}

bool_t	htdb_wget(char *prefix, char *url, int timeout)
{
	int err = 0;
	CURL *curl = curl_easy_init();
	curldata_t	cdata;

	cdata.response_buffer = NULL;
	cdata.response_size = 0;

	if (!curl) {
		htdb_setptrvarg(prefix, "error", "wget() error in curl_easy_init()");
		return BOOL_FALSE;
	}
	/* set up curl options */
	curl_easy_setopt(curl, CURLOPT_URL, url);

	/* guard against not being able to connect */
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout);

	/* SSL stuff */
	curl_easy_setopt(curl, CURLOPT_SSL_OPTIONS, (long)CURLSSLOPT_ALLOW_BEAST | CURLSSLOPT_NO_REVOKE);
	curl_easy_setopt (curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);

	/* set up the callback function and the data to pass to it */
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, htdb_writeCurlData);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &cdata);

	err = curl_easy_perform(curl);

	/* TODO:  translate curl errors into internal errors */
	if (err) {
		curl_easy_cleanup(curl);
		htdb_setptrvarg(prefix, "error", "wget() error connecting/reading - ERRNO %d", err);
		return BOOL_FALSE;
	}

	cdata.response_buffer[cdata.response_size] = '\0';

	htdb_setval(prefix, cdata.response_buffer);

	curl_easy_cleanup(curl);

	return BOOL_TRUE;
}

#else

bool_t	htdb_wget(char *prefix, char *url, int timeout_seconds)
{
	htdb_setptrval(prefix, "error", "wget() not supported. recompile with LIBCURL");
}

#endif

#if	(defined(HAVE_LIBJANSSON) && defined(USE_LIBJANSSON))

#include <jansson.h>

char	*htdb_htdb2json(char *prefix)
{
	json_t	*json = json_object();
	htdb_setval(prefix, "{ \"json\": \"output\" }");

	spaceData   *b;

	int len = strlen(prefix);
	htdb_initwalk();
	while ((b = htdb_walkspace())) {
		/*
			find all htdb keys that match the pattern:
			prefix->
			OR
			prefix[
		 */
		if (strncmp(b->key, prefix, len) == 0 &&
			((b->key[len] == '-' && b->key[len + 1] == '>') ||
			b->key[len] == '[')) {
			/*
				TODO - not sure how to build objects
				the way jansson wants
				https://jansson.readthedocs.io/en/latest/apiref.html#string
			 */
			json_object_set_new(json, b->key, json_string(b->value));
		}
	}

	{
		spaceData	o;
		data_init(&o);
		char *str = json_dumps(json, JSON_ENCODE_ANY);
		thing_append(&o, str, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		free(str);
		return (char *)o.value;
	}
}

void	recurse_json(char *prefix, json_t *obj)
{
	char	pre[1024];
	const char *key;
	json_t	*value;
	switch (json_typeof(obj)) {
		case JSON_OBJECT:
			/*
				is object
			 */
			json_object_foreach(obj, key, value) {
				sprintf(pre, "%s->%s", prefix, key);
				recurse_json(pre, value);
			}
		break;
		case JSON_ARRAY:
			/*
				is array
			 */
			for (size_t i = 0 ; i < json_array_size(obj) ; i++) {
				sprintf(pre, "%s[%ld]", prefix, i);
				recurse_json(pre, json_array_get(obj, i));
			}
		break;
		case JSON_STRING:
			htdb_setval(prefix, (char *)json_string_value(obj));
		break;
		case JSON_INTEGER:
			htdb_setint(prefix, (size_t)json_integer_value(obj));
		break;
		case JSON_REAL:
			htdb_setfloat(prefix, (float)json_real_value(obj), 5);
		break;
		case JSON_TRUE:
			htdb_setint(prefix, 1);
		break;
		case JSON_FALSE:
			htdb_setint(prefix, 0);
		break;
		case JSON_NULL:
			htdb_setval(prefix, "");
		break;
	}
}

bool_t	htdb_json2htdb(char *prefix, char *data)
{
	json_error_t	error;
	json_t *root = json_loads(data, 0, &error);
	if (!root) {
		char err[1024];
		sprintf(err, "json2htdb() - error on line %d: %s", error.line, error.text);
		htdb_setptrval(prefix, "error", err);
		return BOOL_FALSE;
	}
	recurse_json(prefix, root);

	return BOOL_TRUE;
}

#else

bool_t	htdb_htdb2json(char *prefix, char *data)
{
	htdb_setptrval(prefix, "error", "htdb2json() not supported. recompile with LIBJANSSON)");
}

bool_t	htdb_json2htdb(char *prefix, char *data)
{
	htdb_setptrval(prefix, "error", "json2htdb() not supported. recompile with LIBJANSSON)");
}

#endif

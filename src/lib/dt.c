#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

month_t	monthData[] = {
	{ 31, "01",
		"${localeTextMonth[01]->short}", "${localeTextMonth[01]->long}",
		"${localeTextMonth[02]->short}", "${localeTextMonth[02]->long}",
		"${localeTextMonth[12]->short}", "${localeTextMonth[12]->long}" },
	{ 28, "02",
		"${localeTextMonth[02]->short}", "${localeTextMonth[02]->long}",
		"${localeTextMonth[03]->short}", "${localeTextMonth[03]->long}",
		"${localeTextMonth[01]->short}", "${localeTextMonth[01]->long}" },
	{ 31, "03",
		"${localeTextMonth[03]->short}", "${localeTextMonth[03]->long}",
		"${localeTextMonth[04]->short}", "${localeTextMonth[04]->long}",
		"${localeTextMonth[02]->short}", "${localeTextMonth[02]->long}" },
	{ 30, "04",
		"${localeTextMonth[04]->short}", "${localeTextMonth[04]->long}",
		"${localeTextMonth[05]->short}", "${localeTextMonth[05]->long}",
		"${localeTextMonth[03]->short}", "${localeTextMonth[03]->long}" },
	{ 31, "05",
		"${localeTextMonth[05]->short}", "${localeTextMonth[05]->long}",
		"${localeTextMonth[06]->short}", "${localeTextMonth[06]->long}",
		"${localeTextMonth[04]->short}", "${localeTextMonth[04]->long}" },
	{ 30, "06",
		"${localeTextMonth[06]->short}", "${localeTextMonth[06]->long}",
		"${localeTextMonth[07]->short}", "${localeTextMonth[07]->long}",
		"${localeTextMonth[05]->short}", "${localeTextMonth[05]->long}" },
	{ 31, "07",
		"${localeTextMonth[07]->short}", "${localeTextMonth[07]->long}",
		"${localeTextMonth[08]->short}", "${localeTextMonth[08]->long}",
		"${localeTextMonth[06]->short}", "${localeTextMonth[06]->long}" },
	{ 31, "08",
		"${localeTextMonth[08]->short}", "${localeTextMonth[08]->long}",
		"${localeTextMonth[09]->short}", "${localeTextMonth[09]->long}",
		"${localeTextMonth[07]->short}", "${localeTextMonth[07]->long}" },
	{ 30, "09",
		"${localeTextMonth[09]->short}", "${localeTextMonth[09]->long}",
		"${localeTextMonth[10]->short}", "${localeTextMonth[10]->long}",
		"${localeTextMonth[08]->short}", "${localeTextMonth[08]->long}" },
	{ 31, "10",
		"${localeTextMonth[10]->short}", "${localeTextMonth[10]->long}",
		"${localeTextMonth[11]->short}", "${localeTextMonth[11]->long}",
		"${localeTextMonth[09]->short}", "${localeTextMonth[09]->long}" },
	{ 30, "11",
		"${localeTextMonth[11]->short}", "${localeTextMonth[11]->long}",
		"${localeTextMonth[12]->short}", "${localeTextMonth[12]->long}",
		"${localeTextMonth[10]->short}", "${localeTextMonth[10]->long}" },
	{ 31, "12",
		"${localeTextMonth[12]->short}", "${localeTextMonth[12]->long}",
		"${localeTextMonth[01]->short}", "${localeTextMonth[01]->long}",
		"${localeTextMonth[11]->short}", "${localeTextMonth[11]->long}" }
};

typedef struct {
	char	*nameShort,
			*nameLong;
}	weekday_t;

weekday_t	weekdayData[] = {
	{ "${localeTextDay[01]->short}", "${localeTextDay[01]->long}" },
	{ "${localeTextDay[02]->short}", "${localeTextDay[02]->long}" },
	{ "${localeTextDay[03]->short}", "${localeTextDay[03]->long}" },
	{ "${localeTextDay[04]->short}", "${localeTextDay[04]->long}" },
	{ "${localeTextDay[05]->short}", "${localeTextDay[05]->long}" },
	{ "${localeTextDay[06]->short}", "${localeTextDay[06]->long}" },
	{ "${localeTextDay[07]->short}", "${localeTextDay[07]->long}" }
};

/*
	isLeapYear()

		well... is it?
 */
bool_t	isLeapYear(int year)
{
	if (year % 4 != 0)
		return BOOL_FALSE;
	else if (year % 400 == 0)
		return BOOL_TRUE;
	else if (year % 100 == 0)
		return BOOL_FALSE;
	else
		return BOOL_TRUE;
}

char	*prettySeconds(long tm)
{
	static char	out[BUF_LIMIT_SMALL + 1];
	char	buf[BUF_LIMIT_SMALL + 1];
	int	secs = tm % 60,
		mins = (tm / 60) % 60,
		hours = (tm / 3600) % 24,
		days = ((tm / 3600) / 24) % 365,
		years = (tm / 31536000);

	strcpy(out, "");

	if (years > 0) {
		snprintf(buf, BUF_LIMIT_SMALL, "%d year%s ",
			years, (years > 1) ? "s" : "");
	} else
		sstrncpy(buf, "");

	strcat(out, buf);

	if (days > 0) {
		snprintf(buf, BUF_LIMIT_SMALL, "%d day%s ",
			days, (days > 1) ? "s" : "");
	} else
		sstrncpy(buf, "");

	strcat(out, buf);

	if (hours > 0)
		snprintf(buf, BUF_LIMIT_SMALL, "%02d:%02d:%02d",
			hours,
			mins,
			secs);
	else if (mins > 0)
		snprintf(buf, BUF_LIMIT_SMALL, "%02d:%02d",
			mins,
			secs);
	else if (secs > 0)
		snprintf(buf, BUF_LIMIT_SMALL, "%d second%s",
			secs,
			(secs > 1 ? "s" : ""));
	else
		strcpy(buf, "now");

	strcat(out, buf);

	return out;
}

char	*prettySecondsTerse(long tm)
{
	static char	out[BUF_LIMIT_SMALL + 1];
	char	buf[BUF_LIMIT_SMALL + 1];
	int	secs = tm % 60,
		mins = (tm / 60) % 60,
		hours = (tm / 3600) % 24,
		days = ((tm / 3600) / 24) % 365,
		years = (tm / 31536000);

	strcpy(out, "");

	if (years > 0) {
		snprintf(buf, BUF_LIMIT_SMALL, "%d year%s ",
			years, (years > 1) ? "s" : "");

		strcat(out, buf);

		if (days > 0)
			snprintf(buf, BUF_LIMIT_SMALL, "%d day%s ",
				days, (days > 1) ? "s" : "");
		else if (hours > 0)
			snprintf(buf, BUF_LIMIT_SMALL, "%d hour%s ",
				hours,
				(hours > 1 ? "s" : ""));
		else if (mins > 0)
			snprintf(buf, BUF_LIMIT_SMALL, "%d minute%s",
				mins,
				(mins > 1 ? "s" : ""));
		else if (secs > 0)
			snprintf(buf, BUF_LIMIT_SMALL, "%d second%s",
				secs,
				(secs > 1 ? "s" : ""));
		else
			sstrncpy(buf, "");

	} else if (days > 0) {
		snprintf(buf, BUF_LIMIT_SMALL, "%d day%s ",
			days, (days > 1) ? "s" : "");

		strcat(out, buf);

		if (hours > 0)
			snprintf(buf, BUF_LIMIT_SMALL, "%d hour%s ",
				hours,
				(hours > 1 ? "s" : ""));
		else if (mins > 0)
			snprintf(buf, BUF_LIMIT_SMALL, "%d minute%s",
				mins,
				(mins > 1 ? "s" : ""));
		else if (secs > 0)
			snprintf(buf, BUF_LIMIT_SMALL, "%d second%s",
				secs,
				(secs > 1 ? "s" : ""));
		else
			sstrncpy(buf, "");

	} else if (hours > 0) {
		snprintf(buf, BUF_LIMIT_SMALL, "%d hour%s ",
			hours,
			(hours > 1 ? "s" : ""));
		strcat(out, buf);
		if (mins > 0)
			snprintf(buf, BUF_LIMIT_SMALL, "%d minute%s ",
				mins,
				(mins > 1 ? "s" : ""));
		else if (secs > 0)
			snprintf(buf, BUF_LIMIT_SMALL, "%d second%s ",
				secs,
				(secs > 1 ? "s" : ""));
		else
			sstrncpy(buf, "");
	} else if (mins > 0) {
		snprintf(buf, BUF_LIMIT_SMALL, "%d minute%s ",
			mins,
			(mins > 1 ? "s" : ""));
		strcat(out, buf);
		if (secs > 0)
			snprintf(buf, BUF_LIMIT_SMALL, "%d second%s ",
				secs,
				(secs > 1 ? "s" : ""));
		else
			sstrncpy(buf, "");
	} else if (secs > 0) {
		snprintf(buf, BUF_LIMIT_SMALL, "%d second%s ",
			secs,
			(secs > 1 ? "s" : ""));
	} else {
		strcpy(buf, "now");
	}

	strcat(out, buf);

	return out;
}

/*
	htdb_timestamp_gmt()

		what's the timestamp of the current (UTC)?
 */
time_t	htdb_timestamp_gmt(void)
{   
	time_t timestamp = (time_t)0;
	time(&timestamp);
	return mktime(gmtime(&timestamp));
}

/*
	htdb_timestamp()

		what's the timestamp of the current box?
 */
time_t	htdb_timestamp(void)
{
	time_t timestamp = (time_t)0;
	time(&timestamp);

	return timestamp;
}

/*
	fetchDaysToEndOfMonth()

		how many days left until the end of the month specified
		by the incoming timestamp value?
 */
int	fetchDaysToEndOfMonth(long timestamp)
{
	int	gotLeap = 0;
	struct tm	*t_now;

	t_now = localtime(&timestamp);
	t_now->tm_year += 1900;	/* ug */

	/*
		find out if we need to add a leap day
	 */
	if (t_now->tm_mon == 1) {
		if (t_now->tm_year % 400) {
			gotLeap = 1;
		} else if (t_now->tm_year % 4) {
			gotLeap = 1;
		}
	}

	return (monthData[t_now->tm_mon].days - t_now->tm_mday + !gotLeap);
}

#define	BITS_FOR_DATE			(0x1 << 1)
#define	BITS_FOR_DATETIME		(0x1 << 2)
#define	BITS_FOR_TIMESTAMP		(0x1 << 3)
#define	BITS_FOR_TIME			(0x1 << 4)
#define	BITS_FOR_YEAR			(0x1 << 5)
#define	BITS_FOR_UNIX			(0x1 << 6)

#define	LOOKING_FOR_NOTHING		(0x0)
#define	LOOKING_FOR_ANYTHING	(BITS_FOR_DATE | BITS_FOR_DATETIME | \
								BITS_FOR_TIMESTAMP | BITS_FOR_TIME | \
								BITS_FOR_YEAR | BITS_FOR_UNIX)

#define	LOOKING_FOR_DATETIME	(BITS_FOR_DATETIME | BITS_FOR_TIMESTAMP | BITS_FOR_UNIX)
#define	LOOKING_FOR_DATE		(LOOKING_FOR_DATETIME | BITS_FOR_DATE)
#define	LOOKING_FOR_DAY			(LOOKING_FOR_DATE)
#define	LOOKING_FOR_MONTH		(LOOKING_FOR_DATE)
#define	LOOKING_FOR_YEAR		(BITS_FOR_YEAR | LOOKING_FOR_DATE)
#define	LOOKING_FOR_TIME		(BITS_FOR_TIME | LOOKING_FOR_DATETIME)
#define	LOOKING_FOR_HOUR		(LOOKING_FOR_TIME)
#define	LOOKING_FOR_MINUTE		(LOOKING_FOR_TIME)
#define	LOOKING_FOR_SECOND		(LOOKING_FOR_TIME)
#define	LOOKING_FOR_AMPM		(LOOKING_FOR_TIME)
#define	LOOKING_FOR_WEEKDAY		(LOOKING_FOR_DATE)
#define	LOOKING_FOR_UNIX		(LOOKING_FOR_DATE)

/*
	used only w/htdb_parseRFC282ISO8601()
 */
#define DATE_FORMAT_RFC2822 "%a, %d %b %Y %T"
#define DATE_FORMAT_ISO8601 "%FT%T"
#define DATE_FORMAT_UNIXCOMMANDLINE "%a %b %d %T %Y"

static bool_t	htdb_parseRFC2822ISO8601(char *in, int *yy, int *mm, int *dd, int *h, int *m, int *s, long mask)
{
	struct	tm	tm;
	time_t	epoch = 0;

	if (!(mask & BITS_FOR_DATETIME))
		/*
			invalid mask for this parser
		 */
		return BOOL_FALSE;

	memset((void *)&tm, 0, sizeof(struct tm));
	/*
		first, try "%a, %d %b %Y %H:%M:%S %z" format
	 */
	strptime(in, DATE_FORMAT_RFC2822, &tm);
	/*
		NO daylight savings..
	 */
	tm.tm_isdst = -1;
	/*
		convert to UNIX time
	 */
	epoch = mktime(&tm);

	if (-1 == tm.tm_year || -1 == epoch) {
		/*
			didn't parse RFC2822.
			try "%FT%T" format
		 */
		memset((void *)&tm, 0, sizeof(struct tm));
		strptime(in, DATE_FORMAT_ISO8601, &tm);
		/*
			NO daylight savings..
		 */
		tm.tm_isdst = -1;
		/*
			convert to UNIX time
		 */
		epoch = mktime(&tm);
	}

	if (-1 == tm.tm_year || -1 == epoch) {
		/*
			didn't parse "%FT%T" 
			try unix commandline date format
		 */
		memset((void *)&tm, 0, sizeof(struct tm));
		strptime(in, DATE_FORMAT_UNIXCOMMANDLINE, &tm);
		/*
			NO daylight savings..
		 */
		tm.tm_isdst = -1;
		/*
			convert to UNIX time
		 */
		epoch = mktime(&tm);
	}

	if (-1 == epoch) {
		/*
			couldn't parse as either RFC2822 or ISO8601 or UNIXCOMMANDLINE
			punt
		 */
		return BOOL_FALSE;
	}

	/*
		handle timezone offset (if it exists)
	 */
	if (strlen(in) > 10) {
		char	*dash = strpbrk(in + 10, "+-");
		if (dash && *dash) {
			/*
				we found a "+/-" after the 10th character.
				let's treat this as a timezone offset
			 */
			int_t	hour = 0,
					min = 0,
					offset_sec = 0;

			if ((2 == sscanf(dash + 1, "%02ld%02ld", &hour, &min)) ||
				(2 == sscanf(dash + 1, "%02ld:%02ld", &hour, &min))) {
				offset_sec = ((hour * 60) + min) * 60;
				if (*dash == '+') {
					offset_sec *= -1;
				}
				epoch += offset_sec;
			}
		}
	}

	/*
		convert from UNIX epoch
	 */
	{
		/*
			split out the various intersting bits.
		 */
		struct	tm	*ts = localtime(&epoch);
		*yy = ts->tm_year + 1900;
		*mm = ts->tm_mon + 1;
		*dd = ts->tm_mday;
		*h = ts->tm_hour;
		*m = ts->tm_min;
		*s = ts->tm_sec;
		if (*mm <= 0 || *mm > 12 ||
			*dd < 0 || *dd > 31 ||
			*h < 0 || *h > 24 ||
			*m < 0 || *m > 60 ||
			*s < 0 || *s > 60) {
			return BOOL_FALSE;
		}
	}

	return BOOL_TRUE;
}

/*
	htdb_parseDTS()

		parse out date/time/timestamp values..

		given an input and a mask type, attempt to parse out
		various date/time parameters.
 */
static bool_t	htdb_parseDTS(char *in, int *yy, int *mm, int *dd, int *h, int *m, int *s, long mask)
{
	int	len = (in && *in) ? strlen(in) : 0;
	bool_t	status = BOOL_TRUE;

	*yy = *mm = *dd = *h = *m = *s = 0;

	if (len == 0)
		return BOOL_FALSE;

	if ((len == 31		/* "Fri, 21 Nov 1997 09:55:06 -0600" */
		|| len == 32	/* "Fri, 21 Nov 1997 09:55:06 -06:00" */
		|| len == 29)	/* "Fri, 21 Nov 1997 09:55:06 GMT" */
		&& strchr(in, ',')) {
		/*
			RFC2822 (maybe)
		 */
		status = htdb_parseRFC2822ISO8601(in, yy, mm, dd, h, m, s, mask);
	} else if (len == 24) {	/* "Wed Feb 22 12:13:44 2017" */
		status = htdb_parseRFC2822ISO8601(in, yy, mm, dd, h, m, s, mask);
	} else if ((len == 19	/* "1998-05-12T14:15:00" */
		|| len == 20		/* "1998-05-12T14:15:00Z" */
		|| len == 24		/* "1998-05-12T14:15:00-0600" */
		|| len == 25)		/* "1998-05-12T14:15:00-06:00" */
		&& strchr(in, 'T')) {
		/*
			ISO8601 (maybe)
		 */
		status = htdb_parseRFC2822ISO8601(in, yy, mm, dd, h, m, s, mask);
	} else if (len == 19 && strchr(in, '-')) {
		/*
			DATETIME
		 */
		if (mask & BITS_FOR_DATETIME) {
			if ((sscanf(in, "%4d-%2d-%2d %2d:%2d:%2d", yy, mm, dd, h, m, s) != 6) ||
				(*mm <= 0 || *mm > 12 ||
				*dd < 0 || *dd > 31 ||
				*h < 0 || *h > 24 ||
				*m < 0 || *m > 60 ||
				*s < 0 || *s > 60))
				status = BOOL_FALSE;
		} else
			status = BOOL_FALSE;
	} else if (len == 14) {
		/*
			TIMESTAMP
		 */
		if (mask & BITS_FOR_TIMESTAMP) {
			if ((sscanf(in, "%4d%2d%2d%2d%2d%2d", yy, mm, dd, h, m, s) != 6) ||
				(*mm <= 0 || *mm > 12 ||
				*dd < 0 || *dd > 31 ||
				*h < 0 || *h > 24 ||
				*m < 0 || *m > 60 ||
				*s < 0 || *s > 60))
				status = BOOL_FALSE;
		} else
			status = BOOL_FALSE;
	} else if ((len == 10 || len == 9 || len == 8) && strchr(in, '/')) {
		/*
			DATE
		 */
		if (mask & BITS_FOR_DATE) {
			if ((sscanf(in, "%d/%d/%4d", mm, dd, yy) != 3) ||
				(*mm <= 0 || *mm > 12 ||
				*dd < 0 || *dd > 31 ||
				*yy < 1000))
				status = BOOL_FALSE;
		} else
			status = BOOL_FALSE;
	} else if (len == 10 && strchr(in, '-')) {
		/*
			DATE
		 */
		if (mask & BITS_FOR_DATE) {
			if ((sscanf(in, "%4d-%2d-%2d", yy, mm, dd) != 3) ||
				(*mm <= 0 || *mm > 12 ||
				*dd < 0 || *dd > 31))
				status = BOOL_FALSE;
		} else
			status = BOOL_FALSE;
	} else if (len == 9 && atoi(in) < 100) {
		/*
			DATE
			01Jan2003
		 */
		if (mask & BITS_FOR_DATE) {
			char	mon[BUF_LIMIT_SMALL + 1];
			if ((sscanf(in, "%2d%3c%4d", dd, mon, yy) != 3) ||
				*dd <= 0 || *dd > 31)
				status = BOOL_FALSE;
			mon[3] = '\0';
			/*
				DEAD CODE
			{
				int	i;
				status = BOOL_FALSE;
				for (i = 0 ; BOOL_FALSE == status && i < 12 ; i++) {
					char	*exp = htdb_expand(monthData[i].nameShort);
					if (0 == strcasecmp(exp, mon)) {
						status = BOOL_TRUE;
						*mm = i + 1;
					}
					free(exp);
				}
			}
			*/
		} else
			status = BOOL_FALSE;
	} else if (len == 8 && strchr(in, ':')) {
		/*
			TIME
		 */
		if (mask & BITS_FOR_TIME) {
			if ((sscanf(in, "%2d:%2d:%2d", h, m, s) != 3) ||
				(*h < 0 || *h > 24 ||
				*m < 0 || *m > 60 ||
				*s < 0 || *s > 60))
				status = BOOL_FALSE;
		} else
			status = BOOL_FALSE;
	} else if (len == 4) {
		/*
			YEAR
		 */
		if (mask & BITS_FOR_YEAR) {
			if (sscanf(in, "%4d", yy) != 1)
				status = BOOL_FALSE;
		} else
			status = BOOL_FALSE;
	} else if ((len > 0) && (mask & BITS_FOR_UNIX)) {
		/*
			else, we fall back and assume that we are dealing
			with a UNIX timestamp value.
			this works for all values except when "len == 4"
			which is an admittedly small number of values.
			i think we've won this battle.
		 */
		time_t	clk = atol(in);
		struct	tm	*ts = localtime(&clk);
		*yy = ts->tm_year + 1900;
		*mm = ts->tm_mon + 1;
		*dd = ts->tm_mday;
		*h = ts->tm_hour;
		*m = ts->tm_min;
		*s = ts->tm_sec;
		if (*mm <= 0 || *mm > 12 ||
			*dd < 0 || *dd > 31 ||
			*h < 0 || *h > 24 ||
			*m < 0 || *m > 60 ||
			*s < 0 || *s > 60) {
			status = BOOL_FALSE;
		}
	} else {
		/*
			unrecognized format
		 */
		status = BOOL_FALSE;
	}

	return status;
}

static time_t	fetchEpochTime(int yy, int mm, int dd, int h, int m, int s)
{
	struct	tm	timestamp;

	timestamp.tm_year = (yy - 1900);
	timestamp.tm_mon = mm - 1;
	timestamp.tm_mday = dd;
	timestamp.tm_hour = h;
	timestamp.tm_min = m;
	timestamp.tm_sec = s;
	timestamp.tm_isdst = -1;

	return mktime(&timestamp);
}

/*
	htdb_parseUnix()

		given an inpt of abritrary type,
		return the number of seconds since 1970
 */
char	*htdb_parseUnix(char *in)
{
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_UNIX)) {
		time_t	ts = fetchEpochTime(yy, mm, dd, h, m, s);
		static char	out[BUF_LIMIT_SMALL + 1];

		sprintf(out, "%ld", ts);
		if (out[0] == '-')
			/*
				if the timestamp has gone negative, force invalidity
			 */
			return "";
		return out;
	} else
		return "";
}

/*
	htdb_parseTime()

		given an inpt of abritrary type,
		return the HH:MM [AM|PM] representation
 */
char	*htdb_parseTime(char *in)
{
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_TIME)) {
		static char	out[BUF_LIMIT_SMALL + 1];
		char	*c = (h < 12 || h > 23) ? "AM" : "PM";
		if (h == 0) h = 12;
		if (h > 12) h -=12;
		snprintf(out, BUF_LIMIT_SMALL, "%d:%.2d %s",
			h, m, c);
		out[BUF_LIMIT_SMALL] = '\0';
		return out;
	} else
		return "";
}

/*
	htdb_parseDate()

		given an inpt of abritrary type,
		return the DDMMMYYYY representation
 */
char	*htdb_parseDate(char *in)
{
	char *expanded;
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_DATE)) {
		static char	out[BUF_LIMIT_SMALL + 1];

		snprintf(out, BUF_LIMIT_SMALL, "%d%s%d",
			dd, monthData[mm - 1].nameShort, yy);
		out[BUF_LIMIT_SMALL] = '\0';
		expanded = htdb_expand(out);
		sstrncpy(out, expanded);
		free(expanded);
		return out;
	} else
		return "";
}

static int	fetchWeekdayIndex(int mm, int yy, int dd)
{
	/*
		This function converts month, date, and year to the day of the week,
		returning an integer between 0 and 6, inclusive, with 0 indicating Sunday.
		This function should work for any year from 1901 to 2099.
		Our base year is 1900.  Since 1 January 1900 was a Monday, we get the
		following keys for the months.
	 */
	static int	mkeys[12] = { 1, 4, 4, 0, 2, 5, 0, 3, 6, 1, 4, 6 };

	mm--;

	if (mm < 0 || mm > 11)
		return 0;

	{
		int	day = (yy - 1900) + (yy - 1900) / 4 + mkeys[mm] + dd - 1;
		/*
			The above counts the leap day even if it occurs later in the year
		 */
		if ((yy % 4 == 0) && (mm < 2))
			day--;
		day %= 7;

		return day;
	}
}

char	*htdb_parseRFC2822(char *in)
{
	char *expanded;
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_DATETIME)) {
		static char	out[BUF_LIMIT_SMALL + 1];
		char	*expanded;

		snprintf(out, BUF_LIMIT_SMALL, "%s, %02d %s %4d %02d:%02d:%02d +0000",
			weekdayData[fetchWeekdayIndex(mm, yy, dd)].nameShort,
			dd,
			monthData[mm - 1].nameShort,
			yy,
			h, m, s);
		out[BUF_LIMIT_SMALL] = '\0';
		expanded = htdb_expand(out);
		sstrncpy(out, expanded);
		free(expanded);
		return out;
	} else
		return "";
}

char	*htdb_parseISO8601(char *in)
{
	char *expanded;
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_DATETIME)) {
		static char	out[BUF_LIMIT_SMALL + 1];
		snprintf(out, BUF_LIMIT_SMALL, "%04d-%02d-%02dT%02d:%02d:%02dZ",
			yy, mm, dd, h, m, s);
		out[BUF_LIMIT_SMALL] = '\0';
		return out;
	} else
		return "";
}

/*
	htdb_parseMysqlTimestamp()

		given an inpt of abritrary type,
		return the YYYYMMDDHHMMSS representation
 */
char	*htdb_parseMysqlTimestamp(char *in)
{
	char *expanded;
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_DATETIME)) {
		static char	out[BUF_LIMIT_SMALL + 1];
		snprintf(out, BUF_LIMIT_SMALL, "%04d%02d%02d%02d%02d%02d",
			yy, mm, dd, h, m, s);
		out[BUF_LIMIT_SMALL] = '\0';
		return out;
	} else
		return "";
}

/*
	htdb_parseMysqlDateTime()

		given an inpt of abritrary type,
		return the YYYY-MM-DD HH:MM:SS representation
 */
char	*htdb_parseMysqlDateTime(char *in)
{
	char *expanded;
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_DATETIME)) {
		static char	out[BUF_LIMIT_SMALL + 1];
		snprintf(out, BUF_LIMIT_SMALL, "%04d-%02d-%02d %02d:%02d:%02d",
			yy, mm, dd, h, m, s);
		out[BUF_LIMIT_SMALL] = '\0';
		return out;
	} else
		return "";
}

/*
	htdb_parseDateTime()

		given an inpt of abritrary type,
		return the DDMMMYYYY HH:MM [AM|PM] representation
 */
char	*htdb_parseDateTime(char *in)
{
	char *expanded;
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_DATETIME)) {
		static char	out[BUF_LIMIT_SMALL + 1];
		char	*c = (h < 12 || h > 23) ? "AM" : "PM";
		if (h == 0) h = 12;
		if (h > 12) h -=12;
		snprintf(out, BUF_LIMIT_SMALL, "%d%s%d %d:%.2d %s",
			dd, monthData[mm - 1].nameShort, yy, h, m, c);
		out[BUF_LIMIT_SMALL] = '\0';
		expanded = htdb_expand(out);
		sstrncpy(out, expanded);
		free(expanded);
		return out;
	} else
		return "";
}

/*
	htdb_parseDay()

		given an inpt of abritrary type,
		return the ordinal of the day
 */
char	*htdb_parseDay(char *in)
{
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_DAY)) {
		static char	out[BUF_LIMIT_SMALL + 1];
		snprintf(out, BUF_LIMIT_SMALL, "%d", dd);
		out[BUF_LIMIT_SMALL] = '\0';
		return out;
	} else
		return "";
}

/*
	htdb_parseMonthNum()

		given an inpt of abritrary type,
		return the ordinal of the month
 */
char	*htdb_parseMonthNum(char *in)
{
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_MONTH))
		return monthData[mm - 1].nameOrdinal;
	else
		return "";
}

/*
	htdb_parseMonth()

		given an inpt of abritrary type,
		return the long month name representation
 */
char	*htdb_parseMonth(char *in)
{
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_DATE))
		return monthData[mm - 1].nameLong;
	else
		return "";
}

/*
	htdb_parseMon()

		given an inpt of abritrary type,
		return the short month name representation
 */
char	*htdb_parseMon(char *in)
{
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_DATE))
		return monthData[mm - 1].nameShort;
	else
		return "";
}

/*
	htdb_parseYear()

		given an inpt of abritrary type,
		return the ordinal of the year
 */
char	*htdb_parseYear(char *in)
{
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_YEAR)) {
		static char	out[BUF_LIMIT_SMALL + 1];
		snprintf(out, BUF_LIMIT_SMALL, "%d", yy);
		out[BUF_LIMIT_SMALL] = '\0';
		return out;
	} else
		return "";
}

/*
	htdb_parseMilHour()

		given an inpt of abritrary type,
		return the ordinal of the hour
		in 24 hour format
 */
char	*htdb_parseMilHour(char *in)
{
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_HOUR)) {
		static char	out[BUF_LIMIT_SMALL + 1];
		snprintf(out, BUF_LIMIT_SMALL, "%d", h);
		out[BUF_LIMIT_SMALL] = '\0';
		return out;
	} else
		return "";
}

/*
	htdb_parseHour()

		given an inpt of abritrary type,
		return the ordinal of the hour
 */
char	*htdb_parseHour(char *in)
{
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_HOUR)) {
		static char	out[BUF_LIMIT_SMALL + 1];
		if (h > 12)
			h -= 12;
		else if (h == 0)
			h = 12;
		snprintf(out, BUF_LIMIT_SMALL, "%d", h);
		out[BUF_LIMIT_SMALL] = '\0';
		return out;
	} else
		return "";
}

/*
	htdb_parseSecond()

		given an inpt of abritrary type,
		return the ordinal of the second
 */
char	*htdb_parseSecond(char *in)
{
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_SECOND)) {
		static char	out[BUF_LIMIT_SMALL + 1];
		snprintf(out, BUF_LIMIT_SMALL, "%d", s);
		out[BUF_LIMIT_SMALL] = '\0';
		return out;
	} else
		return "";
}

/*
	htdb_parseMinute()

		given an inpt of abritrary type,
		return the ordinal of the minute
 */
char	*htdb_parseMinute(char *in)
{
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_MINUTE)) {
		static char	out[BUF_LIMIT_SMALL + 1];
		if (h > 12)
			h -= 12;
		else if (h == 0)
			h = 12;
		snprintf(out, BUF_LIMIT_SMALL, "%02d", m);
		out[BUF_LIMIT_SMALL] = '\0';
		return out;
	} else
		return "";
}

/*
	htdb_parseAMPM()

		given an inpt of abritrary type,
		return [AM|PM]
 */
char	*htdb_parseAMPM(char *in)
{
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_AMPM)) {
		static char	out[BUF_LIMIT_SMALL + 1];
		if (h >= 12)
			strcpy(out, "PM");
		else if (h <= 24)
			strcpy(out, "AM");
		else
			strcpy(out, "");
		return out;
	} else
		return "";
}

/*
	htdb_parseWeekday()

		given an inpt of abritrary type,
		return the day of the week
 */
char	*htdb_parseWeekday(char *in)
{
	int	yy, mm, dd, h, m, s;

	if (BOOL_TRUE == htdb_parseDTS(in, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_WEEKDAY)) {
		static char	out[BUF_LIMIT_SMALL + 1];
		char	*expanded = htdb_expand(weekdayData[fetchWeekdayIndex(mm, yy, dd)].nameShort);
		sstrncpy(out, expanded);
		free(expanded);
		return out;
	} else
		return "";
}

/*
	htdb_date()

		inspired by and almost a completely compatible version of
		that found at: http://www.php.net/manual/en/function.date.php
 */
char	*htdb_date(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ",", &args, 2);
	spaceData   b;

	data_init(&b);

	if (num_args) {
		char	*format = strdup(args[0]),
				*ts;
		if (num_args == 2) {
			ts = strdup(args[1]);
		} else if (num_args == 1) {
			char	buf[512];
			sprintf(buf, "%ld", htdb_timestamp());
			ts = strdup(buf);
		} else
			ts = strdup("");

		if (strlen(ts)) {
			int	yy, mm, dd, h, m, s;

			if (BOOL_TRUE == htdb_parseDTS(ts, &yy, &mm, &dd, &h, &m, &s, LOOKING_FOR_ANYTHING)) {
				char	*ptr = &format[0];
				bool_t	gotLiteral = BOOL_FALSE;

				while (ptr && *ptr) {
					char	buf[512];

					if (BOOL_TRUE == gotLiteral) {
						/*
							previous char was a literal.
							so add the literaled character
						 */
						thing_appendChar(&b, *ptr, BUF_LIMIT_BIG);
						gotLiteral = BOOL_FALSE;
					} else {
						/*
							else, see if if is a formatting character
						 */
						switch (*ptr) {
							case '\\':
								gotLiteral = BOOL_TRUE;
							break;
							case 'a':	/* a - "am" or "pm" */
								thing_append(&b, (h >= 12 ? "pm" : "am"), FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'A':	/* A - "AM" or "PM" */
								thing_append(&b, (h >= 12 ? "PM" : "AM"), FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'B':	/* B - Swatch Internet time */
								sprintf(buf, "\\@%d", (int)((float)((h * 60 * 60) + (m * 60) + s) / (float)86.4));
								thing_append(&b, buf, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'd':	/* d - day of the month, 2 digits with leading zeros; i.e. "01" to "31" */
								sprintf(buf, "%02d", dd);
								thing_append(&b, buf, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'D':	/* D - day of the week, textual, 3 letters; e.g. "Fri" */
								{
									char	*ptr = htdb_expand(weekdayData[fetchWeekdayIndex(mm, yy, dd)].nameShort);
									thing_append(&b, ptr, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
									free(ptr);
								}
							break;
							case 'F':	/* F - month, textual, long; e.g. "January" */
								{
									char	*ptr = htdb_expand(monthData[mm - 1].nameLong);
									thing_append(&b, ptr, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
									free(ptr);
								}
							break;
							case 'g':	/* g - hour, 12-hour format without leading zeros; i.e. "1" to "12" */
								sprintf(buf, "%d", (h > 12 ? h - 12 : h));
								thing_append(&b, buf, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'G':	/* G - hour, 24-hour format without leading zeros; i.e. "0" to "23" */
								sprintf(buf, "%d", h);
								thing_append(&b, buf, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'h':	/* h - hour, 12-hour format; i.e. "01" to "12" */
								sprintf(buf, "%02d", (h > 12 ? h - 12 : h));
								thing_append(&b, buf, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'H':	/* H - hour, 24-hour format; i.e. "00" to "23" */
								sprintf(buf, "%02d", h);
								thing_append(&b, buf, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'i':	/* i - minutes; i.e. "00" to "59" */
								sprintf(buf, "%02d", m);
								thing_append(&b, buf, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'j':	/* j - day of the month without leading zeros; i.e. "1" to "31" */
								sprintf(buf, "%2d", dd);
								thing_append(&b, buf, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'I':	/* I (capital i) - "1" if Daylight Savings Time, "0" otherwise. */
								/* XXX */
								thing_appendChar(&b, *ptr, BUF_LIMIT_BIG);
							break;
							case 'l':	/* l (lowercase 'L') - day of the week, textual, long; e.g. "Friday" */
								{
									char	*ptr = htdb_expand(weekdayData[fetchWeekdayIndex(mm, yy, dd)].nameLong);
									thing_append(&b, ptr, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
									free(ptr);
								}
							break;
							case 'L':	/* L - boolean for whether it is a leap year; i.e. "0" or "1" */
								sprintf(buf, "%d", (isLeapYear(yy) == BOOL_TRUE));
								thing_append(&b, buf, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'm':	/* m - month; i.e. "01" to "12" */
								sprintf(buf, "%02d", mm);
								thing_append(&b, buf, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'M':	/* M - month, textual, 3 letters; e.g. "Jan" */
								{
									char	*ptr = htdb_expand(monthData[mm - 1].nameShort);
									thing_append(&b, ptr, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
									free(ptr);
								}
							break;
							case 'n':	/* n - month without leading zeros; i.e. "1" to "12" */
								sprintf(buf, "%2d", mm);
								thing_append(&b, buf, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'O':	/* O - Difference to Greenwich time in hours; e.g. "+0200" */
								/* XXX */
								thing_appendChar(&b, *ptr, BUF_LIMIT_BIG);
							break;
							case 'R':	/* r - RFC 822 formatted date; e.g. "Thu, 21 Dec 2000 16:01:07 +0200" */
								thing_append(&b, htdb_parseRFC2822(ts), FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 's':	/* s - seconds; i.e. "00" to "59" */
								sprintf(buf, "%02d", s);
								thing_append(&b, buf, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'S':	/* S - English ordinal suffix for the day of the month, 2 characters; i.e. "st", "nd", "rd" or "th" */
								thing_append(&b, htdb_ordinalSuffix(dd), FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 't':	/* t - number of days in the given month; i.e. "28" to "31" */
								sprintf(buf, "%d", monthData[mm - 1].days);
								thing_append(&b, buf, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'T':	/* T - Timezone setting of this machine; e.g. "EST" or "MDT" */
								/* XXX */
								thing_appendChar(&b, *ptr, BUF_LIMIT_BIG);
							break;
							case 'U':	/* U - seconds since the Unix Epoch (January 1 1970 00:00:00 GMT) */
								sprintf(buf, "%ld", fetchEpochTime(yy, mm, dd, h, m, s));
								thing_append(&b, buf, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'w':	/* w - day of the week, numeric, i.e. "0" (Sunday) to "6" (Saturday) */
								{
									int	indx = fetchWeekdayIndex(mm, yy, dd);
									char	buf[512];
									sprintf(buf, "%d", indx);
									thing_append(&b, buf, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
								}
							break;
							case 'W':	/* W - ISO-8601 week number of year, weeks starting on Monday */
								/* XXX */
								thing_appendChar(&b, *ptr, BUF_LIMIT_BIG);
							break;
							case 'Y':	/* Y - year, 4 digits; e.g. "1999" */
								sprintf(buf, "%d", yy);
								thing_append(&b, buf, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'y':	/* y - year, 2 digits; e.g. "99" */
								sprintf(buf, "%d", yy);
								thing_append(&b, &buf[2], FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
							break;
							case 'z':	/* z - day of the year; i.e. "0" to "365" */
								/* XXX */
								thing_appendChar(&b, *ptr, BUF_LIMIT_BIG);
							break;
							case 'Z':	/* Z - timezone offset in seconds (i.e. "-43200" to "43200").
											The offset for timezones west of UTC is always negative, and for those
											east of UTC is always positive. */
								/* XXX */
								thing_appendChar(&b, *ptr, BUF_LIMIT_BIG);
							break;

							default: 
								thing_appendChar(&b, *ptr, BUF_LIMIT_BIG);
							break;
						}
					}
					ptr++;
				}
			}
		}

		free(ts);
		free(format);
	}

	htdb_deleteArgs(args, num_args);

	return b.value;
}

char	*htdb_month2num(char *in)
{
	int	i;

	for (i = 0 ; i < 12 ; i++) {
		month_t	*m = &monthData[i];
		char	*ptr = htdb_expand(m->nameShort);
		if (strcasestr(in, ptr) == in) {
			free(ptr);
			return m->nameOrdinal;
		} else
			free(ptr);
	}

	return "";
}

char	*htdb_num2mon(char *in)
{
	int	i,
		cand = atoi(in);

	for (i = 0 ; i < 12 ; i++) {
		month_t	*m = &monthData[i];
		if (cand == atoi(m->nameOrdinal))
			return m->nameShort;
	}

	return "";
}

char	*htdb_num2month(char *in)
{
	int	i,
		cand = atoi(in);

	for (i = 0 ; i < 12 ; i++) {
		month_t	*m = &monthData[i];
		if (cand == atoi(m->nameOrdinal))
			return m->nameLong;
	}

	return "";
}

/*
	htdb_fetchNextMonth()
		given a number between 1 and 12, return the integer value
		of the next month
			1 -> 2
			12 -> 1
 */
char	*htdb_fetchNextMonth(char *in)
{
	int	i;

	for (i = 0 ; i < 12 ; i++) {
		month_t	*m = &monthData[i];
		char	*ptr = htdb_expand(m->nameShort);
		if (strcasestr(in, ptr) == in) {
			free(ptr);
			return m->nameNextLong;
		} else
			free(ptr);
	}

	return "";
}

/*
	htdb_fetchPrevMonth()
		given a number between 1 and 12, return the integer value
		of the previous month
			1 -> 12
			2 -> 1
 */
char	*htdb_fetchPrevMonth(char *in)
{
	int	i;

	for (i = 0 ; i < 12 ; i++) {
		month_t	*m = &monthData[i];
		char	*ptr = htdb_expand(m->nameShort);
		if (strcasestr(in, ptr) == in) {
			free(ptr);
			return m->namePrevLong;
		} else
			free(ptr);
	}

	return "";
}

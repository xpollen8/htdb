#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

char	*getNewVal(char *prefix, char *name, int row)
{
	if (row == 1)
		return htdb_getptrval((prefix && *prefix ? prefix : ""), (name && *name ? name : ""));
	else
		return htdb_getobjval((prefix && *prefix ? prefix : ""), row, (name && *name ? name : ""));
}

char	*getVal(char *prefix, char *name, int row)
{
	char	lookup[BUF_LIMIT_SMALL + 1];

	snprintf(lookup, BUF_LIMIT_SMALL, "_%s_%d_%s",
		(prefix && *prefix ? prefix : ""), row, (name && *name ? name : ""));

	lookup[BUF_LIMIT_SMALL] = '\0';
	return htdb_getval(lookup);
}

char	*getVals(char *prefix, char *name, int row)
{
	char	buf[BUF_LIMIT_BIG + 1],
			*old = getNewVal(prefix, name, row),
			*prev = getVal("prev", name, row),
			*form = getVal("form", name, row),
			*val;

	if (form && *form)
		/*
			if we have a current value, then it always used
		 */
		val = form;
	else if (prev && *prev)
		/*
			else if we had a previous value, then this
			means that the current value had been cleared
			(this handles deletions)
		 */
		val = form;
	else
		/*
			else, this is probably an initial form insert,
			so look-up any existing environment values
		 */
		val = old;

	/*
		--Ben--
		use a buffer here, because we may htdb_setval
		on the same define we get from here and unintentionally
		free(val) before we're done.
	 */
	snprintf(buf, BUF_LIMIT_BIG, "%s", val);

	buf[BUF_LIMIT_BIG] = '\0';
	return strdup(buf);
}

void	setFormErrors(char *resource, int *index)
{
	if (resource && *resource)
		htdb_setarrayval("formError", ++(*index), htdb_getval(resource));
}

int	setErrors(int set, char *name, int row, char *resource, int *index)
{
	if (row == 1)
		htdb_setptrval("error", (name && *name ? name : ""), (set) ? "y" : "");

	htdb_setobjval("error", row, (name && *name ? name : ""), (set) ? "y" : "");

	if (set)
		setFormErrors(resource, index);

	return set;
}

int	setEmail(char *prefix, char *name, int required, int row, char *resource, int *index)
{
	char	set[BUF_LIMIT_SMALL + 1],
			*val = getVals(prefix, name, row);
	int		exists,
			valid = 0;

	snprintf(set, BUF_LIMIT_SMALL,  "_form_%d_%s",
		row, (name && *name ? name : ""));
	set[BUF_LIMIT_SMALL] = '\0';
	htdb_setval(set, (exists = (val && *val)) ? val : "");

	valid = isValidEmailAddress(val);

	free(val);
	return setErrors((required && (!exists || !valid)), name, row, resource, index);
}

char *	validateURL(char *val)
{
	char *new;
	char *dot = strrchr(val, '.');

	if ( !dot ) 
		return NULL;

	if ( strncasecmp(val, "http://", 7) == 0 || 
			strncasecmp(val, "https://", 8) == 0 ) 
		return strdup(val);

	new = malloc(sizeof(char) + strlen(val) + strlen("http://") + 1);
	if ( !new )
		return NULL;

	sprintf(new, "%s%s", "http://", val);

	return new; 	
}

int	setURL(char *prefix, char *name, int required, int row, char *resource, int *index)
{
	char	set[BUF_LIMIT_SMALL + 1],
			*val = getVals(prefix, name, row), 
			*url;
	int		exists,
			valid = 0;

	snprintf(set, BUF_LIMIT_SMALL,  "_form_%d_%s",
		row, (name && *name ? name : ""));
	set[BUF_LIMIT_SMALL] = '\0';
	htdb_setval(set, (exists = (val && *val)) ? val : "");

	if ( exists ) {
		url = validateURL(val);
		if ( url ) {
			valid = 1;
			htdb_setval(set, url);
			free(url);
		} else
			valid = 0;
	}

	free(val);
	return setErrors((required && (!exists || !valid)), name, row, resource, index);
}

char	*screenameFormat(char *val)
{
	char	*out = strdup(val),
			*ptr = &out[0];
	int	indx = 0;

	while (val && *val) {
		if (!isalnum((int)*val) && *val != '_') {
			free(out);
			return NULL;
		}
		if (*val != '_')
			out[indx++] = *val;
		val++;
	}
	out[indx] = '\0';
	return ptr;
}

bool_t	containsCensoredWords(char *val)
{
	return htdb_inList(val, htdb_getval("confCensoredWords"), BOOL_FALSE);
}

int	setScreenName(char *prefix, char *name, int required, int row, char *resource, int *index)
{
	char	set[BUF_LIMIT_SMALL + 1],
			*val = getVals(prefix, name, row);
	int		exists,
			valid = 0;

	snprintf(set, BUF_LIMIT_SMALL, "_form_%d_%s",
		row, (name && *name ? name : ""));
	set[BUF_LIMIT_SMALL] = '\0';
	htdb_setval(set, (exists = (val && *val)) ? val : "");
	if (val && !strchr(val, ' ') && !strchr(val, '\t')) {
		char	*clean;
		/*
			exists and has no whitespace
		 */
		if ((clean = screenameFormat(val)) && BOOL_FALSE == containsCensoredWords(clean))
			/*
				right format and contains no censored words
			 */
			valid = 1;
		if (clean)
			free(clean);
	}

	free(val);
	return setErrors((required && (!exists || !valid)), name, row, resource, index);
}

int	setForm(char *prefix, char *name, int required, int row, char *resource, int *index)
{
	char	set[BUF_LIMIT_SMALL + 1],
			*val = getVals(prefix, name, row);
	int		exists;

	snprintf(set, BUF_LIMIT_SMALL, "_form_%d_%s",
		row, (name && *name ? name : ""));
	set[BUF_LIMIT_SMALL] = '\0';
	htdb_setval(set, (exists = (val && *val)) ? val : "");

	free(val);
	return setErrors((required && !exists), name, row, resource, index);
}

int	setCheckbox(char *prefix, char *name, int required, int row, char *resource, int *index)
{
	char	set[BUF_LIMIT_SMALL + 1],
			*val = getVals(prefix, name, row);
	int		exists;

	snprintf(set, BUF_LIMIT_SMALL, "_form_%d_%s",
		row, (name && *name ? name : ""));
	set[BUF_LIMIT_SMALL] = '\0';
	htdb_setval(set, (exists = (val && *val)) ? val : "");
	snprintf(set, BUF_LIMIT_SMALL, "_check_%d_%s_%s",
		row, (name && *name ? name : ""), (val && *val ? val : ""));
	set[BUF_LIMIT_SMALL] = '\0';
	htdb_setval(set, val ? "CHECKED" : "");

	free(val);
	return setErrors((required && !exists), name, row, resource, index);
}

int	setRadio(char *prefix, char *name, int required, int row, char *resource, int *index)
{
	char	set[BUF_LIMIT_SMALL + 1],
			*val = getVals(prefix, name, row);
	int		exists;

	snprintf(set, BUF_LIMIT_SMALL, "_form_%d_%s",
		row, (name && *name ? name : ""));
	set[BUF_LIMIT_SMALL] = '\0';
	htdb_setval(set, (exists = (val && *val)) ? val : "");
	snprintf(set, BUF_LIMIT_SMALL, "_radio_%d_%s_%s",
		row, (name && *name ? name : ""), (val && *val ? val : ""));
	set[BUF_LIMIT_SMALL] = '\0';
	htdb_setval(set, val ? "CHECKED" : "");

	free(val);
	return setErrors((required && !exists), name, row, resource, index);
}

int	setLimitedIntOption(char *prefix, char *name, int required, int row, char *resource, int *index)
{
	char	set[BUF_LIMIT_SMALL + 1],
			*val = getVals(prefix, name, row);
	int		exists;

	snprintf(set, BUF_LIMIT_SMALL, "_form_%d_%s",
		row, (name && *name ? name : ""));
	set[BUF_LIMIT_SMALL] = '\0';
	htdb_setval(set,
		(exists = (val && *val && (atoi(val) > 1))) ? val : "");
	snprintf(set, BUF_LIMIT_SMALL, "_option_%d_%s_%s",
		row, (name && *name ? name : ""), (val && *val ? val : ""));
	set[BUF_LIMIT_SMALL] = '\0';
	htdb_setval(set, val ? "SELECTED" : "");

	free(val);
	return setErrors((required && !exists), name, row, resource, index);
}

int	setYear(char *prefix, char *name, int required, int row, char *resource, int *index)
{
	char	set[BUF_LIMIT_SMALL + 1],
			*val = getVals(prefix, name, row);
	int		exists,
			valid = 0;

	snprintf(set, BUF_LIMIT_SMALL, "_form_%d_%s",
		row, (name && *name ? name : ""));
	set[BUF_LIMIT_SMALL] = '\0';
	htdb_setval(set, (exists = (val && *val)) ? val : "");
	if (val && (strlen(val) == 4) && (atoi(val) > 0))
		/*
			exists and is 4 characters and int value is greater than zero
		 */
		valid = 1;

	free(val);
	if (exists)
		return setErrors((!exists || !valid), name, row, resource, index);
	else
		return setErrors((required && (!exists || !valid)), name, row, resource, index);
}

int	setIntOption(char *prefix, char *name, int required, int row, char *resource, int *index)
{
	char	set[BUF_LIMIT_SMALL + 1],
			*val = getVals(prefix, name, row);
	int		exists;

	snprintf(set, BUF_LIMIT_SMALL, "_form_%d_%s",
		row, (name && *name ? name : ""));
	set[BUF_LIMIT_SMALL] = '\0';
	htdb_setval(set,
		(exists = (val && *val && (atoi(val) > 0))) ? val : "");
	snprintf(set, BUF_LIMIT_SMALL, "_option_%d_%s_%s",
		row, (name && *name ? name : ""), (val && *val ? val : ""));
	set[BUF_LIMIT_SMALL] = '\0';
	htdb_setval(set, val ? "SELECTED" : "");

	free(val);
	return setErrors((required && !exists), name, row, resource, index);
}

int	setTextOption(char *prefix, char *name, int required, int row, char *resource, int *index)
{
	char	set[BUF_LIMIT_SMALL + 1],
			*val = getVals(prefix, name, row);
	int		exists;

	snprintf(set, BUF_LIMIT_SMALL, "_form_%d_%s",
		row, (name && *name ? name : ""));
	set[BUF_LIMIT_SMALL] = '\0';
	htdb_setval(set, (exists = (val && *val)) ? val : "");
	snprintf(set, BUF_LIMIT_SMALL, "_option_%d_%s_%s",
		row, (name && *name ? name : ""), (val && *val ? val : ""));
	set[BUF_LIMIT_SMALL] = '\0';
	htdb_setval(set, val ? "SELECTED" : "");

	free(val);
	return setErrors((required && !exists), name, row, resource, index);
}

#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

spaceName	dbSpace = (spaceName)NULL;
static bool_t		dbMasterOnly = BOOL_FALSE;

#undef	DEBUG

/*

	*** TODO: need libdbi equivalents for: ***

		_dbResultHandle	*htdb_dbSQLQuery(char *cmd, char **handle, int *num_rows, int *success)
		int	htdb_dbQuery(char *fmt, ...)
		int_t	htdb_dbInsert(char *fmt, ...)
		int	dbBinaryQuery(char *cmd, unsigned int length)
		int	htdb_dbDelete(char *fmt, ...)
		int	htdb_dbUpdate(char *fmt, ...)
		int	htdb_dbQueryResult(char *table, char *fmt, ...)
		int	htdb_dbQueryCallback(void (*fptr)(int, int, char **, char **), char *fmt, ...)
		int	htdb_dbQueryLoop(char *cmd, char *resource_prefix)
		time_t	htdb_dbTimestamp(void)
 */

/********************************************************************************

	begin private functions

 ********************************************************************************/

static _dbHandle	htdb_dbOpenSocket(char *driver, char *host, char *port, char *dbname, char *username, char *password)
{
	char	*user = (char *)NULL,
			*passwd = (char *)NULL;
	_dbHandle	dbHandle = (_dbHandle)NULL;

	if (username && *username && password && *password) {

		if (*username == '|')
			user = htdb_encryptor("be1", username);
		else
			user = strdup(username);

		if (*password == '|')
			passwd = htdb_encryptor("be1", password);
		else
			passwd = strdup(password);
	}

#if	(defined(USE_MYSQL) && defined(HAVE_LIBMYSQLCLIENT))

	dbHandle = (_dbHandle) malloc(sizeof(MYSQL));

	mysql_init(dbHandle);

	if (username && *username && password && *password) {
		char	*dbhost = (strlen(host) == 0 ||
				strcasecmp(host, "NULL") == 0 ||
				strcasecmp(host, "localhost") == 0) ?
				NULL : host;

		_dbHandle ret = mysql_real_connect(dbHandle, 
			dbhost,
			user,
			passwd, 
			dbname,
			atoi(port), NULL, 0);

			/*
				from mysql online docs:
				"A MYSQL* connection handle if the connection was successful,
				NULL if the connection was unsuccessful. For a successful
				connection, the return value is the same as the value
				of the first parameter."
			 */
		if (ret != dbHandle) {
				free(dbHandle);
				dbHandle = NULL;
		}
	}

	/*
	if (dbHandle)
		mysql_select_db(dbHandle, dbname);
		*/

#endif

#if	(defined(USE_DBI) && defined(HAVE_LIBDBI))

	dbHandle = dbi_conn_new(driver);

	if (dbHandle) {
		dbi_conn_set_option(dbHandle, "host", host);
		dbi_conn_set_option(dbHandle, "username", user);
		dbi_conn_set_option(dbHandle, "password", passwd);
		dbi_conn_set_option(dbHandle, "dbname", dbname);
		dbi_conn_set_option_numeric(dbHandle "port", port);

		dbi_conn_connect(dbHandle);
	}

#endif

	if (user)
		free(user);
	if (passwd)
		free(passwd);

	return dbHandle;
}

static _dbHandle	htdb_dbGetSocket(char *handle)
{
	spaceData	*lookup = db_lookup(handle && *handle ? handle : "master");

	if (lookup != NULL) {
		htdb_db_t	*data = (htdb_db_t *)lookup->value;
		return data->socket;
	} else
		return NULL;
}

static char	*htdb_dbGetHandle(char **cmd)
{
	char	*pipe = strchr(*cmd, '|'),
			*space = strchr(*cmd, ' ');
	static char	*handle = (char *)NULL;

	if (pipe && *pipe && space && *space && pipe < space) {
		/*
			command was given in "handle|SQL..." format, so we'll use
			the database handle that they've explicitly requested.
			we've checked that the pipe has occurred
			before any spaces, which seems to be a safe assumption
			for legal syntax.
		 */
		*pipe = '\0';
		handle = *cmd;
		*cmd = pipe + 1;
	} else {
		if (BOOL_TRUE == dbMasterOnly)
			return "master";

		/*
			was given in "SQL..." format, so we'll use
			either "master" or "slave" as the database handle
			depending upon SQL context.
		 */
		while ((*cmd) && *(*cmd) && isspace((int)*(*cmd)))
			/*
				eat leading whitespace
			 */
			(*cmd)++;

		/*
			determine which database handle ("master" or "slave") to direct to
			(based upon the simple-minded assumption that "selects"
			will go to the slave, all others go to the master).
		 */
		if (strncasecmp(*cmd, "select", 6) == 0)
			handle = "slave";
		else
			handle = "master";
	}
#ifdef	DEBUG
	{
		FILE	*dp = fopen("/tmp/sqldebug.log", "a+");
		int	col = 0;
		char	*str = *cmd;
		while (str && *str) {
			if (++col > 50 && isspace(*str)) {
				fprintf(dp, "\n\t");
				col = 0;
			}
			fputc(*(str++), dp);
		}
		fprintf(dp, ";\n\n");
		fclose(dp);
	}
#endif
	return handle;
}

static int	htdb_dbQueryInternal(_dbHandle *dbHandle, char *cmd, char **handle)
{
	static	bool_t	usingDatabase = BOOL_FALSE;

	usingDatabase = (!(*htdb_getval("confUseDatabase") == 'n')) ? BOOL_TRUE : BOOL_FALSE;

#if	defined(USE_DATABASE)
	if (BOOL_TRUE == usingDatabase) {
		*handle = htdb_dbGetHandle(&cmd);
		*dbHandle = htdb_dbGetSocket(*handle);
		if (*dbHandle)
			return _dbQuery(*dbHandle, cmd);
	}
#endif
	return -1;
}

static int	htdb_dbBinaryQueryInternal(_dbHandle *dbHandle, char *cmd, char **handle, unsigned int length)
{
	static	bool_t	usingDatabase = BOOL_FALSE;

	usingDatabase = (!(*htdb_getval("confUseDatabase") == 'n')) ? BOOL_TRUE : BOOL_FALSE;

#if	defined(USE_DATABASE)
	if (BOOL_TRUE == usingDatabase) {
		*handle = htdb_dbGetHandle(&cmd);
		*dbHandle = htdb_dbGetSocket(*handle);
		if (*dbHandle)
			return _dbBinaryQuery(*dbHandle, cmd, length);
	}
#endif
	return -1;
}

static int	htdb_dbSetError(_dbHandle *dbHandle, char *prefix, char *query, int status)
{
#if	defined(USE_DATABASE)
	htdb_setint("_error_database", status);
	htdb_setval("_error_query", query);
	/*
	if (htdb_checkval("confDebugLogDBErrors")) {
		char *errstr = (char *)_dbFetchErrorString(*dbHandle);
		if (errstr && *errstr) {
			log_fatal(prefix, query);
			log_fatal(prefix, errstr);
		}
	}
	*/
	return status;
#else
	return 0;
#endif
}

static _dbResultHandle	*htdb_dbSQLQuery(char *cmd, char **handle, int *num_rows, int *success)
{
	*num_rows = *success = 0;

#if	defined(USE_DATABASE)

#if	(defined(USE_MYSQL) && defined(HAVE_LIBMYSQLCLIENT))
	{
		_dbHandle	dbHandle;

		if (htdb_dbQueryInternal(&dbHandle, cmd, handle) == 0) {
			*success = 1;
			{
				_dbResultHandle	*resultHandle = mysql_store_result(dbHandle);
				if (resultHandle) {
					*num_rows = mysql_num_rows(resultHandle);
					if (*num_rows > 0)
						return (resultHandle);
					_dbFreeResultHandle(resultHandle);
				}
			}
		}
	}
#endif

#if	(defined(USE_DBI) && defined(HAVE_LIBDBI))
#endif

#endif

	return NULL;
}

static _dbResultHandle	*dbSQLBinaryQuery(char *cmd, char **handle, int *num_rows, int *success, unsigned int length)
{
	*num_rows = *success = 0;

#if	defined(USE_DATABASE)

#if	(defined(USE_MYSQL) && defined(HAVE_LIBMYSQLCLIENT))
	{
		_dbHandle	dbHandle;

		if (htdb_dbBinaryQueryInternal(&dbHandle, cmd, handle, length) == 0) {
			*success = 1;
			{
				_dbResultHandle	*resultHandle = mysql_store_result(dbHandle);
				if (resultHandle) {
					*num_rows = mysql_num_rows(resultHandle);
					if (*num_rows > 0)
						return (resultHandle);
					_dbFreeResultHandle(resultHandle);
				}
			}
		}
	}
#endif

#if	(defined(USE_DBI) && defined(HAVE_LIBDBI))
#endif

#endif

	return NULL;
}

/********************************************************************************

	begin public functions

 ********************************************************************************/

void		htdb_dbSync()
{
	dbMasterOnly = BOOL_TRUE;
}

void		htdb_dbUnsync()
{
	dbMasterOnly = BOOL_FALSE;
}

int	htdb_dbQuery(char *fmt, ...)
{
#if	defined(USE_DATABASE)
	va_list	args;
	char	cmd[BUF_LIMIT_MAX + 1];

	va_start(args, fmt);
	vsnprintf(cmd, BUF_LIMIT_MAX, fmt, args);
	cmd[BUF_LIMIT_MAX] = '\0';
	va_end(args);

	{
		int	success,
			num_rows;
		char	*handle = NULL;
		_dbResultHandle	*resultHandle = htdb_dbSQLQuery(cmd, &handle, &num_rows, &success);
		if (resultHandle)
			_dbFreeResultHandle(resultHandle);
		return num_rows;
	}
#endif

	return 0;
}

int_t	htdb_dbInsert(char *fmt, ...)
{
#if	defined(USE_DATABASE)
	va_list	args;
	char	cmd[BUF_LIMIT_MAX + 1];

	va_start(args, fmt);
	vsnprintf(cmd, BUF_LIMIT_MAX, fmt, args);
	cmd[BUF_LIMIT_MAX] = '\0';
	va_end(args);

	{
		int	success,
			num_rows;
		char	*handle = NULL;
		_dbResultHandle	*resultHandle = htdb_dbSQLQuery(cmd, &handle, &num_rows, &success);

		if (resultHandle)
			free(resultHandle);

		if (success)
			return mysql_insert_id(htdb_dbGetSocket(handle));
		else {
			_dbHandle dbHandle = htdb_dbGetSocket(handle);
			htdb_dbSetError(&dbHandle, "insert", (char *) cmd,  ERROR_DATABASE_RESULTHANDLE);
			return 0;
		}
	}
#endif

	return 0;
}

int	htdb_dbQueryBinary(char *cmd, unsigned int length)
{
#if	defined(USE_DATABASE)
	{
		int	success,
			num_rows;
		char	*handle = NULL;
		_dbResultHandle	*resultHandle = dbSQLBinaryQuery(cmd, &handle, &num_rows, &success, length);
		if (resultHandle)
			_dbFreeResultHandle(resultHandle);
		return success;
	}
#endif

	return 0;
}

int	htdb_dbDelete(char *fmt, ...)
{
#if	defined(USE_DATABASE)
	va_list	args;
	char	cmd[BUF_LIMIT_MAX + 1];

	va_start(args, fmt);
	vsnprintf(cmd, BUF_LIMIT_MAX, fmt, args);
	cmd[BUF_LIMIT_MAX] = '\0';
	va_end(args);

	{
		int	success,
			num_rows;
		char	*handle = NULL;
		_dbResultHandle	*resultHandle = htdb_dbSQLQuery(cmd, &handle, &num_rows, &success);
		if (resultHandle)
			_dbFreeResultHandle(resultHandle);
		return num_rows;
	}
#endif

	return 0;
}

int	htdb_dbUpdate(char *fmt, ...)
{
#if	defined(USE_DATABASE)
	va_list	args;
	char	cmd[BUF_LIMIT_MAX + 1];

	va_start(args, fmt);
	vsnprintf(cmd, BUF_LIMIT_MAX, fmt, args);
	cmd[BUF_LIMIT_MAX] = '\0';
	va_end(args);

	{
		int	num_rows,
			success;
		char	*handle = NULL;
		_dbResultHandle	*resultHandle = htdb_dbSQLQuery(cmd, &handle, &num_rows, &success);

		if (resultHandle)
			_dbFreeResultHandle(resultHandle);
		
		if (success)
			return num_rows;
		else {
			_dbHandle dbHandle = htdb_dbGetSocket(handle);
			htdb_dbSetError(&dbHandle, "update", (char *) cmd,  ERROR_DATABASE_RESULTHANDLE);
			return 0;
		}
		return success;
	}
#endif

	return 0;
}

int	htdb_dbQueryResult_log(char *table, char *fmt, ...)
{
	va_list		args;
	char		cmd[BUF_LIMIT_MAX + 1];

	va_start(args, fmt);
	vsnprintf(cmd, BUF_LIMIT_MAX, fmt, args);
	cmd[BUF_LIMIT_MAX] = '\0';
	va_end(args);

	log_fatal(table, cmd);
	htdb_dbQueryResult(table, cmd);	
	return 0;
}

#define	ISABLOB(x)	(((x).type == FIELD_TYPE_BLOB) && ((x).flags & BINARY_FLAG))

int	htdb_dbQueryResult(char *table, char *fmt, ...)
{
#if	defined(USE_DATABASE)
	va_list		args;
	char		cmd[BUF_LIMIT_MAX + 1];
	int			rows = 0;
	_dbHandle	dbHandle;
	char		*handle = NULL;

	va_start(args, fmt);
	vsnprintf(cmd, BUF_LIMIT_MAX, fmt, args);
	cmd[BUF_LIMIT_MAX] = '\0';
	va_end(args);

#if	defined(USE_DB_RESULT_CACHING)
	if (htdb_getptrint(table, "numResults") > 0 &&
		0 == strcasecmp(htdb_getptrval(table, "query"), cmd)) {
		/*
			whoo-hoo.  cache hit.
			return right now
		 */
		htdb_setptrval(table, "cached", "yes");
		return htdb_getptrint(table, "numResults");
	}
	htdb_setptrval(table, "cached", "no");
#endif
	/*
		save the query as the cache key for this result set.
	 */
	htdb_setptrval(table, "query", cmd);
	/*
		start wid nutin
	 */
	htdb_setptrint(table, "numResults", rows);

#if	(defined(USE_MYSQL) && defined(HAVE_LIBMYSQLCLIENT))
	if (htdb_dbQueryInternal(&dbHandle, cmd, &handle) == 0) {
		_dbResultHandle	*resultHandle = mysql_store_result(dbHandle);
		if (resultHandle) {
			_dbResultRow	sqlRow;
			unsigned int	numFields = mysql_num_fields(resultHandle);
			_dbField	*fields = mysql_fetch_fields(resultHandle);
			int	i;

			htdb_setptrint(table, "numFields", numFields);
			for (i = 0 ; i < numFields ; i++) {
				char	*pref = htdb_getptrdyname(table, "field");
				/*
					store field name in:
						prefix->field[]
				 */
				htdb_setarrayval(pref, i + 1, fields[i].name);
				free(pref);
			}

			while ((sqlRow = _dbFetchRow(resultHandle))) {
				unsigned long *lengths = mysql_fetch_lengths(resultHandle);
				for (i = 0 ; i < numFields ; i++) {
					off_t	bytes = lengths[i];
					/*
						array stylee: store as {table}[row]->{field}
					 */
					{
						char	*name = htdb_getobjname(table, rows + 1, fields[i].name),
								*jnk = (ISABLOB(fields[i])) ?
								prepareBinaryFromDatabase(sqlRow[i], &bytes) :
								prepareStringFromDatabase(sqlRow[i]);

						if (jnk && *(jnk)) {
							if (ISABLOB(fields[i]))
								htdb_binary(name, jnk, (bytes));
							else
								htdb_store(name, jnk);
						} else
							htdb_setval(name, DATABASE_EMPTY_FIELD);
					}

					/*
						single result/first row stylee: store as '{table}->{field}
					 */
					if (rows == 0) {
						char	*name = htdb_getptrname(table, fields[i].name),
								*jnk = (ISABLOB(fields[i])) ?
								prepareBinaryFromDatabase(sqlRow[i], &bytes) :
								prepareStringFromDatabase(sqlRow[i]);
						if (jnk && *(jnk)) {
							if (ISABLOB(fields[i]))
								htdb_binary(name, jnk, (bytes));
							else 
								htdb_store(name, jnk);
						} else 
							htdb_setval(name,
								DATABASE_EMPTY_FIELD);
					}
				}
				++rows;
			}
			htdb_setptrint(table, "numResults", rows);
			if (resultHandle)
				_dbFreeResultHandle(resultHandle);
		} else
			return htdb_dbSetError(&dbHandle, table, cmd, ERROR_DATABASE_RESULTHANDLE);
	} else {
		return htdb_dbSetError(&dbHandle, table, cmd, ERROR_DATABASE_QUERY);
	}

	if (rows)
		return rows;

	return htdb_dbSetError(&dbHandle, table, cmd, 0);
#endif

#if	(defined(USE_DBI) && defined(HAVE_LIBDBI))
#endif

#endif
	return 0;
}

/*
	Takes two arguments, a query and a function pointer
	that we call back to after each row. 
*/
int	htdb_dbQueryCallback(void (*fptr)(int, int, char **, char **), char *fmt, ...)
{
#if	defined(USE_DATABASE)

	va_list		args;
	char		**fieldBuf;	
	int			rows = 0,
				num_rows;
	_dbHandle	dbHandle;
	char		*handle,
				cmd[BUF_LIMIT_MAX + 1];

	va_start(args, fmt);
	vsnprintf(cmd, BUF_LIMIT_MAX, fmt, args);
	cmd[BUF_LIMIT_MAX] = '\0';
	va_end(args);

#if	(defined(USE_MYSQL) && defined(HAVE_LIBMYSQLCLIENT))

	if (htdb_dbQueryInternal(&dbHandle, cmd, &handle) == 0) {
		_dbResultHandle	*resultHandle;
		resultHandle = mysql_store_result(dbHandle);
		if (resultHandle) {
			_dbResultRow	sqlRow;
			unsigned int	numFields = mysql_num_fields(resultHandle);
			_dbField		*fields = mysql_fetch_fields(resultHandle);
			int				i;

			fieldBuf = malloc(sizeof(char *) * numFields);
			for (i = 0; i < numFields; i++) {
				fieldBuf[i] = malloc(sizeof(char) * (strlen(fields[i].name) + 1));
				strcpy(fieldBuf[i], fields[i].name);
			}

			num_rows = mysql_num_rows(resultHandle);

			while ((sqlRow = _dbFetchRow(resultHandle))) {
				++rows;
				fptr(numFields, num_rows, fieldBuf, sqlRow);
			}
			for(i=0; i < numFields; i++)
				free(fieldBuf[i]);
			free(fieldBuf);
			if (resultHandle)
				_dbFreeResultHandle(resultHandle);
		} else
			return htdb_dbSetError(&dbHandle, "callbackError", cmd, ERROR_DATABASE_RESULTHANDLE);
	} else
		return htdb_dbSetError(&dbHandle, "callbackError", cmd, ERROR_DATABASE_QUERY);

	return rows;
#endif

#if	(defined(USE_DBI) && defined(HAVE_LIBDBI))
#endif

#endif

	return 0;
}

int	htdb_dbQueryLoop(char *cmd, char *resource_prefix)
{
#if	defined(USE_DATABASE)
	int			rows = 0;
	_dbHandle	dbHandle;
	char		resourceIn[BUF_LIMIT_SMALL + 1],
				resourceOut[BUF_LIMIT_SMALL + 1],
				*handle = NULL;

	snprintf(resourceIn, BUF_LIMIT_SMALL, "li%s", resource_prefix);
	resourceIn[BUF_LIMIT_SMALL] = '\0';
	snprintf(resourceOut, BUF_LIMIT_SMALL, "lo%s", resource_prefix);
	resourceOut[BUF_LIMIT_SMALL] = '\0';

	htdb_setptrint(resource_prefix, "numResults", rows);

#if	(defined(USE_MYSQL) && defined(HAVE_LIBMYSQLCLIENT))
	if (htdb_dbQueryInternal(&dbHandle, cmd, &handle) == 0) {
		_dbResultHandle	*resultHandle = mysql_store_result(dbHandle);
		if (resultHandle) {
			_dbResultRow	sqlRow;
			unsigned int	numFields = mysql_num_fields(resultHandle);
			_dbField		*fields = mysql_fetch_fields(resultHandle);
			int				i;

			htdb_setval(resourceOut, "");

			htdb_setptrint(resource_prefix, "numFields", numFields);
			for (i = 0 ; i < numFields ; i++) {
				char	*pref = htdb_getptrdyname(resource_prefix, "field");
				/*
					store field name in:
						prefix->field[]
				 */
				htdb_setarrayval(pref, i + 1, fields[i].name);
				free(pref);
			}

			while ((sqlRow = _dbFetchRow(resultHandle))) {
				unsigned long *lengths = mysql_fetch_lengths(resultHandle);

				for (i = 0 ; i < numFields ; i++) {
					off_t	bytes = lengths[i];
					htdb_setptrint(resource_prefix, "loopIndex", rows + 1);
					/*
						stylee: {table}->{field}
					 */
					{
						char	*jnk = (ISABLOB(fields[i])) ?
							prepareBinaryFromDatabase(sqlRow[i], &bytes) :
							prepareStringFromDatabase(sqlRow[i]),
							*name = htdb_getptrname(resource_prefix, fields[i].name);

						if (jnk && *(jnk)) {
							if (ISABLOB(fields[i]))
								htdb_binary(name, jnk, (bytes));
							else
								htdb_store(name, jnk);
						} else
							htdb_setval(name,
								DATABASE_EMPTY_FIELD);
					}
				}
				htdb_loop(resourceOut, resourceIn);
				for (i = 0 ; i < numFields ; i++) {
					htdb_setptrval(resource_prefix, fields[i].name, "");
					htdb_setptrval(resource_prefix, "loopIndex", "");
				}
				++rows;
			}
			htdb_setptrint(resource_prefix, "numResults", rows);
			if (resultHandle)
				_dbFreeResultHandle(resultHandle);
		} else
			return htdb_dbSetError(&dbHandle, resource_prefix, cmd, ERROR_DATABASE_RESULTHANDLE);
	} else
		return htdb_dbSetError(&dbHandle, resource_prefix, cmd, ERROR_DATABASE_QUERY);

	return rows;
#endif

#if	(defined(USE_DBI) && defined(HAVE_LIBDBI))
#endif

#endif
	return 0;
}

void	htdb_dbWaitSync(void)
{
	int ret = dbGetRowByKey("__hws", "master|show master status");
	if ( !ret ) 
		return;

	dbQuery("slave|select master_pos_wait('%s', %ld)",
		htdb_getint("__hws->File"), htdb_getint("__hws->Position"));			
	
	return;
}

time_t	htdb_dbTimestamp(void)
{
	time_t	out = (time_t)0;

	if (0 == dbGetRowByKey("__tm",
		"select UNIX_TIMESTAMP() as now") ||
		0 == (out = htdb_getint("__tm->now()")))
		/*
			punt and get system time
		 */
		out = htdb_timestamp();

	return out;
}

bool_t	htdb_dbConnect(void)
{
	int	i;
	char	*prefix = "confDatabase";

	/*
		initialize the database handle hash
	 */
	db_initspace();

	/*
		and then loop to find defined database configurations
	 */
	for (i = 0 ; ; i++) {
		char	*name = htdb_getobjval(prefix, i + 1, "name");
		if (name && *name) {
			char	*driver = htdb_getobjval(prefix, i + 1, "driver"),
					*database = htdb_getobjval(prefix, i + 1, "database"),
					*username = htdb_getobjval(prefix, i + 1, "username"),
					*password = htdb_getobjval(prefix, i + 1, "password"),
					*host = htdb_getobjval(prefix, i + 1, "host"),
					*port = htdb_getobjval(prefix, i + 1, "port");
			_dbHandle	socket = htdb_dbOpenSocket(driver, host, port, database, username, password);

#if      (defined(USE_MYSQL) && defined(HAVE_LIBMYSQLCLIENT))
			if (socket == (_dbHandle)NULL) {
				/*
					could not access one of the specified databases
					bomb out completely.
					(we really need a good mechanism for communicating errors..)
				 */
				db_clearspace();
				return BOOL_FALSE;
			}
#endif

			{
				htdb_db_t	*p = (htdb_db_t *)malloc(sizeof(htdb_db_t));
				/*
					populate the dbhandle struct
				 */
				p->name = strdup(name);
				p->driver = strdup(driver);
				p->database = strdup(database);
				p->username = strdup(username);
				p->password = strdup(password);
				p->host = strdup(host);
				p->port = strdup(port);
				p->socket = socket;
				/*
					add this to the database hash
				 */
				db_store(name, p);
			}
		} else
			/*
				no more databases defined
			 */
			break;
	}

	/*
		else return success, since
		we are able to run without any
		valid database.
	 */
	return BOOL_TRUE;
}

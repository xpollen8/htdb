#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#if	0
#include "htdb.h"
#include "flash.h"

#if (defined(HAVE_LIBMING) && defined(USE_LIBMING))

#define	USE_GIF	1

bool_t	htdb_flash(void)
{
	SWFMovie m = newSWFMovieWithVersion(htdb_getint("movieAttribute->flashVersion"));

	/*
		we be ming'n
	 */
	Ming_init();

	SWFMovie_setRate(m,
		atof(htdb_getval("movieAttribute->rate")));

	{
		int			r = 0, g = 0, b = 0;

		sscanf(htdb_getval("movieAttribute->background_r"), "%x", &r);
		sscanf(htdb_getval("movieAttribute->background_g"), "%x", &g);
		sscanf(htdb_getval("movieAttribute->background_b"), "%x", &b);
		SWFMovie_setBackground(m, r, g, b);
	}

	SWFMovie_setDimension(m,
		htdb_getint("movieAttribute->width"),
		htdb_getint("movieAttribute->height"));

	SWFMovie_setNumberOfFrames(m,
		htdb_getint("movieAttribute->frames"));

	/*
		loop through the externally-defined movie attributes
		and dump them into the movie
	 */
	{
		int_t	i = 1;
		while (*htdb_getobjval("movieAttribute", i, "type")) {
			char	*type = htdb_getobjval("movieAttribute", i, "type");

			if ( !*htdb_getobjval("movieAttribute", i, "r") &&
					!*htdb_getobjval("movieAttribute", i, "g") &&
					!*htdb_getobjval("movieAttribute", i, "b") ) {
				htdb_setobjint("movieAttribute", i, "r", 
					htdb_getint("movieAttribute->default_r"));
				htdb_setobjint("movieAttribute", i, "g", 
					htdb_getint("movieAttribute->default_g"));
				htdb_setobjint("movieAttribute", i, "b", 
					htdb_getint("movieAttribute->default_b"));
			}
			if (strcasestr(type, "button")) {
				htdb_flashMakeButton(m,
					htdb_getobjint("movieAttribute", i, "button_x"),
					htdb_getobjint("movieAttribute", i, "button_y"),
					htdb_getobjint("movieAttribute", i, "text_x"),
					htdb_getobjint("movieAttribute", i, "text_y"),
					htdb_getobjint("movieAttribute", i, "width"),
					htdb_getobjint("movieAttribute", i, "height"),
					htdb_getobjval("movieAttribute", i, "r"),
					htdb_getobjval("movieAttribute", i, "g"),
					htdb_getobjval("movieAttribute", i, "b"),
					htdb_getobjint("movieAttribute", i, "fontSize"),
					htdb_getobjint("movieAttribute", i, "shadow"),
					htdb_getobjval("movieAttribute", i, "font"),
					htdb_getobjval("movieAttribute", i, "string"),
					htdb_getobjval("movieAttribute", i, "name"),
					htdb_getobjval("movieAttribute", i, "actionscript"),
					atof(htdb_getobjval("movieAttribute", i, "angle")),
					type);
			} else if (strcasestr(type, "script")) {
				SWFAction	a = compileSWFActionCode(htdb_getobjval("movieAttribute", i, "actionscript"));
				SWFMovie_add(m, (SWFBlock) a);
			} else if (strcasestr(type, "textfield")) {
				htdb_flashMakeTextField(m,
					htdb_getobjint("movieAttribute", i, "x"),
					htdb_getobjint("movieAttribute", i, "y"),
					htdb_getobjint("movieAttribute", i, "width"),
					htdb_getobjint("movieAttribute", i, "height"),
					htdb_getobjval("movieAttribute", i, "font"),
					htdb_getobjint("movieAttribute", i, "fontSize"),
					htdb_getobjval("movieAttribute", i, "r"),
					htdb_getobjval("movieAttribute", i, "g"),
					htdb_getobjval("movieAttribute", i, "b"),
					htdb_getobjval("movieAttribute", i, "name"),
					htdb_getobjval("movieAttribute", i, "string"),
					htdb_getobjint("movieAttribute", i, "maxChars"),
					htdb_getobjval("movieAttribute", i, "flags"));
			} else if (strcasestr(type, "textlink")) {
				htdb_flashMakeTextLink(m,
					htdb_getobjint("movieAttribute", i, "x"),
					htdb_getobjint("movieAttribute", i, "y"),
					htdb_getobjval("movieAttribute", i, "r"),
					htdb_getobjval("movieAttribute", i, "g"),
					htdb_getobjval("movieAttribute", i, "b"),
					htdb_getobjval("movieAttribute", i, "url"),
					htdb_getobjval("movieAttribute", i, "font"),
					htdb_getobjint("movieAttribute", i, "fontSize"),
					htdb_getobjval("movieAttribute", i, "string"));
			} else if (strcasestr(type, "text")) {
				htdb_flashMakeText(m,
					htdb_getobjint("movieAttribute", i, "x"),
					htdb_getobjint("movieAttribute", i, "y"),
					htdb_getobjval("movieAttribute", i, "r"),
					htdb_getobjval("movieAttribute", i, "g"),
					htdb_getobjval("movieAttribute", i, "b"),
					htdb_getobjint("movieAttribute", i, "fontSize"),
					atof(htdb_getobjval("movieAttribute", i, "angle")),
					htdb_getobjint("movieAttribute", i, "shadow"),
					htdb_getobjval("movieAttribute", i, "font"),
					htdb_getobjval("movieAttribute", i, "string"));
			} else if (strcasestr(type, "imageDBL")) {
				htdb_flashLoadImageAt(m,
					htdb_getobjval("movieAttribute", i, "file"),
					htdb_getobjint("movieAttribute", i, "thing_id"),
					htdb_getobjval("movieAttribute", i, "imgType"),
					htdb_getobjint("movieAttribute", i, "x"),
					htdb_getobjint("movieAttribute", i, "y"),
					htdb_getobjint("movieAttribute", i, "scale"),
					htdb_getobjval("movieAttribute", i, "url"),
					newSWFDBLBitmap_fromInput); 
			} else if (strcasestr(type, "imageJPEG")) {
				htdb_flashLoadImageAt(m,
					htdb_getobjval("movieAttribute", i, "file"),
					htdb_getobjint("movieAttribute", i, "thing_id"),
					htdb_getobjval("movieAttribute", i, "imgType"),
					htdb_getobjint("movieAttribute", i, "x"),
					htdb_getobjint("movieAttribute", i, "y"),
					htdb_getobjint("movieAttribute", i, "scale"),
					htdb_getobjval("movieAttribute", i, "url"),
					newSWFJpegBitmap_fromInput); 
			}
			i++;
		}
	}

	{
		/*
		FILE	*fp =fopen("/tmp/test.swf", "wb");
		SWFMovie_output(m, fileOutputMethod, fp->stdio_stream, -1);
		fclose(fp);
		*/
	}
	printf("Content-type: application/x-shockwave-flash\n\n");
	fflush(stdout);

	SWFMovie_output(m, fcgi_outputMethod, NULL, -1);

	destroySWFMovie(m);

	return BOOL_FALSE;
}

char	*htdb_flashAttributePath(char *file)
{
	static char	path[BUF_LIMIT_SMALL + 1];

	if ( file && *file && *file == '/' ) {
		/* interpret as based off the root */
		snprintf(path, BUF_LIMIT_SMALL, "%s%s", htdb_getval("cgi->document_root"), file);
	} else {	
		snprintf(path, BUF_LIMIT_SMALL, "%s/%s", htdb_getval("movieAttribute->path"), file);
	}
	return path;
}

SWFFont	htdb_flashLoadFont(char *font)
{
	FILE	*fp = fopen(htdb_flashAttributePath(font), "rb");

	if (fp) {
		
#if		(defined(USE_FASTCGI) && defined(HAVE_LIBFCGI))
		SWFFont f = loadSWFFontFromFile(fp->stdio_stream);
#else
		SWFFont f = loadSWFFontFromFile(fp);
#endif

		fclose(fp);

		return f;
	}
	return NULL;
}

SWFText	htdb_flashBuildText(int height, char *rr, char *gg, char *bb, int a, char *font, char *string)
{
	SWFFont	f = htdb_flashLoadFont(font);

	if (f) {
		SWFText t = newSWFText();

		SWFText_setFont(t, f);
		SWFText_setHeight(t, height);
		{
			int			r = 0, g = 0, b = 0;

			sscanf(rr, "%x", &r);
			sscanf(gg, "%x", &g);
			sscanf(bb, "%x", &b);

			SWFText_setColor(t, (byte)r, (byte)g, (byte)b, a);
		}
		SWFText_setSpacing(t, 0.0);
		SWFText_addString(t, string, NULL);
		return t;
	}
	return NULL;
}

void htdb_flashMakeText(SWFMovie movie, int x, int y, char *rr, char *gg, char *bb, int height, float angle, int shadow, char *font, char *string)
{
	if (shadow != 0) {
		SWFText			t = htdb_flashBuildText(height, "0x00", "0x00", "0x00", 0xcc, font, string);
		if (t) {
			SWFDisplayItem	d = SWFMovie_add(movie, (SWFBlock) t);

			SWFDisplayItem_move(d, x + shadow, y + shadow +(height*1.0));

			if (angle != 0)
				SWFDisplayItem_rotate(d, angle);
		}
	}
	{
		SWFText			t = htdb_flashBuildText(height, rr, gg, bb, 0xff, font, string);
		if (t) {
			SWFDisplayItem	d = SWFMovie_add(movie, (SWFBlock) t);

			SWFDisplayItem_move(d, x, y+(height*1.0));

			if (angle != 0)
				SWFDisplayItem_rotate(d, angle);
		}
	}
}

void htdb_flashMakeTextLink(SWFMovie movie, int x, int y, char *rr, char *gg, char *bb, char *url, char *font, int fontSize, char *string)
{
	char actionScript[BUF_LIMIT_BIG + 1];
	int width, height = fontSize;
	SWFButton	button = newSWFButton();
	SWFShape	shape;
	SWFFill f; 
	SWFDisplayItem i;
	int			r = 0, g = 0, b = 0;

	y += height;

	SWFText	normalText = htdb_flashBuildText(fontSize, rr, gg, bb, 0xff, font, string);

	i = SWFMovie_add(movie, (SWFBlock) normalText);
	SWFDisplayItem_moveTo(i, x, y);
	
	width = SWFText_getStringWidth(normalText, string);

	// underline the text
	sscanf(rr, "%x", &r);
	sscanf(gg, "%x", &g);
	sscanf(bb, "%x", &b);
	shape = newSWFShape();
	SWFShape_setLineStyle(shape, 10, r, g, b, 0xff);
	SWFShape_movePenTo(shape, 1, 2 );
	SWFShape_drawLineTo(shape, width, 2);
		
	i = SWFMovie_add(movie, (SWFBlock) shape);
	SWFDisplayItem_moveTo(i, x, y);

	snprintf(actionScript, BUF_LIMIT_BIG, "getURL1('%s', '_parent');", url);

	shape = newSWFShape();

	f = SWFShape_addSolidFill(shape, 0x00, 0x00, 0x00, 0x00);

	/*
		the button area mask
	 */
	SWFShape_setRightFill(shape, f);
	SWFShape_drawLine(shape, width, 0);
	SWFShape_drawLine(shape, 0, height);
	SWFShape_drawLine(shape, -width, 0);
	SWFShape_drawLine(shape, 0, -height);

	SWFButton_addAction(button, compileSWFActionCode(strdup(actionScript)), SWFBUTTON_MOUSEUP);
	SWFButton_addShape(button, shape, SWFBUTTON_OVER | SWFBUTTON_HIT);
	
	i = SWFMovie_add(movie, (SWFBlock) button);
	SWFDisplayItem_moveTo(i, x, y - height);
}

int	htdb_flashMakeFlags(char *in)
{
	int	flags = 0;
	if (strcasestr(in, "HASFONT"))
		flags |= SWFTEXTFIELD_HASFONT;
	if (strcasestr(in, "HASLENGTH"))
		flags |= SWFTEXTFIELD_HASLENGTH;
	if (strcasestr(in, "HASCOLOR"))
		flags |= SWFTEXTFIELD_HASCOLOR;
	if (strcasestr(in, "NOEDIT"))
		flags |= SWFTEXTFIELD_NOEDIT;
	if (strcasestr(in, "PASSWORD"))
		flags |= SWFTEXTFIELD_PASSWORD;
	if (strcasestr(in, "MULTILINE"))
		flags |= SWFTEXTFIELD_MULTILINE;
	if (strcasestr(in, "WORDWRAP"))
		flags |= SWFTEXTFIELD_WORDWRAP;
	if (strcasestr(in, "HASTEXT"))
		flags |= SWFTEXTFIELD_HASTEXT;
	if (strcasestr(in, "USEFONT"))
		flags |= SWFTEXTFIELD_USEFONT;
	if (strcasestr(in, "HTML"))
		flags |= SWFTEXTFIELD_HTML;
	if (strcasestr(in, "DRAWBOX"))
		flags |= SWFTEXTFIELD_DRAWBOX;
	if (strcasestr(in, "NOSELECT"))
		flags |= SWFTEXTFIELD_NOSELECT;
	if (strcasestr(in, "HASLAYOUT"))
		flags |= SWFTEXTFIELD_HASLAYOUT;
	if (strcasestr(in, "AUTOSIZE"))
		flags |= SWFTEXTFIELD_AUTOSIZE;
	return flags;
}

void	htdb_flashMakeTextField(SWFMovie movie, int_t x, int_t y, int_t width, int_t height,
	char *font, int_t fontSize, char *rr, char *gg, char *bb, char *name, char *string, int_t maxChars, char *flagstr)
{
	SWFFont f = htdb_flashLoadFont(font);

	if (f) {
		int		flags = htdb_flashMakeFlags(flagstr);
		SWFTextField	t = newSWFTextField();

		SWFTextField_setFont(t, f);
		SWFTextField_setFlags(t, flags);

		SWFTextField_setBounds(t, width, height);
		if (maxChars > 0)
			SWFTextField_setLength(t, maxChars);
		SWFTextField_setHeight(t, fontSize);
		{
			int			r = 0, g = 0, b = 0;

			sscanf(rr, "%x", &r);
			sscanf(gg, "%x", &g);
			sscanf(bb, "%x", &b);
			SWFTextField_setColor(t, r, g, b, 0xff);
		}
		SWFTextField_setVariableName(t, name);
		SWFTextField_addChars(t, "0123456789");
		SWFTextField_addString(t, string);

		{
			SWFDisplayItem i = SWFMovie_add(movie, (SWFBlock) t);
			SWFDisplayItem_moveTo(i, x, y);
		}
	}
}

void	htdb_flashLoadImageAt(SWFMovie movie, char *file, int_t thing_id, char *type, int x, int y, int scale_width, char *url, SWFBitmap(func)(SWFInput))
{
	off_t size = 0;
	struct stat	st;
	char *buf = NULL;
	if ( file && *file ) {
		char	*path = htdb_flashAttributePath(file);

		if (0 == stat(path, &st)) {
			int	fd = open(path, O_RDONLY);
			buf = (char *)malloc(sizeof(char) * st.st_size);
			read(fd, (void *)buf, st.st_size);
			close(fd);
	
			size = st.st_size;
		}
	} else if ( thing_id && type && *type ) {
		int	width = 0;
		char    *c = strpbrk(type, "0123456789");
		if (c && *c) {
			width = atoi(c);
			*c = '\0';
		}
	
		dbGetRowByKey("img",
			"select data, ext, width, height from image.folio where name like '%s\\_%ld\\_%dx%%' limit 1",
			type, thing_id, width);
	
		if (htdb_getint("img->numResults") == 0) {
			if (dbGetRowByKey("img",
				"select data, ext from image.folio where name = 'default_0_%dx%d' limit 1", width, width) == 0)
				dbGetRowByKey("img",
					"select data, ext from image.folio where name = 'default_0_%dx%d' limit 1", 250, 250);
		}

		buf = htdb_getbin("img->data");
		size = sizeof(char) * htdb_getint("img->data->bytes");
	}	
	{	
		SWFInput	input = newSWFInput_buffer(buf, size);
		SWFBitmap	bitmap = func(input);
		if (bitmap) {
			int			width = SWFBitmap_getWidth(bitmap),
						height = SWFBitmap_getHeight(bitmap);
			SWFShape	shape = newSWFShape();
			SWFFill		f = SWFShape_addBitmapFill(shape, bitmap, SWFFILL_CLIPPED_BITMAP);

			SWFShape_setRightFill(shape, f);

			SWFShape_drawLine(shape, width, 0);
			SWFShape_drawLine(shape, 0, height);
			SWFShape_drawLine(shape, -width, 0);
			SWFShape_drawLine(shape, 0, -height);


			if ( url && *url ) {
				char actionScript[BUF_LIMIT_BIG + 1];
				SWFDisplayItem i;
				SWFButton	button = newSWFButton();
				SWFShape	shape = newSWFShape();
				{
					SWFFill f = SWFShape_addSolidFill(shape, 0x00, 0x00, 0x00, 0x00);

					/*
						the button area mask
					 */
					SWFShape_setRightFill(shape, f);
					SWFShape_drawLine(shape, width, 0);
					SWFShape_drawLine(shape, 0, height);
					SWFShape_drawLine(shape, -width, 0);
					SWFShape_drawLine(shape, 0, -height);

					snprintf(actionScript, BUF_LIMIT_BIG, " getURL1('%s', '_parent'); \n", url);

					SWFButton_addAction(button, compileSWFActionCode(strdup(actionScript)), SWFBUTTON_MOUSEUP);
					SWFButton_addShape(button, shape, SWFBUTTON_OVER | SWFBUTTON_HIT);
					
					i = SWFMovie_add(movie, (SWFBlock) button);
					SWFDisplayItem_moveTo(i, x, y);
				
				}
			}	

			{
				SWFDisplayItem i = SWFMovie_add(movie, (SWFBlock) shape);

				SWFDisplayItem_moveTo(i, x, y);

				if (scale_width != 0)
					SWFDisplayItem_scale(i, scale_width / (float)width, scale_width / (float)height);

			}
		}
	}
}

void gb_error_default(const char *msg, ...)
{
/*
    va_list args;

    va_start(args, msg);
    vprintf(msg, args);
    va_end(args);
    exit(0);
*/
}

void	htdb_flashMakeButton(SWFMovie movie, int button_x, int button_y, int text_x, int text_y, int width, int height,
	char *rr, char *gg, char *bb, int fontSize, int shadow, char *font, char *string, char *name, char *actionscript, float angle, char *type)
{
	SWFButton	button = newSWFButton();
	SWFShape	shape = newSWFShape();
	{
		SWFFill f = SWFShape_addSolidFill(shape, 0x00, 0x00, 0x00, 0x00);

		/*
			the button area mask
		 */
		SWFShape_setRightFill(shape, f);
		SWFShape_drawLine(shape, width, 0);
		SWFShape_drawLine(shape, 0, height);
		SWFShape_drawLine(shape, -width, 0);
		SWFShape_drawLine(shape, 0, -height);

		/*
			button ornamentation
		 */
		if (strcasestr(type, "BUTTON_UNDERLINE")) {
			SWFShape_setLine(shape, 2, 0xff, 0x00, 0x00, 0x99);
			SWFShape_movePenTo(shape, 1, height-8);
			SWFShape_drawLineTo(shape, width, height-8);
		} else if (strcasestr(type, "BUTTON_SIDELINE")) {
			SWFShape	vline = newSWFShape(),
						sline = newSWFShape();

			SWFShape_setLine(vline, 1, 0xff, 0x99, 0x00, 0xcc);
			SWFShape_movePenTo(vline, 0, 0);
			SWFShape_drawLineTo(vline, 0, height);
			SWFButton_addShape(button, vline, 1);

			SWFShape_setLine(sline, 1, 0x00, 0x00, 0x00, 0xff);
			SWFShape_movePenTo(sline, 2, 2);
			SWFShape_drawLineTo(sline, 2, height + 2);
			SWFButton_addShape(button, sline, 1);

			/*
				the button mask
			 */
			SWFShape_setLine(shape, 1, 0xcc, 0xff, 0x00, 0xff);
			SWFShape_movePenTo(shape, 0, 0);
			SWFShape_drawLineTo(shape, 0, height);


		} else if (strcasestr(type, "BUTTON_SHADED")) {
			f = SWFShape_addSolidFill(shape, 0x00, 0x00, 0x00, 0x33);
			SWFShape_setRightFill(shape, f);
			SWFShape_drawLine(shape, width, 0);
			SWFShape_drawLine(shape, 0, height);
			SWFShape_drawLine(shape, -width, 0);
			SWFShape_drawLine(shape, 0, -height);
		} 
	} 

	/*
		the action to perform when the button is released
	 */
	if (actionscript && *actionscript) {
		SWFButton_addAction(button, compileSWFActionCode(actionscript),
			SWFBUTTON_MOUSEOUT | SWFBUTTON_MOUSEOVER |
			SWFBUTTON_HIT | SWFBUTTON_DOWN | SWFBUTTON_OVER | SWFBUTTON_UP);
	}
	/*
		add the shape to the button
	 */
	SWFButton_addShape(button, shape, SWFBUTTON_OVER | SWFBUTTON_HIT);

	/*
		and add the button to the movie
	 */
	{
		SWFDisplayItem i = SWFMovie_add(movie, (SWFBlock) button);
		SWFDisplayItem_setName(i, name);
		SWFDisplayItem_moveTo(i, button_x, button_y);
	}

	if (string && *string) {
		if (shadow) {
			/*
				the shadow text
			 */
			SWFText	shadowText = htdb_flashBuildText(fontSize, "0x00", "0x00", "0x00", 0x33, font, string);
			if (shadowText) {
				SWFDisplayItem i = SWFMovie_add(movie, (SWFBlock) shadowText);
				SWFDisplayItem_moveTo(i, text_x+shadow, text_y+shadow+(fontSize*1.0));
				SWFDisplayItem_rotate(i, angle);
			}
		}

		/*
			the normal text
		 */
		{
			SWFText	normalText = htdb_flashBuildText(fontSize, rr, gg, bb, 0xff, font, string);
			if (normalText) {
				SWFDisplayItem i = SWFMovie_add(movie, (SWFBlock) normalText);
				SWFDisplayItem_moveTo(i, text_x, text_y+(fontSize*1.0));
				SWFDisplayItem_rotate(i, angle);
			}
		}
	}

	setSWFErrorFunction(gb_error_default);
	setSWFWarnFunction(gb_error_default);
}

void	fcgi_outputMethod(byte b, void *data)
{
	putchar(b);
}

#else

bool_t	htdb_flash(void)
{
	return BOOL_FALSE;
}

#endif
#endif

#ident	"$Header$"

/*
	HTDB is shared under MIT License
	David Whittemore <del@adjective.com>
*/
#include	"libhtdb.h"

#ifdef	USE_TERNARY

Pnode root;

Pnode	htdb_ternary_create(void (*destructor)(void *), int size)
{
	Pnode   newternary = (Pnode)malloc(sizeof(Tnode));

	memset(newternary, 0L, (sizeof(Tnode)));

	return newternary;
}

void	*htdb_ternary_value(Pnode p, char *s)
{
	Pdata	v = htdb_ternary_search(p, s);
	if (v)
		return (void *)v->value;
	else
		return (void *)NULL;
}

Pdata	htdb_ternary_search(Pnode p, char *s)
{
	while (p) {
		if (*s < p->splitchar)
			p = p->lokid;
		else if (*s == p->splitchar) {
			if (*s++ == 0)
				return (Pdata)p->eqkid;
			p = p->eqkid;
		} else
			p = p->hikid;
	}
	return NULL;
}

void	htdb_ternary_insert(Pnode *p, char *s, Pdata val)
{
	int		d;
	Pnode	pp;

	while ((pp = *p)) {
		if ((d = *s - pp->splitchar) == 0) {
			if (*s++ == 0) return;
			p = &(pp->eqkid);
		} else if (d < 0)
			p = &(pp->lokid);
		else
			p = &(pp->hikid);
	}
	for (;;) {
		pp = *p = (Pnode) malloc(sizeof(Tnode));
		pp->splitchar = *s;
		pp->lokid = pp->eqkid = pp->hikid = 0;
		if (*s++ == 0) {
			pp->eqkid = (Pnode) val;
			return;
		}
		p = &(pp->eqkid);
	}
}

/*

Partial-Match Searching

We turn next to the more subtle problem of "partial-match" or "crossword puzzle"
searching: A query string may contain both regular letters and the "don't care" character
".". Searching the dictionary for the pattern ".u.u.u" matches the single word auhuhu, while
the pattern ".a.a.a" matches 94 words, including banana, casaba, and pajama. (That pattern
does not match abracadabra, though. The entire word must match the pattern; we do not
look for patterns in the middle of longer words.)

This venerable problem has been studied in many papers, such as in "The World's Fastest
Scrabble Program," by A.W. Appel and G.J. Jacobson (Communications of the ACM, May
1988). In "Partial-Match Retrieval Algorithms," (SIAM Journal on Computing, 5, 1976),
R.L. Rivest presents an algorithm for partial-match searching in digital tries: Take the
single given branch if a letter is specified, for a don't-care character, recursively search all
branches. Function pmsearch implements Rivest's method in ternary search trees. The
function puts pointers to matching words in srcharr[0..srchtop-1]. It is called, for instance,
by:

     srchtop = 0;
     pmternary_search(root, ".a.a.a");

The following is the complete search code:
*/

/*
     void pmternary_search(Pnode p, char *s)
     {    if  (!p) return;
	  nodecnt++;
	  if (*s == '.' || *s < p->splitchar)
	     pmternary_search(p->lokid, s);
	  if (*s == '.' || *s == p->splitchar)
	     if (p->splitchar && *s)
		 pmternary_search(p->eqkid, s+1);
	  if (*s == 0 && p->splitchar == 0)
		 srcharr[srchtop++] =
		     (char *) p->eqkid;
	  if (*s == '.' || *s > p->splitchar)
		 pmternary_search(p->hikid, s);
     }
*/

/*
Function pmsearch has five if statements. The second and fifth if statements are
symmetric; they recursively search the lokid (or hikid) when the search character is the
don't care "." or when the search string is less (or greater) than the splitchar. The third if
statement recursively searches the eqkid if both the splitchar and string are nonnull. The
fourth if statement detects a match to the query and adds the pointer to the complete word
(stored in eqkid by our sleazy hack).

Rivest states that partial-match search in a trie requires "time about O(n(k-s)/k) to respond
to a query word with s letters specified, given a file of n k-letter words." Ternary search
trees can be viewed as an implementation of his tries (with binary trees implementing
multiway branching), so we expected his results to apply immediately to our program. Our
experiments, however, led to a surprise: Unspecified positions at the front of the query
word are dramatically more costly than unspecified characters at the end of the word.
(Rivest suggested shuffling the characters in a word to avoid such problems.) Table 3 gives
the flavor of our experiments. The first line says that the search visited 18 nodes to find the
single match to the pattern "banana." The tree contains a total of 1,026,033 nodes; searches
with some of the first letters specified visit just a tiny fraction of those nodes.

Near-Neighbor Searching

We finally examine the problem of "near-neighbor searching" in a set of strings: We are
to find all words in the dictionary that are within a given Hamming distance of a query
word. For instance, a search for all words within distance two of Dobbs finds Debby,
hobby, and 14 other words.

Function nearsearch performs a near-neighbor search in a ternary search tree. Its three
arguments are a tree node, string, and distance. The first if statement returns if the node is
null or the distance is negative. The second and fourth if statements are symmetric: They
search the appropriate child if the distance is positive or if the query character is on the
appropriate side of splitchar. The third if statement either checks for a match or
recursively searches the middle child.
*/

/*
     void nearternary_search(Pnode p, char *s, int d)
     {   if (!p || d < 0) return;
	 if (d > 0 || *s < p->splitchar)
	     nearternary_search(p->lokid, s, d);
	 if (p->splitchar == 0) {
	    if ((int) strlen(s) <= d)
	       srcharr[srchtop++] = (char *) p->eqkid;
	 } else
	    nearternary_search(p->eqkid, *s ? s+1:s,
	       (*s == p->splitchar) ? d:d-1);
	 if (d > 0 || *s > p->splitchar)
	     nearternary_search(p->hikid, s, d);
     }
*/

Pdata	htdb_ternary_newvalue(char *key, void *value, int is_string)
{
	char	*str = (char *)value;
	Pdata	ret = (Pdata)malloc(sizeof(Tdata));
	int len = (is_string == 1 && str && *str ? strlen(str) : abs(is_string));

	ret->key = (char *)strdup(key);

	if (is_string < 0)
		ret->size = ret->limit = abs(is_string);
	else {

		while (len > 0 && str && *str && isspace((int)*str)) {
			str++;
			len--;
		}

		if (str && *str && len > 0)
			while (len > 0) {
				if (isspace((int)str[len - 1]))
					str[--len] = '\0';
				else
					break;
			}
		ret->value = str;
		ret->size = ret->limit = len;
	}
	return ret;
}

Pdata	htdb_ternary_node(Pnode p, char *key)
{
	Pdata	data = htdb_ternary_search(p, key);
	if (data)
		return data;
	return NULL;
}

int	htdb_ternary_size(Pnode p, char *key)
{
	Pdata	data = htdb_ternary_search(p, key);
	if (data)
		return data->size;
	return 0;
}

static void	htdb_ternary_printit(char *key)
{
	Pdata	data = htdb_ternary_search(root, key);
	if (data)
		printf("%d:%s:%s\n", data->size, data->key, (char *)data->value);
	fflush(stdout);
}

spacePData htdb_ternary_traverse(Pnode p)
{
	if (!p)
		return NULL;
	return htdb_ternary_traverse(p->lokid);
	if (p->splitchar)
		return htdb_ternary_traverse(p->eqkid);
	else  {
		Pdata buh = (Pdata)p->eqkid;
		return buh;
		//printf("%s:%d<hr>\n", (char *) buh->key, buh->size);
		//fflush(stdout);
	}
	return htdb_ternary_traverse(p->hikid);
}

void htdb_ternary_dupe(Pnode p, Pnode pp)
{
	if (!p)
		return;
	htdb_ternary_dupe(p->lokid, pp);
	if (p->splitchar)
		htdb_ternary_dupe(p->eqkid, pp);
	else  {
		Pdata buh = (Pdata)p->eqkid;
		htdb_ternary_insert(&pp,
			buh->key, htdb_ternary_newvalue(buh->key, (void *)buh->value, (buh->size < 0 ? buh->size : 1)));
	}
	htdb_ternary_dupe(p->hikid, pp);
}

void htdb_ternary_destroy(Pnode p)
{
	if (!p)
		return;
	htdb_ternary_destroy(p->lokid);
	htdb_ternary_destroy(p->hikid);
	if (p->splitchar)
		htdb_ternary_destroy(p->eqkid);
	else  {
		Pdata buh = (Pdata)p->eqkid;
		printf("(%s)<hr>\n", buh->key);
		fflush(stdout);
		free(buh->key);
		free(buh->value);
		free(buh);
	}
}
#endif

#ident	"$Header$"

/*
	HTDB is shared under MIT License
	David Whittemore <del@adjective.com>
*/
#include	"libhtdb.h"

#if (defined(HAVE_LIBXML2) && defined(USE_LIBXML2))

#undef	DEBUG 

/*
	prefix = "foo", name = "bar"	
	shortname == 0 ? 
		foo, bar  -> foo->bar[1]
	shortname == 1 ?
		foo, bar  -> foo->bar

	sets foo->bar->numResults as a side effect, ONLY when shortName is 0.
*/

static char	*htdb_parseXML_getdynname(char *prefix, char *name, int shortName)
{
	int cnt;
	char *buf = malloc((prefix ? strlen(prefix) : 0) + (name ? strlen(name) : 0) + 20);

	if ( !prefix ) {
		/* dealing with the top level node */
		if ( shortName ) {
			if (buf) {
				free(buf);
				buf = NULL;
			}
			return NULL;
		} else
			snprintf(buf, BUF_LIMIT_BIG, "%s", name);
	} else {
		snprintf(buf, BUF_LIMIT_BIG, "%s->%s->numResults", prefix, name);
		cnt = htdb_getint(buf);

		if ( shortName ) {
			if ( cnt ) {
				if (buf) {
					free(buf);
					buf = NULL;
				}
				return NULL;
			} else
				snprintf(buf, BUF_LIMIT_BIG, "%s->%s", prefix, name);
		} else { 
			htdb_setint(buf, ++cnt);
			snprintf(buf, BUF_LIMIT_BIG, "%s->%s[%d]", prefix, name, cnt);
		}
	} 	
	
	return buf;
}

static void	htdb_parseXML_addField(char *prefix, char *name)
{
	int field_cnt;
	char *pref;

	field_cnt = htdb_getptrint(prefix, "numFields");
	field_cnt++;

	/*
			store field name in:
					prefix->field[]
	 */
	pref = htdb_getptrdyname(prefix, "field");
	htdb_setarrayval(pref, field_cnt, name);
#ifdef	DEBUG
	log_file("/tmp/xml.log", "setting %s[%d] -> '%s'", pref, field_cnt, name);
#endif
	free(pref);
	
	htdb_setptrint(prefix, "numFields", field_cnt);
}


void
htdb_parseXML_clearspace(char *prefix)
{
	spaceData   *b;
	int len = strlen(prefix);
	htdb_initwalk();
	while ((b = htdb_walkspace())) {
		if (strncmp(b->key, prefix , len) == 0) {
			free(b->value);
			b->value = strdup("");
		}
	}
}

static int	htdb_parseXML_node(char *prefix,  xmlNodePtr node, bool_t isRoot);

static int	handle_content_node(char *prefix, xmlNodePtr node, int shortName)
{
	xmlAttrPtr attrib;
	xmlChar *content;
	char *newprefix, *attribname;

	newprefix = htdb_parseXML_getdynname(prefix, (char *)node->name, shortName);

	if ( !newprefix ) /* most likely couldn't get a short name for it */
		return 0;

	if ( prefix && shortName == 0 ) 	
		htdb_parseXML_addField(prefix, (char *) node->name);


	attrib = node->properties;
	while(attrib) {
		attribname = htdb_parseXML_getdynname(newprefix, (char *) attrib->name, shortName);
		content = xmlNodeGetContent(attrib->children);
		htdb_setval(attribname, (char *)content);			
#ifdef	DEBUG
		log_file ("/tmp/xml.log", "Setting %s -> %s", attribname, content);
#endif
		htdb_parseXML_addField(newprefix, (char *) attrib->name);
		attrib = attrib->next;
		free(attribname);
	}		

	if ( node->children ) 
		htdb_parseXML_node(newprefix, node->children, BOOL_FALSE);

	free(newprefix);
	return 0;
}

static int	htdb_parseXML_node(char *prefix,  xmlNodePtr node, bool_t isRoot)
{
	xmlAttrPtr attrib;
	xmlNodePtr walk = node;
	xmlChar *content;
	char *newprefix, *attribname;

	while ( walk ) {
		/* skip over whitespace nodes */
		if ( !walk->name || xmlIsBlankNode(walk) ) {
			walk = walk->next;		
			continue;
		}

		if (BOOL_TRUE == isRoot && prefix && (char *)walk->name && *(char *)walk->name &&
			!*htdb_getptrval(prefix, "root")) {
			htdb_setptrval(prefix, "root", (char *)walk->name);
		}

		if ( xmlNodeIsText(walk) ) {
			xmlChar *content =  xmlNodeGetContent(walk);
			char *encoded, 
					*enc_to;

			char *ugh = filterProgramArguments((char *)content);	
#ifdef	DEBUG			
			log_file ("/tmp/xml.log", "Setting %s -> %s", prefix, ugh);
#endif
			htdb_setval(prefix, ugh);				
			free(ugh);
		} else { 
			handle_content_node(prefix, walk, 1);	
			handle_content_node(prefix, walk, 0);	
		}

		walk = walk->next;
	}
	return 0;
}

/* 
	parse the XML in the given buffer to an htdb-friendly representation.

	if prefix is null, use the name of the first top level xml node:
		------
		<xbel version="1.0" folded="no" id="139597352">
			<title>My Bookmarks</title>
			 <info>
			 ....
		-----
		transforms to xbel->version = 1.0, 
					  xbel->title = "my Bookmarks",
		and on and on.
 */
bool_t		htdb_parseXML(char *prefix,  char *buf) 
{
	xmlNodePtr root;
	xmlInitParser();

	htdb_setptrval(prefix, "raw", buf);
	htdb_setptrval(prefix, "error", "");
	htdb_setptrval(prefix, "root", "");

	{
		xmlDocPtr doc = xmlParseDoc((unsigned char *)buf);
		if ( !doc ) {
			htdb_setptrval(prefix, "error", "htdb_parseXML() document could not be parsed");
			return BOOL_FALSE;
		}

		root = xmlDocGetRootElement(doc);
		htdb_parseXML_node(prefix, root, BOOL_TRUE);

		xmlFreeDoc(doc);
	}
	xmlCleanupParser();

	if (NULL == prefix)
		return BOOL_TRUE;

	if (!*htdb_getptrval(prefix, "root")) {
		htdb_setptrval(prefix, "error", "htdb_parseXML() could not find namespace");
		return BOOL_FALSE;
	} else
		return BOOL_TRUE;
}

#else

bool_t		htdb_parseXML(char *prefix,  char *buf) 
{
	return BOOL_FALSE;
}

#endif


void	htdb_clearXML(char *prefix)
{
	spaceData   *b;
	int len = strlen(prefix);
	htdb_initwalk();
	while ((b = htdb_walkspace())) {
		if (strncmp(b->key, prefix , len) == 0) {
			free(b->value);
			b->value = strdup("");
		}
	}
}

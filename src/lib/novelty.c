#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

struct roman_number {
    char	letter;
    int		value;
};

static struct roman_number roman[] = {
    {'I', 1},
    {'V', 5},
    {'X', 10},
    {'L', 50},
    {'C', 100},
    {'D', 500},
    {'M', 1000},
    {'\0', 0}
};

int	htdb_rtoi(char *rom)
{
	int	num = 0,
		state = 0,
		idx;
	char *p = rom + strlen(rom);

	while (p >= rom){
		idx = 0;
		while (roman[idx].value > 0) {
			if (*p == roman[idx].letter || *p == roman[idx].letter + (char)32) {
				if (state > roman[idx].value) {
					num -= roman[idx].value;
				} else {
					num += roman[idx].value;
					state = roman[idx].value;
				}
			}
			idx++;
		}
		p--;
	}
	return num;
}

void htdb_itor(int num, char **str)
{
	int	idx = sizeof(roman) / sizeof(struct roman_number) - 2,
		foo;
	char *p = *str;

	while (idx >= 0) {
		foo = num / roman[idx].value;
		if (foo != 0)
			num = num % (foo * roman[idx].value);
		if (foo > 4 && foo < 9) {
			*(p++) = roman[idx+1].letter;
			*p = roman[idx+1].letter;
			foo -= 5;
		}
		if (foo == 4) {
			*(p++) = roman[idx].letter;
			*(p++) = roman[idx+1].letter;
		} else if (foo == 9) {
			*(p++) = roman[idx].letter;
			*(p++) = roman[idx+2].letter;
		} else {
			while (foo != 0) {
				if (roman[idx].letter)
					*(p++) = roman[idx].letter;
				else
					*(p++) = foo;
				foo--;
			}
		}
		idx-=2;
	}
	*p = '\0';
}

char	*htdb_braille(char *in)
{
	bool_t	prevWasDigit = BOOL_FALSE;
	spaceData	b;

	data_init(&b);

	thing_append(&b, "<!-- ", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	thing_append(&b, in, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	thing_append(&b, "-->", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);

	while (in && *in) {
		if (isupper((int)*in)) {
			thing_append(&b, "${mkBraille(w=cap)}", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			thing_append(&b, "${mkBraille(w=", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			thing_appendChar(&b, tolower(*in), BUF_LIMIT_BIG);
			thing_append(&b, ")}", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			prevWasDigit = BOOL_FALSE;
		} else if (islower((int)*in)) {
			thing_append(&b, "${mkBraille(w=", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
			thing_append(&b, ")}", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			prevWasDigit = BOOL_FALSE;
		} else if (isdigit((int)*in)) {
			char	c = 0;
			if (BOOL_FALSE == prevWasDigit) {
				/*
					mark as number
				 */
				thing_append(&b, "${mkBraille(w=num)}", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			}
			switch (*in) {
				case '1': c = 'a'; break;
				case '2': c = 'b'; break;
				case '3': c = 'c'; break;
				case '4': c = 'd'; break;
				case '5': c = 'e'; break;
				case '6': c = 'f'; break;
				case '7': c = 'g'; break;
				case '8': c = 'h'; break;
				case '9': c = 'i'; break;
				case '0': c = 'j'; break;
			}
			thing_append(&b, "${mkBraille(w=", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			thing_appendChar(&b, c, BUF_LIMIT_BIG);
			thing_append(&b, ")}", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			prevWasDigit = BOOL_TRUE;
		} else if (*in == ',') {
			thing_append(&b, "${mkBraille(w=comma)}", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			prevWasDigit = BOOL_FALSE;
		} else if (*in == '!') {
			thing_append(&b, "${mkBraille(w=ex)}", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			prevWasDigit = BOOL_FALSE;
		} else if (*in == '.') {
			thing_append(&b, "${mkBraille(w=period)}", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			prevWasDigit = BOOL_FALSE;
		} else if (*in == '?') {
			thing_append(&b, "${mkBraille(w=qu)}", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			prevWasDigit = BOOL_FALSE;
		} else if (*in == '\'') {
			thing_append(&b, "${mkBraille(w=quote)}", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			prevWasDigit = BOOL_FALSE;
		} else if (isspace((int)*in)) {
			thing_append(&b, " ", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		} else if (*in == '<') {
			while (in && *in && *in != '>') {
				thing_appendChar(&b, *in, BUF_LIMIT_BIG);
				in++;
			}
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
		}
		if (in && *in)
			in++;
	}	
	return b.value;
}

char	*htdb_igpayatinlay(char *in)
{
	char	*ptr = strtok(in, "\n\r\t ");
	spaceData	b;

	data_init(&b);

	do {
		if (ptr && *ptr != '<') {
			int		len = strlen(ptr);
			char	*pigish = (char *)malloc(sizeof(char) * (len + 3));

			strcpy(pigish, &ptr[1]);
			pigish[len - 1] = ptr[0];
			pigish[len] = '\0';
			strcat(pigish, "ay");

			thing_append(&b, pigish, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			thing_appendChar(&b, ' ', BUF_LIMIT_BIG);
			free(pigish);
		}
	} while (ptr && (ptr = strtok(NULL, "\n\r\t ,")));

	return (b.value);
}

char	*htdb_rot13(char *in)
{
	spaceData	b;
	data_init(&b);
	while (in && *in) {
		if (*in >= 'A' && *in < 'M')
			thing_appendChar(&b, *in + 13, BUF_LIMIT_BIG);
		else if (*in >= 'a' && *in < 'm')
			thing_appendChar(&b, *in + 13, BUF_LIMIT_BIG);
		else if (*in >= 'N' && *in <= 'Z')
			thing_appendChar(&b, *in - 13, BUF_LIMIT_BIG);
		else if (*in >= 'n' && *in <= 'z')
			thing_appendChar(&b, *in - 13, BUF_LIMIT_BIG);
		else
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
		in++;
	}

	return (b.value);
}

char	*htdb_morse(char *in)
{
	spaceData	b;
	static spaceName morse = NULL;

	if (morse == NULL) {
		space_init(morse, HASHTABSIZE_SMALL, htdb_destroyStringTuple);
		space_store(morse, "A", strdup(".-"), 1);
		space_store(morse, "B", strdup("-..."), 1);
		space_store(morse, "C", strdup("-.-."), 1);
		space_store(morse, "D", strdup("-.."), 1);
		space_store(morse, "E", strdup("."), 1);
		space_store(morse, "F", strdup("..-."), 1);
		space_store(morse, "G", strdup("--."), 1);
		space_store(morse, "H", strdup("...."), 1);
		space_store(morse, "I", strdup(".."), 1);
		space_store(morse, "J", strdup(".---"), 1);
		space_store(morse, "K", strdup("-.-"), 1);
		space_store(morse, "L", strdup(".-.."), 1);
		space_store(morse, "M", strdup("--"), 1);
		space_store(morse, "N", strdup("-."), 1);
		space_store(morse, "O", strdup("---"), 1);
		space_store(morse, "P", strdup(".--."), 1);
		space_store(morse, "Q", strdup("--.-"), 1);
		space_store(morse, "R", strdup(".-."), 1);
		space_store(morse, "S", strdup("..."), 1);
		space_store(morse, "T", strdup("-"), 1);
		space_store(morse, "U", strdup("..-"), 1);
		space_store(morse, "V", strdup("...-"), 1);
		space_store(morse, "W", strdup(".--"), 1);
		space_store(morse, "X", strdup("-..-"), 1);
		space_store(morse, "Y", strdup("-.--"), 1);
		space_store(morse, "Z", strdup("--.."), 1);
		space_store(morse, "1", strdup(".----"), 1);
		space_store(morse, "2", strdup("..---"), 1);
		space_store(morse, "3", strdup("...--"), 1);
		space_store(morse, "4", strdup("....-"), 1);
		space_store(morse, "5", strdup("....."), 1);
		space_store(morse, "6", strdup("-...."), 1);
		space_store(morse, "7", strdup("--..."), 1);
		space_store(morse, "8", strdup("---.."), 1);
		space_store(morse, "9", strdup("----."), 1);
		space_store(morse, "0", strdup("-----"), 1);
		space_store(morse, ".", strdup(".-.-.-"), 1);
		space_store(morse, ",", strdup("--..--"), 1);
		space_store(morse, "?", strdup("..--.."), 1);
	}

	data_init(&b);

	while (in && *in) {
		if (isspace((int)*in))
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
		else {
			char	lk[2],
				*lookup;
			lk[0] = *in;
			lk[1] = '\0';
			lookup = space_value(morse, lk);
			if (lookup && *lookup) {
				thing_append(&b, lookup, strlen(lookup), BUF_LIMIT_BIG);
				thing_appendChar(&b, ' ', BUF_LIMIT_BIG);
			}
		}
		in++;
	}

	return (b.value);
}

char	*htdb_demorse(char *in)
{
	char	*ptr = strtok(in, "\n\r\t ");
	spaceData	b;
	static spaceName demorse = NULL;

	if (demorse == NULL) {
		space_init(demorse, HASHTABSIZE_SMALL, htdb_destroyStringTuple);
		space_store(demorse, ".-", strdup("A"), 1);
		space_store(demorse, "-...", strdup("B"), 1);
		space_store(demorse, "-.-.", strdup("C"), 1);
		space_store(demorse, "-..", strdup("D"), 1);
		space_store(demorse, ".", strdup("E"), 1);
		space_store(demorse, "..-.", strdup("F"), 1);
		space_store(demorse, "--.", strdup("G"), 1);
		space_store(demorse, "....", strdup("H"), 1);
		space_store(demorse, "..", strdup("I"), 1);
		space_store(demorse, ".---", strdup("J"), 1);
		space_store(demorse, "-.-", strdup("K"), 1);
		space_store(demorse, ".-..", strdup("L"), 1);
		space_store(demorse, "--", strdup("M"), 1);
		space_store(demorse, "-.", strdup("N"), 1);
		space_store(demorse, "---", strdup("O"), 1);
		space_store(demorse, ".--.", strdup("P"), 1);
		space_store(demorse, "--.-", strdup("Q"), 1);
		space_store(demorse, ".-.", strdup("R"), 1);
		space_store(demorse, "...", strdup("S"), 1);
		space_store(demorse, "-", strdup("T"), 1);
		space_store(demorse, "..-", strdup("U"), 1);
		space_store(demorse, "...-", strdup("V"), 1);
		space_store(demorse, ".--", strdup("W"), 1);
		space_store(demorse, "-..-", strdup("X"), 1);
		space_store(demorse, "-.--", strdup("Y"), 1);
		space_store(demorse, "--..", strdup("Z"), 1);
		space_store(demorse, ".----", strdup("1"), 1);
		space_store(demorse, "..---", strdup("2"), 1);
		space_store(demorse, "...--", strdup("3"), 1);
		space_store(demorse, "....-", strdup("4"), 1);
		space_store(demorse, ".....", strdup("5"), 1);
		space_store(demorse, "-....", strdup("6"), 1);
		space_store(demorse, "--...", strdup("7"), 1);
		space_store(demorse, "---..", strdup("8"), 1);
		space_store(demorse, "----.", strdup("9"), 1);
		space_store(demorse, "-----", strdup("0"), 1);
		space_store(demorse, ".-.-.-", strdup("."), 1);
		space_store(demorse, "--..--", strdup(","), 1);
		space_store(demorse, "..--..", strdup("?"), 1);
	}

	data_init(&b);

	do {
		if (ptr) {
			char	*lookup = space_value(demorse, ptr);
			if (lookup && *lookup) {
				thing_appendChar(&b, *lookup, BUF_LIMIT_BIG);
				thing_appendChar(&b, ' ', BUF_LIMIT_BIG);
			}
		}
	} while (ptr && (ptr = strtok(NULL, "\n\r\t ,")));

	return (b.value);
}

char	*htdb_imagify(char *in)
{
	int	intag = 0;
	spaceData	b;

	data_init(&b);

	while (in && *in) {
		if (intag && *in == '>') {
			intag = 0;
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
		} else if (!intag && *in == '<') {
			intag = 1;
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
		} else if (intag || !isalpha((int)*in)) {
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
		} else {
			thing_append(&b, "<img border=\"0\" src=\"/images/alpha/",
				FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
			thing_append(&b, ".gif\"", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			if (isupper((int)*in))
					thing_append(&b, " height=\"14\" width=\"15\" alt=\"", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			else
					thing_append(&b, " height=\"10\" width=\"11\" alt=\"", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
			thing_append(&b, "\">", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		}
		in++;
	}

	return (b.value);
}


#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

int_t	htdb_fetchVerbage(char *prefix, char *lookup)
{
	return dbGetRowByKey(prefix,
		"select * from verbage where resource='%s'", lookup);
}

char	*htdb_lookupVerbage(char *lookup)
{
    if (htdb_fetchVerbage("_esp", lookup) > 0) {
		char	*cleaned = htdb_unfilter_unsafe(htdb_getval("_esp->body")),
				*expanded = htdb_expand(cleaned);
		free(cleaned);
		return expanded;
	} else
		return strdup("");
}

void	htdb_emailVerbage(char *lookup)
{
	char	*ptr = htdb_lookupVerbage(lookup);
	sendmail(htdb_getval("confSendmail"), ptr);
	free(ptr);
}


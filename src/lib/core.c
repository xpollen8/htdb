#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

/*
	resSpace
		holds vanilla name/value resource definitions
 */
spaceName	resSpace = (spaceName)NULL;

/*
	staticSpace
		holds resource definitions from config.htdb
 */
spaceName	staticSpace = (spaceName)NULL;

/*
	funcSpace
		holds function() definitions
		these functions are cache-able (and over-rideable)
 */
spaceName	funcSpace = (spaceName)NULL;

/*
	cfuncSpace
		for lookup of static HTDB functions
		and loaded DSO HTDB functions.
 */
spaceName	cfuncSpace = (spaceName)NULL;

/*
	fileSpace
		holds open file pointers created by ${fopen()}
		used by ${fwrite()}/${fread()} and closed by ${fclose()}
 */
spaceName	fileSpace = (spaceName)NULL;

#ifdef	USE_XML_STYLEE
/*
	tagSpace
		holds <tag> definitions
 */
spaceName	tagSpace = (spaceName)NULL;
#endif

/*
	used in timing how long each page generation takes
 */
struct timeval  istart;

#define	TOKEN_EOF		0
#define	TOKEN_INCLUDE	1
#define	TOKEN_DEFINE	2
#define	TOKEN_COMMENT	3
#define	TOKEN_LIVE		4

/*
	== BOOL_FALSE when outside main fcgiLoop
	== BOOL_TRUE when inside main fcgiLoop

	used to know when "cached" files are being read.
	(so that we can maintain a list of them for
	file-changed/reload-needed detection)
 */
static bool_t	inMainLoop = BOOL_FALSE;
#ifdef	 USE_DELAYED_LOOKUP
#define	shoveDefine(a, b)	htdb_store((a), (BOOL_TRUE == inMainLoop ? strdup(b) : htdb_expand(b)))
#else
#define	shoveDefine(a, b)	htdb_store((a), (BOOL_TRUE == inMainLoop ? htdb_expand(b) : htdb_expand(b)))
//#define	shoveDefine(a, b)	htdb_store((a), (strstr(a, ".html") ? strdup(b) : (BOOL_TRUE == inMainLoop ? htdb_expand(b) : htdb_expand(b))))
#endif

/********************************************************************************

	begin private functions

 ********************************************************************************/

/*
	we use hash tables to store various classes of things:
	database connection information, resource name:value pairs,
	script function definitions, etc.

	each class has an associated destructor function.
	these functions follow..
 */
void	htdb_destroyStringTuple(void *value)
{
	free(value);
	value = NULL;
}

void	htdb_destroyScriptFunction(void *value)
{
	sfunc_t	*sf = (sfunc_t *)value;

	htdb_deleteArgs(sf->args, sf->num_args);
	htdb_deleteArgs(sf->defaults, sf->num_args);
	free(sf->body);

	free(sf);
	sf = NULL;
}

void	htdb_destroyDatabase(void *value)
{
	if (value) {
		htdb_db_t	*d = (htdb_db_t *)value;

		if (d->name)
			free(d->name);
		if (d->driver)
			free(d->driver);
		if (d->database)
			free(d->database);
		if (d->username)
			free(d->username);
		if (d->password)
			free(d->password);
		if (d->host)
			free(d->host);
		if (d->port)
			free(d->port);
#if	(defined(USE_MYSQL) && defined(HAVE_LIBMYSQLCLIENT))
		if (d->socket)
			mysql_close(d->socket);
#endif
		if (d->socket)
			free(d->socket);

		if (d)
			free(d);
		d = NULL;
	}
}

void	htdb_destroyFunctionPointer(void *value)
{
	/*
		no op
	 */
}

void	htdb_destroyFilePointer(void *value)
{
	filep_t	*fpt = (filep_t *)value;

	if (fpt != (filep_t *)NULL) {
		if ((FILE *)fpt->fp != (FILE *)NULL)
			fclose((FILE *)fpt->fp);
		fpt->fp = NULL;
		if (fpt->path && *fpt->path)
			free(fpt->path);
		fpt->path = NULL;
		if (fpt->mode && *fpt->mode)
			free(fpt->mode);
		fpt->mode = NULL;

		free(fpt);
		fpt = NULL;
	}
}

#ifdef	USE_XML_STYLEE
/*
	htdb_settag()

		shove this tag declaration into the tagSpace hash

	returns: nada
 */
static void	htdb_settag(char *x, char *body)
{
	if (x && *x && body && *body) {
		Hashtable	tH = htdb_hash_create(htdb_destroyStringTuple, HASHTABSIZE_SMALL);
		char	*key = strtok(x, "\n\r\t ,"),
			*args;

		if (key)
			/*
				if we found another token,
				then we have arguments..
			 */
			args = strtok(NULL, "\n\r\t ,");

		if (args)
			/*
				squirrel the raw arguments away
			 */
			htdb_hash_define(tH, "raw_args", strdup(args), 1);
		else
			/*
				blank argument place-holder
			 */
			htdb_hash_define(tH, "raw_args", strdup(""), 1);
		{
			/*
				each tag entry is a hashtable
				which hold the original argument list, each
				argument name, and the tag body.
			 */
			char	*ptr = args;
			int	argno = 0;

			/*
				loop and store the argument names in the hash
			 */
			do {
				if (ptr)
					htdb_hash_define(tH, htdb_getarrayname("arg", argno++), strdup(ptr), 1);
			} while (ptr && (ptr = strtok(NULL, "\n\r\t ,")));

			/*
				note the number of arguments for later..
			 */
			{
				char	jnk[BUF_LIMIT_TINY + 1];
				snprintf(jnk, BUF_LIMIT_TINY, "%d", argno);
				jnk[BUF_LIMIT_TINY] = '\0';
				htdb_hash_define(tH, "arg->numResults", strdup(jnk), 1);
			}

		}
		/*
			store the body of the tag
		 */
		htdb_hash_define(tH, "body", strdup(body), 1);

		/*
			and shove all this into the tagSpace hash table
		 */
		tag_define(key, tH);
	}
}
#endif

/*
	htdb_getToken()

		from the incoming string, find the next
		occurrence of "\n#", updating the pointer
		and returning the token type after the occurrence.

	returns: enum for token type
 */
static int	htdb_getToken(char *ptr, off_t *indx)
{
	if (ptr && *ptr) {
		char	*hash = strstr(ptr, "\n#");
		off_t	diff = (hash - ptr);
		if (diff < 0)
			return TOKEN_EOF;
		*indx += diff + 1;
		if (*ptr == 'd' && strncmp(ptr, "define", 6) == 0)
			return TOKEN_DEFINE;
		if (*ptr == 'l' && strncmp(ptr, "live", 4) == 0)
			return TOKEN_LIVE;
		if (*ptr == 'i' && strncmp(ptr, "include", 7) == 0)
			return TOKEN_INCLUDE;
		return TOKEN_COMMENT;
	}
	return TOKEN_EOF;
}

/*
	htdb_finishDefinition()

		we are at the completion of a declaration..
		determine the type of the declaration (vanilla, function, tag)
		and shove the findings into the appropriate hash table
		(def, func, tag), and then free() the processed string.
 */
static void	htdb_finishDefinition(spaceData *b)
{
	char	*in = (char *)b->value;

	/*
		remove trailing whitespaces
	 */
	while (b->size > 0 && isspace((int)in[b->size - 1]))
		in[--b->size] = '\0';

	if (in && *in && b->size > 6) {
		char	*ptr = in + 7;	/* skip past `#define' */

		b->size -= 7;

		/*
			eat space after keyword token
		 */
		while (ptr && *ptr && isspace((int)*ptr)) {
			ptr++;
			b->size--;
		}

#ifdef	USE_XML_STYLEE
		if (ptr && *ptr == '<') {
			/*
				start of a TAG definition
			 */
			char	*tag = ++ptr;
			ptr = strchr(ptr, '>');
			if (ptr && *ptr) {
				*(ptr++) = '\0';
				htdb_settag(htdb_stripWhitespace(tag), ptr);
			}
		} else
#endif
		if (ptr && *ptr) {
			/*
				start of resource definition or function definition

				syntax: #define x() y

			 */
			char	*p = &ptr[0],
					*key = &ptr[0],
					*def = (char *)NULL;
			int		is_func = 0,
					end_func = 0;

			/*
				eat until next whitespace.
				also detects a function definition.
			 */
			while (p && *p && !isspace((int)*p)) {
				if (*p == '(')
					is_func = 1;
				else if (*p == ')')
					end_func = 1;
				p++;
				b->size--;
			}
			if (is_func == 1 && end_func == 0)
				p = strchr(p, ')');

			if (*p == ')')
				p++;

			if (p && *p) {
				*p = '\0';
				def = p + 1;

				while (def && *def && isspace((int)*def)) {
					def++;
					b->size--;
				}

				if (is_func) {
					char	*paren = strchr(key, ')');

					if (paren && *paren) {
						/*
							guaranteed valid function syntax
						 */
						*(paren--) = '\0';
						paren = strchr(key, '(');
						*(paren++) = '\0';
						/*
							so the body knows in what resource it was defined
						 */
						htdb_setfunc(key, paren, def, BOOL_FALSE);
					} else
						/*
							syntax error: "func("
						 */
						;
				} else {
					/*
						so the body knows in what resource it was defined
					htdb_setval("res", key);
					 */
					/*
						store the result
					 */
					shoveDefine(key, def);
				}
			} else {
				/*
					empty definition
					eg:
						#define thing
				 */
				shoveDefine(key, "");
			}
		}
	}
	data_destroy(b);
	data_init(b);
}

/*
	htdb_read_open()

		given an already-opened datafile, process its
		contents by shoving its cleaned definitions into *Space.

		we understand the following syntax:

			#
			#	include another file
			#
			#include	relative_to_htdocs/../htdb
			#include	/explicit_system_path
			#include	~/relative_to_htdocs
			#
			#	vanilla definition
			#
			#define	x	y
			#
			#	script function definition
			#
			#define	x([args[=default],..])	y
			#
			#	tag definition
			#	(eventually)
			#
			#define	<x [arg=[value],..]>	y

	returns: BOOL_TRUE on success, BOOL_FALSE on failure.
 */
static bool_t	htdb_read_open(int ip)
{
	off_t		buf_len,
				buf_indx = 0,
				last = 0;
	char		*buf;
	struct		stat	s;
	int			tok = 0;
	spaceData	b;

	if (ip == -1)
		/*
			could not read the datafile
		 */
		return BOOL_FALSE;

	if (fstat(ip, &s) == -1)
		/*
			could not stat an opened file?!
		 */
		return BOOL_FALSE;

	/*
		suck this datafile into memory so
		that we can walk through it quickly.
	 */
	buf_len = s.st_size + 3;
	if ((buf = (char *)malloc(sizeof(char) *
		(buf_len + 1))) == (char *)NULL)
		/*
			out of memory!
		 */
		return BOOL_FALSE;

	if (read(ip, (void *)&buf[1], s.st_size) != s.st_size)
		/*
			read error!
		 */
		return BOOL_FALSE;

	/*
		to simplify EOF-detection parsing logic,
		append a fake comment to the end of the buffer,
		and insert a newline at the top of the buffer.
	 */
	buf[0] = '\n';
	buf[buf_len - 2] = '\n';
	buf[buf_len - 1] = '#';
	buf[buf_len] = '\0';

	data_init(&b);

	while (buf_indx < buf_len &&
		(last = buf_indx),
		(tok = htdb_getToken(&buf[buf_indx], &buf_indx)) != TOKEN_EOF) {
		off_t	len = buf_indx - last;
		char	*chunk = &buf[last];

		chunk[len] = '\0';

		if (tok == TOKEN_DEFINE) {
			/*
				#define
			 */
			htdb_finishDefinition(&b);
			thing_append(&b, chunk, len, BUF_LIMIT_BIG);
		} else if (tok == TOKEN_INCLUDE) {
			/*
				#include
			 */
			htdb_finishDefinition(&b);
			/*
				jump past `#include'
			 */
			chunk += 8;
			htdb_read(htdb_stripWhitespace(chunk));
		} else if (tok == TOKEN_LIVE) {
#ifdef	USE_XML_STYLEE
			/*
				#live

				we provide backward compatibility,
				by converting `#live' notation into
				`<>' notation..
			 */
			char	*jnk = strdup(chunk);

			if (jnk == (char *)NULL)
				/*
					malloc error!
				 */
				return BOOL_FALSE;

			/*
				change `#live' into `<'
			 */
			jnk[0] = '<';
			jnk[1] = '\0';
			/*
				walk past `#live'
			 */
			chunk += 5;
			if (strncmp(chunk, "end", 3) == 0) {
				sstrcat(jnk, "/", len);
				chunk += 3;
			}
			/*
				and append what's left of the original
				#live statement, followed by a newline.
			 */
			{
				char	*eol = strchr(chunk, '\n');
				if (eol) {
					*eol = '\0';
					sstrcat(jnk, htdb_stripWhitespace(chunk), len);
					sstrcat(jnk, ">\n", len);
					chunk = eol + 1;
				} else
					sstrcat(jnk, ">\n", len);
			}
			sstrcat(jnk, htdb_stripWhitespace(chunk), len);
			sstrcat(jnk, "\n", len);
			/*
				and append this to the current def
			 */
			thing_append(&b, jnk, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			free(jnk);
#else
			/*
				and append this to the current def
			 */
			thing_append(&b, "#", 1, BUF_LIMIT_BIG);
			thing_append(&b, chunk, len, BUF_LIMIT_BIG);
#endif
		} else {
			/*
				else we have a comment of some form.
			 */
			char	*nl = strchr(chunk, '\n');
			int	new_len = len - (nl - chunk);

			/*
				walk to the end of the line and
				recalculate the length of the string.
			 */
			chunk = nl + 1;

			if (*chunk)
				/*
					if we get here, then this
					was a comment embedded
					within a definition.
					(we've eaten the comment
					itself, now append the rest of
					the buffer to the current def)
				 */
				thing_append(&b, chunk, new_len - 1, BUF_LIMIT_BIG);
		}
		/*
			walk past the EOS
		 */
		buf_indx++;
	}
	/*
		finish up the last definition
	 */
	htdb_finishDefinition(&b);

	/*
		its been real.
	 */
	data_destroy(&b);
	free(buf);

	/*
		return success
	 */
	return BOOL_TRUE;
}

static char	*htdb_getfile2path(char *file)
{
	char	*htdb_root = htdb_getval("_htdb_root"),
			*needs_extension = (strcasestr(file, ".htdb")) ? "" : ".htdb";
	static char	path[BUF_LIMIT_SMALL + 1];

	if (*file == '~' && *(file + 1) == '/')
		/*
			if it starts with `~/', we assume it is
			a path relative to htdocs.
		 */
		snprintf(path, BUF_LIMIT_SMALL, "%s/../%s%s",
			htdb_root, &file[1], needs_extension);
	else if (*file == '/')
		/*
			if it starts with `/', we assume it is
			an explicit path to this file.
		 */
		snprintf(path, BUF_LIMIT_SMALL, "%s%s",
			file, needs_extension);
	else if (*file == '.')
		/*
			if it starts with `.', we assume it is
			a file given from a command-line argument
		 */
		snprintf(path, BUF_LIMIT_SMALL, "%s%s",
			file, needs_extension);
	else
		/*
			else, we go off of the document root
		 */
		snprintf(path, BUF_LIMIT_SMALL, "%s/%s%s",
			htdb_root, file, needs_extension);
	path[BUF_LIMIT_SMALL] = '\0';

	return path;
}

/*
	htdb_fileTimestampModified

		returns when the requested file was last modified.
		used as path of the auto-reload of cached files.
 */
static time_t	htdb_fileTimestampModified(char *path)
{
	struct stat	buf;
	if (stat(path, &buf) == 0)
		return buf.st_mtime;
	return 0;
}

/*
	htdb_fileTimestamp

		returns `path' prepended with `fileTimestamp->'
		used to store file modification times for cached files
 */
static char	*htdb_fileTimestamp(char *path)
{
	static char	lookup[BUF_LIMIT_MEDIUM + 1];
	snprintf(lookup, BUF_LIMIT_MEDIUM, "fileTimestamp->%s", path);
	lookup[BUF_LIMIT_MEDIUM] = '\0';
	return lookup;
}

/*
	htdb_fetchDBPage()

		given an input string (PATH_INFO), determine the proper
		values to use for the `db' and `page' resources.  perform
		any necessary decryption.
 */
static void	htdb_fetchDBPage(char *in)
{
	char	*db = "",
			*page = "";

	if (in && *in == '/')
		in++;
	if (strchr(in, '/') == NULL) {
		/*
			no slash
		 */
		if (strstr(in, ".html") || (in && *in == '|')) {
			/*
				explicit document
			 */
			page = htdb_setval("page", in);
		} else {
			/*
				no slash at all:
					jbc
					del
					...
				etc
			 */
			if (in && *in)
				db = htdb_setval("db", in);
			page = htdb_setval("page", "index.html");
		}
	} else {
		if (!(in && *in)) {
			db = htdb_setval("db", "site");
			page = htdb_setval("page", "index.html");
		} else {
			int	len = strlen(in);
			/*
				we have a slash somewhere
			 */
			if (in[len - 1] == '/') {
				/*
					slash at end of URL:
						jbc/
						del/
						...
					etc
				 */
				in[len - 1] = '\0';
				db = htdb_setval("db", in);
				page = htdb_setval("page", "index.html");
			} else {
				if (strchr(in, '.') || strchr(in, '|')) {
					/*
						all other cases:
							jbc/index.html
							jbc/index.xml
							jbc/index.txt
							...
						etc
					 */
					int	i;
					for (i = len ; i > 0 && in[i] != '/' ; --i)
						;
					in[i] = '\0';
					db = htdb_setval("db", in);
					page = htdb_setval("page", &in[i + 1]);
				} else {
					/*
						multi-depth directory
						but no page defined:
							jbc/lyrics
							...
						etc
					 */
					db = htdb_setval("db", in);
					page = htdb_setval("page", "index.html");
				}
			}
		}
	}
	/*
		we can support encrypted values of `db' and `page',
		and encrypted GET args hidden in encrypted PATH_INFO values

		ala: http:/blah.com/|pe1|encryptedstuff/file.html

		(which WILL FAIL if `confCrypto->X->enabled' is set to `no'!)
	 */
	if (in && *in == '|')
		htdb_SetFromPathInfo("db", db);
	if (page && *page == '|')
		htdb_SetFromPathInfo("page", page);

	/*
		creat an easy-to-use lookup
	 */
	htdb_setvarg("uri", "%s%s%s%s%s",
		htdb_getval("cgi->script_name") + 1,
		(htdb_checkval("db") ? "/" : ""),
		(htdb_checkval("db") ? htdb_getval("db") : ""),
		(htdb_checkval("page") ? "/" : ""),
		(htdb_checkval("page") ? htdb_getval("page") : ""));

	if (!htdb_checkval("db"))
		htdb_setval("db", htdb_getval("cgi->script_name") + 1);
}

static void	htdb_setShortcuts()
{
	htdb_setint("_timestamp", htdb_timestamp());
	htdb_setint("_timestamp_gmt", htdb_timestamp_gmt());
	{
		char	*ts = htdb_getval(htdb_checkval("_db_timestamp") ?
					"_db_timestamp" : "_timestamp");
		htdb_setval("_current_date", htdb_parseDate(ts));
		htdb_setval("_current_time", htdb_parseTime(ts));
		htdb_setval("_current_year", htdb_parseYear(ts));
		htdb_setval("_current_day", htdb_parseDay(ts));
		htdb_setval("_current_hour", htdb_parseMilHour(ts));
		htdb_setval("_current_min", htdb_parseMinute(ts));
		htdb_setval("_current_sec", htdb_parseSecond(ts));
		htdb_setval("_current_weekday", htdb_parseWeekday(ts));
		htdb_setval("_current_mon", htdb_parseMon(ts));
		htdb_setval("_current_month", htdb_parseMonth(ts));
		htdb_setval("_current_monthnum", htdb_parseMonthNum(ts));
	}
	htdb_setint("_current_pid", (int)getpid());
}

/*
	htdb_start()
		start up the look-up table and store some server values.
		process the incoming GET and POST values.
		read the default HTDB file.
		begin the document header.
 */
static bool_t	htdb_start(appinfo_t *appinfo)
{
	htdb_srand();

	/*
		capture start time for page generation
	 */
	gettimeofday(&istart, (void *)NULL);

	/*
		initialize debugging subsystem
	 */
	log_intro();

	{
		/*
			transfer the current staticSpace into resSpace.
			this, in effect, allows all config.htdb and static.htdb
			resources to be cached.
		 */
		htdb_initspace();
		space_duplicate(staticSpace, resSpace);
	}

	sfunc_initspace();

	/*
		a place to store open file pointers
	 */
	file_initspace();

	/*
		set up some handy shorthands
	 */
	htdb_setint("_db_timestamp", htdb_dbTimestamp());
	htdb_setShortcuts();

	/*
		copy the environment into our world.
		this is so much cleaner than hardcoding
		specific values to be grabbed.
	 */
	{
		extern char **environ;
		char	**env = environ,
				*this;
		int		num = 0;

		while (env && *env && (this = strdup(*(env++)))) {
			char	nm[BUF_LIMIT_SMALL + 1],
					*equal = strchr(this, '=');
			
			if (equal && *equal) {
				*equal = '\0';

				/*
					store as "cgi->name=value"
				 */
				htdb_setptrval("cgi", this, equal + 1);
				/*
					store as "cgi[index]->name=name"
				 		and
					store as "cgi[index]->value=value"
				 */
				htdb_setobjval("cgi", ++num, "name", this);
				htdb_setobjval("cgi", num, "value", equal + 1);
				/*
					store as "cgi[name]=value"
				 */
				snprintf(nm, BUF_LIMIT_SMALL, "cgi[%s]", this);
				nm[BUF_LIMIT_SMALL] = '\0';
				htdb_setval(nm, equal + 1);
			}
			free(this);
		}
		/*
			store how many env values were found so that
			a walk through the array is a walk in the park.
		 */
		htdb_setint("cgi->numResults", num);
	}

	if (!static_checkval(htdb_fileTimestamp(htdb_getval("cgi->script_filename")))) {
		/*
			this is the first time in this fastcgi loop.
			store the timestamp of the CGI so that we
			can later detect if the binary has changed.
		 */
		static_setint(htdb_fileTimestamp(htdb_getval("cgi->script_filename")),
			htdb_fileTimestampModified(htdb_getval("cgi->script_filename")));
	}

	/*
		yes, we really need to re-set these values, because
		in a multi-homed server environment, the startup value
		of DOCUMENT_ROOT is used to read the config.
		but now we might be working within a different DOCUMENT_ROOT.
		this is especially true in a fastCGI world, as the
		original startup value would have been cached.
	 */
	htdb_setvarg("_htdb_root", "%s/../htdb", htdb_getval("cgi->document_root"));

	if (!htdb_checkval("cgi->server_name"))
		/*
			is a TTY?  if so, return right now,
			since from here on in, we're dealing with
			server-set values.
		 */
		return BOOL_TRUE;

	/*
		here we use the value of `server_port' and turn it
		into something universally usable in later URL generation
	 */
	if (htdb_equal("cgi->server_port", "80") ||
		htdb_equal("cgi->server_port", "443"))
		htdb_setval("cgi->http_port", "");
	else
		htdb_setvarg("cgi->http_port", ":%s",
			htdb_getval("cgi->server_port"));
	if (htdb_equal("cgi->server_port", "443"))
		htdb_setvarg("cgi->http_protocol", "https://");
	else
		htdb_setvarg("cgi->http_protocol", "http://");

	/*
		process the incoming CGI values.
		this pulls GET and POST values into the HTDB world.
		it is here that we filter all incoming data and
		make it database and HTDB script safe.
	 */
	uncgi();

	/*
		handle sessioning, and user login
	 */
	if (htdb_handleSessioning())
		/*
			we got here if a redirection has occurred
		 */
		return BOOL_FALSE;

	/*
		split PATH_INFO into `db' and `page'
	 */
	htdb_fetchDBPage(htdb_getval("cgi->path_info"));

	htdb_setvarg("logResource", "%s/%s", htdb_getval("db"), htdb_getval("page"));

	/*
		(optionally) log the page hit count
		do it here so that `_hit_count' is available on the page.
	 */
	if (htdb_checkval("confLogHitTable"))
		htdb_setint("_hit_count",
			htdb_fetchHitCount(htdb_getint("_domain_id"),
			htdb_getval("logResource"),
			htdb_getval("confLogHitTable")));

	return BOOL_TRUE;
}

/*
	start_of_cgi()

 		outside-the-fastcgi-loop initialization:
		process commandline options, read config.htdb
		and static.htdb, connect to the declared database(s),
		set up our namespaces, load the DSOs that exist.
 */
static bool_t	start_of_cgi(int argc, char **argv, appinfo_t *appinfo)
{
	bool_t	standalone = BOOL_FALSE;

	htdb_srand();

	/*
		create our playground
	 */
	htdb_initspace();

	if (getenv("DOCUMENT_ROOT") == NULL) {
		int	argStart = 1;
		/*
			there's no `DOCUMENT_ROOT' in the runtime environment,
			so it is assumed that we are running via the command line.
			in this case, the 1st argument is required to be the full
			path to the web server's document root directory -
			because that is where our configuration files may be found,
			and we need to read that file to get database and other
			configuration values.
		 */
		standalone = BOOL_TRUE;

		if (argc >= 2) {
			/*
				set the value of DOCUMENT_ROOT.
				this will trick code further on into thinking
				we are running as a web CGI program.
			 */
			argStart = 2;
			htdb_setval("cgi->document_root", argv[1]);
		} else {
			/*
				none given on command-line.
				fall back to the distribution's config.htdb
				(yeah, i know.  this should be a string
				dynamically-built from the automake process)
			 */
			argStart = 1;
			htdb_setval("cgi->document_root", "/usr/local/htdb");
		}

		{
			static	char	jnk[BUF_LIMIT_SMALL + 1];
			snprintf(jnk, BUF_LIMIT_SMALL, "DOCUMENT_ROOT=%s", htdb_getval("cgi->document_root"));
			jnk[BUF_LIMIT_SMALL] = '\0';
			putenv(jnk);
		}

		htdb_setval("cgi->document_root", getenv("DOCUMENT_ROOT"));

		{
			int	i,
				argvopts = 0;

			/*
				transfer command-line arguments into resSpace
			 */
			htdb_setarrayval("argv", 0, argv[0]);
			for (i = argStart ; i < argc ; i++) {
				if (argv[i] && *(argv[i]) == '-' && *(argv[i] + 1)) {
					/*
						this shoves '-option value' command-line args into special
						'argvopt[*]->name' and 'argvopt[*]->value' objects.
					 */
					htdb_setobjval("argvopt", ++argvopts, "name", (argv[i]) + 1);
					if (i < argc - 1)
						htdb_setobjval("argvopt", argvopts, "value", argv[i + 1]);

					if (0 == strcasecmp(argv[i], "-include") && i < argc - 1)
						/*
							we got us a file to include.
							do it right here.
							note that command-line inclusion order MATTERS.
						 */
						htdb_read(argv[i + 1]);
				}
				{
					/*
						this allows the setting of arbitrary 'name=value'
						tuples from the command-line
					 */
					char	*equal = strchr(argv[i], '=');
					if (equal && *equal) {
						*equal = '\0';
						htdb_setval(argv[i], equal + 1);
					}
				}
				htdb_setarrayval("argv", i - 1, argv[i]);
			}

			htdb_setint("argc", argc - 1);	/* because the world expects it.. */
			htdb_setint("argv->numResults", argc - 1);
			htdb_setint("argvopt->numResults", argvopts);
		}
	} else {
		/*
			CGI/fastCGI mode
		 */
		htdb_setval("cgi->document_root", getenv("DOCUMENT_ROOT"));
	}
	
	/*
		set up some handy shorthands
	 */
	htdb_setShortcuts();

	/*
		set up where we'll be able to find config.htdb
	 */
	htdb_setvarg("_htdb_root", "%s/../htdb", htdb_getval("cgi->document_root"));

	/*
		a place to store pointers to DSOs
	 */
	cfunc_initspace();

	/*
		a place to store script functions.
		note - unlike resSpace, script functions (currently)
		only have a single namespace - which caches across
		subsequent page requests.
	 */
	sfunc_initspace();

	/*
		there are some synonyms for the "getval" function.
		shove them into a hashtable for easy lookup.
	 */
	cfunc_store("defined", &live_getval);
	cfunc_store("getval", &live_getval);
	cfunc_store("getnum", &live_getval);
	cfunc_store("getint", &live_getval);
	cfunc_store("uname", &htdb_uname);

	/*
		the `config.htdb' file should contain only important
		configuration definitions - pointers to databases and the like.
		it should not, however, contain session-specific logic.
	 */
	{
		char	*config_paths[] = {
			"config",	/* first choice is relative to htdocs */
			"/etc/config",
			"/etc/htdb/config",
			"/usr/local/etc/config",
			"/usr/local/htdb/etc/config"
		};
		int	num_paths = sizeof(config_paths) / sizeof(char *),
			indx = 0;
		bool_t	configRead = BOOL_FALSE;

		/*
			we'll look in some "common" places for
			our config file.
		 */
		do {
			/*
				set `htdb->path_config.htdb' so that if we detect
				that the distributed config.htdb file has not been
				changed from its default distribution values, we can
				point people to the correct file that needs modification.
				not the prettiest thing in the world, but it works.
			 */
			htdb_setval("htdb->path_config.htdb", config_paths[indx]);
			configRead = htdb_read(config_paths[indx++]);
		} while (indx < num_paths && BOOL_FALSE == configRead);

		if (BOOL_FALSE == configRead) {
			if (BOOL_TRUE == standalone) {
				fprintf(stderr, "\n%s (%s) (%s-%s)\n\nERROR: config.htdb not found!\n"
					"I looked for:\n"
					"\t%s/config.htdb\n",
					appinfo->name, appinfo->version,
					LIBHTDB_NAME, LIBHTDB_VERSION,
					htdb_getval("_htdb_root"));
				for (indx = 1 ; indx < num_paths ; indx++) {
					fprintf(stderr, "\t%s.htdb\n", config_paths[indx]);
				}
			} else {
				/*
					this un's gotta be hardcoded, because we
					couldn't find config.htdb, which contains
					the other error link defs.
				 */
				htdb_httpRedirect("http://www.htdb.org/htdb/errno/100.html?dr=%s",
					htdb_getval("cgi->document_root"));
			}
			htdb_clearspace();
			cfunc_clearspace();
			sfunc_clearspace();
			return BOOL_FALSE;
		}
	}
	/*
		handle any appinfo default arg declarations
	 */
	if (appinfo->args && *appinfo->args) {
		char	*tmpbuf = strdup(appinfo->args);
		uncgi_scanquery(tmpbuf);
		free(tmpbuf);
	}

#if	0
	/*
		walk through the encryption handle definitions found in config.htdb
		and dynamically declare the associated script functions.  doing this
		allows new encryption handles to be created without having to
		create compiled DSOs.  which is sort of a cool thing.

		NOTE: THIS WILL NOT WORK.
		because: to declare a script function, one needs to say what
		the argument name will be.
		doing that triggers logic inherent in script functions that say
		"if we are passed args with equal signs, then the value must
		be passed by name".
		knowing this, ${ye1(song_id=666)} will then fail, since the
		string we want to encode is "song_id=666", but the script function logic
		expects the argument name to now be "song_id", not "thearg", as it is
		set up below.
		in a word: shit.
	 */
	{
		int	i = 0;
		char	*name;

		while ((name = htdb_getobjval("confFilter", ++i, "name")) && name && *name) {
			char	*args = "thearg",
					body[BUF_LIMIT_SMALL + 1];
					
			snprintf(body, BUF_LIMIT_SMALL, "${crypto(%s, ${thearg})}", name);
			htdb_setfunc(name, args, body, BOOL_TRUE);
		}
	}
#endif

	/*
		got DSO?
	 */
	if (htdb_defined("confDSOs") && BOOL_FALSE == dso_loadFiles(htdb_getval("confDSOs"))) {
		/*
			could not load one or more of the specified DSOs.
		 */
		htdb_clearspace();
		cfunc_clearspace();
		sfunc_clearspace();
		return BOOL_FALSE;
	}

	/*
		connect to the database (if desired)
		this happens outside of the fastCGI page request loop
		to enable persistent database connections.
	 */
	if (!(*htdb_getval("confUseDatabase") == 'n') &&
		BOOL_FALSE == htdb_dbConnect()) {
		/*
			could not connect to the database.
		 */
		htdb_doHeader("Content-type: text/html");

		htdb_print("DB CONNECT ERROR - check values in ");
		htdb_print(htdb_getval("htdb->path_config.htdb"));
		htdb_print("\n");

		/*
			clean up
		 */
		htdb_clearspace();
		cfunc_clearspace();
		sfunc_clearspace();
		return BOOL_FALSE;
	}

	/*
		suck in our static definitions (if any).
	 */
	htdb_read("static");

	/*
		if the application is not running with a database,
		then we also need to disable any conf-file options
		that rely upon database connectivity.
	 */
	if (*htdb_getval("confUseDatabase") == 'n') {
			htdb_setval("confUseCookieSessioning", "no");
			htdb_setval("confLogTimeToGenerateTable", "");
			htdb_setval("confLogHitTable", "");
			htdb_setval("confLogEntranceTable", "");
			htdb_setval("confLogPageTable", "");
	}

	/*
		we want the values as read from config.htdb and static.htdb
		to live in their own namespace in the `staticSpace' object.
		`staticSpace' will serve as the starting resources when each
		new page request in a fastCGI loop arrives.
	 */
	staticSpace = resSpace;
	resSpace = NULL;

	return BOOL_TRUE;
}

/*
	end_of_cgi()

 		past the fast cgi loop teardown.
		free namespace memory, disconnect from the database.
 */
static void	end_of_cgi(void)
{
#if	(defined(HAVE_LIBFCGI) && defined(USE_FASTCGI))
		FCGI_Finish();
#endif
	/*
		clear static resource storage
	 */
	static_clearspace();
	/*
		clear DSO storage
	 */
	cfunc_clearspace();
	/*
		clear scripted function storage
	 */
	sfunc_clearspace();
	/*
		clear database handle storage
		(which also shuts down the databases)
	 */
	db_clearspace();

	/*
		free dso handle space
	 */
	dso_unloadFiles();
}

#if	(defined(HAVE_LIBFCGI) && defined(USE_FASTCGI))
/*
	htdb_configChanged()

		returns BOOL_TRUE if any of the files read outside of the fastCGI
		loop have been changed.  this will cause the fastCGI loop to exit
		after this request so that a new fastCGI thread will then start up.
		this new thread will pick-up the the changed file.
 */
static bool_t	htdb_configChanged(void)
{
	spaceData	*b;
	static_initwalk();
	while ((b = static_walkspace()))
		if (strstr(b->key, "fileTimestamp->") &&
			htdb_fileTimestampModified(strstr(b->key, "->") + 2) > atol(b->value))
				return BOOL_TRUE;
	return BOOL_FALSE;
}
#endif

/*
	htdb_end()

		end of the page request cycle.
		clear debugging and clear the namespace.
 */
static void	htdb_end(void)
{
	/*
		close any open file pointers
	 */
	file_clearspace();

	/*
		clear debugging values
	 */
	log_outro();

	/*
		clean up our mess
	 */
	htdb_clearspace();
}

/********************************************************************************

	begin public functions

 ********************************************************************************/

/*
	htdb_setfunc()

		shove this function declaration into the funcSpace hash

	returns: nada
 */
void	htdb_setfunc(char *funcname, char *arglist, char *bd, bool_t internally_defined)
{
	if (funcname && *funcname && bd && *bd && !sfunc_lookup(funcname)) {
		/*
			only store this script function if
			it is named, has a body, and is not
			already in the hash table.
		 */
		int			argno = 0,
					funcnameLength = strlen(funcname);
		char		*body = strdup(bd);
		sfunc_t		*sf = (sfunc_t *)malloc(sizeof(sfunc_t));

		sf->internally_defined = internally_defined;

		sf->num_args = htdb_splitArgs(arglist, ",", &(sf->args), 0);

		sf->defaults = (char **)malloc(sizeof(char *) * sf->num_args);

		/*
			we can have default values for variable declarations.
			ala: #define func(arg1=buh1, arg2=buh2)
		 */
		for (argno = 0 ; argno < sf->num_args ; argno++) {
			char	*ptr = sf->args[argno],
					*equal = strchr(ptr, '=');

			if (equal && *equal) {
				*equal = '\0';
				sf->defaults[argno] = strdup(equal + 1);
			} else {
				sf->defaults[argno] = strdup("");
			}
		}

		/*
			we'd like the parameters into the function to
			be private to the function.  one (simple) way
			to reduce the risk of namespace collisions is
			to look at the body of the function, and change
			occurrences of parameters into the form:
				func_[funcname]->[pararameter].
			in this way, when the function is actually called, we can
			convert the incoming parameters into private values.
			so... we got some processing to do up-front.
		 */

		for (argno = 0 ; argno < sf->num_args ; argno++) {
			char	*arg = sf->args[argno];
			int		argLength = strlen(arg),
					lookupLength = 3 + argLength,
					replacementLength = 3 + 5 + funcnameLength + 2 + argLength,
					bodyLength = strlen(body);
			char	*lookup = (char *)malloc(sizeof(char) * (lookupLength + 1)),
					*replacement = (char *)malloc(sizeof(char) * (replacementLength + 1)),
					*c;

			/*
				this handles occurrences of:

					'${thing}'
			 */
			sprintf(lookup, "${%s}", arg);
			sprintf(replacement, "${func_%s->%s}", funcname, arg);

			{
				int	replen = replacementLength,
					looklen = lookupLength;

				while ((c = strstr(body, lookup))) {
					/*
						found an occurrence
						chop up the string and rebuild it
					 */
					char	*newbody = (char *)malloc(sizeof(char) * 
							((bodyLength += (replen - looklen)) + 1));

					*c = '\0';
					strcpy(newbody, body);
					strcat(newbody, replacement);
					strcat(newbody, c + looklen);
					free(body);
					body = newbody;
				}
			}

			/*
				this handles occurrences of:

					'getval(thing)'
			 */
			sprintf(lookup, "(%s)", arg);
			sprintf(replacement, "(func_%s->%s)", funcname, arg);

			{
				int	replen = replacementLength - 1,	/* because "()" is one less in length than "${}" */
					looklen = lookupLength - 1;		/* because "()" is one less in length than "${}" */

				while ((c = strstr(body, lookup))) {
					/*
						found an occurrence
						chop up the string and rebuild it
					 */
					char	*newbody = (char *)malloc(sizeof(char) * 
							((bodyLength += (replen - looklen)) + 1));

					*c = '\0';
					strcpy(newbody, body);
					strcat(newbody, replacement);
					strcat(newbody, c + looklen);
					free(body);
					body = newbody;
				}
			}

			/*
				clean up the lookup and replacement strings
			 */
			free(lookup);
			free(replacement);
		}

		/*
			shove all this into the funcSpace hash table
		 */
		sf->body = body;
		sfunc_store(funcname, sf);
	}
}

void	htdb_log(char *resource, char *aux_value)
{
	if (htdb_equal("user->authenticated", "yes"))
		/*
			spy on them; set user.lastactive for this user
		 */
		dbUpdate("update LOW_PRIORITY user set "
			"lastactive = now(), "
			"ts=ts "
			"where user_id = %s",
			htdb_getval("user->user_id"));

	/*
		drop some stuff to the database
	 */
	log_page(htdb_getval("_session_int"),
		htdb_getval("_domain_id"),
		htdb_getval("user->user_id"),
		htdb_getval("_browser_id"),
		resource,
		aux_value);

	/*
		(optionally) log time-to-generate the page
	 */
	log_ttg(resource);
}

/*
	htdb_main()

		here's where it all begins.
		this is the entry point to htdblib-driven applications.
		* process (optional) command-line arguments,
		* accept a few strings for command-line mode,
		* connect to the database,
		* read global config files.
		* enter a fastCGI loop to process per-session page requests.
		* after the desired number of pages have been processed,
		  cleanup our mess and return to the caller (main(), presumably).
 */
bool_t	htdb_main(int argc, char **argv, appinfo_t *appinfo)
{
	/*
		populate staticSpace from config.htdb and static.htdb,
		connect to the database.
	 */
	if (start_of_cgi(argc, argv, appinfo) == 0)
		/*
			failure
		 */
		return BOOL_TRUE;

	/*
		the page request loop
	 */
	{
#if	(defined(HAVE_LIBFCGI) && defined(USE_FASTCGI))
		int	keep_alive = appinfo->fcgi_keepalive;
#endif

		inMainLoop = BOOL_TRUE;
#if	(defined(HAVE_LIBFCGI) && defined(USE_FASTCGI))
		while ((keep_alive-- >= 0) && (FCGI_Accept() >= 0)) {
#endif

			/*
				loopy, loopy, pagey, pagey.
			 */
			if (htdb_start(appinfo))
				/*
					not an internal redirect -
					handle the page request
				 */
				appinfo->func();

			/*
				cleanup
			 */
			htdb_end();

#if	(defined(HAVE_LIBFCGI) && defined(USE_FASTCGI))
			if (htdb_configChanged())
				/*
					one of the static files changed.
					drop out of the fastCGI loop right here.
					this will cause apache to re-spawn a copy of
					this process which will force a re-read of the
					files that may have changed.  hey... it works,
					and is simpler than writing a SIG_HUP handler..
				 */
				break;
		}
#endif
		inMainLoop = BOOL_FALSE;
	}

	/*
		disconnect from database, clean up remaining hashtables.
	 */
	end_of_cgi();

	/*
		and outta here, like vladimir
	 */
	return BOOL_FALSE;
}

static char	*htdb_detectContentTypeFromPageName(char *page, char *document)
{
	if (document && *document &&
		0 == strncasecmp(document, "Content-type:", strlen("Content-type:"))) {
		/*
			the document body begins with the string "Content-type:",
			so we will not auto-spit out a header.
		 */
		return "";
	} else {
		/*
			otherwise, we'll see if the document extension maps
			to a defined content-type as defined in config.htdb
		 */
		char	*reversed = reverseString(page),
				*dot = strchr(reversed, '.'),
				*ret = "";

		if (dot && *dot) {
			*dot = '\0';
			{
				char	*extension = reverseString(reversed),
						*lookup = (char *)malloc(sizeof(char) * (strlen("confContentType") + strlen(extension) + 10));

				sprintf(lookup, "confContentType[%s]", extension);
				ret = htdb_getval(lookup);
				free(extension);
				free(lookup);
			}
		}

		if (0 == strlen(ret))
			/*
				nope.
				fall-back to "confContentType[default]"
			 */
			ret = htdb_getval("confContentType[default]");

		free(reversed);

		return ret;
	}
}

/*
	htdb_page_handler()

		this is the built-in page handler.
		really, this is the guts of the htdb cgi itself.
 */
void	htdb_page_handler(void)
{
	/*
		read macros here because it could redefine htdb datafile mappings.
	 */
	htdb_read("macros");

	/*
		the next block is simply attempting to get valid values for
		`db' and `page' so that there's actually something to display
	 */
	if (BOOL_FALSE == htdb_read(htdb_getval(htdb_getval("db")))) {
		/*
			if there's no indirectly-defined value of `db'..
			then try to look it up directly
		 */
		if (BOOL_FALSE == htdb_read(htdb_getval("db"))) {
			/*
				hell.. even *that's* undefined..
				fall-back to "site.htdb"
			 */
			if (BOOL_FALSE == htdb_read("site")) {
				/*
					they obviously don't know what they're doing..
					send them to the help page so they
					have some clue as to what to do to bootstrap..
				 */
				htdb_httpRedirect("%s?dr=%s&d=%s",
					htdb_getval("confHelpDocumentErrno101"),
					htdb_getval("cgi->document_root"),
					htdb_getval("db"));
				return;
			}
		}
	}

	if (htdb_checkval(htdb_getval("page")) == 0) {
		/*
			if the value of `page' points nowhere, then fallback
			to index.html
		 */
		htdb_setval("page", "index.html");
		if (htdb_checkval(htdb_getval("page")) == 0) {
			char *content_type = htdb_getval("confContentType[default]");
			/*
				if the value of `page' *still* points nowhere,
				then send them to the help page so they
				have some clue as to what to do to bootstrap..
			 */
			/*
				out goes the HTTPD header
			 */
			htdb_doHeader(content_type);

			/*
				screw it. let's just force 'em
				to write a 404 resource.
			 */
			htdb_print("${404}");

			/*
				finish up the HTML
			 */
			htdb_doFooter(content_type);
			/*
			htdb_httpRedirect("%s?dr=%s&d=%s",
				htdb_getval("confHelpDocumentErrno102"),
				htdb_getval("cgi->document_root"),
				htdb_getval("db"));
			*/
			return;
		}
	}

	/*
		make the name of the page being processed available
		to script-land - someone might like to use this value
		for realtime conditionalities, etc.
	 */
	htdb_setvarg("pageResource", "%s_%s", htdb_getval("db"), htdb_getval("page"));

	{
		char	*document = htdb_getval(htdb_getval("page")),
				*content_type = htdb_detectContentTypeFromPageName(htdb_getval("page"), document);

		/*
			out goes the HTTPD header
		 */
		htdb_doHeader(content_type);

		/*
			output the page
		 */
		htdb_print(document);

		/*
			finish up the HTML
		 */
		htdb_doFooter(content_type);
	}

	/*
		dump to the log database
	 */
	htdb_log(htdb_getval("logResource"), "");
}

/*
	htdb_read()
		process an HTDB file given a datafile relative to
		the server's document root.
 */
bool_t	htdb_read(char *file)
{
	bool_t	status = BOOL_FALSE;

	if (file && *file) {
		/*
			first - read the default version
		 */
		char	*fn = htdb_getfile2path(file);
		int	ip = open(fn, O_RDONLY);

		status = htdb_read_open(ip);

		if (status == BOOL_TRUE) {
			close(ip);
			if (BOOL_FALSE == inMainLoop) {
				/*
					if outside the page request loop,
					make a note of the modification time
					on this file so that we can detect when
					the file changes - which will trigger
					an exit of the page request loop and
					force a re-read of the file on the *next*
					page request.
				 */
				htdb_setint(htdb_fileTimestamp(fn),
					htdb_fileTimestampModified(fn));
			}
		}
	}
	return status;
}

/*
	htdb_usage()

			seen when an application is called inappropriately from the command-line
 */
void	htdb_usage(appinfo_t *appinfo)
{
	fprintf(stderr, "-----=[ application name (version) (lib-version) ]=--------------------------");
	fprintf(stderr, "\n\n\t%s (%s) (%s-%s)\n\n", appinfo->name, appinfo->version, LIBHTDB_NAME, LIBHTDB_VERSION);
	fprintf(stderr, "-----=[ command-line usage ]=------------------------------------------------");
	fprintf(stderr, "\n\n\t%s <path to directory containing config.htdb> %s\n\n", appinfo->name, appinfo->cmd_args);
	if (appinfo->description && *appinfo->description) {
		fprintf(stderr, "-----=[ brief description ]=---------------------------------------------");
		fprintf(stderr, "\n\n%s\n\n", appinfo->description);
	}
}

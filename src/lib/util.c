#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

/*
	pretty()

		Convert MIKES AUTOMOTIVE -> Mikes Automotive
		credits: Colby Krabill from the bigbook.com heyday.
 */
char	*pretty(char *ugly)
{
	char	*out = strdup(ugly),
			*cp = &out[0];
	bool_t	BOW = BOOL_TRUE;	/* beginning of word */

	while (cp && *cp) {
		/* 
			*cp check only to get around a gcc-bounds
			bug. 
		*/
		while (*cp && strchr(" &(-_/.,", *cp)) {
			BOW = BOOL_TRUE;
			cp++;
		}
		if (BOOL_TRUE == BOW) {
			char	*end = cp;
			while (*end && (*end != ' ') && (*end != ','))
				end++;
			if (end != cp) {
				char	foo = *end;
				int		curtoklen = end - cp;
				/*
				 * FooBar CHURCHOFBOB BAZ
				 *        ^cp        ^end
				 */
				*end = '\0';
				if ((curtoklen >= 3) &&
					(*(cp+0) == 'M') &&
					(*(cp+1) == 'C')) {
					/*
						McIntosh
					 */
					*(cp + 1) = 'c';
					cp += 2;
				} else if ((curtoklen > 3) &&
					(*(cp+1) == '\'') &&
					(*cp != 'I')) {
					/*
						D'Arcy
					 */
					cp += 2;
				}
				*end = foo;	/* was set to end token */
			}

			if (islower((int)*cp)) {
				*cp = toupper((int)*cp);
				BOW = BOOL_FALSE;
			} else if (isupper((int)*cp) || isdigit((int)*cp))
				BOW = BOOL_FALSE;
		} else
			if (isupper((int)*cp))
				*cp = tolower((int)*cp);
		if (*cp) cp++;
	}
	return out;
}

char	*underscore2space(char *in)
{
	char *out = strdup(in),
		*hold = &out[0];
	while (out && *out) {
		if (*out == '_')
			*out = ' ';
		out++;
	}
	return hold;
}

char	*space2underscore(char *in)
{
	char *out = strdup(in),
		*hold = &out[0];
	while (out && *out) {
		if (isspace((int)*out))
			*out = '_';
		out++;
	}
	return hold;
}

char	*newline2br(char *in)
{
	spaceData b;	
	/*translate newlines to <BR>'s*/
	char *ptr, *nl, *dup;
	data_init(&b);

	dup = strdup(in);
	ptr = dup;
	while((nl = strpbrk(ptr, "\r\n"))) {
		if ( *nl == '\r' && (*(nl + 1) == '\n') ) {
			/* CR LF format */
			nl += 2;
		} else
			nl++;

		thing_append(&b, ptr, (nl - ptr), BUF_LIMIT_BIG);

		/* don't do a <br> tag if there's a # char.
		   I don't know why anymore. Maybe for comment lines? */
		if ( strncmp( nl, "&#35;", 5 ) != 0 )
			thing_append(&b, "<BR>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);

		ptr = nl;
	}
	thing_append(&b, ptr, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	free(dup);
	return ((char *) b.value);
}

char *br2newline(char *body)
{
	spaceData b;	
	/*translate breaks back to newlines*/
	char *ptr, *br, *dup;
	data_init(&b);
	
	dup = strdup(body);
	ptr = dup;
	while(ptr && *ptr && (br = strcasestr(ptr, "<BR>"))) {
		thing_append(&b, ptr, (br - ptr), BUF_LIMIT_BIG);
		ptr = br +=4;
		//thing_append(&b, "\n", 2, BUF_LIMIT_BIG);
	}
	thing_append(&b, ptr, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	free(dup);
	return ((char *) b.value);
}

/*
	sendmail()

		mailer = mailer application (pipe)
		message = message headers / body to send off
 */ 
bool_t	sendmail(char *mailer, char *message)
{
	if (mailer && *mailer) {
		FILE    *pp = popen(mailer, "w");

		fprintf(pp, "%s\n", message);

		pclose(pp);
		return BOOL_TRUE;
	} else {
		return BOOL_FALSE;
	}
}

bool_t	amRoot(void)
{
	return (getuid() == 0 ||
		geteuid() == 0 ||
		getgid() == 0 ||
		getegid() == 0) ? BOOL_TRUE : BOOL_FALSE;
}

char	*prepareBinaryFromDatabase(char *in, off_t *out_bytes)
{
	/*
		no translation needed..
		so just make space for the thing and return it.
	 */
	char	*out = (char *)malloc(sizeof(char) * *out_bytes);
	memcpy(out, in, *out_bytes);
	return out;
}

char	*prepareBinaryForDatabase(char *in, off_t in_bytes, off_t *out_bytes)
{
	char	*out = (char *)malloc(sizeof(char) * in_bytes * 2);
	int	i;

	*out_bytes = 0;

	for (i = 0 ; i < in_bytes ; i++) {
		if (in[i] == 0) {
			out[(*out_bytes)++] = '\\';
			out[(*out_bytes)++] = '0';
		} else if (in[i] == 92) {
			out[(*out_bytes)++] = '\\';
			out[(*out_bytes)++] = '\\';
		} else if (in[i] == 39) {
			out[(*out_bytes)++] = '\\';
			out[(*out_bytes)++] = '\'';
		} else if (in[i] == 34) {
			out[(*out_bytes)++] = '\\';
			out[(*out_bytes)++] = '\"';
		} else
			out[(*out_bytes)++] = in[i];
	}
	out[(*out_bytes)] = '\0';
	return out;
}

char	*prepareStringForBBSDatabase(char *in)
{
	int i, j;
	char *buf = (char *) malloc(sizeof(char) * strlen(in) * 2);
	for (i=0, j=0; in && in[i]; i++) {
		if (in[i] == '\'') {
			buf[j++] = '\\'; 
			buf[j++] = '\'';
		} else if (in[i] == '\"') {
			buf[j++] = '\\';
			buf[j++] = '\"';
		} else
			buf[j++] = in[i];
	} 

	buf[j] = '\0';
	return(buf);
}			

typedef struct
{
	char	ch;
	int		code;
}	filterChar;


static filterChar _filterChars[] = 
{
	{'\'', 39},
	{'\\', 92},
	{'\"', 34},
	{'=', 61},
	{'$', 36},
	{')', 41},
	{'%', 37},
	{'#', 35},
	{(char)0, (int)'\0'}
};
	
char	*filterProgramArguments(char *in)
{
	spaceData	b;

	int i, found;	
	char *start = in;
	data_init(&b);

	while (in && *in) {
		found = 0;
		for (i=0; _filterChars[i].ch; i++) {
			if (*in == _filterChars[i].ch ) {
				char codeBuf[3];

				/* skip over previously translated &#foo's */
/*
we believe that simply literalizing
will suffice
*/
				if ( _filterChars[i].ch == '#' && (in == start || *(in - 1) == '&' ) )
					continue;
				sprintf(codeBuf, "%d", _filterChars[i].code); 
				thing_appendChar(&b, '&', BUF_LIMIT_BIG);
				thing_appendChar(&b, '#', BUF_LIMIT_BIG);
				thing_append(&b, codeBuf, 2, BUF_LIMIT_BIG);
				thing_appendChar(&b, ';',  BUF_LIMIT_BIG);
/* nope
				thing_appendChar(&b, '\\', BUF_LIMIT_BIG);
				thing_appendChar(&b, _filterChars[i].ch, BUF_LIMIT_BIG);
*/
				found = 1;
			} 
		}

		if (!found) 			
			thing_appendChar(&b, (char)*in, BUF_LIMIT_BIG);

		in++;
	}
	return (char *)b.value;
}

/* the reverse of filterProgramArguments */
static char	*_htdb_unfilter(char *jin, int nodoublequotes, int safe)
{
	char *temp, *out = strdup(jin), *in = strdup(jin);
	int i = 0, j=0, ch, found, z;

	while (in && in[i]) {
		if (in[i] == '&' && in[i + 1] == '#' && isdigit((int) in[i + 2]))  {
			/* walk the temp variable to the semicolon, max 3 spaces. */
			for(z=0, temp = in + i + 2; isdigit((int) temp[z]) && z < 4; z++);
			
			temp += z;

			if (*temp == ';') {
				*temp = '\0';
		
				found = 0;	
				ch = atoi(in + i + 2);

				
				if (nodoublequotes && ch == '"') {
					found = 1;
					out[j] = '\'';
				} else {	
					for (z = 0; _filterChars[z].ch; z++) {
						if (_filterChars[z].code == ch) {
							/* if "safety" is on, only unfilter "$" if not followed by "{" */
							if ( _filterChars[z].ch == '$' && safe) {
								if (temp[1] == '{') 
									break;
							}

							found = 1;
							out[j] = _filterChars[z].ch;
						}
					}
				}	

				if (!found) {
					*temp =';';
					out[j++] = '&';
					i++;
				} else {
					i += (temp + 1) - (in + i); 
					j++;
				}
			} else
				out[j++] = in[i++];
		} else  {
			out[j++] = in[i++];
		}
	}
	out[j] = '\0';
	free(in);
	return(out);
}

char *htdb_unfilter(char *in)
{
	return _htdb_unfilter(in, 0, 1);
}

/* please use only when you know 
   there's no untrusted text in your htdb. */

char *htdb_unfilter_unsafe(char *in)
{
	return _htdb_unfilter(in, 0, 0);
}

char	*htdb_unfilter_nodoublequotes(char *in)
{
	return _htdb_unfilter(in, 1, 1);
}

static char	*cleanString(char *in)
{
	while (in && *in && isspace((int)*in))
		in++;
	if (in && *in)
		return in;
	else
		return "";
}

char	*prepareStringForDatabase(char *in)
{
	return filterProgramArguments(cleanString(in));
}

char	*prepareStringFromDatabase(char *in)
{
	return strdup(cleanString(in));
}

/*
	htdb_stripWhitespace()

		while whitespace at start of incoming string,
		humps the start pointer along.  NULLs trailing
		whitespace.

	returns: a pointer to the string modified IN PLACE
 */
char	*htdb_stripWhitespace(char *in)
{
	char	*preClean = cleanString(in);
	int		len = (preClean && *preClean) ? strlen(preClean) : 0;

	if (preClean && *preClean && len > 1)
		while (len > 1) {
			if (isspace((int)preClean[len - 1]))
				preClean[--len] = '\0';
			else
				break;
		}
	return preClean;
}

char	*encode(char *source)
{
	char	*object = (char *)
		malloc(sizeof(char) * ((strlen(source) * 3) + 1)),
		*temp1 = NULL,
		*temp2 = NULL;

	for (temp1 = source, temp2 = object ;
		*temp1 != '\0' ; temp1++, temp2++) {
		char	ch = *temp1;
		if (ch == ' ')
			*temp2 = '+';
		else {
			if ((ch == '|') || (ch == '#') || (ch == '^') || (ch == '&') ||
				(ch == '~') || (ch == '%') || (ch == ':') ||
				(ch == ',') || (ch == '@') || (ch == ';') ||
				(ch == '/') || (ch == '[') || (ch == ']') ||
				(ch == '`') || (ch == '<') || (ch == '+') ||
				(ch == '\\') || (ch == '\'') || (ch == '"') ||
				(ch == '\n') || (ch == '\r')) {
				char	hexnum[BUF_LIMIT_SMALL + 1];
				*(temp2++) = '%'; 
				snprintf(hexnum, BUF_LIMIT_SMALL, "%2X", ch);
				hexnum[BUF_LIMIT_SMALL] = '\0';
				*(temp2++) = hexnum[0]; 
				*temp2 = hexnum[1];
			} else
				*temp2 = ch;
		}
	}

	*temp2 = '\0';

	return object;
}

/*
	makeFileSystemSafe()
		turn non alphanumeric characters found in the string
		into `_' characters.

	NOTE: modifies the string IN-PLACE
 */
char	*makeFileSystemSafe(char *in)
{
	char	*out = &in[0];

	while (in && *in) {
		if (!isalnum((int)*in))
			*in = '_';
		in++;
	}

	return out;
}

char	**constructArgumentList(char *in, char separator, int *num)
{
	char	*prev = strdup(in),
			*hold = &prev[0],
			*ptr = strchr(prev, separator),
			**args = (char **)malloc(sizeof(char *) * strlen(prev));
	*num = 0;
	while (ptr && *ptr) {
		args[(*num)++] = prev;
		*ptr = '\0';
		prev = ptr + 1;
		ptr = strchr(prev, separator);
	}
	args[(*num)++] = prev;
	free(hold);
	return args;
}

void	destroyArgumentList(char ***args)
{
	if (args && *args)
		free(*args);
}

bool_t	isTag(char *tag, char *in, char **gottag)
{
	int	len_tag = strlen(tag);

	if ((len_tag + 2) > strlen(in))
		/*
			would walk past the end of the buffer
		 */
		return BOOL_FALSE;

	if (strncasecmp((in + 1), tag, len_tag) == 0) {
		/*
			the next few chars in the buffer match our tag..
		 */
		if (*gottag) {
			free(*gottag);
		}
		*gottag = strdup(tag);
		return BOOL_TRUE;
	}
	/*
		tag not found at front of buffer
	 */
	return BOOL_FALSE;
}

char	*reverseString(char *in)
{
	int	i,
		len = strlen(in),
		indx = 0;
	char	*out = (char *)malloc(sizeof(char) * (len + 1));

	for (i = len - 1 ; i >= 0 ; i--)
		out[indx++] = in[i];
	out[indx] = '\0';

	return out;
}

int	myIndexOf(char *table, char *field, char *value)
{
	char	*val;
	int		 i;

	for (i = 1 ; (val = htdb_getobjval(table, i, field)) && *val ; i++) {
		if (0 == strcasecmp(val, value))
			return i;
	}
	return 0;
}

/*
	IF NO strncpy
char	*strncpy(char *out, const char *in, size_t bytes)
{
	return snprintf(out, bytes, in);
}

	IF NO snprintf
int	snprintf(char *out, size_t bytes, const char *fmt, ...)
{
	va_list	args;
	if (fmt && *fmt) {
		va_start(args, fmt);
		vsnprintf(out, bytes, fmt, args);
		out[bytes - 1] = '\0';
		va_end(args);
	} else
		strcpy(out, "");
	return bytes;
}
*/

char	*sstrcat(char *dest, const char *src, size_t n)
{
	register char *d;

	for(d = dest; *d && n > 1; d++, n--) ;

	while(n-- > 1 && *src)
		*d++ = *src++;

	*d = 0;
	return dest;
}

static bool_t	htdb_inThing(char *src, char *in, char *sep, bool_t exact)
{
	char	*str = strdup(in),
			*str_h = &str[0];
	bool_t		gotOne = BOOL_FALSE;

	if (src && *src && str && *str) {
		char	*ptr = strtok(str, sep);

		do {
			if (ptr) {
				if (BOOL_TRUE == exact) {
					if (0 == strcasecmp(src, ptr))
						gotOne = BOOL_TRUE;
				} else {
					if (strcasestr(src, ptr))
						gotOne = BOOL_TRUE;
				}
			}
		} while (gotOne == BOOL_FALSE &&
			ptr && (ptr = strtok(NULL, sep)));
	}

	free(str_h);

	return gotOne;
}

bool_t	htdb_inList(char *src, char *in, bool_t exact)
{
	return htdb_inThing(src, in, "\n\r\t ", exact);
}

bool_t	htdb_inLine(char *src, char *in, bool_t exact)
{
	return htdb_inThing(src, in, "\n\r", exact);
}

/* This program implements the Mersenne twister algorithm for generation of pseudorandom numbers. 
The program returns random integers in the range 0 to 2^32-1 (this holds even if a long int is
larger than 32 bits). Timing with gcc indicates that it is about twice as fast as the built in 
rand function. The original code was written by Michael Brundage and has been placed in the 
public domain. There are a three minor changes here: 
(1) This comment has been added to the program.
(2) Type specifiers (ul) have been appended to constants.
(3) A commented out block near the end has been removed. */

#define MT_LEN 624
#include <stdlib.h>

int mt_index;
unsigned long mt_buffer[MT_LEN];

void mt_init() {
    int i;
    for (i = 0; i < MT_LEN; i++)
        mt_buffer[i] = rand();
    mt_index = 0;
}

#define MT_IA           397
#define MT_IB           (MT_LEN - MT_IA)
#define UPPER_MASK      0x80000000
#define LOWER_MASK      0x7FFFFFFF
#define MATRIX_A        0x9908B0DF
#define TWIST(b,i,j)    ((b)[i] & UPPER_MASK) | ((b)[j] & LOWER_MASK)
#define MAGIC(s)        (((s)&1)*MATRIX_A)

unsigned long mt_random() {
    unsigned long * b = mt_buffer;
    int idx = mt_index;
    unsigned long s;
    int i;
	
    if (idx == MT_LEN*sizeof(unsigned long))
    {
        idx = 0;
        i = 0;
        for (; i < MT_IB; i++) {
            s = TWIST(b, i, i+1);
            b[i] = b[i + MT_IA] ^ (s >> 1) ^ MAGIC(s);
        }
        for (; i < MT_LEN-1; i++) {
            s = TWIST(b, i, i+1);
            b[i] = b[i - MT_IB] ^ (s >> 1) ^ MAGIC(s);
        }
        
        s = TWIST(b, MT_LEN-1, 0);
        b[MT_LEN-1] = b[MT_IA-1] ^ (s >> 1) ^ MAGIC(s);
    }
    mt_index = idx + sizeof(unsigned long);
    return *(unsigned long *)((unsigned char *)b + idx);
    /* Here there is a commented out block in MB's original program */
}

/*
	TODO - replace with mt_random(), above
 */
int_t	htdb_random(int_t max)
{
	return 1 + (int_t)((double)max * rand() / (RAND_MAX + 1.0));
}

static void	do_tag(char **in, char* tags, int *tag_index)
{
	char *p = *in;
	p++;

	while(p && *p && isspace((int) *p))
		 p++;

	if (p && *p)
		switch (toupper(*p)) {
			case 'A':
			case 'B':
			case 'I':
				if (p && (p + 1) && (*(p + 1) == '>' || isspace((int) *(p + 1)))) {
					tags[(*tag_index)++] = toupper(*p);
					tags[(*tag_index)] = '\0';
				}
				break;
			case 'F':
				if (strncasecmp(p, "FONT", 4) == 0) {
					tags[(*tag_index)++] = 'F';
					tags[(*tag_index)] = '\0';
				}
				break;
			case '/':
				p++;
				if (toupper(*p) == tags[*(tag_index) - 1]) {
					tags[--(*tag_index)] = '\0';
				}
		}
	while (p && *p && *p != '>') 
		p++;	
	
	if (p && *p)
		p++;
	*in = p;		
}

/* 
	A simple html text truncation function.
	treats multiple spaces as a single space, does not 
	consider html tags as part of the text length, and recognizes
	<A HREF>, <B>, <I>, and <FONT> tags as tags that need to be closed.
	Don't use this on tables or anything complex.
*/
char	*html_safe_trunc(char *string, int len)
{
	char tags[1024] = { "" }, *in;
	char *begin,
		*lastSpace;
	int i = 0, new_len;
	int tag_index = 0;

	if (!(string && *string))
		return strdup("");

	if (len == 0 || len > strlen(string))
		return strdup(string);

	while(string && *string && isspace((int) *string))
		string++;
	
	begin = strdup(string);
	lastSpace = in = begin;
	while(in && *in) {
		if (i >= len) {
			/* step back to previous word boundary */
			*lastSpace = '\0';
			new_len = strlen(begin) + 3;
		
			/* add the puntos suspensivos */	
			begin = realloc(begin, (new_len + 1) * sizeof(char)); 
			begin[new_len] = '\0';
			strcat(begin, "...");
			break;
		} else if (isspace((int) *in)) {
			/* treat 1+ spaces as one space.  ahhh html. */
			lastSpace = in;
			while (in && *in && isspace((int) *in)) 
				in++;
			i++;
		} else if (*in == '<') {
			do_tag(&in, tags, &tag_index);
		} else {
			i++;
			in++;
		}
	}

	/* close off any old tags that we may have truncated between */
	while(tag_index >= 0) {
		char buf[BUF_LIMIT_SMALL + 1];
		switch (tags[tag_index]) {
			case 'A':
			case 'B':
			case 'I':
				if (begin && *begin) {
					new_len = strlen(begin) + 5;
					begin = realloc(begin, (new_len + 1) * sizeof(char)); 
					snprintf(buf, BUF_LIMIT_SMALL, "</%c>", tags[tag_index]);
					buf[BUF_LIMIT_SMALL] = '\0';
					strcat(begin, buf);
				}
				break;
			case 'F':
				if (begin && *begin) {
					new_len = strlen(begin) + 8;
					begin = realloc(begin, (new_len + 1) * sizeof(char)); 
					strcat(begin, "</FONT>");
				}
		}
		tag_index--;
	}
	return (begin);
}

/*
	storeIt()

		internal function used to maintain the object built by htdb_splitArgs()
 */
static void	storeIt(char ***out, char *str, int *num)
{
	if (str && *str)
		str = htdb_stripWhitespace(str);

	if ((*out) == (char **)NULL)
		(*out) = (char **)malloc(sizeof(char *) * ((*num) + 1));
	else
		(*out) = (char **)realloc((*out), sizeof(char *) * ((*num) + 1));

	(*out)[(*num)++] = strdup((str && *str) ? str : "");
}

static char	*findNextSeperator(char *start, char *sep)
{
	char	*e = strcasestr(start, sep);
	/*
		if there's a seperator with a literal
		before it, then we keep looking.
	 */
	while (e && *e && e > start && *(e - 1) == '\\')
		e = strcasestr(e + 1, sep);
	if (e)
		return e;
	return (char *)NULL;
}

/*
	htdb_splitArgs()

		this is intended to allow for easy parsing of separator-separated
		fields into a DSO function.  htdb_splitArgs() will creat an array
		of whitespace-cleaned arguments as found in the input string.

	example:

		input
			src: " hello, world , wow  "
			sep: ","
			max_args: 3

		yields:
			out[0]: "hello"
			out[1]: "world"
			out[2]: "wow"

	example:

		input
			src: " hello|||world| wow  "
			sep: "|"
			max_args: 4

		yields:
			out[0]: "hello"
			out[1]: ""
			out[2]: ""
			out[3]: "world| wow"

	example:

		input
			src: "hello,world,bugme,now wow  "
			sep: ","
			max_args: 2

		yields:
			out[0]: "hello"
			out[1]: "world,bugme,now wow"

	example:

		input
			src: "hello,world,bugme,now, wow  "
			sep: ","
			max_args: 0	(special case - signals no upper bound of args)

		yields:
			out[0]: "hello"
			out[1]: "world"
			out[2]: "bugme"
			out[3]: "now"
			out[4]: "wow"

	get it?

	NOTES:
		returns a DYNAMIC string array which needs to be cleaned-up
		by a call to htdb_deleteArgs() before the return from a DSO.
 */
int	htdb_splitArgs(char *src, char *sep, char ***out, int max_args)
{
	char	*str = strdup(src && *src ? src : ""),
			*str_h = &str[0];
	int		num = 0;

	*out = (char **)NULL;

	if (str && *str) {
		char	*b = str,
				*e;

		while (((max_args == 0) || (num < max_args - 1)) &&
			(e = findNextSeperator(b, sep))) {
			*e = '\0';
			storeIt(out, b, &num);
			b = e + strlen(sep);
		}
		if (b && *b)
			storeIt(out, b, &num);
	}

	free(str_h);

	return num;
}

/*
	htdb_deleteArgs()

		frees up memory stored in string arrays.
		such as used by htdb_splitArgs().
 */
void	htdb_deleteArgs(char **args, int num)
{
	int	i;
	for (i = 0 ; i < num ; i++)
		if (args && args[i]) {
			free(args[i]);
			args[i] = NULL;
		}

	if (args) {
		free(args);
		args = (char **)NULL;
	}
}

bool_t	isValidEmailAddress(char *email)
{
	/*
		exists and has no whitespace and contains an `@' and a `.'
		and at least as long as "a@x.uk"
		and doesn't contain "@."
	 */
	return (email && strchr(email, '@') && strchr(email, '.') &&
		!strchr(email, ' ') && !strchr(email, '\t') && (strlen(email) >= strlen("a@x.uk")) &&
		!strstr(email, "@.")) ? BOOL_TRUE : BOOL_FALSE;
}

bool_t	isStringNumeric(char *in)
{
	if (isdigit((int)*in) || (*in == '-' && atof(&in[1]) > 0)) {
		int	dot_count = 0;
		in++;
		while (in && *in) {
			if (*in == '.')
				dot_count++;
			if (dot_count > 1 || !(isdigit((int)*in) || *in == '.'))
				/*
					we ran into a character that was not a digit
					or the `.' and `-' characters, or we have too many
					dots to be a number (such as an IP address).
				 */
				return BOOL_FALSE;
			in++;
		}
		return BOOL_TRUE;
	}
	return BOOL_FALSE;
}

/*
	swiped verbatim from mysql
 */
char	*htdb_strcasestr(const char *str, const char *search)
{
	unsigned char *i,*j,*pos;

	pos=(unsigned char*) str;
skipp:
	while (*pos != '\0') {
		if (toupper((unsigned char) *pos++) == toupper((unsigned char) *search)) {
			i=(unsigned char*) pos; j=(unsigned char*) search+1;
			while (*j)
				if (toupper(*i++) != toupper(*j++))
					goto skipp;
			return ((char*) pos-1);
		}
	}
	return ((char *) 0);
}

#ifndef	strcasestr
char    *strcasestr(const char *s1, const char *s2)
{
	return htdb_strcasestr(s1, s2);
}
#endif

char	*htdb_cleanLines(char *in)
{
	spaceData	b;

	data_init(&b);

	while (in && *in) {
		if (*in == '\n' || *in == '\r') {
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
			while (in && *in && isspace((int)*in))
				in++;
		}
		if (in && *in) {
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
			in++;
		}
	}
	return b.value;
}

char	genAlnum(void)
{
	char	ret;
	int		digit;
	//digit = (int)((double) 36.0 * (double) (rand()/((double) RAND_MAX+1.0)));
	digit = (int) (36.0 * drand48());
	if (digit < 10)
		ret = '0' + digit;
	else
		ret = 'A' + digit - 10;

	return ret;
}

char	*genAlnumKey(char *table, char *field, int keylen)
{
	char	*key = (char *)malloc(sizeof(char) * (keylen + 1));
	int	i, found = 0, danger, seed, ret;

	for (seed = 0 ; seed < 3 && !found; seed++) {
		for (danger = 0; danger < 20; danger++) {
			for (i = 0 ; i < keylen ; i++)
				key[i] = genAlnum();
			key[i] = '\0';
	
			ret = dbQuery("select * from %s where %s = '%s' limit 1",
					table, field, key);

			if ( !ret ) {
				found = 1;
				break;
			}
		}

		/* assume that we're going down a bad path. 
		   re-seed the random generator */
		if ( !found )
			htdb_srand();
	}

	if ( !found ) {
		free(key);
		return NULL;
	} else
		return key;
}

#define	LEN_GOKEY	10

char	*genGoKey(void)
{
	return genAlnumKey("go", "gokey", LEN_GOKEY);
}

static char	*literalizeChar(char target, char *in)
{
	spaceData	b;
	data_init(&b);
	while (in && *in) {
		if (*in == target)
			thing_appendChar(&b, '\\', BUF_LIMIT_BIG);
		thing_appendChar(&b, *in, BUF_LIMIT_BIG);
		in++;
	}

	return (char *)b.value;
}

char	*literalizeCommas(char *in)
{
	spaceData	b;
	data_init(&b);
	while (in && *in) {
		if (*in == ',')
			thing_append(&b, "\\,", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		else 
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
		in++;
	}
	return (char *)b.value;
}

char	*literalizeQuotes(char *in)
{
	char	*singleQ = literalizeChar('\'', in),
			*doubleQ = literalizeChar('\"', singleQ);

	free(singleQ);

	return doubleQ;
}

int	removeDirectory(char *dir)
{
	DIR		*dp;

	if ((dp = opendir(dir)) != NULL) {
		struct dirent   *db;
		int	status = 1;
		while (status == 1 && (db = readdir(dp)) != NULL) {
			char	path[PATH_MAX + 1];
			struct	stat    buf;
			if (db->d_ino == 0 ||
				*db->d_name == '.')
				continue;
			snprintf(path, PATH_MAX, "%s/%s", dir, db->d_name);
			if (stat(path, &buf) == -1)
				return -1;
			if (S_ISDIR(buf.st_mode))
				status &= removeDirectory(path);
			else
				if (unlink(path) == -1)
					return -2;
		}
		closedir(dp);
		if (status == 0)
			return -3;
	} else
		return -4;
	if (rmdir(dir) == -1)
		return -5;

	return 0;
}

char	*DSO_RETURNS_SUCCESS_CHALLENGED(char *msg)
{
	char	*lit = literalizeCommas(msg),
			*out = (char *)malloc(sizeof(char) * (strlen(lit) + strlen(GENERIC_FAILURE_STRING) + 2));
	sprintf(out, "%s %s", GENERIC_FAILURE_STRING, lit);
	free(lit);
	return out;
}

int	lookupIndexByName(char *prefix, char *name)
{
	int	i;

	for (i = 0 ; ; i++) {
		char	*val = htdb_getobjval(prefix, i + 1, "name");
		if (val && *val) {
			if (strcasecmp(val, name) == 0) {
				/*
					found our man
				 */
				return i + 1;
			}
		} else
			/*
				no more XX[*]->name's found
			 */
			return 0;
	}
}

bool_t	parseFilterFunction(char *source, char **filterThis, int *index)
{
	char	*pipe = (source && *source) ? strchr(source, '|') : NULL;

	*filterThis = (char *)NULL;

	if (pipe && *pipe) {
		pipe++;
		if (pipe && *pipe) {
			char	*nextPipe = strchr(pipe, '|');
			if (nextPipe && *nextPipe) {
				char	*sdup = strdup(pipe),
						*chunk = strchr(sdup, '|');
				*chunk = '\0';
				if ((*index = lookupIndexByName("confFilter", sdup)) == 0) {
					free(sdup);
					*filterThis = (char *)NULL;
					return BOOL_FALSE;
				}
				free(sdup);
				*filterThis = nextPipe + 1;
				return BOOL_TRUE;
			}
		}
	}
	return BOOL_FALSE;
}

/*	returns malloc'ed copy of input with everything but numbers stripped */
char	*ccNumber(char *in)
{
	char	ret[BUF_LIMIT_SMALL + 1];
	int		indx = 0;
	while (in && *in && indx < BUF_LIMIT_SMALL) {
		if (isdigit((int)*in))
			ret[indx++] = *in;
		in++;
	}
	ret[indx] = '\0';
	return(strdup(ret));
}

int	ccValid(char *in)
{
	char	*number = ccNumber(in);
	int		length = strlen(number),
			odd = (length & 1),
			sum = 0,
			count;
	for (count = 0 ; count < length ; count++) {
		char	dig[2];
		int digit;
		dig[0] = number[count];
		dig[1] = '\0';
		digit = atoi(dig);
		if (count & (1 ^ odd))
			;
		else {
			digit += digit;
			if (digit > 9)
				digit -= 9;
		}
		sum += digit;
	}
	free(number);
	if (sum && (sum % 10 == 0))
		return 1;
	else
		return 0;
}

char	*htdb_ordinalSuffix(int in)
{
	static char	*out = "<sup>th</sup>";
	char	buf[512],
			*rev;

	sprintf(buf, "%0d", in);
	rev = reverseString(buf);

	if (strlen(rev) > 1 && rev[1] == '1') {
		out = "<sup>th</sup>";
	} else {
		switch (rev[0]) {
			case '1': out = "<sup>st</sup>"; break;
			case '2': out = "<sup>nd</sup>"; break;
			case '3': out = "<sup>rd</sup>"; break;
			default: out = "<sup>th</sup>"; break;
		}
	}
	free(rev);
	return out;
}

bool_t	isUnconfirmed(void)
{
	return (htdb_equal("user->isconfirmed", "F")) ? BOOL_TRUE : BOOL_FALSE;
}

bool_t	isConfirmed(void)
{
	return (htdb_equal("user->isconfirmed", "T")) ? BOOL_TRUE : BOOL_FALSE;
}

bool_t	isUser(void)
{
	return (isUnconfirmed() || isConfirmed()) ? BOOL_TRUE : BOOL_FALSE;
}

bool_t	isAdmin(void)
{
	return (isUser() && htdb_defined("user->accountype->admin")) ? BOOL_TRUE : BOOL_FALSE;
}	

typedef	struct	{
	char	*key,
			**fields;
}	htdbsort_t;

static int	htdbObjSort(const void *a, const void *b)
{
	htdbsort_t  *_a = (htdbsort_t *)a,
			*_b = (htdbsort_t *)b;
	long_t	_l1 = (_a->key && *(_a->key) ? atoll(_a->key) : 0),
			_l2 = (_b->key && *(_b->key) ? atoll(_b->key) : 0);

	if (_l1 > 0 && _l2 > 0)
		return (_l1 > _l2);
	else if (_b->key && _a->key)
		return strcasecmp(_a->key, _b->key);
	else
		return -1;
}

static int	htdbObjSortReverse(const void *a, const void *b)
{
	htdbsort_t  *_a = (htdbsort_t *)a,
			*_b = (htdbsort_t *)b;
	long_t	_l1 = (_a->key && *(_a->key) ? atoll(_a->key) : 0),
			_l2 = (_b->key && *(_b->key) ? atoll(_b->key) : 0);

	if (_l1 > 0 && _l2 > 0)
		return (_l2 > _l1);
	else if (_b->key && _a->key)
		return strcasecmp(_b->key, _a->key);
	else
		return -1;
}

void	htdb_objsort(char *p, char *key)
{
	char	*prefix = (p && *p && *p == '-') ? (p + 1) : p;
	bool_t	reverse = (p && *p && *p == '-') ? BOOL_TRUE : BOOL_FALSE;

	{
		int_t	numResults = htdb_getptrint(prefix, "numResults"),
				numFields = htdb_getptrint(prefix, "numFields"),
				i;
		htdbsort_t	*data = (htdbsort_t *)malloc(sizeof(htdbsort_t) * numResults);

		for (i = 0 ; i < numResults ; i++) {
			htdbsort_t	*element = &data[i];
			int_t		j;
			element->key = strdup(htdb_getobjval(prefix, i + 1, key));
			element->fields = (char **)malloc(sizeof(char *) * numFields);
			for (j = 0 ; j < numFields ; j++) {
				char	*nm = htdb_getptrdyname(prefix, "field");
				element->fields[j] = strdup(htdb_getobjval(prefix, i + 1, htdb_getarrayval(nm, j + 1)));
				free(nm);
			}
		}
		if (reverse == BOOL_TRUE)
			qsort(data, numResults, sizeof(htdbsort_t), htdbObjSortReverse);
		else
			qsort(data, numResults, sizeof(htdbsort_t), htdbObjSort);

		for (i = 0 ; i < numResults ; i++) {
			htdbsort_t	*element = &data[i];
			int_t	j;
			for (j = 0 ; j < numFields ; j++) {
				char	*nm = htdb_getptrdyname(prefix, "field");
				htdb_setobjval(prefix, i + 1, htdb_getarrayval(nm, j + 1), element->fields[j]);
				free(nm);
				free(element->fields[j]);
			}
			free(element->fields);
			free(element->key);
		}
		free(data);
	}
}

int		htdb_objtransfer(char *prefix_dest, int index_dest, char *prefix_src, int index_src)
{
	int_t	numFields = htdb_getptrint(prefix_src, "numFields");
	int 	i;

	char	*nm = htdb_getptrdyname(prefix_src, "field");
	char	*fieldPrefix = htdb_getptrdyname(prefix_dest, "field");
	for (i = 1; i <= numFields ; i++) {
		char *val = htdb_getobjval(prefix_src, index_src, htdb_getarrayval(nm, i));
		htdb_setobjval(prefix_dest, index_dest, htdb_getarrayval(nm, i), val);
		htdb_setarrayval(fieldPrefix, i, htdb_getarrayval(nm, i));
		if ( index_dest == 1 ) {
			htdb_setptrval(prefix_dest, htdb_getarrayval(nm, i), val);
		}
	}

	htdb_setptrint(prefix_dest, "numFields", numFields);
	free(nm);
	free(fieldPrefix);
	return(i);
}

char	*htdb_mapStr(char *pattern1, char *pattern2, char *input)
{
	char	*tmppattern2 = pattern2;

	spaceData   b;

	data_init(&b);

	if (strlen(tmppattern2) == 0)
		tmppattern2 = " ";

	while (input && *input) {
		if (input == strcasestr(input, pattern1)) {
			thing_append(&b, tmppattern2, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
			input += strlen(pattern1);
		} else {
			thing_appendChar(&b, *input, BUF_LIMIT_BIG);
			input++;
		}
	}
	{
		char	*ret = strdup((char *)b.value);
		data_destroy(&b);
		return ret;
	}
}

char	*htdb_prettyNumber(char *in)
{
	int	len = strlen(in),
		i = len - 1,
		indx = 0,
		dig = 0;
	char	*tmp = (char *)malloc(sizeof(char) * (len * 2)),
			*out,
			*dot = strchr(in, '.');

	if ( dot ) {
		for ( i = len - 1; in[i] != '.'; i-- ) 
			tmp[indx++] = in[i];

		tmp[indx++] = in[i--];
	}	


	for ( ; i >= 0 ; i--) {
		tmp[indx++] = in[i];
		dig++;
		if ((dig % 3) == 0)
			if (i > 0 && in[i - 1] != '-')
				tmp[indx++] = ',';
	}
	tmp[indx] = '\0';

	out = reverseString(tmp);

	free(tmp);

	return out;
}

void	htdb_srand(void)
{
	struct timeval	clock;
	gettimeofday(&clock, (void *)NULL);
	srand(getpid() ^ ((unsigned int)0x00000000FFFFFFFF & (clock.tv_sec + clock.tv_usec)));
	srand48(getpid() ^  (clock.tv_sec + (clock.tv_usec * 10000)));
}

char	*htdb_system(char *cmd)
{
	FILE	*pp = popen(cmd, "r");

	if (pp == (FILE *)NULL)
		return strdup("");

	{
		spaceData b;
		char	buf[BUF_LIMIT_SMALL + 1];

		data_init(&b);

		/*
			suck
		 */
		while (fgets(buf, BUF_LIMIT_SMALL, pp))
			thing_append(&b, buf,
				FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);

		pclose(pp);
		return ((char *)b.value);
	}
}

char	*htdb_uname(char *in)
{
	if (in && *in) {
		char	buf[BUF_LIMIT_SMALL + 1];
		snprintf(buf, BUF_LIMIT_SMALL, "uname -%s", in);
		buf[BUF_LIMIT_SMALL] = '\0';
		return htdb_system(buf);
	} else
		return htdb_system("uname");
}

/*
	uncgi_htoi()
		convert two hex digits to a value.
 */
int	uncgi_htoi(unsigned char *s)
{
	int	value;
	char	c = s[0];

	if (isupper((int)c))
		c = tolower((int)c);

	value = (c >= '0' && c <= '9' ? c - '0' : c - 'a' + 10) * 16;

	c = s[1];

	if (isupper((int)c))
		c = tolower((int)c);

	value += c >= '0' && c <= '9' ? c - '0' : c - 'a' + 10;

	return (value);
}

/*
	uncgi_url_unescape()
		get rid of all the URL escaping in a string.
		modify it in place, since the result will always be
		equal in length or smaller.
 */
void	uncgi_url_unescape(unsigned char *str)
{
	unsigned char	*dest = str;

	if (str && *str) {
		while (str && *str) {
			if (str[0] == '+')
				dest[0] = ' ';
			else if (str[0] == '%' && ishex(str[1]) && ishex(str[2])) {
				dest[0] = (unsigned char)uncgi_htoi(str + 1);
				str += 2;
			} else
				dest[0] = str[0];
			str++;
			dest++;
		}

		dest[0] = '\0';
	}
}

char	*url_escape(char *in)
{
	spaceData	b;

	data_init(&b);

	thing_append(&b, "<script language=javascript>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	thing_append(&b, "document.write(unescape(\"", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	while (in && *in) {
		char	hex[BUF_LIMIT_SMALL];
		sprintf(hex, "%%%02x", *in);
		thing_append(&b, hex, 3, BUF_LIMIT_BIG);
		in++;
	}
	thing_append(&b, "\"));", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	thing_append(&b, "</script>", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);

	return strdup(b.value);
}

bool_t	htdb_parseURL(char *url, char **protocol, char **hostname, unsigned *port, char **uri, char **username, char **password)
{
	*port = 0;

	*protocol = *hostname = *uri = *username = *password = NULL;

	if (url && *url) {
		char	*dup = strdup(url),
				*hold = &dup[0],
				*slash = strstr(url, "://");

		if (strchr(url, '@')) {
			/*
				we don't support http://username:password@URL
			 */
			free(hold);
			return BOOL_FALSE;
		}
		if (slash && *slash) {
			/*
				assumed to be "zzz://www.xxx.com/..."
			 */
			if (0 == strncasecmp(url, "https", 5)) {
				*protocol = strdup("https");
				*port = 443;
			} else if (0 == strncasecmp(url, "http", 4)) {
				*protocol = strdup("http");
				*port = 80;
			} else if (0 == strncasecmp(url, "ftp", 3)) {
				*protocol = strdup("ftp");
				*port = 21;
			} else {
				free(hold);
				return BOOL_FALSE;
			}
			*hostname = strdup(slash + 3);
		} else {
			/*
				assumed to be "www.xxx.com/..."
				default to http protocol.
			 */
			*protocol = strdup("http");
			*port = 80;
			*hostname = strdup(url);
		}

		slash = strchr(*hostname, '/');
		if (slash && *slash) {
			*uri = strdup(slash);
			*slash = '\0';
		} else {
			*uri = strdup("/");
		}
		{
			char	*colon = strchr(*hostname, ':');
			if (colon && *colon) {
				*colon = '\0';
				*port = atoi(colon + 1);
			}
		}
		free(hold);
	}

	if (0 == *port)
		return BOOL_FALSE;

	/*
		we can only handle http protocol right now
		(who wants to do https/ftp..?)
	 */
	if (strcasecmp(*protocol, "http")) {
		if (*protocol) { free(*protocol); *protocol = (char *)NULL; }
		if (*hostname) { free(*hostname); *hostname = (char *)NULL; }
		if (*uri) { free(*uri); *uri = (char *)NULL; }
		if (*username) { free(*username); *username = (char *)NULL; }
		if (*password) { free(*password); *password = (char *)NULL; }
		return BOOL_FALSE;
	}

	return BOOL_TRUE;
}

char	*htdb_parseTitleFromDocument(char *doc)
{
	if (doc && *doc) {
		char	*title = strcasestr(doc, "<title>");
		if (title && *title) {
			title += strlen("<title>");
			{
				char	*endtitle = strcasestr(title, "</title>");
				if (endtitle && *endtitle && endtitle < (title + 150)) {
					*endtitle = '\0';
					return (filterProgramArguments(title));
				}
			}
		}
	}
	return strdup("");
}

char	*htdb_htdb2html(char *in)
{
	spaceData	b;
	data_init(&b);

	while (in && *in) {
		if (*in == '#')
			thing_append(&b, "&#35;", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		else if (*in == '$')
			thing_append(&b, "&#36;", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		else if (*in == '<')
			thing_append(&b, "&lt;", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		else if (*in == '>')
			thing_append(&b, "&gt;", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		else if (*in == '\n')
			thing_append(&b, "<br/>\n", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		else if (*in == '&')
			thing_append(&b, "&amp;", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		else if (*in == '\t')
			thing_append(&b, "&nbsp;", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		else
			thing_appendChar(&b, *in, BUF_LIMIT_BIG);
		in++;
	}
	return (b.value);
}

/*
	htdb_ntoa()
		dottted-quad representation of string representation of integer input
 */
char	*htdb_ntoa(char *in)
{
	unsigned long	t = atof(in),
			x1 = 256,
			x2 = 65536,
			x3 = 16777216,
			o1 = t / x3,
			d1 = t - (o1 * x3),
			o2 = d1 / x2,
			d2 = d1 - (o2 * x2),
			o3 = d2 / x1,
			d3 = d2 - (o3 * x1),
			o4 = d3;
	static char	out[BUF_LIMIT_SMALL + 1];
	snprintf(out, BUF_LIMIT_SMALL, "%ld.%ld.%ld.%ld", o1, o2, o3, o4);
	out[BUF_LIMIT_SMALL] = '\0';
	return out;
}

/*
	htdb_aton()
		decoding of dottted-quad representation of network address
 */
char	*htdb_aton(char *in)
{
	char	**args;
	int		num_args = htdb_splitArgs(in, ".", &args, 4);
	unsigned	lout = 0;

	if (num_args == 4)
		lout = (atol(args[0]) * (unsigned)16777216) +
				(atol(args[1]) * (unsigned)65536) +
				(atol(args[2]) * 256) + atol(args[3]);

	htdb_deleteArgs(args, num_args);

	{
		char	static out[BUF_LIMIT_SMALL + 1];

		snprintf(out, BUF_LIMIT_SMALL, "%u", lout);
		out[BUF_LIMIT_SMALL] = '\0';
		return out;
	}
}

/*
	htdb_dynamicResultList()

		walks through all 'field'-named elements of an array
		of htdb objects and builds a mysql-usable comma-separated-list
		
		returns a DYNAMIC string 
 */
char	*htdb_dynamicResultList(char *base, char *field, int *numIds)
{
	int		len = (((*numIds = htdb_getptrint(base, "numResults")) * 20) + 100),
			i;
	char	*resultList = (char *)malloc(sizeof(char) * len),
			*ptr = &resultList[0];

	strcpy(resultList, "");
	for (i = 0 ; i < *numIds ; i++) {
		strcpy(ptr, htdb_getobjval(base, i + 1, field));	
		ptr += strlen(ptr);

		if (i < *numIds - 1) {
			*(ptr++) = ',';
		}	
		*ptr = '\0';
			
	}
	return resultList;
}

// You must free the result if result is non-NULL.
char *_str_replace(char *orig, char *rep, char *with) {
    char *result; // the return string
    char *ins;    // the next insert point
    char *tmp;    // varies
    int len_rep;  // length of rep (the string to remove)
    int len_with; // length of with (the string to replace rep with)
    int len_front; // distance between rep and end of last rep
    int count;    // number of replacements

    // sanity checks and initialization
    if (!orig || !rep)
        return NULL;
    len_rep = strlen(rep);
    if (len_rep == 0)
        return NULL; // empty rep causes infinite loop during count
    if (!with)
        with = "";
    len_with = strlen(with);

    // count the number of replacements needed
    ins = orig;
    for (count = 0; (tmp = strstr(ins, rep)) ; ++count) {
        ins = tmp + len_rep;
    }

    tmp = result = malloc(strlen(orig) + (len_with - len_rep) * count + 1);

    if (!result)
        return NULL;

    // first time through the loop, all the variable are set correctly
    // from here on,
    //    tmp points to the end of the result string
    //    ins points to the next occurrence of rep in orig
    //    orig points to the remainder of orig after "end of rep"
    while (count--) {
        ins = strstr(orig, rep);
        len_front = ins - orig;
        tmp = strncpy(tmp, orig, len_front) + len_front;
        tmp = strcpy(tmp, with) + len_with;
        orig += len_front + len_rep; // move to next "end of rep"
    }
    strcpy(tmp, orig);
    return result;
}

char	*htdb_replaceMagic(char *orig)
{
	char *ret = strdup(orig),
		*c1, *c2;
	if (strstr(ret, "[comma]")) {
		c1 = _str_replace(ret, "[comma]", ",");
		free(ret);
		ret = c1;
	}
	if (strstr(ret, "[space]")) {
		c2 = _str_replace(ret, "[space]", " ");
		free(ret);
		ret = c2;
	}
	return ret;	/* MUST FREE */
}

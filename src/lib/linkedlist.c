#ident	"$Header$"

/*
	HTDB is shared under MIT License
	David Whittemore <del@adjective.com>
*/
#include	"libhtdb.h"

static int
local_compare(void *v1, void *v2)
{
	/* return strcmp((char *)v1, (char *)v2); */

	/* default behavior is to always add to end of list
	 * Note that FOR THE USER, passing NULL to ll_lookup()
	 * is a really stupid thing to do since it will never
	 * find a match :-)
	 */
	return -1;
}

LinkedList
ll_create( void )
{
	LinkedList newlist;

	newlist = (LinkedList)malloc(sizeof(struct linkedlist_t));
	newlist->first = NULL;
	newlist->item  = NULL;
	return newlist;
}


int
ll_is_empty(LinkedList ll)
{
	int empty = 1;

	if (ll == NULL) {
		empty = 1;
	} else if (ll->first == NULL) {
		empty = 1;
	} else {
		empty = 0;
	}
	return empty;
}

int
ll_count(LinkedList ll)
{
	int count = 0;

    if (ll && ll->first) {
		LLItem item = ll->first;
		while (item) {
			count++;
			item = item->next;
		}
    } else {
        count = 0;
    }
    return count;
}

static void
lli_destroy(LLItem item)
{
	if (item) {
		item->value = NULL;
		free(item);
	}
}

void
ll_destroy(LinkedList ll)
{
	if (ll) {
		LLItem item = ll->first;
		while (item) {
			LLItem freeme = item;
			item = item->next;
			lli_destroy(freeme);
		}
		ll->first = NULL;
		ll->item  = NULL;
		free(ll);				/* then the table */
	}
}

LLItem
ll_lookup(LinkedList ll, void *value, int (*compare)(void*, void*))
{
    LLItem item    = ll->first;
    if (compare == NULL) {
		compare = local_compare;
	}
    while (item) {
	    if ((*compare)(item->value,value) == 0) /* Found a match!   */
            return item;
	    item = item->next;			/* else hump along */
    }
    return NULL;
}

int
ll_add(LinkedList ll, void *value, int (*compare)(void*, void*))
{
	LLItem item;

	/* update existing entry */
	if ((item = ll_lookup(ll, value, compare))) {
        item->value = value;
        return 0;
	} else {	/* create a new one */
		LLItem item     = ll->first;
		LLItem previtem = NULL;
        LLItem newitem  = (LLItem)malloc(sizeof(struct llitem_t));

		if ( newitem == NULL)		      /* malloc()  ERROR */
			return -1;

		if (compare == NULL) {
			compare = local_compare;
		}

		newitem->value = value;

        while (item) {
            if ((*compare)(item->value, value) >= 0) {
				if (previtem == NULL) {
					ll->first = newitem;
				} else {
					previtem->next = newitem;
				}
				newitem->next = item;
				return 1;
			}
			previtem = item;
			item = item->next;
        }
        /* add to end of list */
		newitem->next = NULL;
		if (previtem == NULL) {
			ll->first = newitem;
		} else {
			previtem->next = newitem;
		}
	}
	return 1;
}

int
ll_del(LinkedList ll, void *value, int (*compare)(void*, void*))
{
       if ( ll ) {
	   LLItem item;
	   if ((item = ll_lookup(ll, value, compare))) {	 /* got one to del */
		   if (item == ll->first) {
		       ll->first = item->next;
		       if (ll->item == item) {
		           ll->item = NULL;
		       }
		   } else {
		       /*
		        * At this point, back pointers would really be useful 
		        * in removing an item.  ASSUMPTION:  delete will be
		        * an infrequent operation, so the space/time tradeoff 
		        * of this code -vs- another pointer field falls to
		        * the side of more code and less space.        	 
		        *
		        * We know that the bucket MUST be on this chain.
		        * because hash( key ) pointed here  AND		 
		        * 	  ll_lookup( key) suceeded.
		        */
		       LLItem tmp = ll->first;
		       /*
		        *	+-----+   +------+   +------+   +------+
		        *       |     |-->|      |-->|      |-->|      |-->@
		        *	+-----+   +------+   +------+   +------+
		        *         tmp                  item
		        */
		       while (tmp->next && tmp->next != item) {
		           tmp = tmp->next;
		       }
		       /*
		        *	+-----+   +------+   +------+   +------+
		        *       |     |-->|      |-->|      |-->|      |-->@
		        *	+-----+   +------+   +------+   +------+
		        *                   tmp        item
		        */
					 if (tmp->next && tmp->next->next) {
						 tmp->next = tmp->next->next;
					 }
						
		       /*			   +----------+
		        *	+-----+   +------+ | +------+ | +------+
		        *       |     |-->|      |-+ |      | +>|      |-->@
		        *	+-----+   +------+   +------+   +------+
		        *                   tmp        item
		        */
		       if (ll->item == item) {
		           ll->item = tmp;
		       }
		   }
		   lli_destroy(item);
		   return 0;
	       }
	}
	return -1;
}

void *
ll_find(LinkedList ll, void *value, int (*compare)(void*, void*))
{
	LLItem item;
    if ((item = ll_lookup(ll, value, compare)))
		return item->value;
	else
		return NULL;
}


void
ll_iterate(LinkedList ll)
{
	if (ll) {
		ll->item = NULL;
	}
}

LLItem
ll_traverse(LinkedList ll)
{
	if (ll) {
		if (ll->item == NULL) {
			ll->item = ll->first;
		} else {
			ll->item = ll->item->next;
		}
		return ll->item;
	}
	return NULL;
}

void
ll_print(LinkedList ll)
{
    LLItem item;
    int x = 0;

    ll_iterate(ll);
    while ((item = ll_traverse(ll)) ) {
        printf("%2d: 0x%06x\n", x++, (unsigned int)(item->value));
    }
}

#if 0

main()
{
    LinkedList ll;
    int cc;
    LLItem item;

    ll = ll_create();

    printf("LinkedList:\n");
	ll_print(ll);

    printf("LinkedList:\n");
    ll_iterate(ll);
    while ( (item = ll_traverse(ll)) ) {
	printf("%s\n", (char *)(item->value));
    }
    
    cc = ll_add(ll,"Monday",	NULL);
    cc = ll_add(ll,"Tuesday",	NULL);
    cc = ll_add(ll,"Wednesday",	NULL);
    cc = ll_add(ll,"Thursday",	NULL);
    cc = ll_add(ll,"Friday",	NULL);

    printf("LinkedList:\n");
    ll_iterate(ll);
    while ( (item = ll_traverse(ll)) ) {
	printf("%s\n", (char *)(item->value));
    }
    
    ll_destroy(ll);
}
#endif

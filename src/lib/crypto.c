#ident	"$Header$"

#include	"libhtdb.h"

/*
	RC4 stuph.
 */
typedef struct rc4_key {
	unsigned char state[256];
	unsigned char x;
	unsigned char y;
}	rc4_key;

static void rc4_swap_byte(unsigned char *a, unsigned char *b)
{
	unsigned char	swapByte;

	swapByte = *a;
	*a = *b;
	*b = swapByte;
}

static void	rc4_prepare_key(unsigned char *key_data_ptr, int key_data_len,
	rc4_key *key)
{
	unsigned char index1;
	unsigned char index2;
	unsigned char* state;
	short counter;

	state = &key->state[0];
	for(counter = 0; counter < 256; counter++)
		state[counter] = counter;
	key->x = 0;
	key->y = 0;
	index1 = 0;
	index2 = 0;
	for (counter = 0; counter < 256; counter++) {
		index2 = (key_data_ptr[index1] + state[counter] + index2) % 256;
		rc4_swap_byte(&state[counter], &state[index2]);
		index1 = (index1 + 1) % key_data_len;
	}
}

static void	rc4(unsigned char *buffer_ptr, int buffer_len, rc4_key *key)
{
	unsigned char x;
	unsigned char y;
	unsigned char* state;
	unsigned char xorIndex;
	short counter;

	x = key->x;
	y = key->y;

	state = &key->state[0];
	for (counter = 0; counter < buffer_len; counter ++) {
		x = (x + 1) % 256;
		y = (state[x] + y) % 256;
		rc4_swap_byte(&state[x], &state[y]);
		xorIndex = (state[x] + state[y]) % 256;
		buffer_ptr[counter] ^= state[xorIndex];
	}
	key->x = x;
	key->y = y;
}

static unsigned char	*do_rc4_encryptor(char *src, int src_len,  char *key_str)
{
	unsigned char	*dest = (unsigned char *)malloc(sizeof(unsigned char) * (src_len + 1));
	rc4_key	key;

	rc4_prepare_key((unsigned char *)key_str, strlen(key_str), &key);

	/*
		encrypt into a newly-allocated destination.
	 */
	memset(dest, 0, src_len + 1);
	strncpy((char *)dest, src, src_len);
	rc4(dest, src_len, &key);

	return dest;
}

static char	*do_rc4_decryptor(char *src, int src_len, char *key_str)
{
	/*
		decrypt
	 */
	rc4_key	key;
	rc4_prepare_key((unsigned char *)key_str, strlen(key_str), &key);
	rc4((unsigned char *)src, src_len, &key);
	/*
		sanity check...
	 */
	{
		int		i;
		for (i = 0 ; i < src_len && src && src[i] ; i++) {
			/*
				walk through the string.
			 */
		}
		if (i != src_len) {
			/*
				error if not NULL terminated.
			 */
			free(src);
			src = strdup("");
		}
	}
	return src;
}

/*
	do_encryptor()

		given a encryption key, an object to encrypt and
		an crypto "handle", do the following:

			1) call the encryptor as specified by the value of `algo'
			2) return dynamically-allocated base64-decoded encrypted string
 */
char	*do_encryptor(char *key, char *object, char *algo)
{
	char	*ret;

	if (object && *object && key && *key) {
		int		obj_len = strlen(object);
		char	*encrypted = (char *)NULL;

		/*
			if using builtin-rc4 routines...
		 */
		if (0 == strcmp(algo, "rc4")) {
			encrypted = (char *)do_rc4_encryptor(object, obj_len, key);
		} else {
#if	(defined(HAVE_LIBMCRYPT) && defined(USE_LIBMCRYPT))
			/*
				if using libmcrypt..
			 */
			encrypted = strdup(object);
#else
			encrypted = strdup(object);
#endif
		}

		/*
			(modified)base64-encode the result.
			this makes it usable in URL/GET args..
		 */
		ret = base64_encode(encrypted, obj_len, NULL);

		/*
			clean up
		 */
		if (encrypted)
			free(encrypted);
	} else
		/*
			no object to encrypt, or no crypto key.
			return empty string copy.
		 */
		ret = strdup("");

	return ret;
}

/*
	do_decryptor()

		given a decryption key, an object to decrypt and
		an crypto "handle", do the following:

			1) base64-decode the incoming string
			2) call the decryptor as specified by the value of `algo'
			3) return dynamic decrypted string
 */
char	*do_decryptor(char *key, char *object, char *algo)
{
	char	*ret;
	if (object && *object && key && *key) {
		int		obj_len = strlen(object);
		char *un64_obj = base64_decode(object, obj_len);

		if (un64_obj && *un64_obj) {
			/*
				the final result becomes shorter than the
				original base64-encoded string..
			 */
			int	dest_len = (obj_len * 3) / 4;
			/*
				if using builtin-rc4 routines..
				(`un64_obj' is either free'd or returned by do_rc4_decryptor,
				so no need to free it before returning from this function)
			 */
			if (0 == strcmp(algo, "rc4")) {
				ret = do_rc4_decryptor(un64_obj, dest_len, key);
			} else {
#if	(defined(HAVE_LIBMCRYPT) && defined(USE_LIBMCRYPT))
				/*
					if using libmcrypt..
				 */
				ret = strdup(un64_obj);
#else
				ret = strdup(un64_obj);
#endif
			}
		} else {
			/*
				the thing did not properly base64-decode.
				return empty string copy.
			 */
			if (un64_obj)
				free(un64_obj);
			ret = strdup("");
		}

	} else
		/*
			no object to decrypt, or no crypto key.
			return empty string copy.
		 */
		ret = strdup("");

	return ret;
}

/*
	htdb_decryptor()

		input is a string of the form:

			"|handle|base-64-encoded-encrypted-data"

		after looking up the appropriate crypto handler,
		call do_decryptor() to perform the heavy lifting.

		returns dynamic string
 */
char	*htdb_decryptor(char *source)
{
	char	*filterThis,
			*ret;
	int		index;
	if (BOOL_TRUE == parseFilterFunction(source, &filterThis, &index)) {
		char	*valueKey = htdb_expand(htdb_getobjval("confFilter", index, "key")),
				*valueAlgo = htdb_getobjval("confFilter", index, "algo"),
				*valueEnabled = htdb_getobjval("confFilter", index, "enabled");

		if (valueEnabled && *valueEnabled && (int)tolower(*valueEnabled) == 'y')
			/*
				we are being asked to decrypt
			 */
			ret = do_decryptor(valueKey, filterThis, valueAlgo);
		else
			/*
				the crypto handle is defined, but disabled
				(probably for debugging purposes)
				pass back the incoming string en-tact.
			 */
			ret = strdup(source);
		free(valueKey);
	} else
		/*
			the crypto handle was not found,
			eat the incoming string..
		 */
		ret = strdup("");

	return ret;
}

/*
	htdb_encryptor()

		after looking up the appropriate crypto handler,
		call do_encryptor() to perform the heavy lifting.

		returns dynamic string of the form:

			"|handle|base-64-encoded-encrypted-data"
 */
char	*htdb_encryptor(char *name, char *source)
{
	char	*ret;

	if (source && *source == '|') {
		/*
			if the string is already of the form:

				|handle|base-64-encoded-encrypted-data

			then assume incoming string is already encrypted -
			we will return the decrypted string.
		 */
		ret =  htdb_decryptor(source);
	} else {
		int	index = lookupIndexByName("confFilter", name);
		if (index > 0) {
			char	*valueKey = htdb_expand(htdb_getobjval("confFilter", index, "key")),
					*valueAlgo = htdb_getobjval("confFilter", index, "algo"),
					*valueEnabled = htdb_getobjval("confFilter", index, "enabled");

			if (valueEnabled && *valueEnabled && (int)tolower(*valueEnabled) == 'y') {
				/*
					we are being asked to encrypt
				 */
				char	*crypto = do_encryptor(valueKey, source, valueAlgo);

				ret = (char *)malloc(sizeof(char) * (strlen(crypto) + strlen(name) + 2 + 1));
				sprintf(ret, "|%s|%s", name, crypto);
				free(crypto);
			} else {
				/*
					the crypto handle is defined, but disabled
					(probably for debugging purposes)
					pass back the incoming string en-tact.
				 */
				ret = strdup(source);
			}
			free(valueKey);
		} else
			/*
				the crypto handle was not found,
				eat the incoming string..
			 */
			ret = strdup("");
	}

	return ret;
}

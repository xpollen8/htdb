#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/

#include	"htdb.h"

#if	(defined(HAVE_LIBXML2) && defined(USE_LIBXML2))
	#include	<libxml/xinclude.h>
#endif

#ifndef	H_HTDBINTERNAL
#define	H_HTDBINTERNAL

/***********************************************************************
	BEGIN HTDB-PRIVATE PROTOTYPES
 ***********************************************************************/

#ifndef	NAME_MAX
#define	NAME_MAX	1024
#endif

/*
	htdb-internal tweakable constants
 */
#define	SESSIONKEY_LENGTH	20		/* how many characters are in database sessionkeys? */
#define	MAX_IF_RECURSE		10		/* how deep can `if' recursion go? */
#define	MAX_IF_DEPTH		20		/* how deep can `if' statements nest? */

/*
	for token parsing
 */
#define	HTDB_TOKEN_BEGIN_ONE		'$'
#define	HTDB_TOKEN_BEGIN_ONE_LENGTH	1
#define	HTDB_TOKEN_BEGIN_TWO		'{'
#define	HTDB_TOKEN_BEGIN_TWO_LENGTH	1
#define	HTDB_TOKEN_BEGIN			"${"
#define	HTDB_TOKEN_BEGIN_LENGTH		2
#define	HTDB_TOKEN_END				'}'
#define	HTDB_TOKEN_OPENCALLABLE		'('
#define	HTDB_TOKEN_CLOSECALLABLE	')'

extern struct timeval  istart;			/* for timing of page generation */

typedef struct	{						/* for storing monthname data */
	int	days;
	char	*nameOrdinal,
			*nameShort,
			*nameLong,
			*nameNextShort,
			*nameNextLong,
			*namePrevShort,
			*namePrevLong;
}	month_t;

extern  month_t	monthData[];

typedef struct fileinfo {
	char name[NAME_MAX];
	char field[NAME_MAX];	/* name from form input tag */
	char type[NAME_MAX];	/* from Content-Type header */
	int size;		/* file size in bytes */
	int status;		/* 0 if OK, errno otherwise */
} fileinfo;

typedef struct namval {					/* uncgi name/value pair storage type */
	char name[NAME_MAX];
	char *value;
} namval;

#define	ERROR_DATABASE_SOCKET		-1
#define	ERROR_DATABASE_RESULTHANDLE	-2
#define	ERROR_DATABASE_QUERY		-3

#define	DATABASE_EMPTY_FIELD		""

typedef	struct {
	char	*name,		/* associative name */
			*driver,	/* libdbi driver name */
			*database,	/* default database to start with */
			*username,	/* login username */
			*password,	/* login password */
			*host,		/* hostname/IP */
			*port;		/* port to connect on */
	_dbHandle	socket;	/* the open socket */
}	htdb_db_t;

/*
	debugging masks and such
 */

#define	MASK_NONE			(0x0)
#define	MASK_FATAL			(DEBUG_LEVEL_FATAL)
#define	MASK_ERROR			(DEBUG_LEVEL_FATAL | DEBUG_LEVEL_ERROR)
#define	MASK_WARNING		(DEBUG_LEVEL_FATAL | DEBUG_LEVEL_ERROR | DEBUG_LEVEL_WARNING)
#define	MASK_INFO			(DEBUG_LEVEL_FATAL | DEBUG_LEVEL_ERROR | DEBUG_LEVEL_WARNING | DEBUG_LEVEL_INFO)
#define	MASK_DEBUG			(DEBUG_LEVEL_DEBUG)

extern long	debug_visual;
extern long	debug_log;

typedef	struct	{
	char	*ip,
			sessionkey[SESSIONKEY_LENGTH + 1];
	int_t	user_id,
			domain_id,
			referer_id,
			browser_id,
			session_id;
}	user_t;

#define ishex(x) (((x) >= '0' && (x) <= '9') || ((x) >= 'a' && (x) <= 'f') || \
		((x) >= 'A' && (x) <= 'F'))

extern int	rtoi(char *rom);
extern void	itor(int num, char **str);
extern char	*braille(char *in);

extern char	*getVal(char *prefix, char *name, int row);
extern char	*getVals(char *prefix, char *name, int row);

extern int	evaluate(char *line, double *val);

/*
	linklist.c
 */

extern LinkedList	ll_create(void);
extern int		ll_del(LinkedList, void *, int (*)(void*, void*));
extern int	ll_is_empty(LinkedList);
extern int	ll_count(LinkedList);
extern int	ll_add(LinkedList, void *, int (*)(void *, void *));
extern LLItem	ll_lookup(LinkedList, void *, int (*)(void *, void *));
extern void	ll_iterate(LinkedList);
extern void	ll_destroy(LinkedList);
extern LLItem	ll_traverse(LinkedList);

#endif

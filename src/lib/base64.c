#ident	"$Header$"

/*
	HTDB is shared under MIT License
	David Whittemore <del@adjective.com>
*/
#include	"libhtdb.h"

#define WEBSAFE_BASE64EOS   '\0'

#define	boolean			char
#define	BASE64_FALSE	0
#define	BASE64_TRUE		1

#define	BASE64Intn		int
#define	BASE64Status	int
#define	BASE64Int32		long

#define	BASE64_SUCCESS	1
#define	BASE64_FAILURE	-1
#define	BASE64_NOT_REACHED(x)

static unsigned char *base = (unsigned char *)
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-";

static void	base64_encode3to4(const unsigned char *src, unsigned char *dest)
{
	BASE64Uint32 b32 = (BASE64Uint32)0;
	BASE64Intn i, j = 18;

	for (i = 0; i < 3; i++) {
		b32 <<= 8;
		b32 |= (BASE64Uint32)src[i];
	}

	for (i = 0; i < 4; i++) {
		dest[i] = base[(BASE64Uint32)((b32>>j) & 0x3F)];
		j -= 6;
	}
}

static void	base64_encode2to4(const unsigned char *src, unsigned char *dest)
{
	dest[0] = base[(BASE64Uint32)((src[0]>>2) & 0x3F)];
	dest[1] = base[(BASE64Uint32)(((src[0] & 0x03) << 4) |
		((src[1] >> 4) & 0x0F))];
	dest[2] = base[(BASE64Uint32)((src[1] & 0x0F) << 2)];
	dest[3] = (unsigned char)WEBSAFE_BASE64EOS;
}

static void	base64_encode1to4(const unsigned char *src, unsigned char *dest)
{
	dest[0] = base[(BASE64Uint32)((src[0]>>2) & 0x3F)];
	dest[1] = base[(BASE64Uint32)((src[0] & 0x03) << 4)];
	dest[2] = (unsigned char)WEBSAFE_BASE64EOS;
	dest[3] = (unsigned char)WEBSAFE_BASE64EOS;
}

static void	base64_encodeInternal(const unsigned char *src,
		BASE64Uint32 srclen, unsigned char *dest)
{
	while (srclen >= 3) {
		base64_encode3to4(src, dest);
		src += 3;
		dest += 4;
		srclen -= 3;
	}

	switch (srclen) {
		case 2: base64_encode2to4(src, dest); break;
		case 1: base64_encode1to4(src, dest); break;
		case 0: break;
		default: BASE64_NOT_REACHED("coding error");
	}
}

static BASE64Int32	base64_code2value(unsigned char c)
{
	if ((c >= (unsigned char)'A') && (c <= (unsigned char)'Z'))
		return (BASE64Int32)(c - (unsigned char)'A');
	else if ((c >= (unsigned char)'a') && (c <= (unsigned char)'z'))
		return ((BASE64Int32)(c - (unsigned char)'a') +26);
	else if ((c >= (unsigned char)'0') && (c <= (unsigned char)'9'))
		return ((BASE64Int32)(c - (unsigned char)'0') +52);
	else if ((unsigned char)base[62] == c)
		return (BASE64Int32)62;
	else if ((unsigned char)base[63] == c)
		return (BASE64Int32)63;
	else
		return -1;
}

static BASE64Status	base64_decode4to3(const unsigned char *src,
			unsigned char *dest)
{
	BASE64Uint32 b32 = (BASE64Uint32)0;
	BASE64Int32 bits;
	BASE64Intn i;

	for (i = 0; i < 4; i++) {
		bits = base64_code2value(src[i]);
		if (bits < 0)
			return BASE64_FAILURE;

		b32 <<= 6;
		b32 |= bits;
	}

	dest[0] = (unsigned char)((b32 >> 16) & 0xFF);
	dest[1] = (unsigned char)((b32 >>  8) & 0xFF);
	dest[2] = (unsigned char)((b32     ) & 0xFF);

	return BASE64_SUCCESS;
}

static BASE64Status	base64_decode3to2(const unsigned char *src,
			unsigned char *dest)
{
	BASE64Uint32 b32 = (BASE64Uint32)0;
	BASE64Int32 bits;
	BASE64Uint32 ubits;

	bits = base64_code2value(src[0]);
	if (bits < 0)
		return BASE64_FAILURE;

	b32 = (BASE64Uint32)bits;
	b32 <<= 6;

	bits = base64_code2value(src[1]);
	if (bits < 0)
		return BASE64_FAILURE;

	b32 |= (BASE64Uint32)bits;
	b32 <<= 4;

	bits = base64_code2value(src[2]);
	if (bits < 0)
		return BASE64_FAILURE;

	ubits = (BASE64Uint32)bits;
	b32 |= (ubits >> 2);

	dest[0] = (unsigned char)((b32 >> 8) & 0xFF);
	dest[1] = (unsigned char)((b32    ) & 0xFF);

	return BASE64_SUCCESS;
}

static BASE64Status	base64_decode2to1(const unsigned char *src,
			unsigned char *dest)
{
	BASE64Uint32 b32;
	BASE64Uint32 ubits;
	BASE64Int32 bits;

	bits = base64_code2value(src[0]);
	if (bits < 0)
		return BASE64_FAILURE;

	ubits = (BASE64Uint32)bits;
	b32 = (ubits << 2);

	bits = base64_code2value(src[1]);
	if (bits < 0)
		return BASE64_FAILURE;

	ubits = (BASE64Uint32)bits;
	b32 |= (ubits >> 4);

	dest[0] = (unsigned char)b32;

	return BASE64_SUCCESS;
}

static BASE64Status	base64_decodeInternal(const unsigned char *src,
		BASE64Uint32 srclen, unsigned char *dest)
{
	BASE64Status rv = BASE64_FAILURE;

	while (srclen >= 4) {
		rv = base64_decode4to3(src, dest);
		if (BASE64_SUCCESS != rv)
			return BASE64_FAILURE;

		src += 4;
		dest += 3;
		srclen -= 4;
	}

	switch (srclen) {
		case 3:
		rv = base64_decode3to2(src, dest);
		break;
		case 2:
		rv = base64_decode2to1(src, dest);
		break;
		case 1:
		rv = BASE64_FAILURE;
		break;
		case 0:
		break;
		default:
		BASE64_NOT_REACHED("coding error");
	}

	return rv;
}

char	*base64_encode(const char *src, BASE64Uint32 srclen, char *dest)
{
	if (0 == srclen && src && *src)
		srclen = strlen(src);

	if (NULL == dest) {
		BASE64Uint32 destlen = ((srclen + 2) / 3) * 4;
		dest = (char *)malloc(destlen + 1);
		memset(dest, 0, destlen + 1);
	}

	base64_encodeInternal((const unsigned char *)src, srclen, (unsigned char *)dest);
	return dest;
}

char	*base64_decode(const char *src, BASE64Uint32 srclen)
{
	char	*dest;

	if (0 == srclen && src && *src)
		srclen = strlen(src);

	if (srclen > 2 && 0 == (srclen & 3))
		if ((char)WEBSAFE_BASE64EOS == src[srclen - 1]) {
			if ((char)WEBSAFE_BASE64EOS == src[srclen - 2])
				srclen -= 2;
			else
				srclen -= 1;
		}

	{
		BASE64Uint32 destlen = ((srclen * 3) / 4);
		dest = (char *)malloc(destlen + 1);
		memset(dest, 0, destlen + 1);
	}

	if (BASE64_SUCCESS != base64_decodeInternal((const unsigned char *)src, srclen, (unsigned char *)dest)) {
		free(dest);
		dest = strdup("");
	}

	return dest;
}

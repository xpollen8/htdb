#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

void	log_page(char *session_id, char *domain_id, char *user_id,
		char *browser_id, char *resource, char *value)
{
	if (getenv("DOCUMENT_ROOT") != NULL &&
		htdb_checkval("confLogPageTable") &&
		!htdb_equal("browser->isrobot", "T")) {
			dbInsert("INSERT INTO %s set "
				"%s=%s, "
				"%s=%s, "
				"%s=%s, "
				"%s=%s, "
				"%s=%s, "
				"%s='%s', "
				"%s='%s' ",
					htdb_getval("confLogPageTable"),
					"session_id", session_id,
					"domain_id", domain_id,
					"user_id", user_id,
					"affiliate_id", 0,
					"browser_id", browser_id,
					"resource", resource,
					"value", value);

			if (htdb_checkval("cookie->wasSet") &&
				htdb_checkval("confLogEntranceTable"))
				/*
					and right here, we're going to drop some
					information into the `log.entrance' table
					so that we can track:
						1) how people found us
						2) what page they first landed on
				 */
				dbInsert("insert into %s set "
					"%s=%s, "
					"%s=%s, "
					"%s=%s, "
					"%s=%s, "
					"%s=%ld, "
					"%s='%s', "
					"%s='%s', "
					"%s=%s",
						htdb_getval("confLogEntranceTable"),
						"domain_id", domain_id,
						"session_id", session_id,
						"browser_id", browser_id,
						"affiliate_id", 0,
						"referer_id", htdb_getint("referer->referer_id"),
						"resource", resource,
						"value", value,
						"ts", "NULL");
	}
}

/*
	`istart' is a static, global structure!
 */
void	log_ttg(char *thing)
{
	if (htdb_checkval("confLogTimeToGenerateTable")) {
		struct timeval  istop;
		char	*hostname = htdb_uname("n");
		int		len = strlen(hostname);

		while (len > 0 && isspace(hostname[len - 1]))
			hostname[--len] = '\0';

		gettimeofday(&istop, (void *)NULL);
		dbInsert("insert into %s set "
			"domain_id = %ld, "
			"hostname = '%s', "
			"resource = '%s', "
			"begts = %ld.%06ld, "
			"endts = %ld.%06ld",
			htdb_getval("confLogTimeToGenerateTable"),
			htdb_getint("_domain_id"),
			hostname,
			thing,
			istart.tv_sec, istart.tv_usec,
			istop.tv_sec, istop.tv_usec);

		free(hostname);
	}
}

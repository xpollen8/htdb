#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

void htdb_socketClose(int sd)
{
    if (sd > 0) close(sd);
    alarm(0);
}

bool_t	htdb_socketSend(int sd, char *buf, size_t bytes)
{
	return (bytes == write(sd, buf, bytes)) ? BOOL_TRUE : BOOL_FALSE;
}

char	*htdb_socketReceive(int sd)
{
	spaceData   b;
	data_init(&b);

	{
		size_t	bytes = 0;
		char	buf[BUF_LIMIT_MEDIUM + 1];
		do {
			if ((bytes = read(sd, (void *)buf, BUF_LIMIT_MEDIUM)) > 0) {
				buf[bytes] = '\0';
				thing_append(&b, buf, bytes, BUF_LIMIT_BIG);
			}
		} while (bytes > 0);
	}
	return (b.value);
}

int	htdb_socketOpen(char *prefix, char *hostname, unsigned port, int timeout_seconds)
{
	int		sd = 0,
			rc;
	struct sockaddr_in servAddr;
	char	*username = (char *)NULL,
			*password = (char *)NULL;

	{
		struct hostent *h = gethostbyname(hostname);
		if (NULL == h) {
			htdb_setptrvarg(prefix, "error", "htdb_socketOpen() bad hostname '%s'", hostname);
			htdb_socketClose(sd);
			return 0;
		}

		servAddr.sin_family = h->h_addrtype;
		memcpy((char *)&servAddr.sin_addr.s_addr, h->h_addr_list[0], h->h_length);
	}
	servAddr.sin_port = htons(port);

	/*
		create socket
	 */
	sd = socket(AF_INET, SOCK_STREAM, 0);
	if (sd < 0) {
		htdb_setptrvarg(prefix, "error", "htdb_socketOpen() can't open socket to '%s'", hostname);
		htdb_socketClose(sd);
		return 0;
	}

	/*
		bind any port number
	 */
	{
		struct sockaddr_in	localAddr;

		localAddr.sin_family = AF_INET;
		localAddr.sin_addr.s_addr = htonl(INADDR_ANY);
		localAddr.sin_port = htons(0);

		rc = bind(sd, (struct sockaddr *)&localAddr, sizeof(localAddr));
	}
	if (rc < 0) {
		htdb_setptrvarg(prefix, "error", "htdb_socketOpen() can't bind '%s:%u'", hostname, port);
		htdb_socketClose(sd);
		return 0;
	}

	if (fork()) {
		/*
			connection timed-out
		 */
		int jnk;
		wait(&jnk);
		htdb_socketClose(sd);
		if (jnk > 0) {
			htdb_setptrvarg(prefix, "error", "htdb_socketOpen() connection to '%s:%u' timed-out after %d seconds",
				hostname, port, timeout_seconds);
			return 0;
		} else {
			exit(0);
		}
	} else {
		/*
			set our connection alarm
		 */
		signal(SIGALRM, NULL);
		alarm(timeout_seconds);
		/*
			connect to server
		 */
		rc = connect(sd, (struct sockaddr *)&servAddr, sizeof(servAddr));
		if (rc < 0) {
			htdb_setptrvarg(prefix, "error", "htdb_socketOpen() connection to '%s:%u' failed", hostname, port);
			htdb_socketClose(sd);
			return 0;
		}
	}
	return sd;
}

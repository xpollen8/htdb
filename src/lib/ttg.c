#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

#define	TTG_BUCKET_LIMIT	200
#define	TTG_MAX_NORMALIZE	500
#define	TTG_DEFAULT_PRECISION	100
#define	TTG_DEFAULT_DRAWBARS	100
#define	TTG_DEFAULT_LIMIT	1
int	ttgTimes[TTG_BUCKET_LIMIT],
	ttgNumTimes = 0,
	ttgMaxTime = 0,
	ttgMaxHits = 0,
	ttgPrecision = TTG_DEFAULT_PRECISION;

static char	*ttgMakeBar(int num, int normalize)
{
	int	bars = ((float)num / (float)ttgMaxHits) *
		(normalize >= TTG_MAX_NORMALIZE ? TTG_MAX_NORMALIZE : normalize),
		i;
	static char	out[TTG_MAX_NORMALIZE];

	strcpy(out, "");
	for (i = 0 ; i < bars ; i++)
		out[i] = '*';
	out[i] = '\0';
	return out;
}

#if	defined(USE_DATABASE)
static void	ttgCallback(int num_fields, int num_rows, char **fields, _dbResultRow values)
{
	int	ttg = (int)(atof(values[0]) * ttgPrecision);
	if (ttg >= TTG_BUCKET_LIMIT)
		return;
	if (ttg > ttgMaxTime)
		ttgMaxTime = ttg;
	if (++ttgTimes[ttg] > ttgMaxHits)
		ttgMaxHits = ttgTimes[ttg];
	ttgNumTimes++;
}
#endif

static void	doTTG(spaceData *b, char *res, int_t domain_id, int precision, int bars, int limit)
{
#if	defined(USE_DATABASE)
	int	i;

	for (i = 0 ; i < TTG_BUCKET_LIMIT ; i++)
		ttgTimes[i] = 0;

	ttgNumTimes = ttgMaxTime = ttgMaxHits = 0;
	ttgPrecision = precision;

	dbCallbackByRow(ttgCallback,
		"select (endts - begts) as ttg from ttg where resource='%s' and domain_id=%ld order by ttg", res, domain_id);

	thing_append(b, "<table border=0 cellpadding=0 cellspacing=1 width=\"100%%\">\n", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
	for (i = 0 ; i <= ttgMaxTime ; i++) {
		if (ttgTimes[i] > limit) {
			char	jnk[BUF_LIMIT_BIG + 1];
			sprintf(jnk, "<tr>\t<td>\t%5.3f\t</td>\t<td>\t(%4d)\t</td>\t<td>\t%-*s\t</td>\t</tr>\n",
				i / (float)ttgPrecision,
				ttgTimes[i],
				bars,
				ttgMakeBar(ttgTimes[i], bars));
			thing_append(b, jnk, FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
		}
	}
	thing_append(b, "</table>\n", FUNC_CALCULATES_SIZE, BUF_LIMIT_BIG);
#endif
}

char *ttgDetail(char *res, int_t domain_id, int precision, int bars, int limit)
{
	spaceData	b;

	data_init(&b);

	if (precision == 0)
		precision = TTG_DEFAULT_PRECISION;
	if (bars == 0)
		bars = TTG_DEFAULT_DRAWBARS;
	if (limit == 0)
		limit = TTG_DEFAULT_LIMIT;
	doTTG(&b, res, domain_id, precision, bars, limit);

	return b.value;
}

char	*htdb_ttg(char *in)
{
	/*
		`istart' is a static, global structure!
	 */
	struct timeval  istop;

	gettimeofday(&istop, (void *)NULL);

	{
		char	beg[BUF_LIMIT_SMALL + 1],
				end[BUF_LIMIT_SMALL + 1],
				ret[BUF_LIMIT_SMALL + 1];

		sprintf(beg, "%ld.%06ld", istart.tv_sec, (long)istart.tv_usec);
		sprintf(end, "%ld.%06ld", istop.tv_sec, (long)istop.tv_usec);
		sprintf(ret, "%lf", atof(end) - atof(beg));
		return strdup(ret);
	}
}

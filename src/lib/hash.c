#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#include	"libhtdb.h"

/*
	htdb_hash()
		The published hash algorithm used in the UNIX ELF format for object files.
 */
unsigned long	htdb_hash(char *key, int size)
{
	unsigned long	h = 0, g;
	while (key && *key) {
		h = ( h << 4 ) + tolower(*key++);
		if ((g = (h & 0xF0000000)))
			h ^= g >> 24;
		h &= ~g;
	}
	return h % size;
}

/*
	htdb_hash_create()				
		Allocate and initialize a hash table with at most HASHTABSIZE_MAX elements.	
 */
Hashtable	htdb_hash_create(void (*destructor)(void *), int size)
{
	Hashtable	newtable = (Hashtable)malloc(sizeof(struct hashtable_t));

	memset(newtable, 0L, (sizeof(struct hashtable_t)));

	if (size > HASHTABSIZE_MAX)
		size = HASHTABSIZE_MAX;

	newtable->size = size;
	newtable->numelements = newtable->maxchainlen = 0;
	newtable->destructor = destructor;

	return newtable;
}

static Bucket		htdb_hash_bucket_duplicate(Bucket b)
{
	Bucket new; 
	new = (Bucket) malloc(sizeof(struct bucket_t));
	new->key = strdup(b->key);

	new->value = (void *)malloc(b->size + 1);
	memcpy(new->value, b->value, b->size + 1);

	new->size = b->size;
	new->limit = b->limit;
	new->next = b->next;
	return new;
}
		
/*
	htdb_hash_destroy()				
		Unallocate storage used by the bucket
 */
void	htdb_hash_destroy(Hashtable ht)
{
	int	idx;

	if (ht) {
		for (idx = 0; idx < ht->size; idx++) {
			if (ht->ht_table[idx]) {
				Bucket bkt = ht->ht_table[idx];
				while (bkt) {
					/*
						free the buckets
					 */
					Bucket freeme = bkt;
					bkt = bkt->next;
					if (freeme) {
						/*
							and the contents
						 */
						if (freeme->key)
							free(freeme->key);
						/*
							call the data destructor.
						 */
						if (ht->destructor && freeme->value)
							ht->destructor(freeme->value);
						free(freeme);
					}
				}
			}
		}
		/*
			then the table
		 */
		free(ht);
		ht = NULL;
	}
}

/*
	htdb_hash_duplicate()				
		duplicate a hash table.  Will only function properly with 
		freeable (read string-based) hash tables. 
		
*/
void	htdb_hash_duplicate(Hashtable from, Hashtable to)
{
	int	idx;

	if (!from || !to || !(from->destructor == htdb_destroyStringTuple))
		return;

	to->numelements = from->numelements;
	to->maxchainlen = from->maxchainlen;

	for (idx = 0; idx < from->size; idx++) {
		if (from->ht_table[idx]) {
			Bucket bkt = from->ht_table[idx];
			Bucket last_bkt = (Bucket) NULL;
			to->chainlen[idx] = from->chainlen[idx];
			while (bkt) {
				Bucket new_bkt = htdb_hash_bucket_duplicate(bkt);
				if ( last_bkt ) 
					last_bkt->next = new_bkt;
				else /* first entry on the chain. */
					to->ht_table[idx] = new_bkt;
			
				last_bkt = new_bkt;
				bkt = bkt->next;
			}
		}
	}				
}

/*
	htdb_hash_lookup_hashed()				
		used for when you already have a hash value
		Returns	pointer to Bucket or NULL

 */
static Bucket	htdb_hash_lookup_hashed(Hashtable ht, char *key, long int hash)
{
	Bucket		tmpbkt = ht->ht_table[hash];

	while (tmpbkt) {
		/*
			while there are bkts
		 */
		if (strcasecmp(tmpbkt->key, key) == 0) {
			/*
				Found a match!
			 */
			return tmpbkt;
		}
		/*
			else hump along
		 */
		tmpbkt = tmpbkt->next;
	}

	return NULL;
}


/*
	htdb_hash_lookup()				
		routine used by lookup and insertion functions.	
		Returns	pointer to Bucket or NULL
 */
Bucket	htdb_hash_lookup(Hashtable ht, char *key)
{
	unsigned long hash = htdb_hash(key, ht->size);

	return htdb_hash_lookup_hashed(ht, key, hash);
}

/*
	htdb_hash_delete()				
		routine used by delete functions.	
 */
bool_t	htdb_hash_delete(Hashtable ht, char *key)
{
	unsigned long hash = htdb_hash(key, ht->size);
	Bucket	bkt = ht->ht_table[hash],
			prev = bkt;

	while (bkt) {
		/*
			while there are bkts
		 */
		if (strcasecmp(bkt->key, key) == 0) {
			/*
				found a match
			 */
			Bucket	next = bkt->next;
			/*
				the first bucket slot holds the chainlength
			 */
			(ht->chainlen[hash])--;
			/*
				free the key
			 */
			if (bkt->key)
				free(bkt->key);
			/*
				call the data destructor.
			 */
			if (ht->destructor && bkt->value)
				ht->destructor(bkt->value);

			/*
				link around the removed entry
			 */
			if (bkt == ht->ht_table[hash])
				/*
					@ head of chain - the new head is our next
				 */
				ht->ht_table[hash] = next;
			else
				/*
					make the prev's->next point past us
				 */
				prev->next = bkt->next;

			/*
				free your bucket
			 */
			free(bkt);
			return BOOL_TRUE;
		}
		/*
			else hump along
		 */
		prev = bkt;
		if (bkt && bkt->next)
			bkt = bkt->next;
	}
	return BOOL_FALSE;
}

/*
	htdb_hash_define()
		return 0 if key was added to the table,			
		-1 if key was not added. (new() failed)		
 */
bool_t	htdb_hash_define(Hashtable ht, char *key, void *value, int is_string)
{
	Bucket bkt;
	unsigned long	hash;
	char	*str = (char *)value,
		*hold = &str[0];
	int	len;

	if (is_string == 1) {
		if (str && *str)
			len =  strlen(str);
		else
			len = 0;
	} else
		len = abs(is_string);

#ifndef	USE_DELAYED_LOOKUP
	/*
		clean strings: eat leading and trailing whitespace
	 */
	if (is_string == 1) {
		while (len > 0 && str && *str && isspace((int)*str)) {
			str++;
			len--;
		}

		if (str && *str && len > 0)
			while (len > 0) {
				if (isspace((int)str[len - 1]))
					str[--len] = '\0';
				else
					break;
			}
	}
#endif

	/*
		if `is_string' is set, then we know that we
		are dealing with strings and therefore it is
		safe to use strlen() on our incoming data.
		data stored as strings will be allowed to use
		grow-able buffers for fast(er) append operations.
	 */
	hash = htdb_hash(key, ht->size);
	if ((bkt = htdb_hash_lookup_hashed(ht, key, hash))) {
		/*
			replace existing entry
		 */
		if (bkt->value && is_string == 1) {
			free(bkt->value);
			bkt->value = NULL;
		}

	} else {
		/*
			new key for this table
		 */
		bkt = (Bucket)malloc(sizeof(struct bucket_t));

		if (NULL == ht->ht_table[hash]) {
			/*
				no chain yet at this hash cell
			 */
			bkt->next = NULL;
			ht->chainlen[hash] = 0;
		} else {
			/*
				front of existing chain; save old link
			 */
			bkt->next = ht->ht_table[hash];
		}

		if (++(ht->chainlen[hash]) > ht->maxchainlen)
			ht->maxchainlen = ht->chainlen[hash];

		ht->ht_table[hash] = bkt;
		ht->numelements++;

		bkt->key = strdup(key);

	}

	/*
		store the value
	 */
	if (is_string == 1) {
		bkt->value = (void *)strdup(str);
		/*
			free the passed-in value
		 */
		free(hold);
	} else {
		bkt->value = (void *)value;
	}

	bkt->size = bkt->limit = len;

	if (is_string < 0) {
		char	binsize[BUF_LIMIT_SMALL + 1],
				binvalue[BUF_LIMIT_SMALL + 1];
		snprintf(binsize, BUF_LIMIT_SMALL, "%s.size", key);
		binsize[BUF_LIMIT_SMALL] = '\0';
		snprintf(binvalue, BUF_LIMIT_SMALL, "%d", len);
		binvalue[BUF_LIMIT_SMALL] = '\0';
		htdb_hash_define(ht, binsize, strdup(binvalue), 1);
		{
			/*
				this'll add a "key->bytes" value
				to the environment
			 */
			char	*lookup = htdb_getptrdyname(key, "bytes");
			htdb_hash_define(ht, lookup, strdup(binvalue), 1);
			free(lookup);
		}
	}

	return BOOL_TRUE;
}

int	htdb_hash_size(Hashtable ht, char *key)
{
	Bucket	b = htdb_hash_lookup(ht, key);
	if (b)
		return b->size;
	return 0;
}

/*
	htdb_hash_value()				
		return the value associated with the key if key is
		in the table, NULL if it is not in the table				
 */
void	*htdb_hash_value(Hashtable ht, char *key)
{
	if (ht) {
		Bucket	bkt = htdb_hash_lookup(ht, key);
		if (bkt)
			return bkt->value;
	}
	return NULL;
}

void	htdb_hash_init(Hashtable ht)
{
	ht->bkt = NULL;
}

void	htdb_hash_bucket_init(Bucket b)
{
	b->value = (void *)strdup("");
	b->size = b->limit = 0;
}

void	htdb_hash_bucket_destroy(Bucket b)
{
	free(b->value);
	b->value = (char *)NULL;
	b->size = b->limit = 0;
}

Bucket	htdb_hash_traverse(Hashtable ht)
{
	if (ht->bkt == NULL)
		ht->idx = 0;
	else
		ht->bkt = ht->bkt->next;

	while (ht->bkt == NULL) {
		if (ht->idx >= ht->size)
			return NULL;
		ht->bkt = ht->ht_table[ht->idx++];
	}
	return ht->bkt;
}

#ident	"$Header$"

/*
	Copyright (c) 1994-2014 David Whittemore
	under The MIT License - see LICENSE.txt
*/
#ifndef	H_HTDB
#define	H_HTDB

/*
	the autoconf-built config file
 */
#include 	"htdbconfig.h"

/*
	standard stuff
 */
#include	<stdio.h>
#include	<stdarg.h>
#include	<signal.h>
#include	<ctype.h>
#include	<math.h>
#include	<errno.h>
//#define _XOPEN_SOURCE /* glibc2 needs this (so sez the `strptime' man page) */

#include	<time.h>

/*
	system-conditional includes
 */
#ifdef	HAVE_STDLIB_H
	#include	<stdlib.h>
#endif
#ifdef	HAVE_FCNTL_H
	#include	<fcntl.h>
#endif
#ifdef	HAVE_LIMITS_H
	#include	<limits.h>
#endif
#ifdef HAVE_MALLOC_H
	#include	<malloc.h>
#endif
#ifdef	HAVE_VALUES_H
	#include	<values.h>
#endif
#ifdef	HAVE_STRING_H
	#include	<string.h>
#endif
#ifdef	HAVE_STRINGS_H
	#include	<strings.h>
#endif
#ifdef	HAVE_SYS_STAT_H
	#include	<sys/stat.h>
#endif
#ifdef	HAVE_SYS_TYPES_H
	#include	<sys/types.h>
#endif
#ifdef	HAVE_SYS_SOCKET_H
	#include	<sys/socket.h>
#endif
#ifdef	HAVE_ARPA_INET_H
	#include	<arpa/inet.h>
#endif
#ifdef	HAVE_SYS_TIME_H
	#include	<sys/time.h>
#endif
#ifdef	HAVE_SYSLOG_H
	#include	<syslog.h>
#endif
#ifdef	HAVE_UNISTD_H
	#include	<unistd.h>
#endif
#ifdef	HAVE_DIRENT_H
	#include	<dirent.h>
#endif
#ifdef	HAVE_DLFCN_H
	#include	<dlfcn.h>
#endif
#ifdef HAVE_CRYPT_H
	#include	<crypt.h>
#endif
#ifdef	HAVE_NETINET_IN_H
	#include	<netinet/in.h>
#endif
#ifdef	HAVE_NETDB_H
	#include	<netdb.h> 
#endif
#ifdef	HAVE_ARGZ_H
	#include	<argz.h> 
#endif
#ifdef	HAVE_INTTYPES_H
	#include	<inttypes.h> 
#endif
#ifdef	HAVE_MEMORY_H
	#include	<memory.h> 
#endif
#ifdef	HAVE_NDIR_H
	#include	<ndir.h> 
#endif
#ifdef	HAVE_STDINT_H
	#include	<stdint.h> 
#endif
#ifdef	HAVE_SYS_DIR_H
	#include	<sys/dir.h> 
#endif
#ifdef	HAVE_SYS_NDIR_H
	#include	<sys/ndir.h> 
#endif

/*
	if fastcgi both specified and being used
 */
#if	(defined(HAVE_LIBFCGI) && defined(USE_FASTCGI))
	#include	<fcgi_stdio.h>
#endif
#if	(defined(HAVE_LIBCURL) && defined(USE_LIBCURL))
	#include	<curl/curl.h>
	#include	<curl/easy.h>
#endif
#if	(defined(HAVE_LIBMING) && defined(USE_LIBMING))
	#include	<ming.h>
#endif
#if	(defined(HAVE_LIBMCRYPT) && defined(USE_LIBMCRYPT))
	#include	<mcrypt.h>
#endif

/*
	database support can come in a couple different flavors
 */
#if (defined(USE_MYSQL) && defined(HAVE_LIBMYSQLCLIENT))
	#define	USE_DATABASE
#elif	(defined(USE_DBI) && defined(HAVE_LIBDBI))
	#define	USE_DATABASE
#else
	#undef	USE_DATABASE
#endif

#define	WGET_DEFAULT_TIMEOUT	10

/*
	common application constants
 */
#define	LEN_GOKEY			10		/* how many characters are in database gokeys? */
#define	FCGI_KEEPALIVE		1000	/* how many pages does fastcgi serve before exiting? */
#define	BUF_LIMIT_TINY		1024	/* resource defs are at least this big */
#define	BUF_LIMIT_SMALL		4096	/* small general-purpose storage size */
#define	BUF_LIMIT_MEDIUM	8192	/* big general-purpose storage size */
#define	BUF_LIMIT_BIG		16384	/* resource buffers are incremented by this much */
#define	BUF_LIMIT_MAX		1638400	/* longest expanded-string allowable in varg */
#define	ARE_YOU_INSANE		"server running as root; function disabled"

#define	HASHTABSIZE_SMALL	71		/* hash.c constants - the 20th prime */
#define	HASHTABSIZE_MEDIUM	409     /* hash.c constants - the 80th prime */
#define	HASHTABSIZE_BIG		3571    /* hash.c constants - the 500th prime */
#define	HASHTABSIZE_MAX		3571

#define BASE64Uint32    unsigned long

typedef	long	int_t;
typedef	long long	long_t;
typedef enum { BOOL_FALSE = 0, BOOL_TRUE = 1 }	bool_t;

typedef struct tdata *Pdata;
typedef struct tdata {					/* ternary bucket type */
	char	*key;
	void	*value;
	int		size,
			limit;
}	Tdata;

typedef struct tnode *Pnode;
typedef struct tnode {					/* ternary type */
	char splitchar;
	Pnode lokid, eqkid, hikid;
} Tnode;

typedef struct bucket_t {				/* hash bucket type */
	char	*key;						/* the name of the thing */
	void	*value;						/* the thing */
	int		size,						/* # of bytes taken up in the buffer by real data */
			limit;						/* buffer size */
	struct bucket_t *next;
}	*Bucket;

typedef struct hashtable_t {			/* hashtable type */
	int		idx,						/* used for traversal operations */
			size,						/* how many bucket cells? */
			numelements,				/* how many values have been stored? */
			maxchainlen,				/* what's the longest bucket chain? */
			chainlen[HASHTABSIZE_MAX];	/* how many buckets per chain? */
	Bucket	bkt,						/* used for traversal operations */
			ht_table[HASHTABSIZE_MAX];	/* the chained buckets themselves */
	void	(*destructor)(void *);		/* how to destroy bucket elements */
}	*Hashtable;

struct llitem_t {						/* linked-list type */
	void	*value;
	struct llitem_t	*next;
};
typedef struct llitem_t *LLItem;

struct linkedlist_t {
	LLItem	     first;
	LLItem	     item;
};

typedef struct linkedlist_t *LinkedList;

typedef struct	{						/* for storing application meta-info */
	char	*name,
			*version,
			*description,
			*args;
	void	(*func)(void);
	int	fcgi_keepalive;
	char	*cmd_args;
}	appinfo_t;

/***********************************************************************
	PUBLIC PROTOTYPES
 ***********************************************************************/

extern void	htdb_httpHeader(char *fmt, ...);
extern void	htdb_httpHeaderEnd(void);
extern void	htdb_httpExpires(time_t the_past);
extern void	htdb_doHeader(char *content_type);
extern void	htdb_doFooter(char *content_type);
extern void	htdb_httpRedirect(char *fmt, ...);

/*
	uncgi.c
 */
extern void	uncgi_stuffenv(char *);
extern void	uncgi_scanquery(char *);
extern void	uncgi_process_name_value(char *, char *);
extern void	uncgi(void);

/*
	web content types
 */
#define	CONTENT_TYPE_TEXT_HTML			"Content-type: text/html"
#define	CONTENT_TYPE_TEXT_XML			"Content-type: text/xml"
#define	CONTENT_TYPE_TEXT_RSS			"Content-type: text/xml"
#define	CONTENT_TYPE_TEXT_PLAIN			"Content-type: text/plain"
#define	CONTENT_TYPE_IMAGE_GIF			"Content-type: image/gif"
#define	CONTENT_TYPE_IMAGE_JPEG			"Content-type: image/jpeg"
#define	CONTENT_TYPE_X_PN_REALAUDIO		"Content-type: audio/x-pn-realaudio"
#define	CONTENT_TYPE_AUDIO_MPEG			"Content-type: audio/mpeg"
#define	CONTENT_TYPE_APPLICATION_SMIL	"Content-type: application/smil"
#define	CONTENT_TYPE_REALTEXT			"Content-type: text/vnd.rn-realtext"
#define	CONTENT_TYPE_NON_CGI			"me? i'm just a program, me"
#define	CONTENT_TYPE_STATUS_204			"Status: 204\n"
#define	CONTENT_TYPE_EOH				"\n"

#define	FUNC_CALCULATES_SIZE		-1

/*
	handy equivalants
 */
#define	htdb_include			htdb_read
#define	htdb_print				htdb_parse

#ifdef	USE_TERNARY
	/*
		NOT YET COMPLETE/ EXPERIMENTAL
	 */
	/*
		using ternary.c and ternary trees for general-purpose storage
	 */
	typedef	Pnode	spaceName;
	typedef	Pdata	spacePData;
	typedef	Tdata	spaceData;

	/*
		ternary.c
	 */
	extern Pdata	htdb_ternary_newvalue(char *key, void *value, int sz);
	extern Pdata	htdb_ternary_search(Pnode p, char *s);
	extern void	htdb_ternary_insert(Pnode *p, char *s, Pdata val);
	extern int	htdb_ternary_size(Pnode p, char *key);
	extern Pdata	htdb_ternary_node(Pnode p, char *key);
	extern spacePData htdb_ternary_traverse(Pnode p);
	extern void htdb_ternary_dupe(Pnode p, Pnode pp);
	extern void	*htdb_ternary_value(Pnode p, char *s);
	extern void htdb_ternary_destroy(Pnode p);

	#define	htdb_ternary_define(r, key, value, sz) \
		htdb_ternary_insert(&(r), (key), htdb_ternary_newvalue((key), (void *)(value), (sz)))

	#define	space_init(h, s, d)			if (!(h)) (h) = htdb_ternary_create((d), (s))
	#define	space_initwalk(h)
	#define	space_walk(h)				htdb_ternary_traverse(h)
	#define	space_destroy(h)			htdb_ternary_destroy(h), (h) = NULL
	#define	space_lookup(h, k)			htdb_ternary_search((h), (k))
	#define	space_value(h, k)			htdb_ternary_value((h), (k))
	#define	space_size(h, a)			htdb_ternary_size((h), (a))
	#define	space_store(h, k, v, s)		htdb_ternary_define((h), (k), (v), (s))
	#define	space_duplicate(a, b)		htdb_ternary_dupe((a), (b))
	#define	data_init(d)
	#define	data_destroy(d)
	#define	space_undefine(h, k)

#else

	/*
		using hash.c and hash tables for general-purpose storage
	 */
	typedef	Hashtable			spaceName;
	typedef	Bucket				spacePData;
	typedef	struct bucket_t		spaceData;

	/*
		hash.c
	 */
	extern Hashtable	htdb_hash_create(void (*destructor)(void *), int size);
	extern void			htdb_hash_init(Hashtable);
	extern Bucket		htdb_hash_traverse(Hashtable);
	extern void			htdb_hash_destroy(Hashtable);
	extern Bucket		htdb_hash_lookup(Hashtable, char *);
	extern void			*htdb_hash_value(Hashtable, char *);
	extern int			htdb_hash_size(Hashtable, char *);
	extern bool_t		htdb_hash_define(Hashtable ht, char *key, void *value, int is_string);
	extern void			htdb_hash_duplicate(Hashtable, Hashtable);
	extern void			htdb_hash_bucket_init(Bucket);
	extern void			htdb_hash_bucket_destroy(Bucket);
	extern bool_t		htdb_hash_delete(Hashtable ht, char *key);

	/*
		hashtable init, walk and teardown macros
	 */
	#define	space_init(h, s, d)			if (!(h)) (h) = htdb_hash_create((d), (s))
	#define	space_initwalk(h)			htdb_hash_init(h)
	#define	space_walk(h)				htdb_hash_traverse(h)
	#define	space_destroy(h)			htdb_hash_destroy(h), (h) = NULL
	#define	space_lookup(h, k)			htdb_hash_lookup((h), (k))
	#define	space_value(h, k)			htdb_hash_value((h), (k))
	#define	space_size(h, a)			htdb_hash_size((h), (a))
	#define	space_store(h, k, v, s)		htdb_hash_define((h), (k), (void *)(v), (s))
	#define	space_duplicate(a, b)		htdb_hash_duplicate((a), (b))
	#define	data_init(d)				htdb_hash_bucket_init((Bucket)d)
	#define	data_destroy(d)				htdb_hash_bucket_destroy((Bucket)d)
	#define	space_undefine(h, k)		htdb_hash_delete((h), (k))

#endif

/***********************************************************************
	BEGIN EXPERIMENTAL
 */
typedef	struct {
	int	len_gokey,
		fcgi_keepalive,
		buf_limit_tiny,
		buf_limit_small,
		buf_limit_medium,
		buf_limit_big,
		buf_limit_max,
		hashtabsize_medium,
		hashtabsize_big,
		hashtabsize_max;
}	htdb_constants_t;

typedef	struct {
	htdb_constants_t	constants;
	spaceName			staticSpace,
						resSpace,		/* per request storage area */
						tagSpace,		/* tag definitions */
						funcSpace,		/* script functions */
						cfuncSpace,		/* DSO functions */
						dbSpace,		/* database handles */
						fileSpace;		/* file pointers */
}	namespace_t;

/*
	each namespace should be allowed to have their
	own constants (hash sizes, gokey lens, fcgi loops, etc).
	so... the `namespace' storage object will store
	pointers to `namespace_t' objects.
 */
extern spaceName	nameSpace;

#define	name_initspace()		space_init(nameSpace, HASHTABSIZE_BIG, htdb_destroyNameSpace)
#define	name_initwalk()			space_initwalk(nameSpace)
#define	name_walkspace()		space_walk(nameSpace)
#define	name_clearspace()		space_destroy(nameSpace)
#define	name_size(a)			space_size(nameSpace, (a))
#define	name_store(k, v)		space_store(nameSpace, (k), (v), 1)
#define	name_value(k)			(char *)space_value(nameSpace, (k))
#define	name_lookup(k)			space_lookup(nameSpace, (k))
#define	name_undefine(k)		space_undefine(nameSpace, (k))

/*
	END EXPERIMENTAL
 ***********************************************************************/
/*
	GLOBAL storage variables (yeah, i know - at least there aren't many of them)
 */
extern	spaceName	staticSpace,	/* resources read from static.htdb */
					resSpace,		/* per request storage area */
					tagSpace,		/* tag definitions */
					funcSpace,		/* script functions */
					cfuncSpace,		/* DSO functions */
					dbSpace,		/* database handles */
					fileSpace;		/* file pointers */

/*
	macros for manipulating the static resource namespace
 */
#define	static_initspace()		space_init(staticSpace, HASHTABSIZE_BIG, htdb_destroyStringTuple)
#define	static_initwalk()		space_initwalk(staticSpace)
#define	static_walkspace()		space_walk(staticSpace)
#define	static_clearspace()		space_destroy(staticSpace)
#define	static_size(a)			space_size(staticSpace, (a))
#define	static_store(k, v)		space_store(staticSpace, (k), (v), 1)
#define	static_value(k)			(char *)space_value(staticSpace, (k))
#define	static_lookup(k)		space_lookup(staticSpace, (k))
#define	static_undefine(k)		space_undefine(staticSpace, (k))

/*
	macros for manipulating the core resource namespace
 */
#define	htdb_initspace()		space_init(resSpace, HASHTABSIZE_BIG, htdb_destroyStringTuple)
#define	htdb_initwalk()			space_initwalk(resSpace)
#define	htdb_walkspace()		space_walk(resSpace)
#define	htdb_clearspace()		space_destroy(resSpace)
#define	htdb_size(a)			space_size(resSpace, (a))
#define	htdb_store(k, v)		space_store(resSpace, (k), (v), 1)
#define	htdb_value(k)			(char *)space_value(resSpace, (k))
#define	htdb_binary(k, v, s)	space_store(resSpace, (k), (v), (0 - (s)))
#define	htdb_lookup(k)			space_lookup(resSpace, (k))
#define	htdb_undefine(k)		space_undefine(resSpace, (k))

/*
	macros for accessing the (c-function) DSO storage area
 */
#define	cfunc_initspace()		space_init(cfuncSpace, HASHTABSIZE_SMALL, htdb_destroyFunctionPointer)
#define	cfunc_initwalk()		space_initwalk(cfuncSpace)
#define	cfunc_walkspace()		space_walk(cfuncSpace)
#define	cfunc_clearspace()		space_destroy(cfuncSpace)
#define	cfunc_store(k, v)		space_store(cfuncSpace, (k), (v), 0)
#define	cfunc_lookup(k)			space_lookup(cfuncSpace, (k))
#define	cfunc_undefine(k)		space_undefine(cfuncsSpace, (k))

/*
	macros for manipulating the database handles
 */
#define	db_initspace()			space_init(dbSpace, HASHTABSIZE_SMALL, htdb_destroyDatabase)
#define	db_initwalk()			space_initwalk(dbSpace)
#define	db_walkspace()			space_walk(dbSpace)
#define	db_clearspace()			space_destroy(dbSpace)
#define	db_store(k, v)			space_store(dbSpace, (k), (v), 0)
#define	db_lookup(k)			space_lookup(dbSpace, (k))
#define	db_undefine(k)			space_undefine(dbSpace, (k))

/*
	macros for manipulating the script function namespace
 */
#define	sfunc_initspace()		space_init(funcSpace, HASHTABSIZE_SMALL, htdb_destroyScriptFunction)
#define	sfunc_initwalk()		space_initwalk(funcSpace)
#define	sfunc_walkspace()		space_walk(funcSpace)
#define	sfunc_clearspace()		space_destroy(funcSpace)
#define	sfunc_store(k, v)		space_store(funcSpace, (k), (v), 0)
#define	sfunc_lookup(k)			space_lookup(funcSpace, (k))
#define	sfunc_undefine(k)		space_undefine(funcSpace, (k))

/*
	macros for manipulating the user-defined tag namespace
 */
#define	tag_initspace()			space_init(tagSpace, HASHTABSIZE_SMALL, htdb_destroyStringTuple)
#define	tag_initwalk()			space_initwalk(tagSpace)
#define	tag_walkspace()			space_walk(tagSpace)
#define	tag_clearspace()		space_destroy(tagSpace)
#define	tag_store(k, v)			space_store(tagSpace, (k), (v), 0)
#define	tag_lookup(k)			space_lookup(tagSpace, (k))
#define	tag_undefine(k)			space_undefine(tagSpace, (k))

/*
	macros for manipulating the user-defined file namespace
 */
#define	file_initspace()		space_init(fileSpace, HASHTABSIZE_SMALL, htdb_destroyFilePointer)
#define	file_initwalk()			space_initwalk(fileSpace)
#define	file_walkspace()		space_walk(fileSpace)
#define	file_clearspace()		space_destroy(fileSpace)
#define	file_store(k, v)		space_store(fileSpace, (k), (v), 0)
#define	file_lookup(k)			space_lookup(fileSpace, (k))
#define	file_undefine(k)		space_undefine(fileSpace, (k))

/*
	the public interfaces
 */
extern void	htdb_usage(appinfo_t *appinfo);
extern void	htdb_log(char *resource, char *aux_value);
extern bool_t	htdb_read(char *);
extern bool_t	htdb_main(int argc, char **argv, appinfo_t *);
extern void	htdb_page_handler(void);
extern void	htdb_setfunc(char *key, char *arglist, char *bd, bool_t internally_defined);

/*
	storage area tear-down stub functions
 */
extern void	htdb_destroyNameSpace(void *value);
extern void	htdb_destroyStringTuple(void *value);
extern void	htdb_destroyScriptFunction(void *value);
extern void	htdb_destroyFunctionPointer(void *value);
extern void	htdb_destroyDatabase(void *value);
extern void	htdb_destroyFilePointer(void *value);

/*
	accessors.c
 */
extern char	*static_setval(char *key, char *val);
extern int_t	static_setint(char *key, int_t val);
extern long_t	static_setlong(char *key, long_t val);
extern char	*static_getval(char *name);
extern int_t	htdb_setint(char *key, int_t val);
extern long_t	htdb_setlong(char *key, long_t val);
extern char	*htdb_setval(char *key, char *val);
extern void	*htdb_setbin(char *key, void *val, off_t size);
extern char	*htdb_setvarg(char *name, char *fmt, ...);
extern char	*static_setvarg(char *name, char *fmt, ...);
extern float	htdb_setfloat(char *key, float val, int precision);
extern char	*htdb_getval(char *name);
extern char	*htdb_getvargval(char *fmt, ...);
extern int_t	htdb_getvargint(char *fmt, ...);
extern long_t	htdb_getvarglong(char *fmt, ...);
extern void	*htdb_getbin(char *name);
extern char	*htdb_getarrayname(char *name, int index);
extern char	*htdb_setarrayval(char *name, int index, char *fmt, ...);
extern char	*static_setarrayval(char *name, int index, char *fmt, ...);
extern char	*htdb_getarrayval(char *name, int index);
extern int_t	htdb_setarrayint(char *name, int index, int_t value);
extern long_t	htdb_setarraylong(char *name, int index, long_t value);
extern int_t	htdb_getarrayint(char *name, int index);
extern long_t	htdb_getarraylong(char *name, int index);
extern char	*htdb_getobjdyname(char *name, int index, char *field);
extern char	*htdb_getobjname(char *name, int index, char *field);
extern char	*htdb_getobjval(char *name, int index, char *field);
extern int_t	htdb_getobjint(char *name, int index, char *field);
extern long_t	htdb_getobjlong(char *name, int index, char *field);
extern float	htdb_getobjfloat(char *name, int index, char *field);
extern char	*htdb_setobjvarg(char *name, int index, char *field, char *fmt, ...);
extern char	*htdb_setobjval(char *name, int index, char *field, char *setto);
extern char	*static_setobjval(char *name, int index, char *field, char *setto);
extern int_t	htdb_setobjint(char *name, int index, char *field, int_t setto);
extern long_t	htdb_setobjlong(char *name, int index, char *field, long_t setto);
extern float	htdb_setobjfloat(char *name, int index, char *field, float val, int precision);
extern float	htdb_setptrfloat(char *name, char *field, float val, int precision);
/* DEAD CODE extern char	*htdb_cond(char *to, char *from); */
extern void	htdb_loop(char *to, char *from);
extern void	htdb_append(char *to, char *from);
extern void	htdb_appendvarg(char *to, char *fmt, ...);
extern char	*htdb_getptrname(char *name, char *field);
extern char	*htdb_getptrdyname(char *name, char *field);
extern char	*htdb_getptrval(char *name, char *field);
extern int_t	htdb_getptrint(char *name, char *field);
extern long_t	htdb_getptrlong(char *name, char *field);
extern float	htdb_getptrfloat(char *name, char *field);
extern int_t	htdb_setptrint(char *name, char *field, int_t setto);
extern long_t	htdb_setptrlong(char *name, char *field, long_t setto);
extern char	*htdb_setptrval(char *name, char *field, char *setto);
extern char	*htdb_setptrvarg(char *name, char *field, char *fmt, ...);

#define	htdb_getfloat(a)	atof(htdb_getval(a))
#define	htdb_getint(a)		atol(htdb_getval(a))
#define	htdb_getlong(a)		atoll(htdb_getval(a))
#define	htdb_getnum(a)		htdb_getint(a)
#define	htdb_checkval(a)	(*htdb_getval(a) != '\0')
#define	htdb_defined(a)		htdb_checkval(a)
#define	static_checkval(a)	(*static_getval(a) != '\0')
#define	htdb_equal(a, b)	(((b) && *(b) && strcasecmp(htdb_getval(a), (b)) == 0) ? 1 : 0)
#define	static_equal(a, b)	(((b) && *(b) && strcasecmp(static_getval(a), (b)) == 0) ? 1 : 0)
#define	htdb_objequal(a, b, c, d)	(((d) && *(d) && strcasecmp(htdb_getobjval(a,b,c), (d)) == 0) ? 1 : 0)
#define	htdb_script(a)		htdb_expand(htdb_getval(a))

#define	htdb_foreach(cnt, prefix, objprefix) \
	for(cnt=1; cnt <= htdb_getptrint(objprefix, "numResults")\
			&& htdb_objtransfer(prefix, 1, objprefix, cnt); cnt++)

/*
	live.c
 */
extern int	live_eval_expr(char *str);
extern char	*live_getval(char *token);
extern void	thing_append(spaceData *b, char *expanded, int length, int growby);
extern void	thing_appendChar(spaceData *b, char expanded, int growby);
extern char	*htdb_expand(char *str);
extern char	*htdb_barewordexpand(char *str);
extern void	htdb_parse(char *str);
extern bool_t	htdb_callable(spaceData *b, char *key, int *arg_length, bool_t *error);

/*
	session.c
 */
extern void	htdb_SetFromPathInfo(char *lookup, char *val);
extern int_t	htdb_fetchHitCount(int_t domain_id, char *resource, char *table);
extern bool_t	htdb_handleSessioning(void);
extern void    htdb_sendSessionCookie(char *sessionkey);

/*
	util.c
 */
typedef	struct	{						/* for storing script functions */
	char	*body,
			**args,
			**defaults;
	int		num_args;
	bool_t	internally_defined;
}	sfunc_t;

typedef	struct	{						/* for storing file pointers */
	char	*path,
			*mode;
	FILE	*fp;
}	filep_t;

extern char	*htdb_ordinalSuffix(int in);
extern char	*prepareStringForDatabase(char *);
extern char	*prepareStringForBBSDatabase(char *);
extern char	*prepareStringFromDatabase(char *);
extern char	*prepareBinaryFromDatabase(char *in, off_t *out_bytes);
extern char	*prepareBinaryForDatabase(char *in, off_t in_bytes, off_t *out_bytes);
extern char	*filterProgramArguments(char *in);
extern char	*pretty(char *ugly);
extern char	*prettyState(char *state);
extern char	*space2underscore(char *in);
extern char	*underscore2space(char *in);
extern char	*newline2br(char *in);
extern char	*br2newline(char *in);
extern bool_t	sendmail(char *, char *);
extern bool_t	amRoot(void);
extern char	*encode(char *source);
extern char	*makeFileSystemSafe(char *in);
extern char	**constructArgumentList(char *in, char separator, int *num);
extern void	destroyArgumentList(char ***args);
extern void	deleteSessionData(int_t session_id, char *name);
extern void	storeSessionData(int_t session_id, char *name, char *value);
extern int	fetchSessionData(int_t session_id, char *name);
extern void	deleteUserData(int_t user_id, char *name);
extern void	storeUserData(int_t user_id, char *name, char *value);
extern void	fetchUserData(int_t user_id, char *name);
extern void	fetchUserAccountTypes(char *prefix, int_t user_id);
extern void	fetchNamedUserData(char *prefix, int_t user_id, char *name);
extern bool_t	isTag(char *tag, char *in, char **gottag);
extern char	*reverseString(char *in);
extern int	myIndexOf(char *table, char *field, char *value);
extern char *htdb_unfilter(char *in);
extern char *htdb_unfilter_unsafe(char *in);
extern char	*htdb_unfilter_nodoublequotes(char *in);
extern char	*genGoKey(void);
extern char	*genAlnumKey(char *table, char *field, int keylen);
extern int	htdb_objtransfer(char *prefix_dest, int index_dest, char *prefix_src, int index_src);
extern char	*htdb_mapStr(char *pattern1, char *pattern2, char *input);
extern char	*sstrcat(char *dest, const char *src, size_t n);
extern bool_t	htdb_inList(char *src, char *in, bool_t exact);
extern bool_t	htdb_inLine(char *src, char *in, bool_t exact);
extern int_t	htdb_random(int_t max);
extern char	*html_safe_trunc(char *string, int len);
extern int	htdb_splitArgs(char *src, char *sep, char ***out, int max_args);
extern void	htdb_deleteArgs(char **args, int num);
extern bool_t	isValidEmailAddress(char *email);
extern bool_t	isStringNumeric(char *in);
extern char	*htdb_stripWhitespace(char *chunk);
extern char	*htdb_strcasestr(const char *str, const char *search);
extern char	*strcasestr(const char *s1, const char *s2);
extern char	*htdb_cleanLines(char *in);
extern char	genAlnum(void);
extern char	*literalizeCommas(char *in);
extern char	*literalizeQuotes(char *in);
extern int	removeDirectory(char *dir);
extern int	lookupIndexByName(char *prefix, char *name);
extern bool_t	parseFilterFunction(char *source, char **filterThis, int *index);
extern char *ttgDetail(char *res, int_t domain_id, int precision, int bars, int limit);
extern void	htdb_objsort(char *prefix, char *key);
extern char	*htdb_prettyNumber(char *in);
extern char	*ccNumber(char *in);
extern int	ccValid(char *in);
extern void	htdb_srand(void);
extern char	*htdb_system(char *cmd);
extern char	*htdb_uname(char *in);
extern bool_t	isUnconfirmed(void);
extern bool_t	isConfirmed(void);
extern bool_t	isUser(void);
extern bool_t	isAdmin(void);
extern int	uncgi_htoi(unsigned char *s);
extern void	uncgi_url_unescape(unsigned char *str);
extern char	*url_escape(char *in);
extern char	*htdb_ttg(char *in);
extern char	*htdb_parseTitleFromDocument(char *doc);
extern bool_t	htdb_wget(char *prefix, char *url, int timeout_seconds);
extern char *htdb_htdb2json(char *prefix);
extern bool_t	htdb_json2htdb(char *prefix, char *data);
extern char	*htdb_htdb2html(char *);
extern char	*htdb_aton(char *in);
extern char	*htdb_ntoa(char *in);
extern char	*htdb_dynamicResultList(char *base, char *field, int *numIds);
extern char	*htdb_replaceMagic(char *orig);

extern int	htdb_rtoi(char *rom);
extern void	htdb_itor(int num, char **str);
extern char	*htdb_braille(char *in);
extern char	*htdb_igpayatinlay(char *in);
extern char	*htdb_rot13(char *in);
extern char	*htdb_morse(char *in);
extern char	*htdb_demorse(char *in);
extern char	*htdb_imagify(char *in);

/*
	verbage.c
 */
extern int_t	htdb_fetchVerbage(char *prefix, char *lookup);
extern char	*htdb_lookupVerbage(char *lookup);
extern void	htdb_emailVerbage(char *lookup);

/*
	a null-terminating strncpy()
	ONLY WORKS FOR STATIC DESTINATION STORAGE
 */
#define	sstrncpy(out, in) \
		strncpy((out), (in), sizeof(out)), (out)[sizeof(out) - 1] = '\0'
/*
	if no strncpy()/snprintf()

extern char	*strncpy(char *out, const char *in, size_t bytes);
extern int	snprintf(char *out, size_t bytes, const char *fmt, ...);
*/

/*
	dt.c
 */
extern bool_t	isLeapYear(int year);
extern char	*prettySeconds(long tm);
extern char	*prettySecondsTerse(long tm);
extern time_t	htdb_timestamp(void);
extern time_t	htdb_timestamp_gmt(void);
extern int	fetchDaysToEndOfMonth(long timestamp);
extern char	*htdb_date(char *in);
extern char	*htdb_parseUnix(char *in);
extern char	*htdb_parseTime(char *in);
extern char	*htdb_parseDate(char *in);
extern char	*htdb_parseMysqlDateTime(char *in);
extern char	*htdb_parseMysqlTimestamp(char *in);
extern char	*htdb_parseRFC2822(char *in);
extern char	*htdb_parseISO8601(char *in);
extern char	*htdb_parseDateTime(char *in);
extern char	*htdb_parseMonthNum(char *in);
extern char	*htdb_parseMonth(char *in);
extern char	*htdb_parseMon(char *in);
extern char	*htdb_parseDay(char *in);
extern char	*htdb_parseYear(char *in);
extern char	*htdb_parseHour(char *in);
extern char	*htdb_parseMilHour(char *in);
extern char	*htdb_parseSecond(char *in);
extern char	*htdb_parseMinute(char *in);
extern char	*htdb_parseAMPM(char *in);
extern char	*htdb_parseWeekday(char *in);

extern char	*htdb_month2num(char *in);
extern char	*htdb_num2mon(char *in);
extern char	*htdb_num2month(char *in);
extern char	*htdb_fetchNextMonth(char *in);
extern char	*htdb_fetchPrevMonth(char *in);

/*
	log.c
 */
extern void	log_page(char *session_id, char *domain_id, char *user_id, char *browser_id, char *resource, char *value);
extern void	log_ttg(char *thing);

/*
	dso.c
 */
#define	GENERIC_SUCCESS_STRING	"<br><b>SUCCESS</b>"
#define	GENERIC_FAILURE_STRING	"<br><b>FAILURE</b>"

extern bool_t	dso_loadFiles(char *dsoList);
extern void	dso_unloadFiles(void);
extern void	dso_fetchFunction(char *, char *(**)(char *));
extern char	*DSO_RETURNS_SUCCESS_CHALLENGED(char *msg);
#define	DSO_RETURNS_SUCCESS_ASIS(msg)		(msg)
#define	DSO_RETURNS_SUCCESS(msg)			strdup(msg)

/*
	crypto.c
 */
extern char	*do_encryptor(char *src, char *key_str, char *algo);
extern char	*do_decryptor(char *src, char *key_str, char *algo);
extern char	*htdb_encryptor(char *prefix, char *source);
extern char	*htdb_decryptor(char *source);

/*
	base64.c
 */
extern char	*base64_encode(const char *src, BASE64Uint32 srclen, char *dest);
extern char	*base64_decode(const char *src, BASE64Uint32 srclen);

/*
	upload.c
 */
extern void doUpLoad(void);
extern void destroyUpLoadInfo(void);
extern int getUpLoadInfo(char *dir, LinkedList *nv, LinkedList *files, char *errstring);
extern void setUpLoadInfo(char *dir, LinkedList nv, LinkedList files);

/*
	db.c
 */
#if	(defined(USE_DBI) && defined(HAVE_LIBDBI))

	#include	<dbi/dbi.h>

	typedef	dbi_conn	_dbHandle;

	#define	_dbQuery			dbi_conn_query
	#define	_dbBinaryQuery		dbi_conn_query_null
	#define	_dbFetchErrorString	dbi_conn_error

#endif

#if	(defined(USE_MYSQL) && defined(HAVE_LIBMYSQLCLIENT))

	#include	<mysql.h>

	typedef	MYSQL_RES	_dbResultHandle;
	typedef	MYSQL_ROW	_dbResultRow;
	typedef	MYSQL_FIELD	_dbField;
	typedef	MYSQL	*	_dbHandle;
	#define	_dbFetchRow		mysql_fetch_row
	#define	_dbFreeResultHandle	mysql_free_result
	#define	_dbQuery			mysql_query
	#define	_dbBinaryQuery		mysql_real_query
	#define	_dbFetchErrorString	mysql_error

#endif

#if	(!defined(USE_DATABASE))
	#define _dbHandle   void    *
	#define	_dbResultHandle	void
#endif

extern bool_t	htdb_dbConnect(void);
extern time_t	htdb_dbTimestamp(void);
extern int		htdb_dbQuery(char *fmt, ...);
extern int		htdb_dbQueryBinary(char *cmd, unsigned int length);
extern int_t	htdb_dbInsert(char *fmt, ...);
extern int		htdb_dbUpdate(char *fmt, ...);
extern int		htdb_dbDelete(char *fmt, ...);
extern int		htdb_dbQueryLoop(char *cmd, char *resource);
extern int		htdb_dbQueryCallback(void (*fptr) (int, int, char **, char **), char *fmt, ...);
extern int		htdb_dbQueryResult(char *table, char *fmt, ...);
extern int		htdb_dbQueryResult_log(char *table, char *fmt, ...);
extern void		htdb_dbSync();
extern void		htdb_dbUnsync();
extern void		htdb_dbWaitSync();
/*
	shorthand - a programmer convenience
	with naming similar to dso_sql()
 */
#define	htdb_sql				htdb_dbQueryResult

/*
	for backwards compatibility
	these'll go away eventually.
 */
#define	dbQuery					htdb_dbQuery
#define	dbQueryBinary			htdb_dbQueryBinary
#define	dbInsert				htdb_dbInsert
#define	dbUpdate				htdb_dbUpdate
#define	dbDelete				htdb_dbDelete
#define	dbProcessByRow			htdb_dbQueryLoop
#define	dbCallbackByRow			htdb_dbQueryCallback
#define	dbGetRowByKey			htdb_dbQueryResult
#define	dbGetRowByKey_log		htdb_dbQueryResult_log
#define	dbSync					htdb_dbSync
#define	dbUnsync				htdb_dbUnsync
#define	dbWaitSync				htdb_dbWaitSync
/*
	debug.c
 */
#define	DEBUG_LEVEL_FATAL	(0x1 << 1)
#define	DEBUG_LEVEL_ERROR	(0x1 << 2)
#define	DEBUG_LEVEL_WARNING	(0x1 << 3)
#define	DEBUG_LEVEL_INFO	(0x1 << 4)
#define	DEBUG_LEVEL_DEBUG	(0x1 << 5)

extern void	debug_it(int mask, char *label, char *msg, char **out_buf);
extern void	log_debug(char *label, char *fmt, ...);
extern void	log_info(char *label, char *fmt, ...);
extern void	log_warning(char *label, char *fmt, ...);
extern void	log_error(char *label, char *fmt, ...);
extern void	log_fatal(char *label, char *fmt, ...);
extern void	log_intro(void);
extern void	log_outro(void);
extern void	log_file(char *file, char *fmt, ...);
extern void	debug_resDump(char *file);
extern void	profile_begin(void);
extern double 	profile_end(void);
extern void	log_profile_end(char *mesg);

extern void	dump(char *str);

/*
	forms.c
 */
extern void	setFormErrors(char *resource, int *index);
extern int	setErrors(int set, char *name, int row, char *resource, int *index);
extern int	setEmail(char *prefix, char *name, int required, int row, char *resource, int *index);
extern int	setForm(char *prefix, char *name, int required, int row, char *resource, int *index);
extern int	setCheckbox(char *prefix, char *name, int required, int row, char *resource, int *index);
extern int	setRadio(char *prefix, char *name, int required, int row, char *resource, int *index);
extern int	setIntOption(char *prefix, char *name, int required, int row, char *resource, int *index);
extern int	setLimitedIntOption(char *prefix, char *name, int required, int row, char *resource, int *index);
extern int	setTextOption(char *prefix, char *name, int required, int row, char *resource, int *index);
extern int	setScreenName(char *prefix, char *name, int required, int row, char *resource, int *index);
extern int	setYear(char *prefix, char *name, int required, int row, char *resource, int *index);
extern bool_t	containsCensoredWords(char *val);

/*
	graphics.c
 */
extern bool_t	imageStore(char *table, int_t user_id, char *name, char *buf, char *ext, off_t media_bytes, int w, int h);
extern bool_t	imageScaleAndStore(char *table, int_t user_id, char *name, char *dir, char *file, int width, int height);
extern bool_t	imageGetDimensions(char *path, int *width, int *height);
extern char		*imageGetContentByExtension(char *ext);
extern char		*imageGetContentByData(char *buf);
extern char		*imageGetExtensionByData(char *buf);

/*
	md5.c
 */
extern void md5_digest(char *str, unsigned char digest[16], long offset, long bytes);

/*
	xml.c
 */
extern bool_t	htdb_parseXML(char *prefix, char *buf);
extern void	htdb_clearXML(char *prefix);

/*
	flash.c
 */
extern bool_t	htdb_flash(void);

/*
	socket.c
 */
extern void htdb_socketClose(int sd);
extern bool_t	htdb_socketSend(int sd, char *buf, size_t bytes);
extern char	*htdb_socketReceive(int sd);
extern int	htdb_socketOpen(char *prefix, char *hostname, unsigned port, int timeout_seconds);

#endif
